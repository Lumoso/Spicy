-- |
-- Module      : Spicy.Formats.Molden.Reader
-- Description : Reading Molden files
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides functions to read Molden files.
module Spicy.Formats.Molden.Reader where

import Data.Attoparsec.Text
import Data.Default
import Data.IntMap as IntMap
import Data.Massiv.Array as Massiv hiding (index, takeWhile)
import Data.Monoid
import Spicy.Attoparsec
import Spicy.Data
import Spicy.Formats.Molden.Conversion
import Spicy.Formats.Molden.Types
import Spicy.Molecule.Physical
import Spicy.Prelude hiding (exponent)
import Spicy.Wavefunction.Basis hiding (angularMomentum)
import Spicy.Wavefunction.Wavefunction
import Text.Parser.Permutation

-- | Transform a raw molden file (as 'Text') into Spicy's internal types.
-- Note that molden files can in prinicple also contain information about
-- geometry optimisations or other calculation types, but this information
-- is not parsed herein.
getResultsFromMolden :: MonadThrow m => Text -> m (Wavefunction, Basis, Vector B Atom)
getResultsFromMolden = parse' molden >=> moldenToData

----------------------------------------------------------------------------------------------------

-- | Parser for molden files.
molden :: Parser Molden
molden = do
  string "[Molden Format]" *> skipLine
  permute moldenPerm

----------------------------------------------------------------------------------------------------

-- | Permutation parser for molden files, able to account for the input blocks in any order. Part of
-- the 'molden' parser and does not need to be used directly, usually.
moldenPerm :: Permutation Parser Molden
moldenPerm =
  Molden
    <$$> getLine
    <||> moldenAtoms
    <||> moldenAtomicShells
    <||> moldenOrbitalFlags
    <||> moldenMOs

----------------------------------------------------------------------------------------------------

-- | Parse the list of atoms present in a molden file and automatically convert units.
moldenAtoms :: Parser (IntMap Atom)
moldenAtoms = do
  _ <- string "[Atoms]"
  units <- lex (string "Angs" $> id <|> string "AU" $> bohr2Angstrom) <* skipLine
  atoms <- IntMap.fromList <$> many1 (moldenAtom units)
  skipSpace
  return atoms

-- | Parse a single atom from a molden file, including its index.
moldenAtom :: (Double -> Double) -> Parser (Int, Atom)
moldenAtom units = do
  _ <- lex $ many1 letter
  i <- lex (decimal @Int)
  an <- lex decimal
  x <- units <$> lex double
  y <- units <$> lex double
  z <- units <$> lex double
  lex endOfLine

  element <- case aNToElement an of
    Nothing -> fail "Unrecognised atomic number while parsing Molden file"
    Just element' -> return element'

  let label = mempty
      isLink = NotLink
      isDummy = False
      ffType = FFXYZ
      coordinates = VectorS $ Massiv.fromList Massiv.Seq [x, y, z]
      multipoles = def
      formalCharge = 0
      formalExcEl = 0

  return (i, Atom {..})

----------------------------------------------------------------------------------------------------

-- | Parser for the orbital shells in a molden file.
moldenAtomicShells :: Parser (Vector B MoldenAtomicShell)
moldenAtomicShells = do
  string "[GTO]" *> skipLine
  atomicShells <- Massiv.fromList Par <$> many1 moldenAtomicShell
  skipSpace
  return atomicShells

-- | Molden files contain orbital shells on a per-atom basis.
-- This parser parses all shells belonging to one atom.
moldenAtomicShell :: Parser MoldenAtomicShell
moldenAtomicShell = do
  index <- lex decimal <* skipLine
  moldenShells <- Massiv.fromList Par <$> many1 moldenShell
  skipSpace

  return MoldenAtomicShell {..}

-- | Parser for a single 'MoldenShell'.
moldenShell :: Parser MoldenShell
moldenShell = do
  l <- lex (manyTill letter space) <* skipLine
  case l of
    "s" -> regularShell 0
    "p" -> regularShell 1
    "d" -> regularShell 2
    "f" -> regularShell 3
    "g" -> regularShell 4
    "sp" -> spShell
    _ -> fail "Unrecognised Shell type"
  where
    regularShell i = RegularMoldenShell i <$> (Massiv.fromList Par <$> many1 moldenPrimitive)
    spShell = SPMoldenShell <$> (Massiv.fromList Par <$> many1 moldenSPPrimitive)

-- | Parser for regular primitive gauss functions (those with a defined angular momentum)
moldenPrimitive :: Parser Gauss
moldenPrimitive = do
  exponent <- lex double
  coefficient <- lex double
  skipLine
  return Gauss {..}

-- | Parser for SP hybrid shells
moldenSPPrimitive :: Parser (Gauss, Double)
moldenSPPrimitive = do
  exponent <- lex double
  coefficient <- lex double
  spcoefficient <- lex (char '(') *> double <* char ')'
  skipLine

  return (Gauss {..}, spcoefficient)

----------------------------------------------------------------------------------------------------

-- | Parse the flags dictating the type of basis functions (Cartesian/Spherical).
-- | Note that [5D] implies [7F] but the reverse is not true.
moldenOrbitalFlags :: Parser MoldenFlags
moldenOrbitalFlags = do
  flags <- many $ lex (choice [fD, fDsF, fDtF, sF, nG]) <* skipLine
  skipSpace
  return $ (appEndo $ foldMap Endo flags) def
  where
    fD = (string "[5d]" <|> string "[5D]") $> (#fiveD .~ True) . (#sevenF .~ True)
    fDsF = (string "[5d7f]" <|> string "[5D7F]") $> (#fiveD .~ True) . (#sevenF .~ True)
    fDtF = (string "[5d10f]" <|> string "[5D10F]") $> (#fiveD .~ True) . (#sevenF .~ False)
    sF = (string "[7f]" <|> string "[7F]") $> (#sevenF .~ True)
    nG = (string "[9g]" <|> string "[9G]") $> (#nineG .~ True)

----------------------------------------------------------------------------------------------------

-- | Parser for the MO block of a molden file.
moldenMOs :: Parser (Vector B MoldenMO)
moldenMOs = do
  string "[MO]" *> skipLine
  mos <- Massiv.fromList Par <$> many1 moldenMO
  skipSpace
  return mos

-- | Parser for a single molden MO.
moldenMO :: Parser MoldenMO
moldenMO = do
  string "Sym" *> lex (string "=") *> skipLine
  energy <- string "Ene" *> lex (string "=") *> lex double <* skipLine
  spin <- string "Spin" *> lex (string "=") *> lex (string "Alpha" $> Alpha <|> string "Beta" $> Beta) <* skipLine
  occupation <- string "Occup" *> lex (string "=") *> lex double <* skipLine
  coefficients' <- many1 $ do
    lex (decimal :: Parser Int) *> lex double <* skipLine
  let coefficients = Massiv.fromList Par coefficients'

  return MoldenMO {..}
