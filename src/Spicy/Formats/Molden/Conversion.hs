-- |
-- Module      : Spicy.Formats.Molden.Conversion
-- Description : Converting Molden files
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides functions to convert from the primitive 'Molden' type
-- into the internally used data types.
module Spicy.Formats.Molden.Conversion where

import qualified Data.IntMap as IntMap
import Data.Massiv.Array as Massiv hiding (index)
import Spicy.Formats.Molden.Types
import Spicy.Massiv
import Spicy.Molecule.Physical
import Spicy.Prelude
import Spicy.Wavefunction.Basis
import Spicy.Wavefunction.Wavefunction

-- | Transform a parsed 'Molden' file into the respective Spicy data structures.
-- Note that there are no guarantees made regarding the order of values:
--
--  * MO coefficients occur in the order specified by the file format
--    (see <https://www3.cmbi.umcn.nl/molden/molden_format.html>).
--  * The keys in the 'IntMap Atom' may not correspond to those in the ONIOM tree.
moldenToData :: MonadThrow m => Molden -> m (Wavefunction, Basis, Vector B Atom)
moldenToData molden = do
  wavefunction <- moldenToWavefunction molden
  basis <- moldenToBasis molden
  let atoms = Massiv.fromList Par . fmap snd . IntMap.toList $ molden ^. #atoms

  return (wavefunction, basis, atoms)

----------------------------------------------------------------------------------------------------

-- | Extract information about the wave function from a parsed 'Molden' file.
moldenToWavefunction :: MonadThrow m => Molden -> m Wavefunction
moldenToWavefunction Molden {..} = do
  let partitionelOrbitals' = partitionIntoVectors (views #spin (== Alpha)) molecularOrbitals
      partitionedOrbitals = bimap (convert @B) (convert @B) partitionelOrbitals'
      toCoefficientMatrix mos = MatrixS . convert <$> stackInnerSlicesM (mos ^. mapping #coefficients)

  moCoefficients <- case partitionedOrbitals of
    (a, b) | null b -> Restricted <$> toCoefficientMatrix a
    (a, b) | null a -> Restricted <$> toCoefficientMatrix b
    (a, b) | size a /= size b -> throwM . localExc $ "Unequal number of alpha and beta orbitals"
    (a, b) -> Unrestricted <$> toCoefficientMatrix a <*> toCoefficientMatrix b

  return $ Wavefunction (Just moCoefficients) Nothing
  where
    localExc = DataStructureException "moldenToWavefunction"

----------------------------------------------------------------------------------------------------

-- | Extract information about the orbital basis from a parsed 'Molden' file.
moldenToBasis :: MonadThrow m => Molden -> m Basis
moldenToBasis Molden {..} = do
  let name = mempty :: Text

  mshellsWithCoordinates <-
    maybe2MThrow
      (DataStructureException "moldenToBasis" "Could not find coordinates for a shell")
      (traverse findCoordinates atomicOrbitals)

  shells <- VectorG . convert <$> concatM (Dim 1) (formShells <$> mshellsWithCoordinates)

  return Basis {..}
  where
    formShells (ss, c) = moldenShellToShell moldenFlags c <$> ss
    coordinateMap = atoms ^. mapping #coordinates
    findCoordinates MoldenAtomicShell {..} = do
      cs <- IntMap.lookup index coordinateMap
      return (moldenShells, cs)

----------------------------------------------------------------------------------------------------

-- | Helper function to go from shells as recorded in a molden file to Spicy's internal 'Shell's.
moldenShellToShell ::
  -- | Flags for spherical/cartesian basis function.
  MoldenFlags ->
  -- | Coordinates of the atoms as given in the 'Molden' file. Unit is Angstrom.
  VectorS Double ->
  MoldenShell ->
  Shell
moldenShellToShell MoldenFlags {..} coordinates moldenShell = case moldenShell of
  RegularMoldenShell l p ->
    let primitives = VectorG p
        shellType = case l of
          2 -> if fiveD then SphericalShell 2 else CartesianShell 2
          3 -> if sevenF then SphericalShell 3 else CartesianShell 3
          4 -> if nineG then SphericalShell 4 else CartesianShell 4
          l' -> SphericalShell . fromIntegral $ l'
        atomIndex = Nothing
     in Shell {..}
  SPMoldenShell v ->
    let (p, sp) = unzip v
        primitives = VectorG . convert $ p
        shellType = SPShell . VectorS . convert $ sp
        atomIndex = Nothing
     in Shell {..}
