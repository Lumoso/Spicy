-- |
-- Module      : Spicy.Formats.Molden.Types
-- Description : Types for the Molden file format
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides underlying types for the representation of Molden files.
module Spicy.Formats.Molden.Types where

import Data.Default
import Data.Massiv.Array
import Spicy.Molecule.Physical
import Spicy.Prelude
import Spicy.Wavefunction.Basis

-- | Data representation of a molden file.
data Molden = Molden
  { title :: !Text,
    atoms :: !(IntMap Atom),
    atomicOrbitals :: !(Vector B MoldenAtomicShell),
    moldenFlags :: !MoldenFlags,
    molecularOrbitals :: !(Vector B MoldenMO)
  }
  deriving (Show, Generic)

----------------------------------------------------------------------------------------------------

-- | A group of orbital shells belonging to the same atom.
data MoldenAtomicShell = MoldenAtomicShell
  { -- | Index of the atom this shell belongs to.
    index :: !Int,
    -- | All shells belonging to that atom.
    moldenShells :: !(Vector B MoldenShell)
  }
  deriving (Show, Generic)

----------------------------------------------------------------------------------------------------

-- | A shell as specified by the molden file format.
data MoldenShell
  = -- | A regular shell. The 'Int' value represents the angular momentum.
    RegularMoldenShell !Int !(Vector U Gauss)
  | -- | A SP hybrid shell. The second part of the tuple contains the P contraction coefficients.
    SPMoldenShell !(Vector U (Gauss, Double))
  deriving (Show)

----------------------------------------------------------------------------------------------------

-- | Molecular orbital as recorded in a molden file.
-- The order of the coefficients is as follows:
--
--  * Spherical D: D 0, D+1, D-1, D+2, D-2
--  * Cartesian D: xx, yy, zz, xy, xz, yz
--
--  * Spherical F: F 0, F+1, F-1, F+2, F-2, F+3, F-3
--  * Cartesian F: xxx, yyy, zzz, xyy, xxy, xxz, xzz, yzz, yyz, xyz
--
--  * Spherical G: G 0, G+1, G-1, G+2, G-2, G+3, G-3, G+4, G-4
--  * Cartesian G: xxxx yyyy zzzz xxxy xxxz yyyx yyyz zzzx zzzy,
--                 xxyy xxzz yyzz xxyz yyxz zzxy
data MoldenMO = MoldenMO
  { energy :: !Double,
    occupation :: !Double,
    spin :: !Spin,
    coefficients :: !(Vector U Double)
  }
  deriving (Show, Generic)

----------------------------------------------------------------------------------------------------

-- | Electron Spin.
data Spin = Alpha | Beta
  deriving (Show, Eq)

----------------------------------------------------------------------------------------------------

-- | Flags dictating the use of Spherical/Cartesian basis functions in the MO block
-- of a molden file. Unlike in FChks, it's not possible to mix functions arbitrarily.
data MoldenFlags = MoldenFlags
  { fiveD :: !Bool,
    sevenF :: !Bool,
    nineG :: !Bool
  }
  deriving (Show, Generic)

-- | By default, molden assumes the use of Cartesian basis functions
instance Default MoldenFlags where
  def = MoldenFlags False False False
