-- |
-- Module      : Spicy.Formats.FChk
-- Description : Functions to work with FChk files
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides functions to read, manipulate and write FChk files. The module can obtain
-- 'Molecule' from an 'FChk', and can also serialise a 'Molecule' into a 'FChk'.
-- The ability to write and manipulate fchks is commonly used to interact with other quantum
-- chemistry programs.
module Spicy.Formats.FChk
  ( module Spicy.Formats.FChk.Writer,
    module Spicy.Formats.FChk.Types,
    module Spicy.Formats.FChk.Reader,
    module Spicy.Formats.FChk.Conversion,
  )
where

import Spicy.Formats.FChk.Writer
import Spicy.Formats.FChk.Types
import Spicy.Formats.FChk.Reader
import Spicy.Formats.FChk.Conversion
