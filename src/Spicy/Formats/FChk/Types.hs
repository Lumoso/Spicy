-- |
-- Module      : Spicy.Formats.FChk.Types
-- Description : Types for the FChk format
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides types relating to the FChk format.
module Spicy.Formats.FChk.Types
  ( FChk (..),
    Content (..),
    ConvertContent (..),
    CalcType (..),
    ScalarVal (..),
    ArrayVal (..),
    _Scalar,
    _ScalarDouble,
    _ScalarInt,
    _ScalarLogical,
    _ScalarText,
    _Array,
    _ArrayDouble,
    _ArrayInt,
    _ArrayLogical,
    _ArrayText,
    retrieve,
  )
where

import Data.Massiv.Array
import qualified RIO.Map as Map
import Spicy.Prelude

-- | The contents of an FChk.
data FChk = FChk
  { -- | Short title from the header of the FChk.
    title :: !Text,
    -- | Calculation type from the header.
    calcType :: !CalcType,
    -- | Primary orbital basis from the FChk header.
    basisSet :: !Text,
    -- | Calculation method (such as CCSD, HF, MP2) from the header.
    method :: !Text,
    -- | Labeled of 'Content's.
    blocks :: !(Map Text Content)
  }
  deriving (Eq, Show, Generic)

----------------------------------------------------------------------------------------------------

-- | In the FChk format after 2 lines of header an arbitrary amount of contents will follow. These are
-- either scalar values or arrays of values. The arrays are always printed as vectors but might
-- actually refer to matrices. The meaning of the content blocks must be obtained by the label fields.
data Content
  = Scalar !ScalarVal
  | Array !ArrayVal
  deriving (Eq, Show)

-- Prisms
_Scalar :: Prism' Content ScalarVal
_Scalar = prism Scalar $ \s -> case s of
  Scalar b -> Right b
  a -> Left a

_Array :: Prism' Content ArrayVal
_Array = prism Array $ \s -> case s of
  Array b -> Right b
  a -> Left a

----------------------------------------------------------------------------------------------------

-- | Data types an array can hold.

-- The text value is very strange. A normal text is split into chunks of 12 or 8 characters and
-- print without separator. Therefore it appears as a normal text but for Fortran it is actually an
-- array of text fragments. I don't care about the crazy Fortran internals and treat it as a single
-- text, which it actually is.
data ArrayVal
  = ArrayInt !(Vector S Int)
  | ArrayDouble !(Vector S Double)
  | ArrayText !Text
  | ArrayLogical !(Vector S Bool)
  deriving (Eq, Show)

-- Prisms
_ArrayInt :: Prism' ArrayVal (Vector S Int)
_ArrayInt = prism ArrayInt $ \s -> case s of
  ArrayInt b -> Right b
  a -> Left a

_ArrayDouble :: Prism' ArrayVal (Vector S Double)
_ArrayDouble = prism ArrayDouble $ \s -> case s of
  ArrayDouble b -> Right b
  a -> Left a

_ArrayText :: Prism' ArrayVal Text
_ArrayText = prism ArrayText $ \s -> case s of
  ArrayText b -> Right b
  a -> Left a

_ArrayLogical :: Prism' ArrayVal (Vector S Bool)
_ArrayLogical = prism ArrayLogical $ \s -> case s of
  ArrayLogical b -> Right b
  a -> Left a

----------------------------------------------------------------------------------------------------

-- | Possible scalar values in an FChk.
data ScalarVal
  = ScalarInt !Int
  | ScalarDouble !Double
  | ScalarText !Text
  | ScalarLogical !Bool
  deriving (Eq, Show)

-- Prisms
_ScalarInt :: Prism' ScalarVal Int
_ScalarInt = prism' ScalarInt $ \s -> case s of
  ScalarInt b -> Just b
  _ -> Nothing

_ScalarDouble :: Prism' ScalarVal Double
_ScalarDouble = prism' ScalarDouble $ \s -> case s of
  ScalarDouble b -> Just b
  _ -> Nothing

_ScalarText :: Prism' ScalarVal Text
_ScalarText = prism' ScalarText $ \s -> case s of
  ScalarText b -> Just b
  _ -> Nothing

_ScalarLogical :: Prism' ScalarVal Bool
_ScalarLogical = prism' ScalarLogical $ \s -> case s of
  ScalarLogical b -> Just b
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Possible types of calculation types from an FChk file.
data CalcType
  = -- | Single point
    SP
  | -- | Full optimisation to minimum
    FOPT
  | -- | Partial optimisation to an minimum
    POPT
  | -- | Full optimisation to a transition state
    FTS
  | -- | Partial optimisation to a transition state
    PTS
  | -- | Full optimisation to a saddle point of higher order
    FSADDLE
  | -- | Partial optimisation to a saddle point of higher order
    PSADDLE
  | -- | Energy + gradient calculation
    FORCE
  | -- | Frequency calculation (2nd derivative)
    FREQ
  | -- | Potential energy surface scan
    SCAN
  | -- | Generates molecular orbitals only, also orbital localisation
    GUESS
  | -- | Linear synchronous transit calculation
    LST
  | -- | SCF stability analysis
    STABILITY
  | -- | Generate archive information from checkpoint file
    REARCHIVE
  | -- | Generate archive information from checkpoint file
    MSRESTART
  | -- | Compound models such as G2, G3, ...
    MIXED
  deriving (Eq, Show)

----------------------------------------------------------------------------------------------------

-- | Auxilliary class to prevent code duplication.
class ConvertContent a where
  -- | Turn a value of type 'a' into a FChk content.
  toContent :: a -> Content

  -- | Get a value of type 'a' from a FChk content.
  fromContent :: Content -> Maybe a

instance ConvertContent ScalarVal where
  toContent = Scalar
  fromContent = preview _Scalar

instance ConvertContent ArrayVal where
  toContent = Array
  fromContent = preview _Array

instance ConvertContent Int where
  toContent = Scalar . ScalarInt
  fromContent = preview $ _Scalar % _ScalarInt

instance ConvertContent Double where
  toContent = Scalar . ScalarDouble
  fromContent = preview $ _Scalar % _ScalarDouble

instance ConvertContent Bool where
  toContent = Scalar . ScalarLogical
  fromContent = preview $ _Scalar % _ScalarLogical

instance (Load r Ix1 Int, Mutable r Int) => ConvertContent (Vector r Int) where
  toContent = Array . ArrayInt . compute @S
  fromContent = fmap convert . preview (_Array % _ArrayInt)

instance (Load r Ix1 Double, Mutable r Double) => ConvertContent (Vector r Double) where
  toContent = Array . ArrayDouble . convert @S
  fromContent = fmap convert . preview (_Array % _ArrayDouble)

instance (Load r Ix1 Bool, Mutable r Bool) => ConvertContent (Vector r Bool) where
  toContent = Array . ArrayLogical . convert @S
  fromContent = fmap convert . preview (_Array % _ArrayLogical)

----------------------------------------------------------------------------------------------------

-- | Utility function to retrieve values from 'FChk's. Usually needs a TypeApplication,
-- i.e. @retrieve \@(Vector S Double)@ to work, since it's polymorphic in the return type.
retrieve ::
  (ConvertContent a, MonadThrow m) =>
  -- | The blocks of an 'FChk'
  Map Text Content ->
  -- | The block to be retrieved
  Text ->
  -- | Resulting value.
  m a
retrieve m k = do
  c <- localThrow $ m Map.!? k
  localThrow $ fromContent c
  where
    localThrow :: MonadThrow m => Maybe a -> m a
    localThrow = maybe2MThrow . DataStructureException "retrieve" $ "Could not find " <> show k
