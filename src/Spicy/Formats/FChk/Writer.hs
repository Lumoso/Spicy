-- |
-- Module      : Spicy.Formats.FChk.Writer
-- Description : Writing FChk files to Text
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides a function to convert the FChk type into plain Text
-- as recognized by other programs.
module Spicy.Formats.FChk.Writer (writeFChk) where

import Data.Massiv.Array ()
import Data.Massiv.Array as Massiv
import Data.Text.Lazy.Builder (Builder)
import qualified Data.Text.Lazy.Builder as TB
import qualified RIO.Map as Map
import qualified RIO.Set as Set
import qualified RIO.Text as Text
import qualified RIO.Text.Lazy as TL
import Spicy.Common
import Spicy.Formats.FChk.Types
import Spicy.Prelude hiding (Builder)

-- | A function to construct the FChk as a textual object from its internal representation.
-- Unfortunately, the order of the content blocks seems to be important and it is not entirely clear
-- what the correct order is.
writeFChk :: FChk -> Text
writeFChk fchk =
  let --Header line by line
      titleB = fA 72 (fchk ^. #title) <> nL

      infoLineB =
        (fA 10 . tshow $ fchk ^. #calcType)
          <> fA 30 (fchk ^. #method)
          <> fA 30 (fchk ^. #basisSet)
          <> nL

      -- CONTENT BLOCKS
      cntntBlks = fchk ^. #blocks

      -- Some blocks are expected in a given order apparently. The order of the hardcoded blocks is
      -- given here.
      blockOrder :: [Text]
      blockOrder =
        [ "Number of atoms",
          "Charge",
          "Multiplicity",
          "Number of electrons",
          "Number of alpha electrons",
          "Number of beta electrons",
          "Number of basis functions",
          "Number of independent functions",
          "Atomic numbers",
          "Nuclear charges",
          "Current cartesian coordinates",
          "Integer atomic weights",
          "Real atomic weights",
          "Number of primitive shells",
          "Number of contracted shells",
          "Pure/Cartesian d shells",
          "Pure/Cartesian f shells",
          "Highest angular momentum",
          "Largest degree of contraction",
          "Number of primitive shells",
          "Shell types",
          "Number of primitives per shell",
          "Shell to atom map",
          "Primitive exponents",
          "Contraction coefficients",
          "Coordinates of each shell",
          "P(S=P) Contraction coefficients",
          "Total SCF Density",
          "Total MP2 Density",
          "Total CI Density",
          "Total CC Density"
        ]

      orderedBlocks =
        let localMap = Map.restrictKeys cntntBlks $ Set.fromList blockOrder
            keyVec = Massiv.fromList Par blockOrder :: Vector B Text
         in Massiv.foldMono
              (\k -> fromMaybe mempty . fmap (blockWriter k) $ localMap Map.!? k)
              keyVec
      unorderedOtherBlocks =
        let localMap = Map.withoutKeys cntntBlks $ Set.fromList blockOrder
         in Map.foldlWithKey' (\acc k v -> acc <> blockWriter k v) mempty localMap

      fchkBuilder =
        titleB
          <> infoLineB
          <> orderedBlocks
          <> unorderedOtherBlocks
   in TL.toStrict . TB.toLazyText $ fchkBuilder
  where
    nL = TB.singleton '\n'

----------------------------------------------------------------------------------------------------

-- | A writer for FChk content blocks.
blockWriter :: Text -> Content -> Builder
blockWriter label value = case value of
  Scalar a -> scalarWriter label a
  Array a -> arrayWriter label a

----------------------------------------------------------------------------------------------------

-- | A writer for FChk Scalars.
scalarWriter :: Text -> ScalarVal -> Builder
scalarWriter label value = case value of
  ScalarInt a -> fA 40 label <> fX 3 <> fA 1 "I" <> fX 5 <> fI 12 a <> "\n"
  ScalarDouble a -> fA 40 label <> fX 3 <> fA 1 "R" <> fX 5 <> fE 22 15 a <> "\n"
  ScalarText a -> fA 40 label <> fX 3 <> fA 1 "C" <> fX 5 <> fA 12 a <> "\n"
  ScalarLogical a -> fA 40 label <> fX 3 <> fA 1 "L" <> fX 5 <> fA 12 (if a then "T" else "F") <> "\n"

----------------------------------------------------------------------------------------------------

-- | A writer for FChk Arrays. Text arrays are always written in @C@ type instead of @H@ type.
arrayWriter :: Text -> ArrayVal -> Builder
arrayWriter label value = case value of
  ArrayInt a ->
    let nElems = Massiv.elemsCount a
        elemChunks =
          Massiv.ifoldMono
            (\i e -> fI 12 e <> if (i + 1) `mod` 6 == 0 || i == nElems - 1 then "\n" else mempty)
            a
     in fA 40 label <> fX 3 <> fA 1 "I" <> fX 3 <> "N=" <> fI 12 nElems <> "\n"
          <> elemChunks
  ArrayDouble a ->
    let nElems = Massiv.elemsCount a
        elemsChunks =
          Massiv.ifoldMono
            (\i e -> fE 16 8 e <> if (i + 1) `mod` 5 == 0 || i == nElems - 1 then "\n" else mempty)
            a
     in fA 40 label <> fX 3 <> fA 1 "R" <> fX 3 <> "N=" <> fI 12 nElems <> "\n"
          <> elemsChunks
  ArrayText a ->
    let vectors :: Vector B Text
        vectors = Massiv.fromList Par . Text.chunksOf 12 $ a
        elemsChunks =
          Massiv.ifoldMono
            (\i e -> fA 12 e <> if (i + 1) `mod` 5 == 0 || i == nElems - 1 then "\n" else mempty)
            vectors
        nElems = Text.length a `div` 12 + 1
     in fA 40 label <> fX 3 <> fA 1 "C" <> fX 3 <> "N=" <> fI 12 nElems <> "\n"
          <> elemsChunks
  ArrayLogical a ->
    let nElems = Massiv.elemsCount a
        elemsChunks =
          Massiv.ifoldMono
            (\i e -> fL 1 e <> if (i + 1) `mod` 72 == 0 || i == nElems - 1 then "\n" else mempty)
            a
     in fA 40 label <> fX 3 <> fA 1 "L" <> fX 3 <> "N=" <> fI 12 nElems <> "\n"
          <> elemsChunks
