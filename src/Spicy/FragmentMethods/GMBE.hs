{-# LANGUAGE TupleSections #-}

-- |
-- Module      : Spicy.FragmentMethods.GMBE
-- Description : Generalised Many Body Expansion
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- The modules implements basic functionality for the
-- [Generalised Many Body Expansion](https://doi.org/10.1063/1.4742816). This allows constructing
-- an expression for any size-extensive property \(P\) of a given molecule in terms of the properties
-- of overlapping fragments \(F\) of the systems. GMBE defines a "intersection-corrected property"
-- \(\phi_\alpha\) for each fragment \(F_\alpha\):
-- \[
--   \phi_\alpha
--     = E_\alpha
--     - \sum\limits_{\substack{\beta \\ (\beta > \alpha)}} P_{F_\alpha \cap F_\beta}
--     + \sum\limits_{\substack{\beta, \gamma \\ (\gamma > \beta > \alpha)}} P_{F_\alpha \cap F_\beta \cap F_\gamma}
--     - \dots
--     + (-1)^{N - \alpha} P_{F_\alpha \cap F_{\alpha + 1} \cap \dots \cap F_N}
-- \]
-- and the sum of all intersection-corrected properties, give the property of the full system:
-- \[ P = \sum\limits_\alpha \phi_\alpha \]
--  The fragments \(F\) are defined in terms of
-- non-separable groups (see 'Spicy.FragmentMethods.SMF' for details) and the union of all fragments
-- redundantly represents the full system and contains all groups:
-- \[ F_i = \{ G_j | G_j \in G\} \]
-- \[ F_i \subseteq G \]
-- \[\bigcup F = \bigcup G \]
--
-- The actual implementation is based around the concept of fragment-combination-ranges ('FCR'), as
-- described in [A unified and flexible formulation of molecular fragmentation schemes](https://aip.scitation.org/doi/pdf/10.1063/5.0059598),
-- especially equations 1 to 11. A fragment combination range for any fragment \(F_i\), that
-- consists of \(n\) different groups, is the range of all \([n \dots 1]\)-combinations of those
-- groups and labeled \( \{G\}^{\mathbf{c}_n} \), where \(\mathbf{c}_n\) indicates the order. We can
-- now write:
-- \[
--   P(G_\alpha, G_\beta, \dots G_\gamma) = \sum\limits_{\mathbf{c}_l \in \{\mathrm{FCR}\}} \phi^{\mathbf{c}_l} ( \{G\}^{\mathbf{c}_l})
-- \]
-- Now this implies a recursion into the smaller fragments of \(\phi^{\mathbf{c}_l}\).
-- \[
--   \bar{E}^{\mathbf{c}_l} (\{G\}^{\mathbf{c}_l}) =
--     E^{\mathbf{c}_l} (\{G\}^{\mathbf{c}_l}) - \sum\limits_{\mathbf{c}_s \subset \mathbf{c}_l} \bar{E}^{\mathbf{c}_s} (\{G\}^{\mathbf{c}_s})
-- \]
-- \[
--   = \sum\limits_{\substack{\mathbf{c}_s \subseteq \mathbf{c}_l \\ \mathbf{c}_s \in \{\mathrm{FCR}\}}} k^{\mathbf{c}_s, \mathbf{c}_l} E^{\mathbf{c}_s} (\{G\}^{\mathbf{c}_s})
-- \]
-- \[
--   k^{\mathbf{c}_s, \mathbf{c}_l} = (-1)^{l - s}
-- \]
-- The paper now claims that this term is equivalent to the intersection corrected energy/property
-- of GMBE. This is not completely true; it only holds if the fragment combination range was
-- actually built MBE-like, but it is not true, if arbitrary fragments are allowed. However, this
-- does not matter; summing up the contribution from the initial set of fragments, cancels the
-- terms, that do not appear in GMBE's intersection corrected energy exactly and the FCR-sum is then
-- the GMBE sum.
--
-- The integer factors for a givens set of fragments is constructed and simplified just once
-- ('makeGmbeSum', 'GMBEsum', 'GMBEmap').
--
-- An original set of fragments may be post-processed by formation of all \(n\)-combinations of the
-- original set and GMBE will then be carried out on this new set ('makeNmers').
--
-- The transformation of the energies, its nuclear derivatives and of the multipoles into the basis
-- of the full system is finally done by 'gmbeToMono'.
--
-- For the generation of fragments and system partitioning techniques, refer to
-- 'Spicy.FragmentMethods.SMF'.
module Spicy.FragmentMethods.GMBE
  ( -- * \(n\)-mer Generator
    makeNmers,

    -- * GMBE and FCR
    makeGmbeSum,

    -- ** Constructor Functions and Types
    Fragment,
    FCR,
    GMBEsum,
    frag2FCR,
    frags2FCR,
    fcrSum,
    fcrGMBE,

    -- * Collectors
    -- $gmbeCollectors
    transformGMBEEnergies,
    transformGMBEMultipoles,
    transformGMBEBasis,
    gmbeToMono,
  )
where

import Data.Default
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding (toList)
import qualified Data.Massiv.Array as Massiv
import qualified Data.Set as Set
import qualified RIO.Map as Map
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import Spicy.Common
import Spicy.FragmentMethods.Types
import Spicy.Math.Combinatorics
import Spicy.Molecule.Computational
import Spicy.Molecule.Fragment hiding (SMFA)
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.Prelude
import Spicy.Wavefunction.Basis

-- | [Cartesian product \(\times\)](https://en.wikipedia.org/wiki/Cartesian_product) of two sets.
-- Forms all combinations.
sCartProd :: (Ord a, Ord b) => Set a -> Set b -> Set (a, b)
sCartProd a b =
  let vecA = Massiv.fromList @B Par . Set.toList $ a
      vecB = Massiv.fromList @B Par . Set.toList $ b
      sA = Massiv.size vecA
      sB = Massiv.size vecB
      matA = expandOuter @B @Ix2 sB const vecA
      matB = expandInner @B @Ix2 sA const vecB
      pairs = Set.fromList . Massiv.toList $ Massiv.zip matA matB
   in pairs

----------------------------------------------------------------------------------------------------

-- | Make \(n\)-mers/\(n\)-combinations from a set of monomers. The neighbour-list is used to judge
-- whether to keep a generated \(n\)-mer by a distance criterion. It will form \(n\)-mers by
-- recursively forming dimers. For each step of the dimere generation, the dimers will be checked
-- whether they are close enough to be kept. This works by utilising the neighbour-list to keep the
-- calculation times for large systems low. The distance criterion is the minimal distance of atoms
-- in the dimers.
makeNmers ::
  MonadThrow m =>
  -- | The molecule for which to form n-mers
  Molecule ->
  -- | Monomers obtained by some 'FragPattern'. Can be calculated using, e.g. 'smfa',
  -- 'systematicOverlapFragmentation', 'systematicExclusionFragmentation'
  FragPattern ->
  -- | Up to which \(n\) to form the \(n\)-mers
  Natural ->
  -- | Maximum distance between two fragments, up to which they are allowed to interact.
  Double ->
  m (Seq (Set AuxMonomer))
makeNmers mol@Molecule {atoms, group} monomersByFrag n dist
  | n <= 0 = throwM . localExc $ "Requesting to form 0-mers, which is non-sensical."
  | otherwise = case neighbourListFromMol of
    Just nl -> go nl 1 . Seq.singleton $ monomers
    Nothing -> fallbackNL >>= \nl -> go nl 1 . Seq.singleton $ monomers
  where
    localExc = MolLogicException "makeNmers"
    -- Unwrapped monomers from the fragmentation pattern.
    monomers = case monomersByFrag of
      OverlappingFocus focusSet -> Set.map snd focusSet
      NonOverlappingFocus focusSet -> focusSet
      SMFA smfa -> smfa

    -- Use the next larger matching neighbour list and shrink it to the given distance.
    neighbourListFromMol = do
      nlWithShrinked <- shrinkNeighbourList atoms (mol ^. #neighbourlist) dist
      let (matchingNLs, _tooSmallNLs) = Map.partitionWithKey (\k _ -> k >= dist) nlWithShrinked
      (_, perfectNL) <- Map.lookupMin matchingNLs
      return perfectNL

    -- In case no matching neighbour list could be found in the molecule, calculate a new, matching.
    fallbackNL = neighbourList dist (mol ^. #atoms)

    -- Check if two fragments have at least one pair of neighbouring atoms.
    fragNeighbouring :: NeighbourList -> IntSet -> IntSet -> Bool
    fragNeighbouring nl atomsA atomsB =
      not
        . IntMap.null
        . IntMap.filter (not . IntSet.null)
        . IntMap.map (`IntSet.intersection` atomsB)
        . IntMap.restrictKeys nl
        $ atomsA

    -- Recursively forming dimers. Returns all lower n-mers that have been formed in the process.
    go :: MonadThrow m => NeighbourList -> Natural -> Seq (Set AuxMonomer) -> m (Seq (Set AuxMonomer))
    go _ _ Empty = return Empty
    go nl currentN prevMers@(_ :|> lastMers)
      | currentN >= n = return prevMers
      | otherwise = do
        let allNewNMers = Massiv.fromList @B Par . Set.toList $ sCartProd lastMers monomers
            neighbouringNMers =
              compute @B $
                Massiv.sfilter
                  ( \(fragA, fragB) -> do
                      let atomsA = getAuxMonomerAtoms group fragA
                          atomsB = getAuxMonomerAtoms group fragB
                          areABneighbours = fragNeighbouring nl <$> atomsA <*> atomsB
                       in fromMaybe False areABneighbours
                  )
                  allNewNMers
        newMers <-
          fmap (Set.fromList . Massiv.toList)
            . Massiv.mapM @B (\(a, b) -> auxMonomerUnion group a b)
            $ neighbouringNMers
        go nl (currentN + 1) (prevMers Seq.|> newMers)

{-
====================================================================================================
-}

-- | Make the GMBE sum for a given set of auxiliary monomers/n-mers. The implementation is based on
-- fragment-combination-ranges.
makeGmbeSum :: IntMap Group -> Set AuxMonomer -> GMBEmap
makeGmbeSum = fcrGMBE

----------------------------------------------------------------------------------------------------

-- | Simpler representation of an 'AuxMonomer', just describing the constituting groups. Can later
-- be converted to a 'AuxMonomer' by 'auxMonoFromGroups'.
type Fragment = IntSet

----------------------------------------------------------------------------------------------------

-- | A fragment combination range. For an 'AuxMonomer' of size \(n\), this includes all
-- \([n \dots 1]\)-combinations of the constituting groups.
newtype FCR = FCR {getFCRfrags :: Set Fragment}

----------------------------------------------------------------------------------------------------

-- | The pure GMBE sum. Maps from a 'Fragment', to the contribution this 'Fragment' has in the full
-- sum.
type GMBEsum = Map Fragment Int

----------------------------------------------------------------------------------------------------

-- | Build the fragment-combination-range ('FCR') of a given 'Fragment'.
frag2FCR :: Fragment -> FCR
frag2FCR am = FCR . Set.fromList . concat . intNcombinations (fromIntegral . IntSet.size $ am) $ am

----------------------------------------------------------------------------------------------------

-- | Construct the 'FCR' for the full set of all fragments.
frags2FCR :: Set Fragment -> FCR
frags2FCR am = FCR . Set.unions . fmap (getFCRfrags . frag2FCR) . Set.toList $ am

----------------------------------------------------------------------------------------------------

-- | Calculate the FCR sum as in Eq 9, <https://doi.org/10.1063/5.0059598>.
fcrSum :: FCR -> GMBEsum
fcrSum fcr = Map.filter (/= 0) . Map.unionsWith (+) . fmap innerSum . Set.toList . getFCRfrags $ fcr
  where
    innerSum :: Fragment -> GMBEsum
    innerSum frg =
      let coeff subFrg = (-1) ^ (IntSet.size frg - IntSet.size subFrg)
          frgSubsets = fmap (\sf -> (sf, coeff sf)) . Set.toList . getFCRfrags . frag2FCR $ frg
       in Map.fromList frgSubsets

----------------------------------------------------------------------------------------------------

-- | Obtain the 'GMBEmap' by processing all fragments.
fcrGMBE :: IntMap Group -> Set AuxMonomer -> GMBEmap
fcrGMBE groups frags =
  let gmbeSum = fcrSum . frags2FCR . Set.map (^. #constGroups) $ frags
   in Map.map (,Nothing)
        . Map.mapKeys (\constGroups -> auxMonoFromGroups . flip IntMap.restrictKeys constGroups $ groups)
        $ gmbeSum

{-
====================================================================================================
-}

-- $gmbeCollectors
-- GMBE can use standard ONIOM collectors, with just little tweaks: The GMBE output
-- needs to be converted to a monolithic output, so that it can be collected normally.

-- | Transformation of fragment energy expressions to a full layer description in terms of GMBE.
-- Effectively transforms a 'GMBE' derivatives in something suitable for a 'Monolith' context.
transformGMBEEnergies :: MonadThrow m => OniomLayer -> CalcK -> m EnergyDerivatives
transformGMBEEnergies (OniomLayer laymol@Molecule {calculations, atoms}) calcK = do
  -- Get the correct outputs to collect this GMBE calculation.
  gmbeMap <-
    maybe2MThrow
      (localExc "No matching calculation context available to transform GMBE results.")
      $ calculations ^? _Just % niveauLensGen calcK % #gmbe % _Just

  -- Collect the GMBE terms and transform them to this layers full expression.
  Map.foldlWithKey' goF (return zeroEDerivs) gmbeMap
  where
    localExc = MolLogicException "transformGMBEEnergies"

    -- Energy and its derivatives initialised as zero acuumulators.
    zeroEDerivs =
      EnergyDerivatives
        { energy = Just 0,
          gradient = Just zeroGrad,
          hessian = Just zeroHess
        }
    nonDummyAtoms = IntMap.filter (not . isDummy) atoms
    nNonDummy = IntMap.size nonDummyAtoms
    zeroGrad = VectorS $ Massiv.replicate Par (Sz $ 3 * nNonDummy) 0
    zeroHess = MatrixS $ Massiv.replicate Par (Sz $ 3 * nNonDummy :. 3 * nNonDummy) 0

    -- Folding function to transform all AuxMonomers back into the full layer context.
    goF :: MonadThrow m => m EnergyDerivatives -> AuxMonomer -> (Int, Maybe CalcOutput) -> m EnergyDerivatives
    goF acc' am (fac, out) = do
      EnergyDerivatives {..} <- acc'

      -- Abuse 'getCalcByID' to obtain the auxmonomer as a real system with link atoms and stuff and
      -- its Jacobian
      (_, _, OniomLayer amMol) <- getCalcByID laymol $ CalcID Empty calcK (Just am)

      -- Prepare context for this auxiliary monomer.
      amJacobian <-
        maybe2MThrow (localExc "Fragment must have produced a Jacobian matrix, but there is none.")
          . fmap getMatrixS
          $ amMol ^. #jacobian
      amCalcOutput <-
        maybe2MThrow
          (localExc "Cannot find calculation output for auxiliary monomer")
          out
      let amJacobianT = compute . transpose $ amJacobian

      -- Energy
      let newAccEnergy = do
            accEnergy <- energy
            amEnergy <- amCalcOutput ^. #energyDerivatives % #energy
            return $ accEnergy + (amEnergy * fromIntegral fac)

      -- Gradient
      let newAccGrad = do
            accGrad <- getVectorS <$> gradient
            amGradient <- getVectorS <$> amCalcOutput ^. #energyDerivatives % #gradient
            amTrGradient <- amGradient ><. amJacobian
            (fromIntegral fac *. amTrGradient) .+. accGrad

      -- Hessian
      let newAccHessian = do
            accHess <- getMatrixS <$> hessian
            amHessian <- getMatrixS <$> amCalcOutput ^. #energyDerivatives % #hessian
            amTrHessian <- amJacobianT .><. amHessian >>= (.><. amJacobian)
            (fromIntegral fac *. amTrHessian) .+. accHess

      return $
        EnergyDerivatives
          { energy = newAccEnergy,
            gradient = VectorS <$> newAccGrad,
            hessian = MatrixS <$> newAccHessian
          }

----------------------------------------------------------------------------------------------------

-- | Simple averaging of all multipoles, that have been assigned to an atom. "This works
-- surprisingly well for many cases" ;)
transformGMBEMultipoles :: MonadThrow m => OniomLayer -> CalcK -> m (IntMap Multipoles)
transformGMBEMultipoles (OniomLayer Molecule {calculations = Nothing}) _ = throwM $ MolLogicException "transformGMBEMultipoles" "No GMBE calculations found on this layer."
transformGMBEMultipoles (OniomLayer Molecule {calculations = Just calcContext}) calcK = do
  -- Get the correct outputs to collect this GMBE calculation.
  gmbeMultipoles <-
    maybe2MThrow (localExc "No GMBE output has been found or one of the calculations did not produce multipole moments.") $
      straverse (\(_, co) -> co ^? _Just % #multipoles) . sfromList . Map.elems
        =<< calcContext ^? niveauLensGen calcK % #gmbe % _Just

  -- Average all multipoles of all atoms arithmetically.
  let countablePoles = smap (IntMap.map (\mp -> (1 :: Int, mp))) gmbeMultipoles
      atomicMpSums = IntMap.unionsWith (\(aC, aMP) (bC, bMP) -> (aC + bC, combineMultipoles (+) aMP bMP)) countablePoles
      atomicMPAverages = IntMap.map (\(c, mpSum) -> modifyMultipole (/ fromIntegral c) mpSum) atomicMpSums

  return atomicMPAverages
  where
    localExc = MolLogicException "transformGMBEMultipoles"

----------------------------------------------------------------------------------------------------

-- | Collect the basis from the GMBE calculations. Just concatenates all basis functions into a
-- single, large basis.
transformGMBEBasis :: OniomLayer -> CalcK -> Maybe Basis
transformGMBEBasis (OniomLayer Molecule {calculations = Nothing}) _ = throwM $ MolLogicException "transformGMBEBasis" "No GMBE calculations found on this layer."
transformGMBEBasis (OniomLayer Molecule {calculations = Just calcContext}) calcK = do
  -- Get the correct outputs to collect this GMBE calculation.
  gmbeBases <-
    maybe2MThrow (localExc "No GMBE output has been found or one of the calculations did not have a basis.") $
      traverse (\(_, co) -> co ^? _Just % #basis % _Just) . Map.elems
        =<< calcContext ^? niveauLensGen calcK % #gmbe % _Just

  -- Make the basis into stream vectors, that can be efficiently concatenated.
  let allBasisShells = sconcat . fmap (\Basis {shells} -> toStreamArray . getVectorG $ shells) $ gmbeBases
      gmbeBasis =
        Basis
          { name = case gmbeBases of
              Basis {name} : _ -> name
              _ -> mempty,
            shells = VectorG . compute @B $ allBasisShells
          }

  return gmbeBasis
  where
    localExc = MolLogicException "transformGMBEEnergies"

----------------------------------------------------------------------------------------------------

-- | Transform the data from a GMBE calculation on a layer to a monolithic calculation.
gmbeToMono :: MonadThrow m => OniomLayer -> CalcK -> m CalcOutput
gmbeToMono layer calcK = do
  energyDerivatives <- transformGMBEEnergies layer calcK
  multipoles <- transformGMBEMultipoles layer calcK
  return $
    CalcOutput
      { energyDerivatives,
        multipoles,
        basis = transformGMBEBasis layer calcK,
        wavefunction = def
      }
