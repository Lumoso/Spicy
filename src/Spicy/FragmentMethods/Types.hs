-- |
-- Module      : Spicy.FragmentMethods.Types
-- Description : Common definitions for fragment methods.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
module Spicy.FragmentMethods.Types
  ( FragPattern (..),
    GroupGraph,
  )
where

import Data.Graph.Inductive hiding ((&))
import Spicy.Molecule.Fragment
import Spicy.Prelude

-- | Results of different fragmentation patterns.
data FragPattern
  = -- | Set of all groups with their local sourrounding clusters. The tuple stores the focused
    -- group and its local cluster around it.
    OverlappingFocus (Set (Int, AuxMonomer))
  | -- | Non-overlapping groups but clusters around a focus-group. The tuple stores the focused
    -- group and its local cluster around it.
    NonOverlappingFocus (Set AuxMonomer)
  | -- | SMFA groups
    SMFA (Set AuxMonomer)
  deriving (Eq, Show)

----------------------------------------------------------------------------------------------------

-- | Graph of non-overlapping groups, that partition the molecule. Bond types are unlabeled.
type GroupGraph = Gr Group ()
