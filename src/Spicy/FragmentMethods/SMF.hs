-- |
-- Module      : Spicy.FragmentMethods.SMF
-- Description : Systematic Molecule Fragmentation
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Implementation of
-- [Systematic Molecule Fragmentation by Annihilation](https://doi.org/10.1021/ar500088d).
-- Say we have a molecule, which is defined by a set of atoms \(A\) and a set of bonds between those
-- atoms. We first want to find a [partition](https://en.wikipedia.org/wiki/Partition_of_a_set) of
-- the set atoms \(A\), into a set of groups \(G\), which serves as a basis for fragment-based
-- calculations. Ideally we are looking for a chemically sensible partition, in which the groups
-- form reasonable, smallest elements, that wont be further separated in a calculation. They
-- therefore relate somehow to the concept of chemical functional groups ('makeSMFgroups' and
-- 'toGroupGraph').
-- \[ \bigcup G = A \]
-- \[ G_i \subseteq A \]
-- In a second step we want to form fragments from those groups, which may or may not overlap.
-- Besides the "SMFA" procedure ('smfa'), that is described in the original publication, the much
-- simpler option of ego-graph based fragment generation has been coded
-- ('systematicOverlapFragmentation' and 'systematicExclusionFragmentation').
--
-- The module builds on top of the graph representation, that is available from the bond matrix.
module Spicy.FragmentMethods.SMF
  ( makeSMFgroups,
    toGroupGraph,
    smfa,
    systematicOverlapFragmentation,
    systematicExclusionFragmentation,
  )
where

import Data.Graph.Inductive as Graph hiding ((&))
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding (modify)
import qualified RIO.List as List
import qualified RIO.Set as Set
import RIO.State
import Spicy.Common
import Spicy.FragmentMethods.Types
import Spicy.Molecule.Fragment hiding (SMFA)
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.Prelude

-- | Form groups by the rules of SMF (see <https://doi.org/10.1021/ar500088d>).
makeSMFgroups :: MolGraph -> IntMap Group
makeSMFgroups molGraph =
  let -- 1) Delete all single bonds from a graph, except single bonds involving hydrogen.
      -- Things that are still connected will definitely form a group. Uses transitive closures of
      -- a filtered graph.
      delocHydrogenGraph = enfilter filterDelocHydro molGraph
      delocHydrogenGroups = graph2Groups delocHydrogenGraph

      -- 2) Look for charged or spin-polarised groups. Those will again connect to their neighbours.
      -- Form the final graph from it.
      cpGroups = groupsChargedOrPolarised molGraph delocHydrogenGroups
      cpBonds = smap (chargePolarisationBonds molGraph) cpGroups
      cpGroupGraph = Massiv.sfoldl connectToChargedPolarisedNeighbours delocHydrogenGraph cpBonds

      -- 3) Use the finally correctly connected graph to form non-overlapping groups.
      finalGroupKeys = graph2Groups cpGroupGraph
      finalGroupElectronics = smap (getGroupElectronic cpGroupGraph) finalGroupKeys
      finalGroups =
        Massiv.sizipWith
          ( \i gk (gc, gexc) ->
              ( i,
                Group
                  { label = "SMF-" <> tshow i,
                    chain = Nothing,
                    atoms = gk,
                    charge = gc,
                    excEl = gexc
                  }
              )
          )
          finalGroupKeys
          finalGroupElectronics
   in IntMap.fromAscList . Massiv.stoList $ finalGroups
  where
    -- Filter, that keeps only bonds, that can delocalise or involve a hydrogen.
    filterDelocHydro :: (LNode Atom, LNode Atom, BondOrder) -> Bool
    filterDelocHydro ((_, atA), (_, atB), bo) =
      atA ^. #element == H || atB ^. #element == H || bo == Multiple || bo == Coordinative

    -- Detect charged and spin-polarised groups.
    groupsChargedOrPolarised :: MolGraph -> Vector DS IntSet -> Vector DS IntSet
    groupsChargedOrPolarised mg groups = Massiv.sfilter criterion groups
      where
        criterion :: IntSet -> Bool
        criterion g =
          let (gCharge, gExcEl) = getGroupElectronic mg g
           in gCharge /= 0 || gExcEl /= 0

    -- Charged or spin-polarised groups include their directly bonded neighbour groups. Re-add
    -- single bonds to all direct neighbours of these groups.
    connectToChargedPolarisedNeighbours :: MolGraph -> [LEdge BondOrder] -> MolGraph
    connectToChargedPolarisedNeighbours mg cpBonds = insEdges cpBonds mg

    -- Feed it standard molecule graph with all bonds and a charged group. Obtains a list of bonds,
    -- that need to be added again to the delocalisation-hydrogen-graph.
    chargePolarisationBonds :: MolGraph -> IntSet -> [LEdge BondOrder]
    chargePolarisationBonds mg group =
      let groupContext = fmap (context mg) . IntSet.toAscList $ group
       in flipFoldl' mempty groupContext $ \mgAcc (adj, gN, _, _) ->
            flipFoldl' mgAcc adj $ \mgAcc' (bo, nN) ->
              if nN `IntSet.member` group
                then mgAcc'
                else (gN, nN, bo) : (nN, gN, bo) : mgAcc'

----------------------------------------------------------------------------------------------------

-- | SMFA algorithm to form fragments (monomers) at a given SMFA-level. The ring avoidance rules are
-- slighlty modified to respect rings of any size, that is problematic for the given SMF level.
smfa ::
  -- | Level of SMF
  Natural ->
  -- | Graph representation of the molecule.
  MolGraph ->
  -- | Functional groups, commonly obtained by 'makeSMFgroups'
  IntMap Group ->
  -- | 'SMFA' pattern
  FragPattern
smfa smfaOrd molGraph groups =
  let fragsConstitutingGroups = fst $ execState (go mempty groupGraph) (mempty, mempty)
      fragments =
        Set.map
          ( \grKs ->
              let constGroups = labNodes . nfilter (`IntSet.member` grKs) $ groupGraph
               in flipFoldl' (AuxMonomer 0 0 grKs) constGroups $ \auxMonomer (_, gr) ->
                    auxMonomer
                      & #charge %~ (+ (gr ^. #charge))
                      & #excEl %~ (+ (gr ^. #excEl))
          )
          fragsConstitutingGroups
   in SMFA fragments
  where
    -- Molecule graph reduced to a group graph.
    groupGraph = toGroupGraph molGraph groups

    -- Focus a given group in the graph and construct a local cluster around it, with all groups, that
    -- are up to the given order/level \(n\) away from the focused group. If at least one group within
    -- the search distance can be found, the function will return 'Just' a list of length \(n+1\) with
    -- the local cluster and the groups ordered by distance.
    -- If no group at the given search distance can be found, a 'Nothing' is returned and a cut around
    -- this group is not possible.
    localSmfaCluster :: Gr a b -> Int -> Maybe [IntSet]
    localSmfaCluster gr group = do
      let levelCluster = withinJumps smfaOrd gr group
      if length levelCluster < fromIntegral smfaOrd
        then Nothing
        else Just levelCluster

    -- Perform a single fragmentation step of SMFA (Eq. 2.10). If further fragmentation of the given
    -- group graph is impossible, the groups that are present in the graph will be added to the set
    -- of finished fragments.
    -- If further fragmentation is possible, perform a fragmentation step and recursively apply the
    -- fragmentation to the formed fragments.
    --
    -- The state carries a tuple of fragments. First comes the set of fragments, that are finished and
    -- may be not fragmented further. Second comes a set of fragments, whose path have already been
    -- taken. They may not be finished but can be removed from later searches.
    go :: (MonadState (Set IntSet, Set IntSet) m) => Set IntSet -> GroupGraph -> m ()
    go takenInLastStep fragGraph = do
      -- Mark the paths from the last iteration as taken.
      modify $ \(f, taken) -> (f, taken <> takenInLastStep)

      case focusClusterCandidate of
        -- Further fragmentation is not possible. This is a final SMFA fragment in the given order. Add it
        -- to the set of accepted fragments.
        Nothing -> do
          modify $ \(finished, taken) -> (Set.insert fragGroups finished, taken)
        -- Further fragmentation around a node is possible. Perform a fragmentation step and recursively
        -- continue to further fragment the fragments.
        Just focusCluster -> do
          -- Lookup which fragments are already done for later.
          (finishedFrags, takenFrags) <- get

          let -- The focus node is at distance 0. Unsafe indexing is fine here, the construction makes it
              -- impossible to hit an invalid node.
              focusNode = IntSet.findMin . List.genericIndex focusCluster $ (0 :: Int)

              -- The focus group is the set of all groups in the focus cluster.
              focusGroup = foldl' (<>) mempty focusCluster

              -- Ring avoidance:
              --   Check if the local cluster dissambles any ring of a problematic
              --   size, when remove from the molecule. A ring has a problematic size, if a single
              --   group would be removed from it by removing the local cluster. Two cases can
              --   occur: a) the local cluster from step 2 forms a ring of a a problematic size - 1
              --   and therefore leaves a gap of 1 group, or b) one of the groups of the local focus
              --   cluster is part of a ring, that is outside of the local focus cluster, but small
              --   enough not to be segmented further; then those rings must be included in the
              --   local cluster.
              problematicRingSize = smfaOrd * 2 + 2
              ringsAroundFocusNodes = cyclesWithin problematicRingSize fragGraph focusNode
              problematicRingsOfFocusNode =
                Set.filter
                  (\cp -> length cp == fromIntegral problematicRingSize - 1)
                  ringsAroundFocusNodes
              ringsAroundFocusGroup =
                Set.unions
                  . fmap (cyclesWithin problematicRingSize fragGraph)
                  . IntSet.toAscList
                  $ focusGroup
              ringGroupsAroundFocusGroup =
                IntSet.fromList
                  . concat
                  . Set.toList
                  $ ringsAroundFocusGroup

              -- Step 1)
              --   Remove the given group from the fragment. This may or may not disconnect parts of
              --   the fragment and may or may not form more fragments. If the focus node is part of
              --   a problematic ring, do not delete the focus node.
              fragRmFocus =
                if Set.null problematicRingsOfFocusNode
                  then delNode focusNode fragGraph
                  else fragGraph
              step1Frags = Set.fromList . Massiv.stoList . graph2Groups $ fragRmFocus

              -- Step 2)
              --   The local cluster around the focused node and all rings of problematic size or
              --   smaller, that are attached to the focus group, form a single fragment.
              step2Frag = focusGroup <> ringGroupsAroundFocusGroup

              -- Step 3)
              --   Remove the focus group and everything outside the local focus cluster from the graph.
              --   This may or may not form multiple groups.
              step3Graph = nfilter (`IntSet.member` step2Frag) fragRmFocus
              step3Frags = Set.fromList . Massiv.stoList . graph2Groups $ step3Graph

              -- The fragmentation may have formed the same fragment multiple times and it might
              -- have formed itself in step 2. Eliminate those redundancies by forming a set of
              -- fragments and remove this fragment from it anyway.
              thisFrag = IntSet.fromList . nodes $ fragGraph
              nextStepFrags =
                (step1Frags <> Set.singleton step2Frag <> step3Frags)
                  Set.\\ finishedFrags
                  Set.\\ Set.singleton thisFrag
                  Set.\\ takenFrags
              nextStepFragGraphs =
                fmap (\fragGroupKeys -> nfilter (`IntSet.member` fragGroupKeys) fragGraph)
                  . Set.toAscList
                  $ nextStepFrags

          -- A group might produce itself again. This happens if the local cluster happens to be
          -- the group itself. In this case, the local cluster must be added to the finished
          -- groups.
          when (step2Frag == thisFrag) . modify $ \(finished, t) -> (Set.insert step2Frag finished, t)

          -- Mark the paths as being taken and then continue the fragmentation recursion.
          -- modify $ \(f, oldTakenFrags) -> (f, oldTakenFrags <> nextStepFrags)
          traverse_ (go nextStepFrags) nextStepFragGraphs
      where
        -- Lazily form local cluster of the given SMFA level/order around all nodes.
        allClusters = fmap (localSmfaCluster fragGraph) . nodes $ fragGraph
        focusClusterCandidate = join $ List.find isJust allClusters

        -- This fragments nodes.
        fragGroups = IntSet.fromAscList . nodes $ fragGraph

----------------------------------------------------------------------------------------------------

-- | Simple construction of overlapping fragments. For each group a local cluster extending up to a
-- given number of inter-group-bonds will be formed (ego-graph). This is somehow an accuracy
-- parameter. The ego-graph is modified by the ring avoidance rule of SMFA. If a problematic ring
-- cut would be taken, the rest of the ring will be included in the local cluster.
systematicOverlapFragmentation ::
  -- | Size of the local cluster in inter-group-bonds
  Natural ->
  -- | Graph representation of the molecule
  MolGraph ->
  -- | Non-separable functional groups, commonly obtained by 'makeSMFgroups'
  IntMap Group ->
  -- | 'OverlappingFocus' pattern
  FragPattern
systematicOverlapFragmentation ord molGraph groups =
  let groupClusterKeys =
        Massiv.map
          (\n ->
            let egoGraphNodes = foldl' (<>) mempty . withinJumps ord groupGraph $ n
                problematicRingGroups = ringGroupsAroundFocusGroup n
            in  (n, problematicRingGroups <> egoGraphNodes)
          )
          . Massiv.fromList @B Par
          . nodes
          $ groupGraph
      ovlGroups =  Massiv.map (second (makeAuxMonomer groupGraph)) groupClusterKeys
   in OverlappingFocus . Set.fromList . Massiv.toList . compute @B $ ovlGroups
  where
    -- Molecule graph reduced to a group graph.
    groupGraph = toGroupGraph molGraph groups

    -- The ring size that is problematic for this level of ego graphs.
    problematicRingSize = 2 * ord + 2
    ringsAroundFocusNode focusNode = cyclesWithin problematicRingSize groupGraph focusNode
    problematicRingsOfFocusNode focusNode =
      Set.filter
        (\cp -> length cp == fromIntegral problematicRingSize - 1)
        (ringsAroundFocusNode focusNode)
    ringGroupsAroundFocusGroup focusNode =
      IntSet.fromList
        . concat
        . Set.toList
        . problematicRingsOfFocusNode
        $ focusNode

----------------------------------------------------------------------------------------------------

-- | Simple construction of non-overlapping fragments. Forms clusters (ego-graphs) of up to a given
-- number of inter-group-bonds around a focused group and removes the resulting cluster from the
-- graph. Proceeds then to the next group, until the entire graph has been consumed.
systematicExclusionFragmentation ::
  -- | Size of the local cluster in inter-group-bonds
  Natural ->
  -- | Graph representation of the molecule
  MolGraph ->
  -- | Non-separable functional groups, commonly obtained by 'makeSMFgroups'
  IntMap Group ->
  -- | 'NonOverlappingFocus' pattern
  FragPattern
systematicExclusionFragmentation ord molGraph groups =
  let groupKeys = go groupGraph allGroups mempty
      fragments = Massiv.map (makeAuxMonomer groupGraph) . compute @B $ groupKeys
   in NonOverlappingFocus . Set.fromList . Massiv.toList $ fragments
  where
    -- Molecule graph reduced to a group graph.
    groupGraph = toGroupGraph molGraph groups

    -- All groups in the graph.
    allGroups = IntSet.fromAscList . nodes $ groupGraph

    -- The ring size that is problematic for this level of ego graphs.
    problematicRingSize = 2 * ord + 2
    ringsAroundFocusNode focusNode = cyclesWithin problematicRingSize groupGraph focusNode
    problematicRingsOfFocusNode focusNode =
      Set.filter
        (\cp -> length cp == fromIntegral problematicRingSize - 1)
        (ringsAroundFocusNode focusNode)
    ringGroupsAroundFocusGroup focusNode =
      IntSet.fromList
        . concat
        . Set.toList
        . problematicRingsOfFocusNode
        $ focusNode

    -- Recursively consume the graph by constructing local clusters from a start node, remove them
    -- from the graph and then recurse.
    go :: GroupGraph -> IntSet -> Vector DL IntSet -> Vector DL IntSet
    go gg remGrs acc = case minGrView of
      Nothing -> acc
      Just (minGr, _) ->
        let focusEgoNodes = foldl' (<>) mempty $ withinJumps ord gg minGr
            focusRingGroups = ringGroupsAroundFocusGroup minGr
            focusCluster = focusRingGroups <> focusEgoNodes
            newGG = nfilter (\n -> not $ n `IntSet.member` focusCluster) gg
            newRemGrs = remGrs IntSet.\\ focusCluster
         in go newGG newRemGrs (focusCluster `Massiv.cons` acc)
      where
        minGrView = IntSet.minView remGrs

{-
====================================================================================================
-}

-- | Get the charge and number of excess electrons of a group.
getGroupElectronic ::
  MolGraph ->
  -- | Indices of the atoms of the group
  IntSet ->
  -- | (Charge, Excess Electrons)
  (Int, Int)
getGroupElectronic mg g =
  let groupAtoms = fromMaybe mempty . traverse (lab mg) . IntSet.toAscList $ g
      (gCharge, gExcEl) =
        foldl'
          (\(accC, accE) Atom {formalCharge, formalExcEl} -> (accC + formalCharge, accE + formalExcEl))
          (0, 0)
          groupAtoms
   in (gCharge, gExcEl)

----------------------------------------------------------------------------------------------------

-- | Use breadth first search over the graph. Everything that is connected will form a group.
graph2Groups :: Gr a b -> Vector DS IntSet
graph2Groups graph = go (graph, mempty) allNodes
  where
    -- All atoms in the graph
    allNodes = IntSet.fromAscList . nodes $ graph

    -- Recursively decontructs a graph into groups by using depth first search.
    --   - First argument is a graph, that removes one group in each iteration.
    --   - Second argument is the set of atoms, that has not been assigned to a group yet.
    --   - Returns (graph reduced by the group found in this iteration, growing list of atoms that
    --     form a group together). On return the graph must be empty and the list finished.
    go :: (Gr a b, Vector DS IntSet) -> IntSet -> Vector DS IntSet
    go (graphAcc, groupAcc) unassignedAtoms =
      let startAtom = IntSet.findMin unassignedAtoms
          thisGroupL = bfs startAtom graphAcc
          thisGroupS = IntSet.fromList thisGroupL
          newGraphAcc = delNodes thisGroupL graphAcc
          newGroupAcc = groupAcc `Massiv.sappend` ssingleton thisGroupS
          remainingAtoms = unassignedAtoms IntSet.\\ thisGroupS
       in if Graph.isEmpty graphAcc || IntSet.null unassignedAtoms
            then groupAcc
            else go (newGraphAcc, newGroupAcc) remainingAtoms

----------------------------------------------------------------------------------------------------

-- | Transforms a molecule graph into the simpler representation of its groups and their
-- connections.
toGroupGraph :: MolGraph -> IntMap Group -> GroupGraph
toGroupGraph molGraph groups =
  let vertices = IntMap.toAscList groups
      groupBonds =
        concatMap makeLocalNeighbourhoodEdges
          . IntMap.toAscList
          . IntMap.mapWithKey (\gk _ -> groupNeighbours gk)
          $ groups
   in mkGraph vertices groupBonds
  where
    -- Obtains other, neighbouring groups of given group.
    groupNeighbours :: Int -> IntSet
    groupNeighbours groupKey =
      let groupAtoms = fromMaybe mempty ((groups IntMap.!? groupKey) <&> (^. #atoms))
          neighouringAtoms = concatMap (neighbors molGraph) . IntSet.toAscList $ groupAtoms
          neighbouringGroups =
            IntSet.fromList
              . fromMaybe mempty
              . traverse (\ak -> findAtomInGroup ak groups)
              $ neighouringAtoms
       in neighbouringGroups

    -- Make all connections a local neighbourhood has.
    makeLocalNeighbourhoodEdges :: (Int, IntSet) -> [(Node, Node, ())]
    makeLocalNeighbourhoodEdges (centre, neighbours) =
      filter (\(o, t, _) -> o /= t) $
        (,,) <$> pure centre <*> IntSet.toAscList neighbours <*> pure ()

----------------------------------------------------------------------------------------------------

-- | Make an auxiliary monomer/fragment from a set of groups.
makeAuxMonomer :: GroupGraph -> IntSet -> AuxMonomer
makeAuxMonomer groupGraph jumpCluster =
  let clusterGroup = labNodes . nfilter (`IntSet.member` jumpCluster) $ groupGraph
   in flipFoldl' (AuxMonomer 0 0 mempty) clusterGroup $ \acc (grK, gr) ->
        acc
          & #charge %~ (+ (gr ^. #charge))
          & #excEl %~ (+ (gr ^. #excEl))
          & #constGroups %~ IntSet.insert grK
