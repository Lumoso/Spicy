{-# LANGUAGE TemplateHaskell #-}

-- |
-- Module      : Spicy.Outputter
-- Description : Facilties for generating structured output files
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This is the logging infrastructure of Spicy, working with a similar concept as the RIO loggers.
-- An asynchronous, central logging thread receives messages from other threads and serialises them
-- to the output file. While the RIO infrastructure takes care of printing events on screen, this
-- module takes care to write data to a file.
--
-- Several properties and logging events are defined, and each property can be written out on any
-- occurence of events. Reasonable defaults are provided, but in principle the module allows to
-- build a custom mapping of events and properties.
module Spicy.Outputter
  ( -- * Exposed Metadata
    spicyLogo,
    spicyLogoColour,
    versionInfo,

    -- * Section Fonts

    -- Generated with the ASCII art generator <https://patorjk.com/software/taag/#p=display&h=3&v=2&f=Slant&t=%C2%B5%20opt%0A>
    sep,
    txtInput,
    txtSetup,
    txtDirectOpt,
    txtMicroOpt,
    txtSinglePoint,
    txtMD,

    -- * Settings
    HasOutputter (..),
    Outputter (..),
    Verbosity (..),
    PrintVerbosity (..),
    HasPrintVerbosity (..),
    defPrintVerbosity,
    PrintEvent (..),
    MotionEvent (..),
    StartEnd (..),
    PrintEnv (..),
    getCurrPrintEnv,

    -- * Logging Facilities
    loggingThread,
    printSpicy,

    -- * Log Generators
    PrintTarget (..),
    SpicyLog,
    spicyLog,
    molID2OniomHumanID,
    printEnergy,
    printGradient,
    printHessian,
    printCoords,
    printTopology,
    printMultipoles,
    optTableHeader,
    optTableLine,
    spicyLogMol,

    -- * Utility
    renderBuilder,
  )
where

import Data.Aeson
import Data.Default
import Data.FileEmbed
import Data.Foldable
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding (forM, mapM_)
import qualified Data.Text.Lazy.Builder as TB
import Data.Version (showVersion)
import Formatting hiding ((%))
import qualified Formatting as F
import Paths_spicy (version)
import qualified RIO.HashMap as HashMap
import qualified RIO.HashSet as HashSet
import qualified RIO.Map as Map
import qualified RIO.Seq as Seq
import qualified RIO.Text as Text
import RIO.Writer
import Spicy.Containers
import Spicy.Massiv
import Spicy.Molecule.Computational
import Spicy.Molecule.Fragment
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.Prelude as S
import qualified System.Path as Path

-- | The Spicy Logo as ASCII art.
spicyLogo :: Utf8Builder
spicyLogo = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/logo/spicy.ascii")

-- | The Spicy Logo as coloured ASCII art with ANSI colour codes.
spicyLogoColour :: ByteString
spicyLogoColour = $(embedFile . Path.toString . Path.relFile $ "data/logo/spicy.ansi")

-- | Get information from the build by TemplateHaskell to be able to show a reproducible version.
versionInfo :: Utf8Builder
versionInfo = displayShow . showVersion $ version

----------------------------------------------------------------------------------------------------

-- | Section separator.
sep :: Utf8Builder
sep = display $ Text.replicate 100 "=" <> "\n"

-- | Heading for the input echo block.
txtInput :: Utf8Builder
txtInput = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/input.txt")

-- | Heading for the setup phase printing.
txtSetup :: Utf8Builder
txtSetup = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/setup.txt")

-- | Heading for direct optimisations.
txtDirectOpt :: Utf8Builder
txtDirectOpt = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/directopt.txt")

-- | Heading for micro-cyclen driven optimisations.
txtMicroOpt :: Utf8Builder
txtMicroOpt = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/microopt.txt")

-- | Heading for single point calculations.
txtSinglePoint :: Utf8Builder
txtSinglePoint = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/singlepoint.txt")

-- | Heading for molecular dynamics.
txtMD :: Utf8Builder
txtMD = displayBytesUtf8 $(embedFile . Path.toString . Path.relFile $ "data/text/md.txt")

----------------------------------------------------------------------------------------------------

-- | RIO style reader class for environments, that have access to the logging thread and settings.
class HasOutputter env where
  outputterL :: Lens' env Outputter

data Outputter = Outputter
  { outChan :: TBQueue Utf8Builder,
    outFile :: Path.AbsRelFile,
    printVerbosity :: PrintVerbosity
  }
  deriving (Generic)

instance HasOutputter Outputter where
  outputterL = castOptic simple

----------------------------------------------------------------------------------------------------

-- | Abstract shortcuts to set define collections of print levels. Sets reasonable defaults.
data Verbosity
  = Low
  | Medium
  | High
  | Debug
  deriving (Show, Eq, Ord, Generic)

instance ToJSON Verbosity

instance FromJSON Verbosity

-- | Environments, that provide print verbosity depending on the event.
class HasPrintVerbosity env where
  printVerbosityL :: Lens' env PrintVerbosity

-- | Information that can be printed and when to print.
data PrintVerbosity = PrintVerbosity
  { -- | Energy of the full ONIOM tree
    oniomE :: HashSet PrintEvent,
    -- | Gradient of the full ONIOM tree
    oniomG :: HashSet PrintEvent,
    -- | Hessian of the full ONIOM tree
    oniomH :: HashSet PrintEvent,
    -- | Coordinates of the full ONIOM tree
    oniomC :: HashSet PrintEvent,
    -- | Bond topology on the full ONIOM tree
    oniomT :: HashSet PrintEvent,
    -- | ONIOM multipoles of the real system
    oniomMP :: HashSet PrintEvent,
    -- | High and low level energy of a layer
    layerE :: HashSet PrintEvent,
    -- | High and low level gradient of a layer
    layerG :: HashSet PrintEvent,
    -- | High and low level hessian of a layer
    layerH :: HashSet PrintEvent,
    -- | GMBE information (if available) about a level
    layerGMBE :: HashSet PrintEvent,
    -- | Coordinates of a layer
    layerC :: HashSet PrintEvent,
    -- | Bond topology on the full ONIOM tree
    layerT :: HashSet PrintEvent,
    -- | High and low level multipole moments of a layer
    layerMP :: HashSet PrintEvent,
    -- | Optimisation information
    opt :: HashSet PrintEvent,
    -- | MD information
    md :: HashSet PrintEvent
  }
  deriving (Generic)

instance HasPrintVerbosity PrintVerbosity where
  printVerbosityL = castOptic simple

instance Default PrintVerbosity where
  def = defPrintVerbosity Medium

-- | Predefined print verbosities.
defPrintVerbosity :: Verbosity -> PrintVerbosity
defPrintVerbosity v = case v of
  Low ->
    PrintVerbosity
      { oniomE = f [Task End],
        oniomG = f [Task End],
        oniomH = f [Task End],
        oniomC = mempty,
        oniomT = mempty,
        oniomMP = mempty,
        layerE = mempty,
        layerG = mempty,
        layerH = mempty,
        layerGMBE = mempty,
        layerC = mempty,
        layerT = mempty,
        layerMP = mempty,
        opt = f [Motion Macro],
        md = f [Motion Macro]
      }
  Medium ->
    PrintVerbosity
      { oniomE = f [Task End, FullTraversal],
        oniomG = f [Task End, FullTraversal],
        oniomH = f [Task End],
        oniomC = f [Spicy Start, Spicy End, Motion Macro],
        oniomT = f [Spicy Start],
        oniomMP = mempty,
        layerE = mempty,
        layerG = mempty,
        layerH = mempty,
        layerGMBE = f [Task Start, Task End, FullTraversal],
        layerC = mempty,
        layerT = mempty,
        layerMP = mempty,
        opt = f [Motion Micro, Motion Macro],
        md = f [Motion Macro]
      }
  High ->
    PrintVerbosity
      { oniomE = f [Always],
        oniomG = f [Always],
        oniomH = f [Always],
        oniomC = f [Spicy Start, Spicy End, Motion Macro],
        oniomT = f [Spicy Start],
        oniomMP = f [Spicy End],
        layerE = f [Motion Micro],
        layerG = f [Motion Micro],
        layerH = mempty,
        layerGMBE = f [Task Start, Task End, FullTraversal],
        layerC = mempty,
        layerT = mempty,
        layerMP = f [FullTraversal],
        opt = f [Motion Micro, Motion Macro],
        md = f [Motion Macro]
      }
  Debug ->
    PrintVerbosity
      { oniomE = f [Always],
        oniomG = f [Always],
        oniomH = f [Always],
        oniomC = f [Always],
        oniomT = f [Spicy Start],
        oniomMP = f [Motion Macro],
        layerE = f [FullTraversal, Motion Micro],
        layerG = f [FullTraversal, Motion Micro],
        layerH = f [FullTraversal],
        layerGMBE = f [Always],
        layerC = f [Always],
        layerT = f [Spicy Start],
        layerMP = f [Always],
        opt = f [Motion Micro, Motion Macro],
        md = f [Motion Macro]
      }
  where
    f :: (Hashable a, Eq a) => [a] -> HashSet a
    f = HashSet.fromList

----------------------------------------------------------------------------------------------------

-- | Events when to print something.
data PrintEvent
  = -- | Always print
    Always
  | -- | On motions of anything in the system
    Motion MotionEvent
  | -- | On any full traversal of the ONIOM tree
    FullTraversal
  | -- | When a 'Task' starts or ends
    Task StartEnd
  | -- | Initial and final information when Spicy starts and stops
    Spicy StartEnd
  deriving (Show, Eq, Generic, Hashable)

data MotionEvent
  = -- | The full ONIOM tree took a complete motion as a whole.
    Macro
  | -- | Anything in the ONIOM tree moved.
    Micro
  deriving (Show, Eq, Generic, Hashable)

-- | Beginning and start of tasks.
data StartEnd
  = Start
  | End
  deriving (Show, Eq, Generic, Hashable)

-- | A print environemt, that contains necessary data.
data PrintEnv = PrintEnv
  { mol :: Molecule,
    printV :: PrintVerbosity
  }
  deriving (Generic)

instance HasDirectMolecule PrintEnv where
  moleculeDirectL = #mol

instance HasPrintVerbosity PrintEnv where
  printVerbosityL = #printV

-- | From the current runtime environment make a logging environment.
getCurrPrintEnv :: (HasMolecule env, HasOutputter env) => RIO env PrintEnv
getCurrPrintEnv = do
  mol <- view moleculeL >>= readTVarIO
  printV <- view $ outputterL % #printVerbosity
  return PrintEnv {mol = mol, printV = printV}

----------------------------------------------------------------------------------------------------

-- | A simple logging thread, that listens on a message queue forever. All messages from the message
-- queue are written to an output file.
loggingThread :: (HasOutputter env, MonadReader env m, MonadIO m) => m ()
loggingThread = do
  q <- view $ outputterL % #outChan
  f <- view $ outputterL % #outFile
  forever $ do
    msg <- atomically . readTBQueue $ q
    appendFileUtf8 f msg

-- | Log function for arbitrary content in Spicy. Meant to be the content of the output file.
printSpicy :: (HasOutputter env) => Utf8Builder -> RIO env ()
printSpicy t = do
  q <- view $ outputterL % #outChan
  atomically . writeTBQueue q $ t

{-
====================================================================================================
-}

-- $prettyPrinters

-- | A monad transformer, that allows to read an environment and log information about the
-- environment
type LogM r w = WriterT w (Reader r) ()

-- | The logging monad of Spicy. Requires a 'Reader' environment to be logged, and writes to
-- 'Writer' environment.
type SpicyLog r = LogM r TB.Builder

-- | Runs the 'LogM' monad with a given environment to read from.
execLogM :: r -> LogM r w -> w
execLogM r lm = flip runReader r . execWriterT $ lm

-- | Runs the Spicy Logger monad to obtain a 'Utf8Builder' log value.
spicyLog :: r -> SpicyLog r -> TB.Builder
spicyLog r lm = execLogM r lm

----------------------------------------------------------------------------------------------------

-- | Parameter type to give the line length.
lw :: Int
lw = 100

-- | Parameter type, giving the width of an element to print.
ew :: Int
ew = 18

-- | Parameter type, giving the width of a small/narrow element to print.
sew :: Int
sew = 8

-- | Default formatter for floating point numbers.
nf :: (Real a) => Format r (a -> r)
nf = left ew ' ' F.%. fixed (ew - 8)

-- | Default formatter for integer numbers
intf :: Integral a => Format r (a -> r)
intf = left sew ' ' F.%. int

-- | String for non-available data.
nAv :: IsString a => a
nAv = "(Not Available)\n"

-- | Wether to print full ONIOM tree properties or per layer properties.
data PrintTarget = ONIOM | Layer MolID | All

-- | Removes the dummy atoms from the molecule, so that printers only print real atoms.
removeDummy :: (HasDirectMolecule env, MonadReader env m) => m a -> m a
removeDummy = local (& moleculeDirectL %~ molMap rmDummies)
  where
    rmDummies :: Molecule -> Molecule
    rmDummies m = m & #atoms %~ IntMap.filter (\a -> not $ a ^. #isDummy)

-- | Printer for a molecule energy.
printEnergy ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printEnergy pt = do
  tell "\n\n"
  case pt of
    -- Only the total ONIOM energy is printed
    ONIOM -> do
      me <- view $ moleculeDirectL % #energyDerivatives % #energy
      case me of
        Nothing -> tell $ oHeader <> nAv
        Just e -> tell $ bformat (builder F.% nf F.% "\n") oHeader e

    -- Layer-wise information is printed. Includes GMBE information, if present.
    Layer i -> do
      mol <- view moleculeDirectL
      -- Construct
      let e = mol ^? molIDLensGen i % #energyDerivatives % #energy % _Just
          el = mol ^? monoLens i Inherited
          eh = mol ^? monoLens i Original
          elf = gmbePrinter mol i Inherited
          ehf = gmbePrinter mol i Original

      -- Print outputs
      tell $ lHeader i
      tell $ "  ONIOM SubTree -> " <> fromMaybe nAv (bformat (nf F.% "\n") <$> e)
      tell $ "  High Level    -> " <> fromMaybe nAv (bformat (nf F.% "\n") <$> eh)
      mapM_ tell ehf
      tell $ "  Low Level     -> " <> fromMaybe nAv (bformat (nf F.% "\n") <$> el)
      mapM_ tell elf

    -- Print the full tree and all layers.
    All -> do
      mol <- view moleculeDirectL
      printEnergy ONIOM
      mapM_ printEnergy . fmap Layer $ getAllMolIDsHierarchically mol
  where
    protoLens lay niv =
      let nivLens = case niv of
            Original -> castOptic #original
            Inherited -> #inherited % _Just
       in molIDLensGen lay % #calculations % _Just % nivLens
    monoLens lay niv = protoLens lay niv % #output % _Just % #energyDerivatives % #energy % _Just
    gmbeLens lay niv = protoLens lay niv % #gmbe % _Just

    oHeader =
      "@ Energy (ONIOM) / Hartree\n\
      \--------------------------\n"

    lHeader i =
      let txt = "@ Energy (Layer " <> molID2OniomHumanID i <> " ) / Hartree"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    fHeader AuxMonomer {constGroups} =
      let txt = "@ Fragment Energy (Fragment Groups: " <> tshow constGroups <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    gmbeFormatter am mE = fHeader am <> fromMaybe nAv (bformat (nf F.% "\n") <$> mE)
    gmbePrinter m lay niv = do
      gmbeMap <- m ^? gmbeLens lay niv
      let gmbeEnMap = fmap (^? _2 % _Just % #energyDerivatives % #energy % _Just) gmbeMap
      return $ Map.foldMapWithKey gmbeFormatter gmbeEnMap

-- | Printer for the ONIOM gradient of the full tree.
printGradient ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printGradient pt = removeDummy $ do
  tell "\n\n"
  case pt of
    ONIOM -> do
      msg <- view $ moleculeDirectL % #energyDerivatives % #gradient
      atoms <- view $ moleculeDirectL % #atoms
      let mg = atomGradAssoc atoms . getVectorS =<< msg
      case mg of
        Nothing -> tell $ oHeader <> nAv
        Just g -> tell $ oHeader <> tableHeader <> tableContent g
    Layer i -> do
      mol <- view moleculeDirectL
      let atoms = mol ^? molIDLensGen i % #atoms
          g = mol ^? molIDLensGen i % #energyDerivatives % #gradient % _Just
          aG = join $ atomGradAssoc <$> atoms <*> (getVectorS <$> g)
          gl = monoGetTransGrad mol i Inherited
          gh = monoGetTransGrad mol i Original
          glf = gmbeGetTransGrad mol i Inherited
          ghf = gmbeGetTransGrad mol i Original

      tell $ lHeader i
      tell $ "  ONIOM SubTree ->\n" <> fromMaybe nAv (tableContent <$> aG)
      tell $ "  High Level    ->\n" <> fromMaybe nAv (tableContent <$> gh)
      mapM_ tell ghf
      tell $ "  Low Level     ->\n" <> fromMaybe nAv (tableContent <$> gl)
      mapM_ tell glf
    All -> do
      mol <- view moleculeDirectL
      printGradient ONIOM
      mapM_ printGradient . fmap Layer $ getAllMolIDsHierarchically mol
  where
    protoLens lay niv =
      let nivLens = case niv of
            Original -> castOptic #original
            Inherited -> #inherited % _Just
       in molIDLensGen lay % #calculations % _Just % nivLens
    monoLens lay niv = protoLens lay niv % #output % _Just % #energyDerivatives % #gradient % _Just
    gmbeLens lay niv = protoLens lay niv % #gmbe % _Just
    gmbeMapLens = _Just % #energyDerivatives % #gradient % _Just

    monoGetTransGrad :: Molecule -> MolID -> CalcK -> Maybe (IntMap (Atom, Vector S Double))
    monoGetTransGrad m lay niv = do
      grad <- m ^? monoLens lay niv
      atoms <- m ^? molIDLensGen lay % #atoms
      atomGradAssoc atoms . getVectorS $ grad

    gmbeGetTransGrad :: Molecule -> MolID -> CalcK -> Maybe TB.Builder
    gmbeGetTransGrad m lay niv = do
      gmbeMap <- m ^? gmbeLens lay niv
      let gmbeOutMap = fmap snd gmbeMap
          amGradMap = (\co -> getVectorS <$> co ^? gmbeMapLens) <$> gmbeOutMap
      return
        . flip Map.foldMapWithKey amGradMap
        $ \am mGrad ->
          let gA = do
                (_, _, OniomLayer amMol) <- getCalcByID m . CalcID lay niv $ Just am
                let amAtoms = amMol ^. #atoms
                atomGradAssoc amAtoms =<< mGrad
           in fHeader am <> fromMaybe nAv (tableContent <$> gA)

    oHeader =
      "@ Gradient (ONIOM) / (Hartree / Angstrom)\n\
      \-----------------------------------------\n"

    lHeader i =
      let txt = "@ Gradient (Layer " <> molID2OniomHumanID i <> ") / (Hartree / Angstrom)"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    fHeader AuxMonomer {constGroups} =
      let txt = "@ Fragment Gradient (Fragment Groups: " <> tshow constGroups <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    -- Header for the table of gradients.
    tableHeader =
      let hf = center ew ' ' F.%. builder
          headerRow = bformat (hf F.% " | " F.% hf F.% " | " F.% hf F.% " | " F.% hf F.% "\n") "Atom (IX)" "X" "Y" "Z"
          rule =
            TB.fromText $
              Text.replicate (ew + 1) "-"
                <> "+"
                <> Text.replicate (ew + 2) "-"
                <> "+"
                <> Text.replicate (ew + 2) "-"
                <> "+"
                <> Text.replicate (ew + 1) "-"
                <> "\n"
       in headerRow <> rule

    -- Atom associated gradients.
    -- atomGradAssoc :: MonadThrow m => IntMap Atom -> Vector S Double -> m (IntMap (Atom, Vector M Double))
    atomGradAssoc atoms g = do
      let aIx = IntMap.keysSet atoms
      gR <- Massiv.toList . outerSlices <$> resizeM (Sz $ IntMap.size atoms :. 3) g
      let ixGrad = IntMap.fromAscList $ S.zip (IntSet.toAscList aIx) gR
          assoc = IntMap.intersectionWith (,) atoms ixGrad
      return assoc

    -- Gradient table body
    tableContent agMap =
      let af = right ew ' ' F.%. (right 4 ' ' F.%. string) F.% ("(" F.% (left 7 ' ' F.%. int) F.% ")")
       in IntMap.foldlWithKey'
            ( \acc i (a, grad) ->
                let l =
                      bformat
                        (af F.% " | " F.% nf F.% " | " F.% nf F.% " | " F.% nf F.% "\n")
                        (show $ a ^. #element)
                        i
                        (grad Massiv.! 0)
                        (grad Massiv.! 1)
                        (grad Massiv.! 2)
                 in acc <> l
            )
            mempty
            agMap

-- | Printer for Hessians.
printHessian ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printHessian pt = removeDummy $ do
  tell "\n\n"
  case pt of
    ONIOM -> do
      msh <- view $ moleculeDirectL % #energyDerivatives % #hessian
      atoms <- view $ moleculeDirectL % #atoms
      let mh = getMatrixS <$> msh
      tell oHeader
      case mh of
        Nothing -> tell nAv
        Just h -> tell $ fromMaybe mempty (tableContent atoms h)
    Layer i -> do
      mol <- view moleculeDirectL
      let atoms = mol ^. #atoms
          layer = mol ^? molIDLensGen i
          h = layer ^? _Just % #energyDerivatives % #hessian % _Just
          hh = monoGetTransHess mol i Original
          hl = monoGetTransHess mol i Inherited
          hhf = gmbeGetTransHess mol i Original
          hlf = gmbeGetTransHess mol i Inherited
      tell $ lHeader i
      tell $ "  ONIOM SubTree ->\n" <> fromMaybe nAv (h >>= \(MatrixS h') -> tableContent atoms h')
      tell $ "  High Level    ->\n" <> fromMaybe nAv hh
      mapM_ tell hhf
      tell $ "  Low Level     ->\n" <> fromMaybe nAv hl
      mapM_ tell hlf
    All -> do
      mol <- view moleculeDirectL
      printHessian ONIOM
      mapM_ (printHessian . Layer) $ getAllMolIDsHierarchically mol
  where
    protoLens lay niv =
      let nivLens = case niv of
            Original -> castOptic #original
            Inherited -> #inherited % _Just
       in molIDLensGen lay % #calculations % _Just % nivLens
    monoLens lay niv = protoLens lay niv % #output % _Just % #energyDerivatives % #hessian % _Just
    gmbeLens lay niv = protoLens lay niv % #gmbe % _Just
    gmbeMapLens = _Just % #energyDerivatives % #hessian % _Just

    monoGetTransHess :: Molecule -> MolID -> CalcK -> Maybe TB.Builder
    monoGetTransHess m lay niv = do
      hess <- m ^? monoLens lay niv
      atoms <- m ^? molIDLensGen lay % #atoms
      tableContent atoms . getMatrixS $ hess

    gmbeGetTransHess :: Molecule -> MolID -> CalcK -> Maybe TB.Builder
    gmbeGetTransHess m lay niv = do
      gmbeMap <- m ^? gmbeLens lay niv
      let gmbeOutMap = fmap snd gmbeMap
          amHessMap = (\co -> getMatrixS <$> co ^? gmbeMapLens) <$> gmbeOutMap
      return . flip Map.foldMapWithKey amHessMap $ \am mHess ->
        let table = do
              (_, _, OniomLayer amMol) <- getCalcByID m . CalcID lay niv $ Just am
              let amAtoms = amMol ^. #atoms
              tableContent amAtoms =<< mHess
         in fHeader am <> fromMaybe nAv table

    oHeader =
      "@ Hessian (ONIOM) / (Hartree / Angstrom^2)\n\
      \------------------------------------------\n"

    lHeader i =
      let txt = "@ Hessian (Layer " <> molID2OniomHumanID i <> ") / (Hartree / Angstrom^2)"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    fHeader AuxMonomer {constGroups} =
      let txt = "@ Fragment Hessian (Fragment Groups: " <> tshow constGroups <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    -- Atom and cartesian components to print as left column and as chunks as header row.
    cmpnts :: IntMap Atom -> Vector B TB.Builder
    cmpnts atoms =
      compute @B . sconcat . fmap (\(x, y, z) -> Massiv.sfromList [x, y, z]) . IntMap.elems
        . IntMap.map
          ( \a ->
              let e = a ^. #element
               in (bShow e <> "(x)", bShow e <> "(y)", bShow e <> "(z)")
          )
        $ atoms
      where
        bShow = fromString . show

    -- Top row printer chunks. Prints components header
    headerRow :: Source r TB.Builder => Vector r TB.Builder -> TB.Builder
    headerRow chunk =
      bformat (center 7 ' ' F.%. builder F.% "\n") "Cmpnt"
        <> Massiv.foldMono (bformat $ center ew ' ' F.%. builder) chunk

    -- Columns of the Hessian matrix.
    tableContent :: MonadThrow m => IntMap Atom -> Matrix S Double -> m TB.Builder
    tableContent atoms hessian = do
      -- Make chunked print groups of hessian columns. Header missing.
      printValueGroup <- forM colsGroups $ \hc ->
        Massiv.fold . compute @B
          <$> concatM (Dim 1) [leftCmpntArr, hc, rightNewLineArr]
      -- Add the header to a print group and then combine everything into the final builder.
      let printGroup =
            Seq.zipWith
              (\h hc -> headerRow (flatten h) <> hc <> "\n")
              cmpntGroups
              printValueGroup
          table = foldl (<>) mempty printGroup
      return table
      where
        Sz (r :. _) = size hessian
        hessPrintV = Massiv.map (bformat ("|" F.% nf)) hessian
        colsGroups = innerChunksOfN 3 hessPrintV
        rightNewLineArr = Massiv.makeArrayLinear @D Par (Sz $ r :. 1) (const "\n")
        leftCmpntArr = Massiv.expandInner @B @Ix2 (Sz 1) const . cmpnts $ atoms
        cmpntGroups = innerChunksOfN 3 leftCmpntArr

-- | Coordinate printer.
printCoords ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printCoords pt = removeDummy $ do
  tell "\n\n"
  case pt of
    ONIOM -> do
      atoms <- view $ moleculeDirectL % #atoms
      groups <- view $ moleculeDirectL % #group
      tell $ oHeader <> table atoms groups
    Layer i -> do
      mol <- view moleculeDirectL
      let mAtoms = mol ^? molIDLensGen i % #atoms
          mGroups = mol ^? molIDLensGen i % #group
      tell $ lHeader i <> fromMaybe mempty (table <$> mAtoms <*> mGroups)
    All -> do
      mol <- view moleculeDirectL
      printCoords ONIOM
      mapM_ (printCoords . Layer) $ getAllMolIDsHierarchically mol
  where
    oHeader =
      "@ Coordinates (ONIOM) / Angstrom\n\
      \--------------------------------\n"

    lHeader i =
      let txt = "@ Coordinates (Layer " <> molID2OniomHumanID i <> ") / Angstrom"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    tableHeader =
      let hf = center ew ' ' F.%. builder
          shf = center sew ' ' F.%. builder
          topRow =
            bformat
              (hf F.% " | " F.% hf F.% " | " F.% hf F.% " | " F.% hf F.% " | " F.% shf F.% " | " F.% shf F.% " | " F.% shf F.% "\n")
              "Atom (IX)"
              "x"
              "y"
              "z"
              "Group"
              "Charge"
              "Exc El"
          midRule =
            TB.fromText $
              Text.replicate (ew + 1) "-"
                <> "+"
                <> Text.replicate (ew + 2) "-"
                <> "+"
                <> Text.replicate (ew + 2) "-"
                <> "+"
                <> Text.replicate (ew + 2) "-"
                <> "+"
                <> Text.replicate (sew + 2) "-"
                <> "+"
                <> Text.replicate (sew + 2) "-"
                <> "+"
                <> Text.replicate (sew + 1) "-"
                <> "\n"
       in topRow <> midRule

    table atoms groups =
      let af = right ew ' ' F.%. (right 4 ' ' F.%. string) F.% ("(" F.% (left 4 ' ' F.%. int) F.% ")")
       in IntMap.foldlWithKey'
            ( \acc i a ->
                let coords = getVectorS $ a ^. #coordinates
                    line =
                      bformat
                        (af F.% " | " F.% nf F.% " | " F.% nf F.% " | " F.% nf F.% " | " F.% builder F.% " | " F.% intf F.% " | " F.% intf F.% " \n")
                        (show $ a ^. #element)
                        i
                        (coords Massiv.! 0)
                        (coords Massiv.! 1)
                        (coords Massiv.! 2)
                        (fromMaybe nAv $ bformat intf <$> findAtomInGroup i groups)
                        (a ^. #formalCharge)
                        (a ^. #formalExcEl)
                 in acc <> line
            )
            tableHeader
            atoms

-- | Print the bond matrix / topology.
printTopology ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printTopology pt = removeDummy $ do
  tell "\n\n"
  case pt of
    ONIOM -> do
      bondMat <- view $ moleculeDirectL % #bonds
      tell $ oHeader <> table bondMat <> "\n"
    Layer i -> do
      mol <- view moleculeDirectL
      let mBondMat = mol ^? molIDLensGen i % #bonds
      tell $ lHeader i <> fromMaybe mempty (table <$> mBondMat) <> "\n"
    All -> do
      mol <- view moleculeDirectL
      printTopology ONIOM
      mapM_ (printHessian . Layer) $ getAllMolIDsHierarchically mol
  where
    oHeader =
      "@ Bond Topology (ONIOM)\n\
      \-----------------------\n"

    lHeader i =
      let txt = "@ Bond Topology (Layer" <> molID2OniomHumanID i <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    table bondMat =
      snd $
        HashMap.foldlWithKey'
          ( \(cnt, acc) (o, t) bo ->
              let newCnt = cnt + 1
                  lineBreak = if (cnt + 1) `mod` 3 == 0 then "\n" else mempty
                  newAcc = case bo of
                    None -> acc
                    Single -> acc <> bformat ("    " F.% oF F.% " - " F.% tF) o t <> lineBreak
                    Multiple -> acc <> bformat ("    " F.% oF F.% " = " F.% tF) o t <> lineBreak
                    Coordinative -> acc <> bformat ("    " F.% oF F.% " ~ " F.% tF) o t <> lineBreak
               in (newCnt, newAcc)
          )
          (0 :: Int, mempty)
          (makeBondMatUnidirectorial bondMat)
      where
        oF = left 8 ' ' F.%. int
        tF = right 8 ' ' F.%. int

-- | Multipole printer for a given layer. Prints up to quadrupoles. On the layer printer, also
-- individual GMBE fragment multipoles will be printed.
printMultipoles ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printMultipoles pt = removeDummy $ do
  tell "\n\n"
  case pt of
    ONIOM -> do
      atoms <- view $ moleculeDirectL % #atoms
      tell $ oHeader <> content atoms
    Layer i -> do
      mol <- view moleculeDirectL
      let mAtoms = mol ^? molIDLensGen i % #atoms
          lMPs = mpGetTrans mol i Inherited
          hMPs = mpGetTrans mol i Original
      tell $ lHeader i <> fromMaybe mempty (content <$> mAtoms)
      mapM_ tell lMPs
      mapM_ tell hMPs
    All -> do
      mol <- view moleculeDirectL
      printMultipoles ONIOM
      mapM_ (printMultipoles . Layer) $ getAllMolIDsHierarchically mol
  where
    oHeader =
      "@ Multipoles (ONIOM) / ea_0^k for rank k\n\
      \----------------------------------------\n"

    fHeader AuxMonomer {constGroups} =
      let txt = "@ Fragment Multipoles (Fragment Groups: " <> tshow constGroups <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    lHeader i =
      let txt = "\n\n@ Multipoles (Layer " <> molID2OniomHumanID i <> ") / ea_0^k for rank k"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    content atoms = foldl' (\acc a -> acc <> atomPrinter a) mempty atoms

    mpGetTrans :: Molecule -> MolID -> CalcK -> Maybe TB.Builder
    mpGetTrans m lay niv = do
      gmbeMap <- m ^? molIDLensGen lay % #calculations % _Just % niveauLensGen niv % #gmbe % _Just
      let gmbeOutMap = fmap snd gmbeMap
          amMPmap = (\co -> co ^? _Just % #multipoles) <$> gmbeOutMap
      return . flip Map.foldMapWithKey amMPmap $ \am mMP ->
        let amAtomsWithMP = do
              (_, _, OniomLayer amMol) <- getCalcByID m . CalcID lay niv $ Just am
              let amAtoms = amMol ^. #atoms
              IntMap.intersectionWith (\a mp -> a & #multipoles .~ mp) amAtoms <$> mMP
         in fHeader am <> fromMaybe nAv (content <$> amAtomsWithMP)

    atomPrinter :: Atom -> TB.Builder
    atomPrinter a =
      let atomHeader =
            bformat
              ("  " F.% (right 3 ' ' F.%. string) F.% "  x = " F.% nf F.% "  y = " F.% nf F.% "  z = " F.% nf F.% "\n")
              (show $ a ^. #element)
              (getVectorS (a ^. #coordinates) Massiv.! 0)
              (getVectorS (a ^. #coordinates) Massiv.! 1)
              (getVectorS (a ^. #coordinates) Massiv.! 2)
          monopole = do
            mp <- a ^? #multipoles % #monopole % _Just % #q00
            return $ lp <> vf "00" mp <> "\n"
          dipole = do
            dp <- a ^. #multipoles % #dipole
            let q10 = dp ^. #q10
                q11c = dp ^. #q11c
                q11s = dp ^. #q11s
                mag = sqrt $ q10 ** 2 + q11c ** 2 + q11s ** 2
            return $ mf 1 mag <> vf "10" q10 <> vf "11c" q11c <> vf "11s" q11s <> "\n"
          quadrupole = do
            qp <- a ^. #multipoles % #quadrupole
            let q20 = qp ^. #q20
                q21c = qp ^. #q21c
                q21s = qp ^. #q21s
                q22c = qp ^. #q22c
                q22s = qp ^. #q22s
                mag = sqrt $ q20 ** 2 + q21c ** 2 + q21s ** 2 + q22c ** 2 + q22s ** 2
            return $
              mf 2 mag <> vf "21c" q21c <> vf "21s" q21s <> "\n"
                <> lp
                <> vf "22c" q22c
                <> vf "22s" q22s
                <> "\n"
       in atomHeader <> fm monopole <> fm dipole <> fm quadrupole <> "\n"
      where
        fm :: Monoid a => Maybe a -> a
        fm = fromMaybe mempty

        lp :: TB.Builder
        lp = bformat (left 19 ' ' F.%. builder) mempty

        mf :: Real a => Int -> a -> TB.Builder
        mf k num = bformat ("  |Q" F.% int F.% "| = " F.% (left 10 ' ' F.%. fixed 6)) k num

        vf :: Real a => TB.Builder -> a -> TB.Builder
        vf sub num =
          bformat
            ("  Q" F.% (right 3 ' ' F.%. builder) F.% " = " F.% (left 10 ' ' F.%. fixed 6))
            sub
            num

-- | Print the GMBE sum. This is a potentially very long term.
printGMBE ::
  (HasDirectMolecule env, MonadReader env m, MonadWriter TB.Builder m) =>
  PrintTarget ->
  m ()
printGMBE pt = do
  tell "\n\n"
  case pt of
    -- Printing GMBE for ONIOM makes no sense. This is intrinsically layer-wise.
    ONIOM -> return ()
    -- Print the GMBE expression for a layer.
    Layer i -> do
      mol <- view moleculeDirectL
      let highGMBE = getTransGMBE mol i Original
          lowGMBE = getTransGMBE mol i Inherited
      mapM_ tell highGMBE
      mapM_ tell lowGMBE
    All -> do
      mol <- view moleculeDirectL
      mapM_ (printGMBE . Layer) $ getAllMolIDsHierarchically mol
  where
    gmbeLens lay niv =
      let nivLens = case niv of
            Original -> castOptic #original
            Inherited -> #inherited % _Just
       in molIDLensGen lay % #calculations % _Just % nivLens % #gmbe % _Just

    getTransGMBE :: Molecule -> MolID -> CalcK -> Maybe TB.Builder
    getTransGMBE m lay niv = do
      gmbeMap <- m ^? gmbeLens lay niv

      -- Transform the GMBE map. Maps now from AuxMonomer to (Linear Index, sumFactor).
      let gmbeFacList = Map.toList . fmap fst $ gmbeMap
          amPrintInfo = Map.fromList $ S.zipWith (\(am, fac) linIx -> (am, (linIx, fac))) gmbeFacList [0 ..]

      -- Serialise the aux monomer map into a table
      let amTable = printAMTable amPrintInfo

      return $ lnHeader lay niv <> amTable

    lnHeader lay niv =
      let nivTxt = case niv of
            Original -> "High Level"
            Inherited -> "Low Level"
          txt = "@ GMBE Information (layer " <> molID2OniomHumanID lay <> " " <> nivTxt <> ")"
          line = Text.replicate (Text.length txt) "-"
       in TB.fromText . Text.unlines $ [txt, line]

    printAMTable :: Map AuxMonomer (Int, Int) -> TB.Builder
    printAMTable amEnumMap = topRow <> midRule <> Map.foldMapWithKey printAMRow amEnumMap
      where
        nRowWidth = 10
        cRowWidth = 8
        oRowWidth = 8
        fRowWidth = 8
        rRowWidth = 65
        topRow =
          bformat
            ( (center nRowWidth ' ' %. builder) F.% " | "
                F.% (center cRowWidth ' ' %. builder)
                F.% " | "
                F.% (center oRowWidth ' ' %. builder)
                F.% " | "
                F.% (center fRowWidth ' ' %. builder)
                F.% " | "
                F.% (center rRowWidth ' ' %. builder)
                F.% "\n"
            )
            "AuxMono Nr"
            "Charge"
            "N(open)"
            "Sum Fac."
            "Constituting groups"
        midRule =
          TB.fromText $
            Text.replicate (nRowWidth + 1) "-"
              <> "+"
              <> Text.replicate (cRowWidth + 2) "-"
              <> "+"
              <> Text.replicate (oRowWidth + 2) "-"
              <> "+"
              <> Text.replicate (fRowWidth + 2) "-"
              <> "+"
              <> Text.replicate (rRowWidth + 1) "-"
              <> "\n"
        printAMRow :: AuxMonomer -> (Int, Int) -> TB.Builder
        printAMRow AuxMonomer {..} (nr, fac) = lInitContent <> rEnd (IntSet.toAscList constGroups)
          where
            lInitContent =
              bformat
                ( (left nRowWidth ' ' %. int) F.% " | "
                    F.% (left cRowWidth ' ' %. int)
                    F.% " | "
                    F.% (left oRowWidth ' ' %. int)
                    F.% " | "
                    F.% (left fRowWidth ' ' %. int)
                    F.% " | "
                )
                nr
                charge
                excEl
                fac
            rEnd :: [Int] -> TB.Builder
            rEnd cg = foldMap' (bformat (" " F.% (left 6 ' ' %. int) F.% " ")) cg <> "\n"

-- | Table header for optimisations.
optTableHeader ::
  -- | Makes the table header grepable, by adding @~@ to the end of the lines. Shoould be enabled
  -- for the first occurence of the table header in an optimisation, only. Afterwards set to false.
  Bool ->
  Utf8Builder
optTableHeader grepable =
  let header =
        bformat
          (fmt F.% " | " F.% fmt F.% " | " F.% fmt F.% " | " F.% fmt F.% " | " F.% fmt F.% " | " F.% fmt F.% lineEnd)
          "Step (Layer)"
          "ΔE"
          "MAX Force"
          "RMS Force"
          "MAX Displacement"
          "RMS Displacement"
      line = vSep <> hSep <> vSep <> hSep <> vSep <> hSep <> vSep <> hSep <> vSep <> hSep <> vSep <> bformat lineEnd
   in renderBuilder $ header <> line
  where
    fmt = center ew ' ' F.%. builder
    vSep = TB.fromText . Text.replicate ew $ "-"
    hSep = "-+-"
    lineEnd = if grepable then " ~\n" else "\n"

-- | Generate an entry for one optimisation step in the table.
optTableLine :: Int -> Maybe Int -> GeomConv -> Utf8Builder
optTableLine step layer GeomConv {..} =
  let col1StepLayer = case layer of
        Nothing -> bformat (left ew ' ' F.%. int) step
        Just l -> bformat slFmt step l
      col2Ediff = fromMaybe none $ bformat nf <$> eDiff
      col3MaxForce = fromMaybe none $ bformat nf <$> maxForce
      col4RMSForce = fromMaybe none $ bformat nf <$> rmsForce
      col5MaxDispl = fromMaybe none $ bformat nf <$> maxDisp
      col6RMSDispl = fromMaybe none $ bformat nf <$> rmsDisp
   in renderBuilder $
        bformat
          (builder F.% " | " F.% builder F.% " | " F.% builder F.% " | " F.% builder F.% " | " F.% builder F.% " | " F.% builder F.% " ~\n")
          col1StepLayer
          col2Ediff
          col3MaxForce
          col4RMSForce
          col5MaxDispl
          col6RMSDispl
  where
    stepFmt = left 10 ' ' F.%. int
    layerFmt = "(" F.% (left 2 ' ' F.%. int) F.% ")"
    stepLayerFmt = stepFmt F.% " " F.% layerFmt
    slFmt = left ew ' ' F.%. stepLayerFmt
    none = bformat (center ew ' ' F.%. builder) "-"

-- | Log string constructor monad for molecular information on full ONIOM trees.
spicyLogMol ::
  ( HasDirectMolecule env,
    HasPrintVerbosity env
  ) =>
  HashSet PrintEvent ->
  PrintTarget ->
  SpicyLog env
spicyLogMol pe pt = do
  pv <- view printVerbosityL

  -- Event printer.
  tell $ "\nnEvents: " <> (TB.fromText . tshow $ pe) <> "\n"

  -- Full ONIOM tree writers
  when (doLog pv #oniomE) $ printEnergy ONIOM
  when (doLog pv #oniomG) $ printGradient ONIOM
  when (doLog pv #oniomH) $ printHessian ONIOM
  when (doLog pv #oniomC) $ printCoords ONIOM
  when (doLog pv #oniomT) $ printTopology ONIOM
  when (doLog pv #oniomMP) $ printMultipoles ONIOM

  -- Layer writers
  case pt of
    ONIOM -> return ()
    Layer i -> do
      when (doLog pv #layerE) $ printEnergy (Layer i)
      when (doLog pv #layerG) $ printGradient (Layer i)
      when (doLog pv #layerH) $ printHessian (Layer i)
      when (doLog pv #layerGMBE) $ printGMBE (Layer i)
      when (doLog pv #layerC) $ printCoords (Layer i)
      when (doLog pv #layerT) $ printTopology (Layer i)
      when (doLog pv #layerMP) $ printMultipoles (Layer i)
    All -> do
      when (doLog pv #layerE) $ printEnergy All
      when (doLog pv #layerG) $ printGradient All
      when (doLog pv #layerH) $ printHessian All
      when (doLog pv #layerGMBE) $ printGMBE All
      when (doLog pv #layerC) $ printCoords All
      when (doLog pv #layerT) $ printTopology All
      when (doLog pv #layerMP) $ printMultipoles All

  tell . TB.fromText . textDisplay $ sep
  where
    doLog :: PrintVerbosity -> Lens' PrintVerbosity (HashSet PrintEvent) -> Bool
    doLog pv l =
      let pvt = pv ^. l
       in not . HashSet.null $ pvt `HashSet.intersection` pe

----------------------------------------------------------------------------------------------------

-- | Render a text builder as 'Utf8Builder'.
renderBuilder :: TB.Builder -> Utf8Builder
renderBuilder = display . TB.toLazyText

----------------------------------------------------------------------------------------------------

-- | Get a 'Map' of those results of a GMBE calculation, that are available.
getGMBEResults :: Map AuxMonomer (Maybe CalcOutput) -> AffineTraversal' CalcOutput a -> Map AuxMonomer a
getGMBEResults gmbeOutput optic =
  fromMaybe mempty $
    Map.traverseMaybeWithKey (\_ co -> Just $ co ^? _Just % optic) gmbeOutput
