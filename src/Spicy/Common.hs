-- |
-- Module      : Spicy.Common
-- Description : Common data types and functions
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Generic functions and types, that are not specific to the chemical context of Spicy.
module Spicy.Common
  ( -- * Abstract and Generic Classes
    -- $class
    Check (..),
    DefaultIO (..),

    -- * Operations on Data Structures
    -- $dataOperations
    parseYamlFile,
    parseJSONFile,

    -- ** Neighbour Lists
    -- $neighbourList
    NeighbourList,

    -- ** Paths
    -- $pathOperations
    JFilePath (..),
    JFilePathAbs (..),
    JFilePathRel (..),
    JDirPath (..),
    JDirPathAbs (..),
    JDirPathRel (..),
    path2Text,
    path2Utf8Builder,
    makeJFilePathAbsFromCwd,
    makeJDirPathAbsFromCwd,
    replaceProblematicChars,

    -- ** Sockets
    -- $socket
    unixSocket2Path,

    -- *** Fortran Formatting Functions
    -- $fortranFormatters
    fA,
    fI,
    fE,
    fX,
    fL,

    -- * Rotating Bound Channels
    -- $rotatingBoundChannels
    TBRQueue,
    newTBRQueue,
    newTBRQueueIO,
    readTBRQueue,
    tryReadTBRQueue,
    peekTBRQueue,
    tryPeekTBRQueue,
    writeTBRQueue,
    getAllTBRQueue,

    -- * Graphs
    -- $graphOperations
    enfilter,
    withinJumps,
    dffWithin,
    cyclesWithin,
    paths,

    -- * Convenience
    -- $convenienceOperations
    flipFoldl',
    combinationsN,
  )
where

import Data.Aeson
import Data.Attoparsec.Text
import Data.Foldable
import Data.Graph.Inductive
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding
  ( elem,
    mapM_,
  )
import Data.Maybe
import qualified Data.Text.Lazy.Builder as TB
import qualified Data.Text.Lazy.Builder.RealFloat as TB
import Data.Tree
import Data.Yaml hiding (Parser)
import Formatting hiding (char)
import Network.Socket as Net
import qualified RIO.List as List
import qualified RIO.Set as Set
import qualified RIO.Text as Text
import qualified RIO.Text.Lazy as TL
import Spicy.Attoparsec
import Spicy.Prelude hiding ((%))
import qualified System.Path as Path
import System.Path.PartClass

{-
####################################################################################################
-}

-- $classDefinitions

-- | A class for various data structures, which use some assumptions in Spicy. Running a check on them
-- allows to be sure about the correctnes of their assumptions.
class Check a where
  check :: MonadThrow m => a -> m a

----------------------------------------------------------------------------------------------------

-- | A class of default values, which need to be initialised by IO.
class DefaultIO a where
  defIO :: MonadIO m => m a

{-
====================================================================================================
-}

-- $dataOperations
-- Operations on generic data formats, which are commonly used in Spicy.

-- | Parse a YAML file in a RIO monad with logging and print the warnings.
parseYamlFile :: (FromJSON a, HasLogFunc env) => Path.AbsFile -> RIO env a
parseYamlFile yamlPath = do
  let logSource = "Parse YAML file"
  decodeCandidate <- liftIO . decodeFileWithWarnings . Path.toString $ yamlPath
  case decodeCandidate of
    Left exception -> do
      logErrorS logSource $
        "While parsing "
          <> path2Utf8Builder yamlPath
          <> " an error was encountered."
      throwM exception
    Right ([], dat) -> return dat
    Right (warnings, dat) -> do
      logWarnS logSource $
        "While parsing "
          <> path2Utf8Builder yamlPath
          <> " the following warnings were raised:"
      mapM_ (logWarnS logSource . text2Utf8Builder . tshow) warnings
      return dat

----------------------------------------------------------------------------------------------------

-- | Parse a YSON file in a RIO monad with logging and print the warnings.
parseJSONFile :: (FromJSON a, HasLogFunc env) => Path.AbsFile -> RIO env a
parseJSONFile jsonPath = do
  let logSource = "Parse YAML file"
  decodeCandidate <- liftIO . eitherDecodeFileStrict . Path.toString $ jsonPath
  case decodeCandidate of
    Left errorMsg -> do
      logErrorS logSource $
        "While parsing "
          <> path2Utf8Builder jsonPath
          <> " an error was encountered."
      throwM $ DataStructureException "eitherDecodeFileStrict" errorMsg
    Right dat -> return dat

{-
====================================================================================================
-}

-- $neighbourList

-- | A type alias for neighbourlists. Maps from an atom key to its neighbours within a certain
-- distance.
type NeighbourList = IntMap IntSet

{-
====================================================================================================
-}

-- $pathOperations
-- Newtype wrappers around typed paths with JSON serialisation enabled.

-- | Wraper for the pathtype 'AbsRelFile', which has JSON serialisation support.
newtype JFilePath = JFilePath {getFilePath :: Path.AbsRelFile}
  deriving (Show, Eq)

instance ToJSON JFilePath where
  toJSON (JFilePath path) = toJSON . Path.toString $ path

instance FromJSON JFilePath where
  parseJSON = withText "JFilePath" $ return . JFilePath . Path.file . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Wraper for the pathtype 'AbsFile', which has JSON serialisation support.
newtype JFilePathAbs = JFilePathAbs {getFilePathAbs :: Path.AbsFile}
  deriving (Generic, Show, Eq)

instance ToJSON JFilePathAbs where
  toJSON (JFilePathAbs path) = toJSON . Path.toString $ path

instance FromJSON JFilePathAbs where
  parseJSON = withText "JFilePathAbs" $ return . JFilePathAbs . Path.absFile . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Wraper for the pathtype 'AbsFile', which has JSON serialisation support.
newtype JFilePathRel = JFilePathRel {getFilePathRel :: Path.RelFile}
  deriving (Generic, Show, Eq)

instance ToJSON JFilePathRel where
  toJSON (JFilePathRel path) = toJSON . Path.toString $ path

instance FromJSON JFilePathRel where
  parseJSON = withText "JFilePathRel" $ return . JFilePathRel . Path.relFile . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Wraper for the pathtype 'AbsRelFile', which has JSON serialisation support.
newtype JDirPath = JDirPath {getDirPath :: Path.AbsRelDir}
  deriving (Generic, Show, Eq)

instance ToJSON JDirPath where
  toJSON (JDirPath path) = toJSON . Path.toString $ path

instance FromJSON JDirPath where
  parseJSON = withText "JDirPath" $ return . JDirPath . Path.dir . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Wraper for the pathtype 'AbsRelFile', which has JSON serialisation support.
newtype JDirPathAbs = JDirPathAbs {getDirPathAbs :: Path.AbsDir}
  deriving (Generic, Show, Eq)

instance ToJSON JDirPathAbs where
  toJSON (JDirPathAbs path) = toJSON . Path.toString $ path

instance FromJSON JDirPathAbs where
  parseJSON = withText "JDirPathAbs" $ return . JDirPathAbs . Path.absDir . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Wraper for the pathtype 'AbsRelFile', which has JSON serialisation support.
newtype JDirPathRel = JDirPathRel {getDirPathRel :: Path.RelDir}
  deriving (Generic, Show, Eq)

instance ToJSON JDirPathRel where
  toJSON (JDirPathRel path) = toJSON . Path.toString $ path

instance FromJSON JDirPathRel where
  parseJSON = withText "JDirPathRel" $ return . JDirPathRel . Path.relDir . Text.unpack

----------------------------------------------------------------------------------------------------

-- | Converts a pathtype 'Path.Path' to a textual representation as obtained with 'Path.toString' in
-- 'TL.Text' representation instead of 'String'.
path2Text :: (AbsRel ar, FileDir fd) => Path.Path ar fd -> Text
path2Text path' = Text.pack . Path.toString $ path'

----------------------------------------------------------------------------------------------------

-- | Convert a typed path to an 'Utf8Builder', which is often useful for printing log messages.
path2Utf8Builder :: (AbsRel ar, FileDir fd) => Path.Path ar fd -> Utf8Builder
path2Utf8Builder = text2Utf8Builder . path2Text

----------------------------------------------------------------------------------------------------

-- | Make a 'JFilePath' absolute.
makeJFilePathAbsFromCwd :: JFilePath -> IO JFilePathAbs
makeJFilePathAbsFromCwd jFilePath = do
  absolutePath <- Path.genericMakeAbsoluteFromCwd . getFilePath $ jFilePath
  return . JFilePathAbs $ absolutePath

----------------------------------------------------------------------------------------------------

-- | Make a 'JDirPath' absolute.
makeJDirPathAbsFromCwd :: JDirPath -> IO JDirPathAbs
makeJDirPathAbsFromCwd jDirPath = do
  absolutePath <- Path.genericMakeAbsoluteFromCwd . getDirPath $ jDirPath
  return . JDirPathAbs $ absolutePath

----------------------------------------------------------------------------------------------------

-- | Replaces problematic characters from a string before parsing it to a path, so that for example even
-- a string with slashes can be parsed as a file without directories prepended.
replaceProblematicChars :: String -> String
replaceProblematicChars path2Sanitise =
  fmap (\c -> if c `elem` Path.pathSeparators then '_' else c) path2Sanitise

{-
====================================================================================================
-}

-- $sockets

-- | Get the 'Path.Path' from a unix network socket.
unixSocket2Path :: MonadThrow m => SockAddr -> m Path.AbsRelFile
unixSocket2Path sckt =
  case sckt of
    SockAddrUnix path -> return . Path.file $ path
    SockAddrInet {} -> throwM . localExc $ "Wrong socket type given: INET"
    SockAddrInet6 {} -> throwM . localExc $ "Wrong socket type given: INET6"
  where
    localExc = SpicyIndirectionException "unixSocket2Path"

{-
====================================================================================================
-}

-- $fortranFormatters
-- Implementation for common formatter, as used in Fortran. Those are mainly used, when fixed width
-- file formats need to be written. Every formatter is prefixed with @f@ and then uses the Fortran
-- formatter name.

-- | Fortran A formatting.
fA :: Int -> Text -> TB.Builder
fA width a = bformat (right width ' ' %. fitRight width %. stext) a

-- | Fortran I formatting.
fI :: Integral a => Int -> a -> TB.Builder
fI width a = bformat (left width ' ' %. fitLeft width %. int) a

-- | Fortran X formatting.
fX :: Int -> TB.Builder
fX width = TB.fromText . Text.replicate width $ " "

-- | Fortran L formatting.
fL :: Int -> Bool -> TB.Builder
fL width True = bformat (left width ' ' %. builder) "T"
fL width False = bformat (left width ' ' %. builder) "F"

-- | Fortran E formatting.
fE :: RealFloat a => Int -> Int -> a -> TB.Builder
fE width precision a =
  let -- The exponent has a lower letter e and is unsigned. Must be changed.
      (coeff, expo) =
        fromMaybe (0, 0)
          . parse' doubleParser
          . TL.toStrict
          . TB.toLazyText
          . TB.formatRealFloat TB.Exponent (Just precision)
          $ a
      isPos = expo >= 0

      coeffFmt = fixed precision
      expoFmt = (if isPos then "+" else "-") % (left 2 '0' %. int)
   in bformat
        ((fitLeft width %. left width ' ') %. (coeffFmt % "E" % expoFmt))
        coeff
        (abs expo)
  where
    doubleParser :: Parser (Double, Int)
    doubleParser = do
      coeff <- takeTill (\c -> c == 'e' || c == 'E') >>= nextParse double
      _ <- char 'e' <|> char 'E'
      expo <- signed decimal
      return (coeff, expo)

{-
====================================================================================================
-}

-- $rotatingBoundChannels
-- Basically a ring buffer. Extends bound STM channels to discard the oldest value when a new one is
-- inserted. It is therefore a bound lossfull channel, that shifts values.

newtype TBRQueue a = TBRQueue {unwrapTBQueue :: TBQueue a}

----------------------------------------------------------------------------------------------------

-- | Builds and returns a new instance of 'TBRQueue'
newTBRQueue ::
  -- | Maximum number of elements the queue can hold.
  Natural ->
  STM (TBRQueue a)
newTBRQueue c = TBRQueue <$> newTBQueue c

----------------------------------------------------------------------------------------------------

-- | Lifted version of 'newTBRQueue'.
newTBRQueueIO ::
  MonadIO m =>
  -- | Maximum number of elements the queue can hold.
  Natural ->
  m (TBRQueue a)
newTBRQueueIO = atomically . newTBRQueue

----------------------------------------------------------------------------------------------------

-- | Read the next value from a 'TBRQueue'.
readTBRQueue :: TBRQueue a -> STM a
readTBRQueue = readTBQueue . unwrapTBQueue

----------------------------------------------------------------------------------------------------

-- | A version of 'readTBRQueue' which does not retry. Instead it returns 'Nothing' if no value is
-- available.
tryReadTBRQueue :: TBRQueue a -> STM (Maybe a)
tryReadTBRQueue = tryReadTBQueue . unwrapTBQueue

----------------------------------------------------------------------------------------------------

-- | Get the next value from the 'TBRQueue' without removing it, retrying if the channel is empty.
peekTBRQueue :: TBRQueue a -> STM a
peekTBRQueue = peekTBQueue . unwrapTBQueue

----------------------------------------------------------------------------------------------------

-- | A version of 'peekTBRQueue' which does not retry. Instead it returns 'Nothing' if no value is
-- available.
tryPeekTBRQueue :: TBRQueue a -> STM (Maybe a)
tryPeekTBRQueue = tryPeekTBQueue . unwrapTBQueue

----------------------------------------------------------------------------------------------------

-- | Write a value to a 'TBRQueue'; If the queue is full, it removes the oldest element and inserts
-- the next one. The element that was removed is returned as 'Just'. If the queue was not full, the
-- element is inserted and 'Nothing' is returned.
writeTBRQueue :: TBRQueue a -> a -> STM (Maybe a)
writeTBRQueue q e =
  isFullTBQueue qU >>= \isFull ->
    if isFull
      then do
        oldE <- readTBQueue qU
        writeTBQueue qU e
        return . Just $ oldE
      else writeTBQueue qU e $> Nothing
  where
    qU = unwrapTBQueue q

----------------------------------------------------------------------------------------------------

-- | Get all elements from the queue and therefore empty it. Blocks until the queue is empty.
getAllTBRQueue :: TBRQueue a -> STM [a]
getAllTBRQueue q = go q mempty
  where
    go :: TBRQueue a -> [a] -> STM [a]
    go q' xs = do
      x' <- tryPeekTBRQueue q'
      case x' of
        Nothing -> return xs
        Just x -> go q' (x : xs)

{-
####################################################################################################
-}

-- $graphOperations

-- | Filter for edges, based on the connected nodes and the edge label.
enfilter :: DynGraph gr => ((LNode a, LNode a, b) -> Bool) -> gr a b -> gr a b
enfilter fil gr =
  efilter
    ( \(nA, nB, e) ->
        let lnA = (,) nA <$> lab gr nA
            lnB = (,) nB <$> lab gr nB
            dat = (,,) <$> lnA <*> lnB <*> pure e
         in fromMaybe False $ fil <$> dat
    )
    gr

----------------------------------------------------------------------------------------------------

-- | Distance search within a graph. Obtains a list with nodes at distance \(i\) to the origin at
-- list index \(i\). Potentially contains cycles.
withinJumps :: Natural -> Gr a b -> Node -> [IntSet]
withinJumps dist gr n =
  let distTree = dffWithin dist gr n
      nodesAtDists = fmap IntSet.fromList . levels $ distTree
   in nodesAtDists

----------------------------------------------------------------------------------------------------

-- | Construct a depth-first tree of nodes reachable within a distance. Can walk directed or
-- undirected. For undirected walks, it will only use outgoing paths and not revisit nodes.
dffWithin :: Natural -> Gr a b -> Node -> Tree Node
dffWithin dist gr n = go 0 n n
  where
    go :: Natural -> Node -> Node -> Tree Node
    go currDist prevNode currNode =
      Node
        { rootLabel = currNode,
          subForest =
            let currNodeSuccessors = filter (/= prevNode) . suc gr $ currNode
             in if currDist >= dist
                  then mempty
                  else fmap (go (currDist + 1) currNode) currNodeSuccessors
        }

----------------------------------------------------------------------------------------------------

-- | Obtain cyclic paths, that start at a given node and reach it with at most \(n\) steps. Obtained
-- is a list of paths, that contain the steps in between the cycle. Eq the path @[1,2,3,4,5,6,1]@ is
-- cyclic and the path @[2,3,4,5,6]@ will be obtained.
cyclesWithin :: Natural -> Gr a b -> Node -> Set Path
cyclesWithin dist gr startNode =
  let allPaths = paths . dffWithin dist gr $ startNode
      cyclicPaths = filter (elem startNode) . fmap (List.drop 1) $ allPaths
      cyclePaths = Set.fromList . fmap (List.takeWhile (/= startNode)) $ cyclicPaths
   in cyclePaths

----------------------------------------------------------------------------------------------------

-- | All paths in a tree from the root to all of its terminal leaves.
paths :: Tree a -> [[a]]
paths (Node a []) = [[a]]
paths (Node a trs) = (a :) <$> concat (paths <$> trs)

{-
####################################################################################################
-}

-- | Strict left-fold with the folding function being the last argument.
flipFoldl' :: Foldable f => b -> f a -> (b -> a -> b) -> b
flipFoldl' start xs f = foldl' f start xs

----------------------------------------------------------------------------------------------------

-- | Form combinations of size \(n\) for a list of elements.
combinationsN :: Ord a => Natural -> [a] -> [[a]]
combinationsN n l = filter (\p -> length p == fromIntegral n) . go 1 . fmap pure $ l
  where
    go depth acc
      | depth >= n = acc
      | otherwise = go (depth + 1) $ (<:) <$> l <*> acc

    (<:) p [] = [p]
    (<:) p (x : xs) =
      if p < x
        then p : x : xs
        else x : xs
