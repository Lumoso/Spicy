{-# LANGUAGE RankNTypes #-}

-- |
-- Module      : Spicy.SelectionLanguage
-- Description : Types and definitions for a VMD-like selection language
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- To set up large ONIOM calculations and flexibly and conveniently define regions of interest, a
-- VMD inspired selection language can be used. It allows to select atoms based on connectivity,
-- distances, group informations, names, chemical information, etc.
module Spicy.SelectionLanguage
  ( SelectionProg,

    -- * Expressions
    all,
    index,
    element,
    name,
    numbonds,
    groupname,
    groupnum,
    within,
    groupsWithin,
    and,
    or,
    not,

    -- * Interpreter
    runSelection,

    -- * Parser
    parseSelection,
  )
where

import Control.Monad.Free
import Data.Attoparsec.Text
import qualified Data.IntMap.Strict as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding (all, and, index, or, takeWhile)
import RIO.Char
import qualified RIO.HashMap as HashMap
import qualified RIO.HashSet as HashSet
import RIO.Partial (succ)
import qualified RIO.Text as Text
import Spicy.Molecule.Internal.Util (arbDistMat, findAtomInGroup, getDenseSparseMapping)
import Spicy.Molecule.Molecule
import qualified Spicy.Molecule.Physical as P
import Spicy.Prelude hiding (all, and, not, or)
import qualified Spicy.Prelude as S
import qualified Text.Parser.Combinators as PC

-- | The abstract syntax tree for a selection program.
type SelectionProg = Free Selection IntSet

-- | A selection in the embedded language.
data Selection a where
  -- | Select everything. @all@ from VMD.
  All :: (IntSet -> a) -> Selection a
  -- | Selection of an atom by its integer key. @index@ from VMD.
  Index :: [Int] -> (IntSet -> a) -> Selection a
  -- | Select atoms by chemical element. @element@ from VMD.
  Element :: [P.Element] -> (IntSet -> a) -> Selection a
  -- | Select atoms by their name. @name@ from VMD.
  Name :: [Text] -> (IntSet -> a) -> Selection a
  -- | Number of bonds of an atoms. @numbonds@ from VMD.
  NumBonds :: Natural -> (IntSet -> a) -> Selection a
  -- | Selection of a group by its name. Roughly @resname@ from VMD.
  Groupname :: [Text] -> (IntSet -> a) -> Selection a
  -- | Selection of a group by its integer key. Rougly @residue@ from VMD.
  Groupnum :: [Int] -> (IntSet -> a) -> Selection a
  -- | Real-space distance selection. Behaves as the VMD version and will ignorantly cut through
  -- bonds, separate groups and possibly wreak havock to your chemical system. @within@ from VMD.
  Within :: IntSet -> Double -> IntSet -> (IntSet -> a) -> Selection a
  -- | Similar to 'Within' but will respect group information. As soon as one atom of a group
  -- was selected, the selection will be expanded to the full group.
  GroupsWithin :: IntSet -> Double -> IntSet -> (IntSet -> a) -> Selection a
  -- | Intersection of two selections.
  And :: IntSet -> IntSet -> (IntSet -> a) -> Selection a
  -- | Union of two selections.
  Or :: IntSet -> IntSet -> (IntSet -> a) -> Selection a
  -- | Difference of two selections.
  Not :: IntSet -> IntSet -> (IntSet -> a) -> Selection a
  deriving (Functor)

----------------------------------------------------------------------------------------------------

-- | Select everything. @all@ from VMD.
all :: MonadFree Selection m => m IntSet
all = liftF $ All id

-- | Selection of an atom by its integer key. @index@ from VMD.
index :: MonadFree Selection m => [Int] -> m IntSet
index i = liftF $ Index i id

-- | Select atoms by chemical element. @element@ from VMD.
element :: MonadFree Selection m => [P.Element] -> m IntSet
element e = liftF $ Element e id

-- | Select atoms by their name. @name@ from VMD.
name :: MonadFree Selection m => [Text] -> m IntSet
name n = liftF $ Name n id

-- | Number of bonds of an atoms. @numbonds@ from VMD.
numbonds :: MonadFree Selection m => Natural -> m IntSet
numbonds b = liftF $ NumBonds b id

-- | Selection of a group by its name. Roughly @resname@ from VMD.
groupname :: MonadFree Selection m => [Text] -> m IntSet
groupname n = liftF $ Groupname n id

-- | Selection of a group by its integer key. Rougly @residue@ from VMD.
groupnum :: MonadFree Selection m => [Int] -> m IntSet
groupnum i = liftF $ Groupnum i id

-- | Real-space distance selection. Behaves as the VMD version and will ignorantly cut through
-- bonds, separate groups and possibly wreak havock to your chemical system. @within@ from VMD.
within :: MonadFree Selection m => IntSet -> Double -> IntSet -> m IntSet
within a d b = liftF $ Within a d b id

-- | Similar to 'within' but will respect group information. As soon as one atom of a group was
-- selected, the selection will be expanded to the full group.
groupsWithin :: MonadFree Selection m => IntSet -> Double -> IntSet -> m IntSet
groupsWithin a d b = liftF $ GroupsWithin a d b id

-- | Intersection of two selections.
and :: MonadFree Selection m => IntSet -> IntSet -> m IntSet
and a b = liftF $ And a b id

-- | Union of two selections.
or :: MonadFree Selection m => IntSet -> IntSet -> m IntSet
or a b = liftF $ Or a b id

-- | Difference of two selections.
not :: MonadFree Selection m => IntSet -> IntSet -> m IntSet
not a b = liftF $ Not a b id

----------------------------------------------------------------------------------------------------

-- | Interprete the selection monad in a reader monad, that has access to the molecule.
selectionR :: (HasDirectMolecule env, MonadReader env m) => Selection a -> m a
selectionR expr = case expr of
  All next -> next . IntMap.keysSet <$> getAtoms
  Index i next ->
    let ixSel = IntSet.fromList i
     in next . IntMap.keysSet . flip IntMap.restrictKeys ixSel <$> getAtoms
  Element e next ->
    let elSel = HashSet.fromList e
     in next . IntMap.keysSet . IntMap.filter (\a -> (a ^. #element) `HashSet.member` elSel) <$> getAtoms
  Name n next ->
    let nameSel = HashSet.fromList n
     in next . IntMap.keysSet . IntMap.filter (\a -> (a ^. #label) `HashSet.member` nameSel) <$> getAtoms
  NumBonds n next -> next . IntMap.keysSet . IntMap.filter (>= n) . countBonds <$> getBonds
  Groupname n next -> do
    let nameSel = HashSet.fromList n
    groups <- getGroups
    let matchingGroups = IntMap.filter (\Group {label} -> label `HashSet.member` nameSel) groups
        atomSel = atomsFromGroups matchingGroups
    return . next $ atomSel
  Groupnum n next -> do
    groups <- getGroups
    let ixSel = IntSet.fromList n
        matchingGroups = IntMap.restrictKeys groups ixSel
        atomSel = atomsFromGroups matchingGroups
    return . next $ atomSel
  Within a d b next -> next <$> commonWithin a d b
  GroupsWithin a d b next -> do
    rawSel <- IntSet.toAscList <$> commonWithin a d b
    groups <- getGroups
    let matchedGroupKeys =
          fromMaybe mempty
            . fmap IntSet.fromList
            . traverse (`findAtomInGroup` groups)
            $ rawSel
        sel = atomsFromGroups . IntMap.restrictKeys groups $ matchedGroupKeys
    return . next $ sel
  And a b next -> return . next $ IntSet.intersection a b
  Or a b next -> return . next $ IntSet.union a b
  Not a b next -> return . next $ a IntSet.\\ b
  where
    getAtoms = view $ moleculeDirectL % #atoms
    getBonds = view $ moleculeDirectL % #bonds
    getGroups = view $ moleculeDirectL % #group

    -- Get the atom selection, that is obtained by a selection of groups.
    atomsFromGroups :: (Foldable f, Functor f) => f Group -> IntSet
    atomsFromGroups group = foldl' (<>) mempty . fmap (^. #atoms) $ group

    -- Count the number of bonds each atom has. Relies on a bidirectorial bond matrix.
    countBonds :: P.BondMatrix -> IntMap Natural
    countBonds bm =
      HashMap.foldlWithKey'
        ( \acc (o, _) bo ->
            if P.isBond bo
              then IntMap.alter (pure . fromMaybe 1 . fmap succ) o acc
              else acc
        )
        mempty
        bm

    -- Select atoms in sparse indexing from a distance matrix, that are closer than the distance
    -- criterion. Uses the column indices.
    atomsFromDistMat ::
      (Source r Double) =>
      IntSet ->
      Double ->
      Matrix r Double ->
      IntSet
    atomsFromDistMat allAtomIndices maxDist distMat = do
      let dense2Sparse = getDenseSparseMapping @U allAtomIndices
      flip ifoldInnerSlice distMat $ \i dists -> case minimumM dists of
        Nothing -> mempty
        Just dist ->
          if dist <= maxDist
            then IntSet.singleton (dense2Sparse Massiv.! i)
            else mempty

    -- Common within interpreter. Works as @within@ from VMD.
    commonWithin a d b = do
      atoms <- getAtoms
      let atomsA = IntMap.restrictKeys atoms a
          atomsB = IntMap.restrictKeys atoms b
          distMat = compute @S <$> arbDistMat atomsA atomsB
          sel = atomsFromDistMat (IntMap.keysSet atoms) d <$> distMat
      return . fromMaybe mempty $ sel

----------------------------------------------------------------------------------------------------

-- | Evaluate a selection language program on a given molecule.
runSelection :: Molecule -> SelectionProg -> IntSet
runSelection mol prog = runReader (foldFree selectionR prog) mol

----------------------------------------------------------------------------------------------------

-- | Parser for the selection language. It tries to stay close to what VMD does. Examples:
--
-- >>> all
-- >>> (index 11 18 374) and (groupname HEM)
-- >>> (element C H N O P S) not ((element C) and (numbonds 2))
-- >>> (groupname GLY HIS) groupswithin 5 of (element P)
parseSelection :: Parser SelectionProg
parseSelection = PC.chainl1 parseTerm parseOperator

parseTerm :: Parser SelectionProg
parseTerm =
  choice
    [ parseAll,
      parseIndex,
      parseElement,
      parseName,
      parseNumbonds,
      parseGroupname,
      parseGroupnum,
      parseParentheses
    ]

parseOperator :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseOperator =
  choice
    [ parseWithin,
      parseGroupsWithin,
      parseAnd,
      parseOr,
      parseNot
    ]

parseAll :: Parser (Free Selection IntSet)
parseAll = skipSpace *> string "all" $> all <* skipSpace

parseIndex :: Parser (Free Selection IntSet)
parseIndex = do
  void $ skipSpace *> string "index" <* skipSpace
  inds <- many1 $ skipSpace *> signed decimal
  skipSpace
  return $ index inds

parseElement :: Parser (Free Selection IntSet)
parseElement = do
  void $ skipSpace *> string "element" <* skipSpace
  els' <- many1 $ skipSpace *> many1 (satisfy (S.not . isSpace)) <* skipSpace
  case traverse readMaybe els' of
    Nothing -> fail "Cannot parse elements"
    Just e -> return . element $ e

parseName :: Parser (Free Selection IntSet)
parseName = do
  void $ skipSpace *> string "name" <* skipSpace
  names <- many1 $ skipSpace *> parseName' <* skipSpace
  return . name $ names
  where
    parseName' = choice [parseQuoted, parseSingleQuoted, parseOneName]
    parseQuoted = Text.pack <$> ("\"" *> many1 (notChar '\"') <* "\"")
    parseSingleQuoted = Text.pack <$> ("\'" *> many1 (notChar '\'') <* "\'")
    parseOneName = Text.pack <$> many1 (satisfy (\c -> c /= ')' && S.not (isSpace c)))

parseNumbonds :: Parser (Free Selection IntSet)
parseNumbonds = do
  void $ skipSpace *> string "numbonds" <* skipSpace
  nBonds <- decimal @Natural <* skipSpace
  return . numbonds $ nBonds

parseGroupname :: Parser (Free Selection IntSet)
parseGroupname = do
  void $ skipSpace *> string "groupname" <* skipSpace
  names <- many1 $ skipSpace *> many1 (satisfy (S.not . isSpace)) <* skipSpace
  return . groupname . fmap Text.pack $ names

parseGroupnum :: Parser (Free Selection IntSet)
parseGroupnum = do
  void $ skipSpace *> string "groupnum" <* skipSpace
  nums <- many1 $ skipSpace *> signed decimal <* skipSpace
  return . groupnum $ nums

parseWithin :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseWithin = do
  void $ skipSpace *> string "within" <* skipSpace
  dist <- double
  void $ skipSpace *> string "of" <* skipSpace
  return $ \left right -> do
    a <- left
    b <- right
    within a dist b

parseGroupsWithin :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseGroupsWithin = do
  void $ skipSpace *> string "groupswithin" <* skipSpace
  dist <- double
  void $ skipSpace *> string "of" <* skipSpace
  return $ \left right -> do
    a <- left
    b <- right
    groupsWithin a dist b

parseAnd :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseAnd = do
  void $ skipSpace *> string "and" <* skipSpace
  return $ \left right -> do
    a <- left
    b <- right
    a `and` b

parseOr :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseOr = do
  void $ skipSpace *> string "or" <* skipSpace
  return $ \left right -> do
    a <- left
    b <- right
    a `or` b

parseNot :: Parser (SelectionProg -> SelectionProg -> SelectionProg)
parseNot = do
  void $ skipSpace *> string "not" <* skipSpace
  return $ \left right -> do
    a <- left
    b <- right
    a `not` b

parseParentheses :: Parser (Free Selection IntSet)
parseParentheses = do
  void $ skipSpace *> char '(' <* skipSpace
  expr <- parseSelection
  void $ skipSpace *> char ')' <* skipSpace
  return expr
