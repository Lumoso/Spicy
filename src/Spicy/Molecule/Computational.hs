-- |
-- Module      : Spicy.Molecule.Computational
-- Description : Computational details of calculations
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This molecule provides types and functions to express the details of a calculation.
module Spicy.Molecule.Computational (module Spicy.Molecule.Internal.Computational) where

import Spicy.Molecule.Internal.Computational
