-- |
-- Module      : Spicy.Molecule.Internal.Physical
-- Description : Types with physical or chemical meaning
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This molecule provides types that carry some physical or chemical meaning,
-- such as 'Element', 'Atom', or 'BondOrder'.
module Spicy.Molecule.Physical (module Spicy.Molecule.Internal.Physical) where

import Spicy.Molecule.Internal.Physical
