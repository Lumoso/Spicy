-- |
-- Module      : Spicy.Molecule.Fragment
-- Description : Types and functions related to fragment methods
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Support for Spicy's fragment-based methods.
module Spicy.Molecule.Fragment (module Spicy.Molecule.Internal.Fragment) where

import Spicy.Molecule.Internal.Fragment
