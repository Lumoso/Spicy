-- |
-- Module      : Spicy.Molecule.Molecule
-- Description : Spicy's core Molecule type
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides the 'Molecule' data type, which includes nearly all important information
-- about the calculation, including its ONIOM setup, context, input, and output of calculations.
--
-- Molecules are defined with a lot of context and 'Molecule' is the single most important type
-- in Spicy. The layout of the 'Molecule' data type keeps layered calculations, complex input
-- formats and groupation of molecules in mind and tries to apply to them all.
--
-- Regarding the hierarhy of a 'Molecule' in the context of ONIOM, the following conditions apply:
--
-- - The top layer of the recursion is the "real" system in ONIOM terminology.
-- - Stepping down the recursion more "model"-like ONIOM layers are reached.
module Spicy.Molecule.Molecule (module Spicy.Molecule.Internal.Molecule) where

import Spicy.Molecule.Internal.Molecule
