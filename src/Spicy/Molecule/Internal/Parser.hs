-- |
-- Module      : Spicy.Molecule.Internal.Parser
-- Description : Parsers for chemical data formats and computational chemistry output fileSeq.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides parsers for chemical file formats.
module Spicy.Molecule.Internal.Parser
  ( -- * Chemical Data Formats
    xyz,
    txyz,
    mol2,
    pdb,
  )
where

import Control.Applicative
import Data.Attoparsec.Text
import Data.Char
import Data.Default
import Data.Foldable
import qualified Data.IntMap.Strict as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding
  ( index,
    map,
    swap,
    take,
    takeWhile,
    zip,
  )
import Data.Maybe
import qualified RIO.HashMap as HashMap
import qualified RIO.List as List
import qualified RIO.Text as Text
import Spicy.Molecule.Internal.Types
import Spicy.Molecule.Internal.Physical
import Spicy.Molecule.Internal.Molecule
import Spicy.Molecule.Internal.Util
import Spicy.Attoparsec
import Spicy.Prelude hiding (take)

-- | Parse a .xyz file (has no connectivity, atom types or partial charges). Atom counting starts
-- at 1 for XYZ files, which makes it more convenient to use with most visualisation software.
xyz :: Parser Molecule
xyz = do
  nAtoms <- skipHorizontalSpace *> decimal <* skipHorizontalSpace <* endOfLine
  label' <- skipHorizontalSpace *> takeWhile (not . isEndOfLine) <* endOfLine
  atoms' <- count nAtoms xyzLineParser
  let atoms = IntMap.fromList $ zip [1 ..] atoms'
      group =
        IntMap.singleton
          0
          Group
            { label = "all",
              chain = Nothing,
              atoms = IntMap.keysSet atoms,
              charge = 0,
              excEl = 0
            }
  return
    Molecule
      { comment = label',
        atoms = atoms,
        bonds = HashMap.empty,
        subMol = IntMap.empty,
        group = group,
        energyDerivatives = def,
        calculations = Nothing,
        jacobian = Nothing,
        neighbourlist = mempty
      }
  where
    xyzLineParser :: Parser Atom
    xyzLineParser = do
      cElement <- skipHorizontalSpace *> many1 letter
      x <- skipHorizontalSpace *> double
      y <- skipHorizontalSpace *> double
      z <- skipHorizontalSpace *> double
      skipHorizontalSpace
      endOfLine
      return
        Atom
          { element = fromMaybe H . readMaybe $ cElement,
            label = "",
            isLink = NotLink,
            isDummy = False,
            ffType = FFXYZ,
            multipoles = def,
            coordinates = VectorS . Massiv.fromList Seq $ [x, y, z],
            formalCharge = 0,
            formalExcEl = 0
          }

----------------------------------------------------------------------------------------------------

-- | Parse a Tinker XYZ formatted file. It has coordinates and might have connectivity and atom
-- types. This format and therefore parser are not using any submolecules or groups. The atom
-- counting is directly taken from the file.
txyz :: Parser Molecule
txyz = do
  nAtoms <- skipHorizontalSpace *> (decimal :: Parser Int)
  label' <- skipHorizontalSpace *> takeWhile (not . isEndOfLine) <* skipSpace
  conAndAtoms <- count nAtoms txyzLineParser
  let atoms' = IntMap.fromAscList . fmap (\info -> (txyzIndex info, txyzAtom info)) $ conAndAtoms
      bonds' =
        HashMap.fromList
          . concatMap
            ( \info ->
                let originReplicated = List.repeat . txyzIndex $ info
                    bondedAtomPairs = List.zip originReplicated $ txyzBondTargets info
                    bondsWithSameOrigin = List.zip bondedAtomPairs (List.repeat Single)
                 in bondsWithSameOrigin
            )
          $ conAndAtoms
      group =
        Group
          { label = "all",
            chain = Nothing,
            atoms = IntMap.keysSet atoms',
            charge = 0,
            excEl = 0
          }
  return
    Molecule
      { comment = label',
        atoms = atoms',
        bonds = bonds',
        subMol = IntMap.empty,
        group = IntMap.singleton 0 group,
        energyDerivatives = def,
        calculations = Nothing,
        jacobian = Nothing,
        neighbourlist = mempty
      }
  where
    -- Parsing a single line of atomSeq. Tinker's format keeps bonds associated with atomSeq. So a tuple
    -- suitable to construct the 'IntMap' is returned additional to the pure atomSeq.
    txyzLineParser :: Parser TXYZAtomInfo
    txyzLineParser = do
      index <- skipHorizontalSpace *> decimal
      cElement <- skipHorizontalSpace *> many1 letter
      x <- skipHorizontalSpace *> double
      y <- skipHorizontalSpace *> double
      z <- skipHorizontalSpace *> double
      mFFType <- skipHorizontalSpace *> optional (decimal :: Parser Int)
      connectivityRaw <- many' (skipHorizontalSpace *> decimal <* skipHorizontalSpace)
      endOfLine
      let atom =
            Atom
              { element = fromMaybe H . readMaybe $ cElement,
                label = "",
                isLink = NotLink,
                isDummy = True,
                ffType = case mFFType of
                  Nothing -> FFTXYZ 0
                  Just a -> FFTXYZ a,
                coordinates = VectorS . Massiv.fromList Seq $ [x, y, z],
                multipoles = def,
                formalCharge = 0,
                formalExcEl = 0
              }
      return
        TXYZAtomInfo
          { txyzIndex = index,
            txyzAtom = atom,
            txyzBondTargets = connectivityRaw
          }

----------------------------------------------------------------------------------------------------

-- | Parse the "interesting" fields of a MOL2 file. This contains partial charges as well as
-- connectivity. There is no special understanding for the atom types, that are available in MOL2
-- files. They will simply be treated as the force field string. See
-- <http://chemyang.ccnu.edu.cn/ccb/server/AIMMS/mol2.pdf>.
mol2 :: Parser Molecule
mol2 = do
  (label', nAtoms, nBonds) <- moleculeParser
  atomAndGroupInfo <- atomParser nAtoms
  bonds' <- bondParser nBonds
  let -- Construct the atoms and groups of the top layer from the atom block.
      (atoms', groups) = groupAtomInfo2AtomsAndGroups atomAndGroupInfo
  return
    Molecule
      { comment = label',
        atoms = atoms',
        bonds = bonds',
        subMol = IntMap.empty,
        group = groups,
        energyDerivatives = def,
        calculations = Nothing,
        jacobian = Nothing,
        neighbourlist = mempty
      }
  where
    -- Parse the @<TRIPOS>MOLECULE block of MOL2.
    moleculeParser :: Parser (Text, Int, Maybe Int)
    moleculeParser = do
      _header <- manyTill anyChar (string "@<TRIPOS>MOLECULE") <* endOfLine
      -- Line 1 -> "mol_name"
      label' <- takeWhile (not . isEndOfLine) <* endOfLine
      -- Line 2 -> "num_atoms [num_bonds [num_subst [num_feat [num_sets]]]]"
      nAtoms <- skipHorizontalSpace *> decimal
      nBonds <- optional $ skipHorizontalSpace *> decimal
      _nSubMols <- optional $ skipHorizontalSpace *> (decimal :: Parser Int)
      _nFeatures <- optional $ skipHorizontalSpace *> (decimal :: Parser Int)
      _nSets <-
        optional $
          skipHorizontalSpace
            *> (decimal :: Parser Int)
            <* skipHorizontalSpace
            <* endOfLine
      -- Line 3 -> "mol_type"
      _molType <-
        skipHorizontalSpace
          *> string "SMALL"
          <|> string "BIOPOLYMER"
          <|> string "PROTEIN"
          <|> string "NUCLEIC_ACID"
          <|> string "SACCHARIDE"
          <* skipSpace
      -- Line 4 -> "charge_type"
      _chargeType <-
        skipSpace
          *> ( string "NO_CHARGES"
                 <|> string "DEL_RE"
                 <|> string "GASTEIGER"
                 <|> string "GAST_HUCK"
                 <|> string "HUCKEL"
                 <|> string "PULLMAN"
                 <|> string "GAUSS80_CHARGES"
                 <|> string "AMPAC_CHARGES"
                 <|> string "MULLIKEN_CHARGES"
                 <|> string "DICT_CHARGES"
                 <|> string "MMFF94_CHARGES"
                 <|> string "USER_CHARGES"
             )
          <* skipHorizontalSpace
          <* endOfLine
      -- Line 5 -> "[status_bits"
      _statusBit <- optional $ skipHorizontalSpace *> many1 letter <* skipSpace
      -- Line 6 -> "[mol_comment]]"
      _comment <- optional $ skipHorizontalSpace *> takeWhile (not . isEndOfLine) <* skipSpace
      return (label', nAtoms, nBonds)
    -- Parse the @<TRIPOS>ATOM block of MOL2.
    atomParser :: Int -> Parser [GroupAtomInfo]
    atomParser nAtoms = do
      _header <- manyTill anyChar (string "@<TRIPOS>ATOM") <* endOfLine
      -- Parse multiple lines of ATOM data.
      atomAndGroupInfo <- count nAtoms $ do
        index <- skipHorizontalSpace *> decimal
        -- Most often this will be the element symbol.
        label' <- skipHorizontalSpace *> takeWhile (not . isHorizontalSpace)
        -- x, y and z coordinates
        x <- skipHorizontalSpace *> double
        y <- skipHorizontalSpace *> double
        z <- skipHorizontalSpace *> double
        -- Parse the chemical element, which is actually the first part of the SYBYL atom type.
        cElem <- skipHorizontalSpace *> many1 letter
        -- A dot often separates the element from the type of this element.
        ffdot <- optional $ char '.'
        -- And after the dot the rest of the SYBYL atom type might come.
        ffType' <- optional $ takeWhile (not . isHorizontalSpace)
        -- The substructure ID. This is the identifier to identify sub moleculeSeq.
        subID <- skipHorizontalSpace *> (decimal :: Parser Int)
        -- The substructure Name. This should be used as label for the sub molecule.
        subName <- skipHorizontalSpace *> takeWhile (not . isHorizontalSpace)
        -- The partial charge. Parsed as an integer formal charge
        pCharge <- skipHorizontalSpace *> double <* skipSpace
        -- Construct the force field string for the 'MOL2' field.
        let mol2FFText =
              Text.pack cElem
                <> ( case ffdot of
                       Nothing -> ""
                       Just _ -> "."
                   )
                <> fromMaybe "" ffType'
            atom =
              Atom
                { element = fromMaybe H . readMaybe $ cElem,
                  label = label',
                  isLink = NotLink,
                  isDummy = False,
                  ffType = FFMol2 mol2FFText,
                  coordinates = VectorS . Massiv.fromList Seq $ [x, y, z],
                  multipoles = def,
                  formalCharge = round pCharge,
                  formalExcEl = 0
                }
            thisAtomsGroup =
              Group
                { label = subName,
                  chain = Nothing,
                  atoms = IntSet.singleton index,
                  charge = atom ^. #formalCharge,
                  excEl = atom ^. #formalExcEl
                }
        return
          GroupAtomInfo
            { faiAtomIndex = index,
              faiGroupIndex = subID,
              faiAtom = atom,
              faiGroup = thisAtomsGroup
            }
      return atomAndGroupInfo
    --
    -- Parse the @<TRIPOS>BOND part. Unfortunately, the bonds in the MOL2 format are unidirectiorial
    -- and need to be flipped to.
    bondParser :: Maybe Int -> Parser BondMatrix
    bondParser nBonds = do
      let -- How often to parse bond fields depends on if the number of bonds has been specified.
          nParser = case nBonds of
            Nothing -> many'
            Just n -> count n
      _header <- manyTill anyChar (string "@<TRIPOS>BOND") <* endOfLine
      uniBonds <- nParser $ do
        -- Bond id, which does not matter.
        _id <- skipHorizontalSpace *> (decimal :: Parser Int)
        -- Origin atom index
        origin <- skipHorizontalSpace *> (decimal :: Parser Int)
        -- Target atom index
        target <- skipHorizontalSpace *> (decimal :: Parser Int)
        -- Bond type, which we don't care about.
        bondType <- skipHorizontalSpace *> takeWhile (not . isSpace) <* skipSpace
        let bondOrder = case bondType of
              "1" -> Single
              "2" -> Multiple
              "3" -> Multiple
              "am" -> Multiple
              "ar" -> Multiple
              "du" -> None
              "un" -> Multiple
              "nc" -> None
              _ -> Single
        return ((origin, target), bondOrder)
      let -- Make the bonds bidirectorial
          bondTuplesForth = uniBonds
          bondTuplesBack = (\((o, t), bo) -> ((t, o), bo)) <$> bondTuplesForth
          bondTuplesBoth = bondTuplesForth <> bondTuplesBack
          bonds' = HashMap.fromList bondTuplesBoth
      return bonds'

----------------------------------------------------------------------------------------------------

-- | Parse a PDB file as described in
-- <ftp://ftp.wwpdb.org/pub/pdb/doc/format_descriptions/Format_v33_A4.pdf>. If parsing of a single
-- @ATOM@ or @CONECT@ line fails, the parser will stop there and ignore all the other records of
-- same type, directly coming after the failed one.
--
-- The PDB parser uses the columns 61-66 (temperature factor) to parse 'formalExcEl' for atoms and
-- columns 79-80 to parse 'formalCharge' of an atom.
--
-- __This is not an entirely valid PDB parser. You will run into problems for very large structures,
-- where atom indices exist multiple times.__
pdb :: Parser Molecule
pdb = do
  -- Parse the HEADER field as a label. Only the first line of COMPND will be used.
  label' <- optional $ do
    _ <- manyTill anyChar (string "HEADER")
    compoundLabel <- skipHorizontalSpace *> (Text.strip <$> takeWhile (not . isEndOfLine))
    return compoundLabel
  -- Parse atoms only and ignore other fiels
  atomsAndGroupInfo <- many1 atomParser
  -- Parse the bonds to the tuple structure.
  bondTuples <- many' connectParser
  let -- Transform the information from the parsers.
      bonds' = HashMap.fromList $ zip (List.concat bondTuples) (List.repeat Single)
      (atoms', groups) = groupAtomInfo2AtomsAndGroups atomsAndGroupInfo
  return
    Molecule
      { comment = fromMaybe "" label',
        atoms = atoms',
        bonds = bonds',
        subMol = mempty,
        group = groups,
        energyDerivatives = def,
        calculations = Nothing,
        jacobian = Nothing,
        neighbourlist = mempty
      }
  where
    -- Parser fot HETATM and ATOM records.
    atomParser :: Parser GroupAtomInfo
    atomParser = do
      -- First check, that this line really starts with an ATOM or HETATM record (6 characters).
      -- Columns 1-6: record type.
      _recordStart <- Text.pack <$> manyTill anyChar (string "\nATOM  " <|> string "\nHETATM")
      -- Columns 7-11: serial key/number of the atom.
      atomKey <- take 5 >>= nextParse decimal . Text.strip
      -- Column 12: space
      _ <- char ' '
      -- Column 13-16: atom label
      atomLabel <- Text.strip <$> take 4
      -- Column 17: alternate location index. Usually not set and not of interest to Spicy.
      _altLoc <- anyChar
      -- Column 18-20: group name.
      groupLabel <- Text.strip <$> take 3
      -- Column 21: space
      _ <- char ' '
      -- Column 22: chain identifier
      chainID <- letter
      -- Column 23-26: residue sequence ID. This is not unique for in a whole PDB but only unique
      -- within a chain. A group ID therefore needs to be constructed from both together. Also the
      -- writer needs to take care of this conversion.
      groupInChain <- take 4 >>= nextParse decimal . Text.strip
      -- Column 27: "Code for insertion of residues". Does not matter for Spicy.
      _ <- anyChar
      -- Column 29-30: space
      _ <- count 3 $ char ' '
      -- Column 31-38: x coordinates in angstrom.
      xCoord <- take 8 >>= nextParse double . Text.strip
      -- Column 39-46: y coordinates in angstrom.
      yCoord <- take 8 >>= nextParse double . Text.strip
      -- Column 47-54: z coordinates in angstom.
      zCoord <- take 8 >>= nextParse double . Text.strip
      -- Column 55-60: occupancy. Use this as the formal charge of an atom.
      _occupancy <- take 6 >>= nextParse double . Text.strip
      -- Column 61-66: temperature factor. Use this as the number of excess electrons. Respects sign
      formalExcEl <- take 6 >>= nextParse double . Text.strip <&> round
      -- Column 67-76: space:
      _ <- count 10 $ char ' '
      -- Column 77-78: Chemical element in all upper case.
      element' <-
        maybeToParserMonad
          . readMaybe @Element
          . Text.unpack
          . Text.toTitle
          . Text.strip
          =<< take 2
      -- Column 79-80: Charge on the atom as string. Its strange. Can be read as a signed decimal to
      -- get a formal charge of the atom, that will be also used in Spicy.
      charge <- take 2 >>= nextParse (optional $ skipSpace *> signed decimal)
      -- Do not parse an EOL character. This happens at the start of a record.

      let atom =
            Atom
              { element = element',
                label = atomLabel,
                isLink = NotLink,
                isDummy = False,
                ffType = FFPDB atomLabel,
                coordinates = VectorS . Massiv.fromList Massiv.Seq $ [xCoord, yCoord, zCoord],
                multipoles = def,
                formalCharge = fromMaybe 0 charge,
                formalExcEl = formalExcEl
              }
          group' =
            Group
              { label = groupLabel,
                chain = Just chainID,
                atoms = IntSet.singleton atomKey,
                charge = atom ^. #formalCharge,
                excEl = atom ^. #formalExcEl
              }
          -- The residue id is only unique per chain. To become completely unique, enumerate chain
          -- IDs, multiply with the maximum number of residues per chain possible (10000) and add the
          -- local chain ID. When writing the PDB again, this conversion has to be reversed.
          uniqueGroupKey =
            let numA = fromEnum 'A'
                numID = fromEnum chainID
                maxResiduesPerChain = 10000
             in (numID - numA) * maxResiduesPerChain + groupInChain
      return
        GroupAtomInfo
          { faiAtomIndex = atomKey,
            faiGroupIndex = uniqueGroupKey,
            faiAtom = atom,
            faiGroup = group'
          }

    -- Parse CONECT fields of the PDB. PDB bonds are bidirectorial, so no swapping required.
    connectParser :: Parser [(Int, Int)]
    connectParser = do
      -- First check, that this line really starts with a CONECT record (6 characters).
      -- Columns 1-6: record type.
      _recordStart <- Text.pack <$> manyTill anyChar (string "\nCONECT")
      -- Column 7-11: Bond origin
      bondOrigin <- take 5 >>= nextParse decimal
      -- Column 12-16, 17-21, 22-26 and 27-31: Bond targets
      bondTargets <- many1 $ take 5 >>= nextParse decimal . Text.strip
      -- Do no parse EOL. The EOL is parsed in the beginning as the start of an record.
      return $ List.zip (List.repeat bondOrigin) bondTargets

    -- Converts a 'Maybe' result to a 'Parser' result.
    maybeToParserMonad :: Maybe a -> Parser a
    maybeToParserMonad a = case a of
      Nothing -> fail "Could not parse the element symbol."
      Just x -> return x
