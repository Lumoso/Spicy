-- |
-- Module      : Spicy.Molecule.Internal.Physical
-- Description : Types with physical or chemical meaning
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This molecule provides types that carry some physical or chemical meaning, such as 'Element',
-- 'Atom', or 'BondOrder'.
--
-- This module is an internal module. It is not intended for direct import; consider importing
-- 'Spicy.Molecule.Physical' instead.
module Spicy.Molecule.Internal.Physical
  ( -- * Chemistry
    Element (..),
    elementToAN,
    aNToElement,
    Atom (..),
    LinkInfo (..),
    _IsLink,
    _NotLink,
    isAtomLink,
    LinkAtomInfo (..),
    FFType (..),
    BondOrder (..),
    isBond,
    BondMatrix,

    -- * Multipoles
    Multipoles (..),
    Monopole (..),
    Dipole (..),
    Quadrupole (..),
    Octopole (..),
    Hexadecapole (..),
    MultipoleR0,
    MultipoleR1,
    MultipoleR2,
    MultipoleR3,
    MultipoleR4,
  )
where

import Data.Aeson
import Data.Default
import RIO.Partial (toEnum)
import Spicy.Aeson
import Spicy.Prelude

-- | All chemical elements. Have them very clear because force fields and pdb names may interfere
-- and are just arbitrary strings.
data Element
  = H
  | He
  | Li
  | Be
  | B
  | C
  | N
  | O
  | F
  | Ne
  | Na
  | Mg
  | Al
  | Si
  | P
  | S
  | Cl
  | Ar
  | K
  | Ca
  | Sc
  | Ti
  | V
  | Cr
  | Mn
  | Fe
  | Co
  | Ni
  | Cu
  | Zn
  | Ga
  | Ge
  | As
  | Se
  | Br
  | Kr
  | Rb
  | Sr
  | Y
  | Zr
  | Nb
  | Mo
  | Tc
  | Ru
  | Rh
  | Pd
  | Ag
  | Cd
  | In
  | Sn
  | Sb
  | Te
  | I
  | Xe
  | Cs
  | Ba
  | La
  | Ce
  | Pr
  | Nd
  | Pm
  | Sm
  | Eu
  | Gd
  | Tb
  | Dy
  | Ho
  | Er
  | Tm
  | Yb
  | Lu
  | Hf
  | Ta
  | W
  | Re
  | Os
  | Ir
  | Pt
  | Au
  | Hg
  | Tl
  | Pb
  | Bi
  | Po
  | At
  | Rn
  | Fr
  | Ra
  | Ac
  | Th
  | Pa
  | U
  | Np
  | Pu
  | Am
  | Cm
  | Bk
  | Cf
  | Es
  | Fm
  | Md
  | No
  | Lr
  | Rf
  | Db
  | Sg
  | Bh
  | Hs
  | Mt
  | Ds
  | Rg
  | Cn
  | Nh
  | Fl
  | Mc
  | Lv
  | Ts
  | Og
  deriving (Show, Eq, Read, Ord, Enum, Bounded, Generic, NFData, Hashable)

instance ToJSON Element where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Element where
  parseJSON = genericParseJSON spicyJOption

-- | Get the element with the atomic number (nuclear charge). Will throw a
-- 'DataStructureException' if the number is negative or out of range.
aNToElement :: MonadThrow m => Int -> m Element
aNToElement i = do
  unless (i > 0) . throwM . localExc $ "Atomic number is negative"
  unless (i <= fromEnum (maxBound :: Element)) . throwM . localExc $ "Atomic number too large"
  return $ toEnum (i -1)
  where
    localExc = DataStructureException "anToElement"

-- | Get the atomic number on an element.
elementToAN :: Element -> Int
elementToAN = (+ 1) . fromEnum

----------------------------------------------------------------------------------------------------

-- | An atom in a 'Molecule'. Atoms are compared by their indices only and they must therefore be
-- unique. The coordinates of the 'Atom' are defined in cartesian \(\mathbb{R}^3\).
data Atom = Atom
  { -- | Chemical 'Element' of the atom.
    element :: !Element,
    -- | Label, e.g. from a pdb, just for identification, can be empty.
    label :: !Text,
    -- | A field which may indicate that an atom is a link atom in the context of ONIOM. Link atoms
    -- require additional information, which is also provided by this field.
    isLink :: !LinkInfo,
    -- | Whether the atom is a dummy atom, only providing multipole information.
    isDummy :: !Bool,
    -- | Label depending on the MM software used, can be used to identify the atom type in the
    -- context of MM.
    ffType :: !FFType,
    -- | Coordinates of the atom, cartesian in \(\mathbb{R}^3\). Relies on the parser to fill
    -- with exactly 3 values.
    coordinates :: !(VectorS Double),
    -- | Atom-centred multipole information after a calculation.
    multipoles :: !Multipoles,
    -- | The formal charge of an atom. It is fine to have a localised integer formal charge here,
    -- as it would be drawn in one of the mesomeric cases of a lewis structure, as it will migrate
    -- to the groups later and delocalise anyway. For example if you have a Ru(BPy)3⁺² complex,
    -- you may wish to assign a formal charge of +2 to the ruthenium.
    formalCharge :: !Int,
    -- | The excess electrons this atom provides for e.g. the alpha spin orbitals. This is not
    -- actually meant to represent the electronic structure of this atom, but contribute to the
    -- number of unpaired electrons in the group. For example if you have a Fe(II)(H20)6 complex,
    -- you should assign 4 unpaired electrons here. Positive numbers are alpha-electrons, negative
    -- are beta-electrons. They will cancel each other out and allow groups to do a low-spin
    -- coupling and most likely pair the electrons again. Use with caution!
    formalExcEl :: !Int
  }
  deriving (Show, Eq, Generic)

instance ToJSON Atom where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Atom where
  parseJSON = genericParseJSON spicyJOption

-- Lenses (Additional type hints required for those. Generics are not sufficient.)
instance (k ~ A_Lens, a ~ Int, b ~ a) => LabelOptic "formalCharge" k Atom Atom a b where
  labelOptic = lens formalCharge $ \s b -> (s {formalCharge = b} :: Atom)

instance (k ~ A_Lens, a ~ Int, b ~ a) => LabelOptic "formalExcEl" k Atom Atom a b where
  labelOptic = lens formalExcEl $ \s b -> (s {formalExcEl = b} :: Atom)

----------------------------------------------------------------------------------------------------

-- | Flag if an atom is a link atom. If an atom is a link atom, it will contain information about
-- the linking. See "[A new ONIOM implementation in Gaussian98. Part I. The calculation of energies,
-- gradients, vibrational frequencies and electric field derivatives](https://doi.org/10.1016/S0166-1280\(98\)00475-8)"
data LinkInfo
  = -- | The atom is not a link atom.
    NotLink
  | -- | The atom is a link atom and provides information about its link partners and context.
    IsLink LinkAtomInfo
  deriving (Show, Eq, Generic)

instance ToJSON LinkInfo where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON LinkInfo where
  parseJSON = genericParseJSON spicyJOption

-- Prisms
_NotLink :: Prism' LinkInfo ()
_NotLink = prism' (const NotLink) $ \s -> case s of
  NotLink -> Just ()
  _ -> Nothing

_IsLink :: Prism' LinkInfo LinkAtomInfo
_IsLink = prism' IsLink $ \s -> case s of
  IsLink lai -> Just lai
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Translates link info to a simple boolean.
isAtomLink :: LinkInfo -> Bool
isAtomLink NotLink = False
isAtomLink IsLink {} = True

----------------------------------------------------------------------------------------------------

-- | Information about a link atom (see 'LinkInfo').
data LinkAtomInfo = LinkAtomInfo
  { -- | The 'IntMap.key' of the atom in the model system to which this atom binds.
    linkModelPartner :: Int,
    -- | The key of the atom in the real system, which this atom replaces for the model system.
    linkRealPartner :: Int,
    -- | The scaling factor \(g\), usually calculated from a quotient of covalent radii.
    linkGFactor :: Double
  }
  deriving (Show, Eq, Generic)

instance ToJSON LinkAtomInfo where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON LinkAtomInfo where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | These are labels for molecular mechanics software. The strings are basically arbitrary and
-- depend on the MM software used.
data FFType
  = FFMol2 !Text
  | FFTXYZ !Int
  | FFPDB !Text
  | FFXYZ
  | FFBq
  deriving (Show, Generic)

instance Eq FFType where
  FFMol2 _ == FFMol2 _ = True
  FFMol2 _ == _ = False
  FFTXYZ _ == FFTXYZ _ = True
  FFTXYZ _ == _ = False
  FFPDB _ == FFPDB _ = True
  FFPDB _ == _ = False
  FFXYZ == FFXYZ = True
  FFXYZ == _ = False
  FFBq == FFBq = True
  FFBq == _ = False

instance ToJSON FFType where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON FFType where
  parseJSON = genericParseJSON spicyJOption

-- Prisms
_FFMol2 :: Prism' FFType Text
_FFMol2 = prism' FFMol2 $ \s -> case s of
  FFMol2 b -> Just b
  _ -> Nothing

_FFTXYZ :: Prism' FFType Int
_FFTXYZ = prism' FFTXYZ $ \s -> case s of
  FFTXYZ b -> Just b
  _ -> Nothing

_FFPDB :: Prism' FFType Text
_FFPDB = prism' FFPDB $ \s -> case s of
  FFPDB b -> Just b
  _ -> Nothing

_FFXYZ :: Prism' FFType ()
_FFXYZ = prism' (const FFXYZ) $ \s -> case s of
  FFXYZ -> Just ()
  _ -> Nothing

_FFBq :: Prism' FFType ()
_FFBq = prism' (const FFBq) $ \s -> case s of
  FFBq -> Just ()
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | A bond order between atoms.
data BondOrder
  = -- | No bond between atoms. Should usually not be set, as this is the default in the sparse
    -- representation of the 'BondMatrix'.
    None
  | -- | A purely covalent, non-delocalising single bond.
    Single
  | -- | Any bond order higher than single bonds. This includes "formal" single bonds, which still
    -- delocalise to some extent, such as in e.g. conjugated alkenes.
    Multiple
  | -- | Coordinative bonds involving metals or other strong, non-covalent interactions beyond
    -- pure multipole effects. Includes e.g. dative ligand-metal bonds.
    Coordinative
  deriving (Eq, Show, Generic)

instance ToJSON BondOrder

instance FromJSON BondOrder

-- | Check if there is a bond.
isBond :: BondOrder -> Bool
isBond b = case b of
  None -> False
  _ -> True

----------------------------------------------------------------------------------------------------

-- | The bond matrix is represented sparsely by a HashMap with an '(Int, Int)' tuple as the atom
-- indices. The order is @(Origin, Target)@.
type BondMatrix = HashMap (Int, Int) BondOrder

{-
====================================================================================================
-}

-- | Representation of multipoles for expansion of the electrostatic potential. Those definitions
-- use spherical tensors.
data Multipoles = Multipoles
  { monopole :: !(Maybe Monopole),
    dipole :: !(Maybe Dipole),
    quadrupole :: !(Maybe Quadrupole),
    octopole :: !(Maybe Octopole),
    hexadecapole :: !(Maybe Hexadecapole)
  }
  deriving (Eq, Show, Generic)

instance ToJSON Multipoles where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Multipoles where
  parseJSON = genericParseJSON spicyJOption

instance Default Multipoles where
  def =
    Multipoles
      { monopole = Nothing,
        dipole = Nothing,
        quadrupole = Nothing,
        octopole = Nothing,
        hexadecapole = Nothing
      }

----------------------------------------------------------------------------------------------------

-- | A monopole moment.
newtype Monopole = Monopole
  { q00 :: Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON Monopole where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Monopole where
  parseJSON = genericParseJSON spicyJOption

type MultipoleR0 = Monopole

----------------------------------------------------------------------------------------------------

-- | A spherical dipole moment.
data Dipole = Dipole
  { q10 :: Double,
    q11c :: Double,
    q11s :: Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON Dipole where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Dipole where
  parseJSON = genericParseJSON spicyJOption

type MultipoleR1 = Dipole

----------------------------------------------------------------------------------------------------

-- | A spherical quadrupole moment.
data Quadrupole = Quadrupole
  { q20 :: Double,
    q21c :: Double,
    q21s :: Double,
    q22c :: Double,
    q22s :: Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON Quadrupole where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Quadrupole where
  parseJSON = genericParseJSON spicyJOption

type MultipoleR2 = Quadrupole

----------------------------------------------------------------------------------------------------

-- | A spherical octopole moment.
data Octopole = Octopole
  { q30 :: Double,
    q31c :: Double,
    q31s :: Double,
    q32c :: Double,
    q32s :: Double,
    q33c :: Double,
    q33s :: Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON Octopole where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Octopole where
  parseJSON = genericParseJSON spicyJOption

type MultipoleR3 = Octopole

----------------------------------------------------------------------------------------------------

-- | A spherical octopole moment.
data Hexadecapole = Hexadecapole
  { q40 :: Double,
    q41c :: Double,
    q41s :: Double,
    q42c :: Double,
    q42s :: Double,
    q43c :: Double,
    q43s :: Double,
    q44c :: Double,
    q44s :: Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON Hexadecapole where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Hexadecapole where
  parseJSON = genericParseJSON spicyJOption

type MultipoleR4 = Hexadecapole
