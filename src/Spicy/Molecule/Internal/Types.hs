{-# OPTIONS_GHC -Wno-incomplete-record-updates #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

-- |
-- Module      : Spicy.Molecule.Internal.Types
-- Description : Types loosely associated with molecular information and its datastructures
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
module Spicy.Molecule.Internal.Types
  ( -- * Local Helper Types
    GroupAtomInfo (..),
    TXYZAtomInfo (..),
  )
where

import Spicy.Molecule.Internal.Molecule
import Spicy.Molecule.Internal.Physical
import Spicy.Prelude

{-
====================================================================================================
-}

-- $localHelperTypes
-- These types are not meant to be returned or consumed by any exported function. Instead they are
-- used only locally to pass information around, which would otherwise need to be stored in
-- unannotated tuples or something.

-- | Type to pass atomwise information from parsers with atom-group associations around.
data GroupAtomInfo = GroupAtomInfo
  { -- | The index of the atom in the whole structure.
    faiAtomIndex :: Int,
    -- | The index of the group in the whole structure.
    faiGroupIndex :: Int,
    -- | The atom of this line, associated to an group.
    faiAtom :: Atom,
    -- | The group associated to this atom. Possibly the selection of atom indices in the group
    -- contains only one atom and matching groups must then be joined.
    faiGroup :: Group
  }

----------------------------------------------------------------------------------------------------

-- | Type just to handle the information obtained from parsing a single atom line of a TXYZ file.
data TXYZAtomInfo = TXYZAtomInfo
  { -- | Index of this atom in the TXYZ file. First number in a row.
    txyzIndex :: Int,
    -- | The atom of this line.
    txyzAtom :: Atom,
    -- | The indices of the atoms, to which this atom has a bond.
    txyzBondTargets :: [Int]
  }

{-
====================================================================================================
-}
