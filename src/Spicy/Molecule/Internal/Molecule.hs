-- |
-- Module      : Spicy.Molecule.Internal.Molecule
-- Description : Spicy's core Molecule type
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides the 'Molecule' data type, which includes most of the chemical information of
-- the system, including its ONIOM setup, context, input, and output of calculations.
--
-- Molecules are defined with a lot of context and 'Molecule' is the single most important type
-- in Spicy. The layout of the 'Molecule' data type keeps layered calculations, complex input
-- formats and groupings of molecules in mind and tries to apply to them all.
--
-- MC-ONIOM\(n\) can ultimately be represented as a tree, where the real system is the root of the
-- tree. Stepping down the branches, goes to physically smaller systems and usually higher
-- calculation niveaus, which are the model systems. Given a node \((i)\) with the set of its
-- children \(C^{(i)}\), the following applies:
-- \[ C^{(i)} \subseteq (i) \]
-- \[ C^{(i)}_a \cap C^{(i)}_b \quad \forall a \neq b \]
-- therefore all branches are disjoint sets (never overlap) and are subsets of their parents. The
-- molecules in the ONIOM tree shrink, the more we step into the branches.
--
-- This module is an internal module. It is not intended for direct import; consider importing
-- 'Spicy.Molecule.Molecule' instead.
module Spicy.Molecule.Internal.Molecule
  ( -- * Molecule types
    Molecule (..),
    Trajectory,
    CalcID (..),
    CalcK (..),
    niveauLensGen,
    LayerCalcID (..),
    Group (..),
    MolGraph,
    MolID,
    OniomLayer (..),
    HasMolecule (..),
    HasDirectMolecule (..),

    -- * Generic utilities
    molMap,
    molMapWithMolID,
    molTraverse,
    molTraverseWithID,
    molFoldl,
    molFoldlWithMolID,
  )
where

import Data.Aeson
import Data.Graph.Inductive hiding ((&))
import qualified Data.IntMap as IntMap
import RIO.Seq (Seq (..))
import Spicy.Aeson
import Spicy.Common
import Spicy.Molecule.Internal.Computational
import Spicy.Molecule.Internal.Fragment
import Spicy.Molecule.Internal.Physical
import Spicy.Prelude

-- | A 'Molecule' represents any (sub)tree of the ONIOM setup and describes the entire ONIOM layout
-- in a single data structure. It stores all physical information of the system, as well as
-- calculation contexts and partial results.
--
-- The layout is described as a tree with a maximum depth of \(n\), therefore representing any
-- multi-centre ONIOM\(n\) setup. Each node in the tree can be represented by a sequence of steps
-- into branches relative to the root node:
-- \[ (i_k)_{k = 0}^{n - 1} \]
-- The depth of a node (layer) is then given by the length of the sequence \(|(i)| = d\) and the
-- root of the tree is the only node at \(d = 0\).
--
-- Starting from a top level molecule, all atoms and bonds of the system are expected to be in the
-- top layer (except link atoms of deeper layers). Therefore, deeper layers are subsets of layers
-- above, with exception of link atoms. Atoms are always uniquely identified by their 'IntMap.Key',
-- which does not change throughout layers. This even holds for newly introduced link atoms.
--
-- The data structure makes it necessary to construct the 'Molecule' top down, not bottom up.
-- For construction see also "Spicy.ONIOM.Layout".
data Molecule = Molecule
  { -- | Comment or description of a molecule. This is just a potentially helpful description or
    -- hint, not of any deeper relevance.
    comment :: !Text,
    -- | The 'Atom's of the layer. Their 'IntMap.Key's uniquely represent an 'Atom' and are
    -- consistent throughout all layers.
    atoms :: !(IntMap Atom),
    -- | An sparse representation of the bond matrix. Is expected to be defined bidirectorial.
    bonds :: !BondMatrix,
    -- | Branches (deeper layers) in the ONIOM tree.
    subMol :: !(IntMap Molecule),
    -- | [Partitioning](https://en.wikipedia.org/wiki/Partition_of_a_set) of the layer's 'Atom's
    -- into non-separable groups. Groups can represent chemical groups (functional groups), or
    -- residues in macro-molecules (such as in PDB).
    group :: !(IntMap Group),
    -- | The potential energy and its derivatives.
    energyDerivatives :: !EnergyDerivatives,
    -- | Calculations to perform on __this__ layer of the molecule. Except for the root of the ONIOM
    -- tree (the real system), every layer contains two calculations; an original (higher level)
    -- calculation and a lower level calculation, that was inherited from the parent layer.
    calculations :: !(Maybe Calculations),
    -- | The Jacobian matrix for energy derivative transformation from this system to its parent
    -- system.
    jacobian :: !(Maybe (MatrixS Double)),
    -- | Neighbourlists that have possibly been calculated together with a given search distance.
    -- Maps from the search distance to a sparse neighbourlist representation. Also see
    -- 'Spicy.Molecule.Internal.Util.neighbourList'.
    neighbourlist :: !(Map Double NeighbourList)
  }
  deriving (Generic)

instance ToJSON Molecule where
  toEncoding = genericToEncoding spicyJOption

-- | Access to the shared state of the 'Molecule' in a shared variable. This is a stateful
-- representation of the 'Molecule'.
class HasMolecule env where
  moleculeL :: Lens' env (TVar Molecule)

-- | RIO style reader constraint class, where there is direct (pure) access to the 'Molecule' in the
-- environemt. While 'HasMolecule' gives access to the current state of the 'Molecule' in the
-- runtime environemt and therefore requires 'IO', this is the direct access to an immutable
-- 'Molecule' in the environment. While the runtime environment does not hold this type, it is a
-- useful accessor for pure 'MonadReader' stacks.
class HasDirectMolecule env where
  moleculeDirectL :: Lens' env Molecule

instance HasDirectMolecule Molecule where
  moleculeDirectL = castOptic simple

----------------------------------------------------------------------------------------------------

-- | Partial representation of a 'Molecule' as graph. Nodes are atoms and they are connected
-- undirected by the bond order.
type MolGraph = Gr Atom BondOrder

----------------------------------------------------------------------------------------------------

-- | Trajectories are simply Sequences of 'Molecule's.
type Trajectory = Seq Molecule

----------------------------------------------------------------------------------------------------

-- | A data structure to get a layer in the 'Molecule' contructor. Enables to find a specific layer.
-- It works by giving number of lookups stepping down the recursion of the 'Molecule' structure
-- along the 'subMol' structure. An empty sequency of indices is the top layer. Then each element
-- will be used as a key for the 'IntMap' structure in 'subMol' structure, to step down to
-- the next layer.
type MolID = Seq Int

{-
====================================================================================================
-}

-- $calculations
-- These types give access or help to get access to the actual calculations on each layer.

{-
====================================================================================================
-}

-- | Identify a calculation by an unique ID.
data CalcID = CalcID
  { -- | The molecule layer, for which the calculation is defined.
    molID :: MolID,
    -- | Whether this is a 'Original' or 'Inherited' calculation.
    calcK :: CalcK,
    -- | Possibly a calculation on a fragment of the specified layer.
    frag :: Maybe AuxMonomer
  }
  deriving (Eq, Show, Ord)

-- | Accessor type to get either inherited or original calculations of a given layer.
data CalcK
  = Original
  | Inherited
  deriving (Eq, Show, Ord)

----------------------------------------------------------------------------------------------------

-- | Lens generator to access the proper calculation niveau of a context.
niveauLensGen :: CalcK -> Optic An_AffineTraversal NoIx Calculations Calculations CalcContext CalcContext
niveauLensGen calcK = case calcK of
  Original -> castOptic #original
  Inherited -> #inherited % _Just

----------------------------------------------------------------------------------------------------

-- | Identifies all necessary calculations on a given niveau on a layer. Therefore a larger overview
-- on layer calculations than 'CalcID'. Handling 'LayerCalcID' is more useful for the logic of
-- ONIOM, while 'CalcID' is more useful for the context of wrappers. GMBE collectors must make sure
-- to appropriately transform GMBE results into usable 'Monolith'-like results, that can be
-- understood by ONIOM collectors.
data LayerCalcID
  = -- | Monolithic (normal) calculation on a layer without fragments/auxiliary monomers.
    Monolithic MolID CalcK
  | -- | A series of fragment based calculations, that can be transformed to describe an entire
    -- layer. Gives a set of 'AuxMonomer's as a list and in combination those values can be used to
    -- construct a sequence of 'CalcID's with 'Just' values for the 'frag's.
    Fragments MolID CalcK [AuxMonomer]
  deriving (Eq, Show, Ord)

----------------------------------------------------------------------------------------------------

-- | Wrapper around 'Molecule', indicating that a single layer is obtained. Therefore 'subMol' is
-- empty and information from deeper layer may not used or not be returned.
newtype OniomLayer = OniomLayer {getOniomLayer :: Molecule}

----------------------------------------------------------------------------------------------------

-- | Mapping through the 'Molecule' data structure. Applies a function to each layer. To keep this
-- non-mind-blowing, it is best to only use functions, which only act on the current molecule layer
-- and not on the deeper ones as the update function.
molMap :: (Molecule -> Molecule) -> Molecule -> Molecule
molMap f mol =
  let subMols = mol ^. #subMol
   in if IntMap.null subMols
        then f mol
        else f mol & #subMol %~ IntMap.map (molMap f)

----------------------------------------------------------------------------------------------------

-- | Indexed mapping through the 'Molecule' data structure. Applies a function to each molecule. To
-- keep this non-mind-blowing, it is best to only use functions, which only act on the current
-- layer and not on the deeper ones as the update function. The mapping function has access
-- to the current 'MolID'.
molMapWithMolID :: (MolID -> Molecule -> Molecule) -> Molecule -> Molecule
molMapWithMolID f mol = go Empty f mol
  where
    go :: MolID -> (MolID -> Molecule -> Molecule) -> Molecule -> Molecule
    go molIdAcc func mol' =
      let subMols = mol' ^. #subMol
       in if IntMap.null subMols
            then func molIdAcc mol'
            else
              func molIdAcc mol' & #subMol
                %~ IntMap.mapWithKey
                  (\key val -> go (molIdAcc |> key) func val)

----------------------------------------------------------------------------------------------------

-- | Monadic mapping through the 'Molecule' data structure. Applies a function to each molecule. To
-- keep this non-mind-blowing, it is best to only use functions, which only act on the current
-- molecule layer and not on the deeper ones as the update function.
molTraverse :: Monad m => (Molecule -> m Molecule) -> Molecule -> m Molecule
molTraverse f mol = molTraverseWithID (\_ m -> f m) mol

----------------------------------------------------------------------------------------------------

-- | Monadic mapping through the 'Molecule' data structure. Applies a function to each molecule. To
-- keep this non-mind-blowing, it is best to only use functions, which only act on the current
-- molecule layer and not on the deeper ones as the update function.
--
-- This functions works top down through the molecule. The worker function has access to the MolID
-- of the molecule currently processed.
molTraverseWithID :: Monad m => (MolID -> Molecule -> m Molecule) -> Molecule -> m Molecule
molTraverseWithID f mol = go Empty f mol
  where
    go molIDAcc func mol' = do
      let subMols = mol' ^. #subMol
      thisLayerApplied <- func molIDAcc mol'
      if IntMap.null subMols
        then return thisLayerApplied
        else do
          newSubMols <-
            IntMap.traverseWithKey
              ( \key deepMol ->
                  if IntMap.null (deepMol ^. #subMol)
                    then (func $ molIDAcc |> key) deepMol
                    else go (molIDAcc |> key) func deepMol
              )
              subMols
          let newMol = thisLayerApplied & #subMol .~ newSubMols
          return newMol

----------------------------------------------------------------------------------------------------

-- | Like a fold through a molecule. This steps through the left-most branch of a molecule
-- completely before going to the more right sites.
molFoldl :: (a -> Molecule -> a) -> a -> Molecule -> a
molFoldl f s mol =
  let subMols = mol ^. #subMol
      topLayerApplied = f s mol
   in if IntMap.null subMols
        then topLayerApplied
        else IntMap.foldl' (molFoldl f) topLayerApplied subMols

----------------------------------------------------------------------------------------------------

-- | Like a fold through a molecule. This steps through the left-most branch of a molecule
-- completely before going to the more right sites. The folding function has access to the current
-- 'MolID'.
molFoldlWithMolID :: (a -> MolID -> Molecule -> a) -> a -> Molecule -> a
molFoldlWithMolID f s mol = go Empty f s mol
  where
    -- go :: MolID -> (a -> MolID -> Molecule -> a) -> a -> Molecule -> a
    go molIdAcc func start mol' =
      let subMols = mol' ^. #subMol
          thisLayerApplied = (\acc -> func acc molIdAcc) start mol'
       in if IntMap.null subMols
            then thisLayerApplied
            else
              IntMap.foldlWithKey'
                (\acc key molVal -> go (molIdAcc |> key) f acc molVal)
                thisLayerApplied
                subMols
