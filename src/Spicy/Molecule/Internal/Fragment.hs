-- |
-- Module      : Spicy.Molecule.Internal.Fragment
-- Description : Types and functions related to fragment methods
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Support for Spicy's fragment-based methods. It builds around the quite general ideas of the
-- generalised many body expansion (GMBE) and the terminology described in
-- <https://doi.org/10.1063/1.4742816>.
--
-- This module is an internal module. It is not intended for direct import; consider importing
-- 'Spicy.Molecule.Computational' instead.
module Spicy.Molecule.Internal.Fragment
  ( -- * Fragment Methods
    AuxMonomer (..),
    FragmentMethods (..),
    FragType (..),
    _SFMA,
    _Overlap,
    _Exclusion,
    FragEmbedding (..),
    _ParallelDomain,
    Group (..),

    -- ** Auxiliary Monomer Utilities
    auxMonoFromGroups,
    getAuxMonomerAtoms,
    auxMonomerUnion,
    auxMonomerIntersection,
    auxMonomerOvlp,
  )
where

import Data.Aeson
import Data.IntMap as IntMap
import Data.IntSet as IntSet
import Spicy.Aeson
import Spicy.Common
import Spicy.Prelude

-- | Auxiliary monomers in the sense of GMBE. Sometimes also called a fragment and labeled \(F\).
--  Those are expressed in terms of groups \(G\) and may overlap to any extent. The union of all
-- auxiliary monomers is identical to the union of all groups, the union of all atoms \(A\) and
-- represents the entire molecule \(M\).
-- \[ \bigcup F = \bigcup G = \bigcup A = M \]
data AuxMonomer = AuxMonomer
  { -- | Charge of an auxiliary monomer.
    charge :: Int,
    -- | Number of excess electrons in the auxiliary monomer. This might be positive or negative to
    -- refer to the excess electrons in \(\alpha\)- or \(\beta\)-orbitals and allows auxiliary
    -- monomers to couple anti-ferromagnetic.
    excEl :: Int,
    -- | Constituting groups \(G\) (non-overlapping) of this auxiliary monomer \(F\).
    constGroups :: IntSet
  }
  deriving (Show, Generic, ToJSONKey)

instance Eq AuxMonomer where
  a == b = constGroups a == constGroups b

instance Ord AuxMonomer where
  a `compare` b = constGroups a `compare` constGroups b

instance ToJSON AuxMonomer where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON AuxMonomer where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Specification of fragment methods, namely GMBE and its options.
data FragmentMethods = FragmentMethods
  { -- | How auxiliary monomers are formed from groups.
    fragtype :: FragType,
    -- | The original set of fragments can be combined to \(n\)-mers, and this specifies the \(n\).
    -- To keep the original auxiliary monomers unmodified, this can be set to 1.
    nmer :: Natural,
    -- | Inter-fragment embedding. The ONIOM-layer embedding happens as specified by in the
    -- calculation setup of the full layer.
    embedding :: Maybe FragEmbedding,
    -- | A distance cutoff, up to which \(n\)-mers would be formed. If not given, all possible
    -- \(n\)-mers will be formed.
    distCutoff :: Maybe Double
  }
  deriving (Eq, Show, Generic)

instance ToJSON FragmentMethods where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON FragmentMethods where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Method how to form the fragments from the non-separable groups.
data FragType
  = -- | Systematic molecule fragmentation by annihilation in order \(n\)
    SMFA Natural
  | -- | Each group and groups in up to \(n\) inter-group-bonds distance form monomers. Each group
    -- is the focus of one monomer and the monomers usually overlap for \(n > 0\). Those are
    -- overlapping ego-graphs, modified by ring-avoidance rules.
    Overlap Natural
  | -- | Starting with some group, a local cluster of groups up to \(n\) bonds distance will be
    -- formed. This consumes the entire molecule, until no group remains. These monomers never
    -- overlap. Those are non-overlapping ego-graphs, modified by ring-avoidance rules.
    Exclusion Natural
  deriving (Eq, Show, Generic)

instance ToJSON FragType where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON FragType where
  parseJSON = genericParseJSON spicyJOption

_SFMA :: Prism' FragType Natural
_SFMA = prism' SMFA $ \s -> case s of
  SMFA n -> Just n
  _ -> Nothing

_Overlap :: Prism' FragType Natural
_Overlap = prism' Overlap $ \s -> case s of
  Overlap n -> Just n
  _ -> Nothing

_Exclusion :: Prism' FragType Natural
_Exclusion = prism' Exclusion $ \s -> case s of
  Exclusion n -> Just n
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Inter-fragment embedding. To mimic the electric field of the other fragments of a system,
-- electronic embedding might be used.
data FragEmbedding
  = -- | Iteratively polarise fragments in the field of the fragments of the previous iteration
    -- until convergence.
    ParallelDomain Double
  | -- | Use the electric field of all fragments in the gas phase to polarise. Like 'ParallelDomain'
    -- with just 1 iteration.
    Static
  deriving (Eq, Show, Generic)

instance ToJSON FragEmbedding where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON FragEmbedding where
  parseJSON = genericParseJSON spicyJOption

_ParallelDomain :: Prism' FragEmbedding Double
_ParallelDomain = prism' ParallelDomain $ \s -> case s of
  ParallelDomain c -> Just c
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Definition of a group \(G\). Groups are sets of atoms \(A_i\), \(G = \{ A_i | A_i \in A \}\), and
-- \(A\) is the set of all atoms. The groups are strictly a subset of a given layer,
-- \(G \subseteq A\).
data Group = Group
  { -- | The name of a group. Doesn't need to be unique.
    label :: !Text,
    -- | Meant for PDB and similiar molecules, where protein chains
    --   need to be distinguished.
    chain :: !(Maybe Char),
    -- | The atoms and bonds of the group. Relative to the
    --   molecule layer which contains the group.
    atoms :: !IntSet,
    -- | The charge of a group. The charges of all groups must add up to the molecular charge.
    charge :: !Int,
    -- | Number of excess electrons. Respects sign, where positive is \(\alpha\) electrons and
    -- negative is \(\beta\) electrons. Calculated from the constituting atoms.
    excEl :: !Int
  }
  deriving (Show, Eq, Generic)

instance ToJSON Group where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Group where
  parseJSON = genericParseJSON spicyJOption

{-
====================================================================================================
-}

-- | Construct a fragment/auxiliary monomer from a set of groups.
auxMonoFromGroups ::
  -- | Some groups from which to build the 'AuxMonomer'.
  IntMap Group ->
  AuxMonomer
auxMonoFromGroups groups =
  AuxMonomer
    { charge = flipFoldl' 0 groups $ \acc Group {charge} -> acc + charge,
      excEl = flipFoldl' 0 groups $ \acc Group {excEl} -> acc + excEl,
      constGroups = IntMap.keysSet groups
    }

-- | Obtain the constituting atoms of a fragment/auxiliary monomer.
getAuxMonomerAtoms ::
  MonadThrow m =>
  -- | At least the groups of the 'AuxMonomer', can also be a superset.
  IntMap Group ->
  AuxMonomer ->
  m IntSet
getAuxMonomerAtoms groups frag
  | (frag ^. #constGroups) `IntSet.isSubsetOf` IntMap.keysSet groups = return fragAtomsKeys
  | otherwise = throwM $ DataStructureException "getAuxMonomerAtoms" "The supplied groups can not describe the AuxMonomer."
  where
    fragGroupKeys = frag ^. #constGroups
    fragGroups = IntMap.restrictKeys groups fragGroupKeys
    fragAtomsKeys = flipFoldl' mempty fragGroups $ \atomAcc gr -> atomAcc <> gr ^. #atoms

-- | Union of two auxiliary monomers. Needs molecular information to get charge and multiplicity
-- correct.
auxMonomerUnion ::
  MonadThrow m =>
  -- | At least the 'Group's, that are required to represent the two 'AuxMonomer's.
  IntMap Group ->
  AuxMonomer ->
  AuxMonomer ->
  m AuxMonomer
auxMonomerUnion groups a b
  | ((a ^. #constGroups) <> (b ^. #constGroups)) `IntSet.isSubsetOf` IntMap.keysSet groups = return unionAM
  | otherwise = throwM $ DataStructureException "auxMonomerUnion" "The supplied groups can not describe the AuxMonomers."
  where
    atomsA = IntMap.restrictKeys groups $ a ^. #constGroups
    atomsB = IntMap.restrictKeys groups $ b ^. #constGroups
    unionGroups = IntMap.union atomsA atomsB
    unionAM = auxMonoFromGroups unionGroups

-- | Intersection of two auxiliary monomers. Needs group information to get charge and multiplicity
-- correct.
auxMonomerIntersection ::
  MonadThrow m =>
  -- | At least the 'Group's, that are required to represent the two 'AuxMonomer's.
  IntMap Group ->
  AuxMonomer ->
  AuxMonomer ->
  m AuxMonomer
auxMonomerIntersection groups a b
  | ((a ^. #constGroups) <> (b ^. #constGroups)) `IntSet.isSubsetOf` IntMap.keysSet groups = return intersectAM
  | otherwise = throwM $ DataStructureException "auxMonomerIntersection" "The supplied groups can not describe the AuxMonomers."
  where
    groupsA = IntMap.restrictKeys groups $ a ^. #constGroups
    groupsB = IntMap.restrictKeys groups $ b ^. #constGroups
    intersectingGroups = IntMap.intersection groupsA groupsB
    intersectAM = auxMonoFromGroups intersectingGroups

-- | Obtain the overlapping constituting groups of two auxiliary monomers.
auxMonomerOvlp :: AuxMonomer -> AuxMonomer -> IntSet
auxMonomerOvlp a b = (a ^. #constGroups) `IntSet.intersection` (b ^. #constGroups)
