-- |
-- Module      : Spicy.Molecule.Internal.Util
-- Description : Utilities to manipulate basic molecular data structures.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides many of the toolings, that are used to manipulate 'Molecule's, obtain
-- properties from them or operate on 'Molecule' related information, without directly anything that
-- is directly ONIOM itself.
module Spicy.Molecule.Internal.Util
  ( checkMolecule,
    reIndexMolecule,
    reIndex2BaseMolecule,
    getMaxAtomIndex,
    findAtomInSubMols,
    findAtomInGroup,
    getNElectrons,
    getNeutralElectrons,
    getCappedAtoms,
    getMolByID,
    molIDLensGen,
    getCalcByID,
    calcIDLensGen,
    newSubLayer,
    createLinkAtom,
    calcLinkCoords,
    addAtom,
    removeAtom,
    changeBond,
    getCoordinatesAs3NMatrix,
    getCoordinatesAsVector,
    distMat,
    arbDistMat,
    getDenseSparseMapping,
    getSparseDenseMapping,
    getAtomsAsVector,
    neighbourList,
    guessBondMatrix,
    guessTopology,
    groupDetectionDetached,
    getPolarisationCloudFromAbove,
    combineDistanceGroups,
    bondDistanceGroups,
    getAtomAssociationMap,
    groupAtomInfo2AtomsAndGroups,
    getJacobian,
    combineMultipoles,
    modifyMultipole,
    redistributeLinkMoments,
    redistributeLinkMoments',
    removeRealLinkTagsFromModel,
    getAllLayerCalcIDsHierarchically,
    getAllMolIDsHierarchically,
    updatePositionsPosVec,
    updatePositions,
    shrinkNeighbourList,
    isolateMoleculeLayer,
    addLinksToNeighbourList,
    molID2OniomHumanID,
    calcID2Human,
    horizontalSlices,
    gradDense2Sparse,
    calcGeomConv,
    multipoleTransfer,
    checkMultipoleConvergence,
    getCoordsNetVec,
    groupFromAtomKeys,
    mol2Graph,
  )
where

import Control.Parallel.Strategies
import Data.Default
import Data.Foldable
import qualified Data.Graph.Inductive as Graph
import qualified Data.IntMap.Strict as IntMap
import qualified Data.IntSet as IntSet
import Data.Ix
import Data.Massiv.Array as Massiv hiding
  ( all,
    index,
    mapM,
    sum,
    toList,
  )
import Data.Massiv.Array.Manifest.Vector as Massiv
import Data.Maybe
import Data.Monoid
import qualified RIO.HashMap as HashMap
import qualified RIO.HashSet as HashSet
import qualified RIO.List as List
import qualified RIO.Map as Map
import RIO.Partial (toEnum)
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import qualified RIO.Text as Text
import Spicy.Common
import Spicy.Containers
import Spicy.Data
import Spicy.Massiv
import Spicy.Math.Util
import Spicy.Molecule.Internal.Computational
import Spicy.Molecule.Internal.Fragment
import Spicy.Molecule.Internal.Molecule
import Spicy.Molecule.Internal.Physical
import Spicy.Molecule.Internal.Types
import Spicy.Prelude as S hiding (sappend, (:>))
import Spicy.Wrapper.IPI.Types
import System.IO.Unsafe
import System.Path ((</>))
import qualified System.Path as Path

----------------------------------------------------------------------------------------------------

-- | Check sanity of 'Molecule', which means test the following criteria:
--
--   * The subset properties hold for atoms and bonds with exception of link atoms.
--   * Groups of the same layer are completelty disjoint
--   * Bonds of a layer are bidirectorial
--   * The sizes of the coordinates vectors is exactly 3
--   * Groups form a partition of the molecules atoms
--   * The electronic properties of everything is reasonable, e.g. the combination of charges and
--     multiplicities is reasonable.
{-# SCC checkMolecule #-}
checkMolecule :: MonadThrow m => Molecule -> m Molecule
checkMolecule mol = do
  unless layerIndCheck . throwM . localExc $
    "Bonds vs Atoms mismatch. Your bonds bind to non existing atoms."
  unless subMolAtomsDisjCheck . throwM . localExc $
    "The atoms of deeper layers are not disjoint but shared by groups."
  unless subsetCheckAtoms . throwM . localExc $
    "The atoms of deeper layers are not a subset of this layer."
  unless bondBidectorialCheck . throwM . localExc $
    "The bonds are not bidirectorially defined."
  unless atomCoordCheck . throwM . localExc $
    "The dimension of the coordinate vectors of the atoms is not exactly 3 for all atoms."
  unless groupsSelectionRangeCheck . throwM . localExc $
    "The groups contain indices of atoms, that do not exist in this molecule layer."
  unless groupCompletenessCheck . throwM . localExc $
    "The groups must contain all atoms of a layer."
  unless groupsDisjointCheck . throwM . localExc $
    "The groups must be disjoint."
  unless groupsChargeCheck . throwM . localExc $
    "The charge of the groups does not add up to the molecular charge."
  unless groupsMultCheck . throwM . localExc $
    "The multiplicity of the groups does not add up to the molecular multiplicity."
  unless calcCheck . throwM . localExc $
    "A calculation context has an impossible combination of charge and multiplicity."
  if IntMap.null (mol ^. #subMol)
    then return mol
    else do
      subMols <- traverse checkMolecule $ mol ^. #subMol
      return $ mol & #subMol .~ subMols
  where
    localExc = MolLogicException "checkMolecule"
    -- Indices of the atoms
    atomInds = IntMap.keysSet $ mol ^. #atoms

    -- Indices of the bonds.
    bondsInds =
      let tupleInds = HashMap.keys $ mol ^. #bonds
          origins = IntSet.fromList . fmap fst $ tupleInds
          targets = IntSet.fromList . fmap snd $ tupleInds
       in origins <> targets

    -- Check if bond indices do not exceed atom indices.
    layerIndCheck = IntSet.null $ bondsInds IntSet.\\ atomInds

    -- Next layer molecules. Discard the Map structure
    sM = mol ^. #subMol

    -- Disjointment test (no atoms and bonds shared through submolecules). This will not check
    -- dummy atoms (they will be removed before the disjoint check), as the may have common numbers
    -- shared through the groups.
    -- "bA" = Bool_A, "aA" = Atoms_A
    subMolAtomsDisjCheck =
      fst
        . foldl'
          ( \(bA, aA) (_, aB) ->
              if aA `intMapDisjoint` aB && bA then (True, aA `IntMap.union` aB) else (False, aA)
          )
          (True, IntMap.empty)
        . IntMap.map (\atoms' -> (True, IntMap.filter (\a -> not $ a ^. #isDummy) atoms'))
        . fmap (^. #atoms)
        $ sM

    -- Check if the dimension of the atom coordinate vector is exactly 3 for all atoms.
    atomCoordCheck =
      all (== 3)
        . IntMap.map (Massiv.elemsCount . getVectorS . coordinates)
        $ mol
          ^. #atoms

    -- Next Layer atoms all joined
    nLAtoms = IntMap.unions . fmap (^. #atoms) $ sM
    nLAtomsInds = IntMap.keysSet nLAtoms

    -- All link atoms of the next layer set
    nLLinkAtomsInds = IntMap.keysSet . IntMap.filter (\a -> isAtomLink $ a ^. #isLink) $ nLAtoms

    -- Test if the next deeper layer is a proper subset of the current layer.
    subsetCheckAtoms = IntSet.null $ (nLAtomsInds IntSet.\\ nLLinkAtomsInds) IntSet.\\ atomInds

    -- Check if the bonds are bidirectorial
    bondBidectorialCheck = isBondMatrixBidirectorial $ mol ^. #bonds

    -- Indices of all atoms assigned to groups.
    allGroupSelections = IntSet.unions $ mol ^.. #group % each % #atoms

    -- Check if only existing atoms are assigned to groups.
    groupsSelectionRangeCheck = allGroupSelections `IntSet.isSubsetOf` atomInds

    -- Check if all atoms have been assigned to groups. Sorting only a part
    -- of the atoms into groups is not allowed.
    groupCompletenessCheck =
      let diffSet = atomInds IntSet.\\ allGroupSelections
       in diffSet == IntSet.empty

    -- Check that all groups are disjoint sets of atoms.
    groupsDisjointCheck =
      let groupIntersections = IntMap.foldl' (\acc Group {atoms} -> IntSet.intersection acc atoms) mempty (mol ^. #group)
       in groupIntersections == mempty

    -- Check that the summed up charges of groups add up to the molecular charge.
    groupsChargeCheck =
      let groupChargeSum = IntMap.foldl' (\acc Group {charge} -> acc + charge) 0 (mol ^. #group)
          molCharge = mol ^? #calculations % _Just % #original % #input % #qMMMSpec % _QM % #charge
       in fromMaybe True $ (==) groupChargeSum <$> molCharge

    -- The number of excess electrons in all groups must fit the multiplicity of the system.
    groupsMultCheck =
      let groupsExcElSum = IntMap.foldl' (\acc Group {excEl} -> acc + excEl) 0 (mol ^. #group)
          groupsMult = (+ 1) . abs $ groupsExcElSum
          molMult = mol ^? #calculations % _Just % #original % #input % #qMMMSpec % _QM % #mult
       in fromMaybe True $ (==) groupsMult <$> molMult

    -- Check if charge and multiplicity combinations of this layer are fine.
    molNoDummies = mol & #atoms %~ IntMap.filter (\a -> not $ a ^. #isDummy)
    calcCheck =
      let qmCharge = molNoDummies ^? #calculations % _Just % #original % #input % #qMMMSpec % _QM % #charge
          qmMult = molNoDummies ^? #calculations % _Just % #original % #input % #qMMMSpec % _QM % #mult
          qmElectrons = getNElectrons (molNoDummies ^. #atoms) <$> qmCharge
          qmElectronsOK = case (qmElectrons, qmMult) of
            (Just n, Just m) -> n + 1 >= m && ((even n && odd m) || (odd n && even m))
            _ -> True
       in qmElectronsOK

----------------------------------------------------------------------------------------------------

-- | This reindexes all structures in a 'Molecule' with predefined counting scheme. This means
-- counting of 'Atom's will start at 0 and be consecutive. This also influences bonds in
-- 'Molecule.bonds' and layers in 'Molecule.subMol'. Link atoms will be taken care of.
reIndex2BaseMolecule :: MonadThrow m => Molecule -> m Molecule
reIndex2BaseMolecule mol =
  let allAtomIndices = getAtomIndices mol
      repMap = IntMap.fromAscList . (\old -> S.zip old [0 ..]) . IntSet.toList $ allAtomIndices
   in reIndexMolecule repMap mol

----------------------------------------------------------------------------------------------------

-- | Get the indices of all 'Atom's in a 'Molecule', including those of sublayers in
-- 'Molecule.subMol' and link atoms therein. This assumes a sane 'Molecule' according to
-- 'checkMolecule'.
getAtomIndices :: Molecule -> IntSet
getAtomIndices mol =
  let -- The indices of all atoms of the current layer
      thisLayerIndices = IntMap.keysSet $ mol ^. #atoms
      -- The indices of all sublayers + this layer.
      allIndices = foldr' (<>) thisLayerIndices . fmap getAtomIndices $ mol ^. #subMol
   in allIndices

----------------------------------------------------------------------------------------------------

-- | Given the full molecular system, this function will find the maximum index respective key of an
-- atom in all layers.
getMaxAtomIndex :: MonadThrow m => Molecule -> m Int
getMaxAtomIndex mol = do
  let maybeMax = fmap fst . IntSet.maxView . getAtomIndices $ mol
  case maybeMax of
    Nothing ->
      throwM $
        MolLogicException
          "getMaxAtomIndex"
          "Cannot find the maximum index of all atoms in the molecule. The molecule seems to be empty."
    Just k -> return k

----------------------------------------------------------------------------------------------------

-- | Reindex a complete 'Molecule', including all its deeper layers in 'Molecule.subMol') by
-- mappings from a global replacement Map, mapping old to new indices. This function assumes, that
-- your molecule is sane in the overall assumptions of this program. This means that the lower
-- layers obey the counting scheme of the atoms of the higher layers and link come last.
reIndexMolecule :: MonadThrow m => IntMap Int -> Molecule -> m Molecule
reIndexMolecule repMap mol = molTraverse (reIndexMoleculeLayer repMap) mol

----------------------------------------------------------------------------------------------------

-- | Reindex the 'Molecule.atoms' and 'Molecule.bonds' (also takes care of atoms in groups) of a
-- single layer of a 'Molecule' (ignoring anything in the 'Molecule.subMol' field). While the
-- completeness of the reindexing is checked and incompleteness of the replacement 'IntMap' 'Int'
-- will result in 'Left' 'String', it is not checked if the 'Atom's indexing is sane and indices are
-- unique.
reIndexMoleculeLayer ::
  MonadThrow m =>
  -- | 'IntMap' with mappings from old indices to new indices (bonds and atoms).
  IntMap Int ->
  -- | 'Molecule' to reindex.
  Molecule ->
  -- | 'Molecule' with __current__ layer reindexed.
  m Molecule
reIndexMoleculeLayer repMap mol = do
  -- Check for completness of the replacement map for the atom keys.
  unless (intIsRepMapCompleteForSet repMap (IntMap.keysSet $ mol ^. #atoms))
    . throwM
    $ MolLogicException
      "reIndexMoleculeLayer"
      "The remapping of indices is not complete for the atom indices."

  -- Check for completeness of the replacement map for the bond matrix.
  unless (isRepMapCompleteforBondMatrix repMap (mol ^. #bonds)) . throwM $
    MolLogicException
      "reIndexMoleculeLayer"
      "The remapping of indices is not complete for the bond data."

  return $
    mol
      -- Update the atoms indices of a molecule with new indices.
      & #atoms
      %~ intReplaceMapKeys repMap
      -- Update keys and values (IntSet) of the bond type data structure.
      & #bonds
      %~ replaceBondMatrixInds repMap
      -- Update the selection in the groups.
      & #group
        % each
        % #atoms
      %~ intReplaceSet repMap

----------------------------------------------------------------------------------------------------

-- | Given a 'IntMap' key (representing an 'Atom'), determine in which deeper layer
-- ('Molecule.subMol') the 'Atom' is.
findAtomInSubMols ::
  -- | 'Atom' to find in the groups.
  Int ->
  -- | Annotated deeper ONIOM layers in an 'IntMap'
  IntMap Molecule ->
  -- | The 'IntMap' key aka group number in which the atom has been found.
  Maybe Int
findAtomInSubMols atomKey annoGroups =
  fst
    <$> ( IntMap.lookupMin
            . IntMap.filter (== True)
            . IntMap.map (\mol -> atomKey `IntMap.member` (mol ^. #atoms))
            $ annoGroups
        )

----------------------------------------------------------------------------------------------------

-- | Looks to which group an atom is assigned. Fails if the atom is not assigned to any group,
-- which violates the assumptions of 'Molecule'.
findAtomInGroup :: MonadThrow m => Int -> IntMap Group -> m Int
findAtomInGroup ind groups = do
  let matchingGroups = IntMap.filter (\f -> ind `IntSet.member` (f ^. #atoms)) groups
  thisGroup <-
    maybe2MThrow (localExc "Atom not found in any group.")
      . IntMap.minViewWithKey
      $ matchingGroups
  let groupNum = thisGroup ^. _1 % _1
  return groupNum
  where
    localExc = MolLogicException "findAtomInGroup"

----------------------------------------------------------------------------------------------------

-- | Get the number of electrons for a 'Foldable' of 'Atom's with a given charge.
getNElectrons ::
  Foldable f =>
  -- | The 'Atom's to check.
  f Atom ->
  -- | Charge.
  Int ->
  -- | Number of electrons at the given charge.
  Int
getNElectrons atoms charge' = getNeutralElectrons atoms - charge'

-- | Get the number of electrons in a 'Foldable' of 'Atom's, assuming no charge.
getNeutralElectrons ::
  Foldable f =>
  -- | The 'Atom's to check.
  f Atom ->
  -- | Number of electrons at no charge.
  Int
getNeutralElectrons = getSum . foldMap (Sum . elementToAN . (^. #element))

----------------------------------------------------------------------------------------------------

-- | Given the atoms of a model system molecule, find all atoms @LAC@, that have been capped with a
-- link atom.
getCappedAtoms :: IntMap Atom -> IntMap Atom
getCappedAtoms atoms' =
  let linkAtoms = IntMap.filter (isAtomLink . isLink) atoms'
      cappedIndices =
        IntSet.fromList . fmap snd . IntMap.toAscList . fromMaybe IntMap.empty $
          traverse
            (\la -> la ^? #isLink % _IsLink % #linkModelPartner)
            linkAtoms
   in IntMap.restrictKeys atoms' cappedIndices

----------------------------------------------------------------------------------------------------

-- | From the complete data structure of the 'Molecule', get subtree of the specific layer.
-- now the new top layer.
getMolByID :: MonadThrow m => Molecule -> MolID -> m Molecule
getMolByID mol Seq.Empty = return mol
getMolByID mol (i :<| is) =
  let subMols = mol ^. #subMol
   in if IntMap.null subMols
        then
          throwM $
            MolLogicException
              "getMolByID"
              "Maximum recursion depth of your molecule reached and no layers left.\
              \ Cannot go deeper into the structure."
        else case subMols IntMap.!? i of
          Just molDeeperLayer -> getMolByID molDeeperLayer is
          Nothing ->
            throwM $
              MolLogicException
                "getMolByID"
                "Could not find a molecule with this MolID.\
                \ Key not found in the submolecule map."

----------------------------------------------------------------------------------------------------

-- | This generator takes a 'MolID' and generates an Lens to access this molecule.
molIDLensGen :: MolID -> Optic' An_AffineTraversal NoIx Molecule Molecule
molIDLensGen molID' =
  let layerLenses = fmap (\subMolIx -> #subMol % ix subMolIx) molID'
   in -- identityMolLens = castOptic simple -- :: Lens' a Molecule
      castOptic @An_AffineTraversal $ foldl (%) (castOptic @An_AffineTraversal simple) layerLenses

----------------------------------------------------------------------------------------------------

-- | From the full ONIOM tree, obtain the calculation context of a specified calculation and the
-- system (single layer) on which to perform the calculation. In case of a fragment calculation, the
-- returned 'OniomLayer' will not be a normal layer of the ONIOM tree, but the isolated fragment.
-- Also, the calculation input will be adapted to match the electronic structure of the fragment in
-- this case. The calculations for the layer are empty and are not meant to be used.
--
-- __This function should be used whenever a calculation is to be carried out, a correctly polarised
-- layer or fragment is required.__ It will take care of proper polarisation.
getCalcByID :: MonadThrow m => Molecule -> CalcID -> m (CalcInput, Maybe CalcOutput, OniomLayer)
getCalcByID mol CalcID {molID, calcK, frag} = do
  unpolLayer <-
    maybe2MThrow
      (localExc $ "Could not find layer with ID " <> Text.unpack (molID2OniomHumanID molID))
      $ mol ^? molIDLensGen molID
  CalcContext {input, output, gmbe} <-
    maybe2MThrow
      (localExc $ "No calculations defined for layer " <> Text.unpack (molID2OniomHumanID molID))
      $ unpolLayer ^? #calculations % _Just % niveauLensGen calcK

  case (frag, gmbe) of
    -- Errors. Either a GMBE calculation has been requested and there is not GMBE calculation at
    -- this layer, or there is a GMBE calculation at this layer, but no GMBE calculation has been
    -- requested.
    (Just _, Nothing) -> throwM . localExc $ "A fragment was given, but at this layer there is only a monolithic calculation."
    (Nothing, Just _) -> throwM . localExc $ "No fragment was given, but at this layer there is a GMBE calculation."
    -- A normal monolithic calculation, that can use the input and output directly without
    -- modifications.
    (Nothing, Nothing) -> do
      -- If requested, ensure layer polarisation from all layers above.
      layerOfInterest <- case input ^. #embedding of
        Mechanical -> return (OniomLayer unpolLayer)
        Electronic mEEScales -> do
          let eeScales = fromMaybe mempty mEEScales
          polLayer <- getPolarisationCloudFromAbove mol molID eeScales
          return polLayer
      return (input, output, layerOfInterest)
    -- GMBE calculation; the auxiliary monomer is carved out of the layer of interest on the fly and
    -- valid calculation contexts will be created.
    (Just am@AuxMonomer {constGroups}, Just g) -> do
      -- Isolating the auxiliary monomer.
      amAtoms <- getAuxMonomerAtoms (unpolLayer ^. #group) am
      maxAtomKey <- getMaxAtomIndex mol
      (OniomLayer newAM) <- newSubLayer maxAtomKey (OniomLayer unpolLayer) amAtoms Nothing Nothing
      let molAM = newAM {calculations = Nothing}

      -- Make a valid calculation context for the newly created auxiliary monomer. Those are created
      -- on the fly from the generic input and adapted to match the electronic structure of the
      -- auxiliary monomer.
      let amPermaDir = getDirPathAbs (input ^. #permaDir) </> Path.relDir ("GMBE_Groups" <> groupPrinter constGroups)
          amScratchDir = getDirPathAbs (input ^. #scratchDir) </> Path.relDir ("GMBE_Groups" <> groupPrinter constGroups)
          amInput =
            input
              & #qMMMSpec % _QM % #charge .~ (am ^. #charge)
              & #qMMMSpec % _QM % #mult .~ ((am ^. #excEl) + 1)
              & #permaDir .~ JDirPathAbs amPermaDir
              & #scratchDir .~ JDirPathAbs amScratchDir
          amOutput = g ^? ix am % _2 % _Just

      -- Fragment embedding: use 'getPolarisationCloudFromAbove' to transitively form a system
      -- with inter-fragment polarisation
      fragEmbedding <-
        maybe2MThrow
          (localExc "A fragment calculation is beeing processed but no fragment method specification was found.")
          $ amInput ^? #fragmentMethods % _Just % #embedding
      case fragEmbedding of
        -- No interfragment embedding requested, but we still need to check if the higher layers
        -- shall polarise. This works easily by placing the fragment at the hierarchical level where
        -- the entire GMBE layer would normally be.
        Nothing -> case input ^. #embedding of
          Mechanical -> return (amInput, amOutput, OniomLayer molAM)
          Electronic mEEScales -> do
            let eeScales = fromMaybe mempty mEEScales
                auxMonoAtLayer = mol & molIDLensGen molID .~ molAM
            polLayer <- getPolarisationCloudFromAbove auxMonoAtLayer molID eeScales
            return (amInput, amOutput, polLayer)
        -- Inter fragment polarisation has been requested, but is tricky to model. We create the
        -- AuxMonomer transitively below the GMBE layer and then call the polarisation function on
        -- this only temporarily existing layer. To ensure inter-fragment embedding by the
        -- multipoles of the correct level of theory, we modify the multipoles of the full GMBE
        -- layer, also transitively.
        Just _ -> do
          -- Perform the described adjustements for valid embedding behaviour. Attempts the
          -- multipole transfer, but if this fails, this just means this might be the first GMBE
          -- iteration. In this case the fragments are NOT embedded in a cloud even if requested.
          -- The caller must be aware of this logic and potentially perform multiple iterations.
          let layerOfInterestModOrigOut = case calcK of
                Original -> unpolLayer
                Inherited -> unpolLayer & #calculations % _Just % #original % #output .~ output
              layerWithCorrectPoles = fromMaybe unpolLayer $ multipoleTransfer layerOfInterestModOrigOut
              gmbeLayerWithFragmentChild = layerWithCorrectPoles & #subMol .~ IntMap.singleton 0 molAM
              molReadyToPolarise = mol & molIDLensGen molID .~ gmbeLayerWithFragmentChild

          -- Check the embedding of the entire GMBE layer and polarise accordingly.
          case input ^. #embedding of
            -- The entire GMBE layer is only embedded mechanically, but the fragments are embedded
            -- in the field of each other. Take the unpolarised layer and use this for polarisation.
            Mechanical -> do
              polLayer <- getPolarisationCloudFromAbove gmbeLayerWithFragmentChild (Seq.singleton 0) Empty
              return (amInput, amOutput, polLayer)
            -- The entire GMBE layer has electronic embedding AND we need inter-fragment embedding.
            -- Insert the AuxMonomer as a single transitive layer below the unpolarised layer of
            -- interest and then polarise everything.
            Electronic mEEScales -> do
              let eeScales = fromMaybe mempty mEEScales
              OniomLayer polLayer <- getPolarisationCloudFromAbove molReadyToPolarise (molID |> 0) eeScales
              return (amInput, amOutput, OniomLayer polLayer)
  where
    localExc = MolLogicException "getCalcByID"
    groupPrinter g = foldMap (\x -> "-" <> show x) . IntSet.toAscList $ g

----------------------------------------------------------------------------------------------------

-- | Generates a lens for calculation ID in a molecule. Fragment doesn't play a role.
calcIDLensGen :: CalcID -> Optic' An_AffineTraversal NoIx Molecule (Maybe CalcContext)
calcIDLensGen (CalcID molID calcK _) = castOptic @An_AffineTraversal (molIDLensGen molID) % #calculations % _Just % levelLens
  where
    levelLens = case calcK of
      -- Wrap the original lens with a maybe value to get a consistent type.
      Original ->
        lens
          (Just . original)
          ( \s b' -> case b' of
              Nothing -> s
              Just b -> s {original = b}
          )
      Inherited -> #inherited

----------------------------------------------------------------------------------------------------

-- | Separates a new subsystem from the current molecule layer. Covalent bonds that were cut, will be
-- capped with link atoms. The following behaviour is employed for the constructor fields of the
-- sublayer 'Molecule':
--
--   * 'Molecule.comment': String that describes which atoms of which layer were kept to arrive at
--     the result. Not of any deeper meaning and can safely be overriden later.
--   * 'Molecule.atoms': The atoms that are kept will have no multipoles. Dummies will be removed.
--     Link atoms of the parent layer will become normal atoms, if they were part of the new sub
--     layer. New link atoms will be inserted at the largest used atom key + 1.
--   * 'Molecule.bonds': The ones from the top layer restricted to the indices of the new layer.
--     Link atoms will of course get new bonds.
--   * 'Molecule.subMol': there will be no sublayers.
--   * 'Molecule.energyDerivatives': Everything will be 'Nothing'.
--   * 'Molecule.calculations': Wont be set: 'Nothing'.
--   * 'Molecule.jacobian': Will be calculated according to 'getJacobian' and respect the
--     \(g\)-values of the link atoms
--   * 'Molecule.neighbourlist': the neighbourlist of the parent layer will be restricted to atoms
--     of the new sublayer. Link atoms will be added as neighbours of the capped atoms. This is an
--     approximation, but helpful for electronic embedding. See also 'addLinksToNeighbourList'.
{-# SCC newSubLayer #-}
newSubLayer ::
  MonadThrow m =>
  -- | The maximum index of all atoms in the full system.
  Int ->
  -- | Input layer, from which a new layer will be created.
  OniomLayer ->
  -- | Selection of the atoms to keep for the sublayer. Must be a subset of the input layer.
  IntSet ->
  -- | Scaling radius $\(g\). Will be calculated from covalent radii of the involved atoms, if not
  -- given. See 'createLinkAtom'.
  Maybe Double ->
  -- | Settings for the insertion of capping atoms. Specifies the chemical element, that is being
  -- used for capping, the 'Atom.label' of the new link 'Atom', and the 'Atom.ffType' of the link
  -- atom.
  Maybe (Element, Text, FFType) ->
  -- | A new sublayer created from the supplied layer.
  m OniomLayer
newSubLayer maxAtomIndex (OniomLayer mol) newLayerInds covScale capAtomInfo = do
  -- Before doing anything, make sure the molecule is sane.
  _ <- checkMolecule mol

  -- Check if the indices for the new layer are usable aka non empty
  when (IntSet.null newLayerInds) . throwM $
    MolLogicException
      "newSubLayer"
      "Trying to create an empty layer. This is not possible with this function."

  -- Check if the indices for the new layer are strictly a subset of the old layer.
  let oldLayerAtomInds = IntMap.keysSet $ mol ^. #atoms
  unless (newLayerInds `IntSet.isSubsetOf` oldLayerAtomInds) . throwM $
    MolLogicException
      "newSubLayer"
      "The new layer indices are either reffering to atoms, that are not there."

  let atoms' = mol ^. #atoms
      -- Convert the capping atom defaults to individual values.
      (capAtomElement, capAtomLabel, capAtomFFType) = case capAtomInfo of
        Nothing -> (Nothing, Nothing, Nothing)
        Just (caElement, caLabel, caFFType) -> (Just caElement, Just caLabel, Just caFFType)

      -- Define some default data for the new sublayer molecule.
      slComment =
        "Sublayer of "
          <> (mol ^. #comment)
          <> " keeping the indices "
          <> tshow (IntSet.toList newLayerInds)
          <> "."

      -- Just the atoms from the old layer that are kept but no link atoms added yet.
      slAtomsToKeep = (mol ^. #atoms) `IntMap.restrictKeys` newLayerInds

      -- Groups of the new layer filtered by the new atom indices and groups that became empty
      -- removed.
      slGroups =
        let origGroups = mol ^. #group
            groupsRestricted = origGroups & each % #atoms %~ IntSet.intersection newLayerInds
            nonEmptyGroups = IntMap.filter (\f -> not . IntSet.null $ f ^. #atoms) groupsRestricted
         in nonEmptyGroups

      -- Bonds from the old layer to keep but no bonds for link atoms added yet.
      slBondsToKeep = cleanBondMatByAtomInds (mol ^. #bonds) newLayerInds
      slSubMol = IntMap.empty
      slEnergyDerivatives = def :: EnergyDerivatives

      -- Create a set of pairs of atoms, which have cut bonds due to sublayer isolation. The first
      -- part of the tuple will have the sublayer index and the second one the top layer index.
      cutAtomPairs :: HashSet (Int, Int)
      cutAtomPairs =
        HashMap.foldlWithKey'
          ( \accPairs (ixO, ixT) val ->
              let isOinSL = ixO `IntSet.member` newLayerInds
                  isTinSL = ixT `IntSet.member` newLayerInds
               in case (isBond val, isOinSL, isTinSL) of
                    (True, True, False) -> HashSet.insert (ixO, ixT) accPairs
                    _ -> accPairs
          )
          HashSet.empty
          (mol ^. #bonds)

  -- This creates all necessary capping atoms for a new sublayer. The structure maps from the index
  -- of an atom in the new sublayer, to its capping link atoms.
  cappingLinkAtoms <-
    HashSet.foldl'
      ( \accLinkAtomMap' (ixSL, ixTL) -> do
          accLinkAtomMap <- accLinkAtomMap'
          -- Lookup the sublayer atom, which needs a cap.
          slCappedAtom <- case atoms' IntMap.!? ixSL of
            Just a -> return a
            Nothing ->
              throwM
                . MolLogicException "newSubLayer"
                $ "Lookup for atom with index "
                  <> show ixSL
                  <> " failed."
          -- Lookup the top layer atom, which has been removed and for which the cap is introduced.
          tlRemovedAtom <- case atoms' IntMap.!? ixTL of
            Just a -> return a
            Nothing ->
              throwM
                . MolLogicException "newSubLayer"
                $ "Lookup for atom with index "
                  <> show ixTL
                  <> " failed."
          -- Create a new link atom, for the current pair.
          newLinkAtom <-
            Seq.singleton
              <$> createLinkAtom
                covScale
                capAtomElement
                capAtomLabel
                capAtomFFType
                (ixSL, slCappedAtom)
                (ixTL, tlRemovedAtom)
          return . IntMap.insertWith (<>) ixSL newLinkAtom $ accLinkAtomMap
      )
      (return IntMap.empty)
      cutAtomPairs

  let -- Create an intermediate sublayer molecule, which does not yet have link atoms but is
      -- otherwise cleaned of the information from the top layer.
      slIntermediateMol =
        Molecule
          { comment = slComment,
            atoms = slAtomsToKeep,
            bonds = slBondsToKeep,
            subMol = slSubMol,
            group = slGroups,
            energyDerivatives = slEnergyDerivatives,
            calculations = Nothing,
            jacobian = Nothing,
            neighbourlist = fmap restrictNeighbours $ mol ^. #neighbourlist
          }

  -- Add all capping atoms to the sublayer. Goes through all atoms that need to be capped, while the
  -- inner loop in addLinksFromList goes through all link atoms, that need to be added to a single
  -- atom that need to be capped.
  (_, subLayerWithLinkAdded) <-
    IntMap.foldlWithKey'
      ( \accIndxAndMol' slOrigin linkAtomList -> do
          (accIndx, accMol) <- accIndxAndMol'
          (newMaxIndx, newMol) <- addLinksFromList accIndx slOrigin accMol linkAtomList
          return (newMaxIndx, newMol)
      )
      (pure (maxAtomIndex, slIntermediateMol))
      cappingLinkAtoms

  -- Construction of the finaly sub molecule
  let topLayerAtoms = mol ^. #atoms
      subLayerAtoms = subLayerWithLinkAdded ^. #atoms
  slJacobianD <- getJacobian topLayerAtoms subLayerAtoms
  let slJacobian = Massiv.computeAs Massiv.S . Massiv.setComp Par $ slJacobianD

  let -- Add the Jacobian to the otherwise final sublayer and add the sublayer to the input system.
      subLayerWithJacobian = subLayerWithLinkAdded & #jacobian ?~ MatrixS slJacobian

  return . OniomLayer $ subLayerWithJacobian
  where
    -- Add a list of capping link atoms to an existing molecule and a single bond partner. Usually
    -- this will be just one link atom but it could also add multiple caps to a single atom. Returns
    -- the index of the last link atom added and the molecule layer with all the links atoms added.
    addLinksFromList :: MonadThrow m => Int -> Int -> Molecule -> Seq Atom -> m (Int, Molecule)
    addLinksFromList maxAtomIndex' linkBondPartner mol' linkSeq =
      foldl'
        ( \accIndxAndMol' linkAtom -> do
            (accIndx, accMol) <- accIndxAndMol'
            let newAtomIndex = accIndx + 1
            linkGroupID <- findAtomInGroup linkBondPartner $ mol' ^. #group
            molAtomAdded <- addAtomWithKeyLocal accMol newAtomIndex (Just linkGroupID) linkAtom
            molAtomAndBondAdded <- changeBond Single molAtomAdded (linkBondPartner, newAtomIndex)
            -- Process the neighbourlist before the link atom has been updated. addAtomWithKeyLocal
            -- invalidates the neighbourList but as we are exclusively processing link atoms here,
            -- we can use this knowledge to update an old neighbourList instead of discarding it.
            newNL <- traverse (addLinksToNeighbourList (newAtomIndex, linkAtom)) $ accMol ^. #neighbourlist
            let molFinalAcc = molAtomAndBondAdded & #neighbourlist .~ newNL
            return (newAtomIndex, molFinalAcc)
        )
        (pure (maxAtomIndex', mol'))
        linkSeq

    -- Restrict a neighbourlist to atoms of the new layer.
    restrictNeighbours :: NeighbourList -> NeighbourList
    restrictNeighbours nl = IntMap.map (IntSet.intersection newLayerInds) $ IntMap.restrictKeys nl newLayerInds

----------------------------------------------------------------------------------------------------

-- | Function to create a capping link atom from an atom to keep and an atom, that has been cut
-- away. Following special behaviours are used here:
--
--   * The scaling factor \(g\) (see below) might be given. If it is not given, a default value will
--     be used
--   * A chemical 'Element' might be given for the new link 'Atom' ('Atom.element') and if not
--     specified, a hydrogen atom will be used.
--   * A 'Atom.label' might be given. If not, the 'Atom.label' will be empty.
--   * A force-field type might be given. Defaults to 'XYZ'.
--
-- See also 'calcLinkCoords'.
--
-- The position of the link atom is calculated as:
-- \[
--     \mathbf{r}^\mathrm{LA} =
--       \mathbf{r}^\mathrm{LAC} + g ( \mathbf{r}^\mathrm{LAH} - \mathbf{r}^\mathrm{LAC})
-- \]
-- where the scaling factor \(g\) will be calculated from a ratio of covalent radii, if not
-- specified:
-- \[
--     g = \frac{r^\mathrm{cov, LAC} + r^\mathrm{cov, LA}}{r^\mathrm{cov, LAH} + r^\mathrm{cov, LAC}}
-- \]
-- With:
--
--   * \( \mathbf{r}^\mathrm{LA} \): the coordinates of the created link atom
--   * \( \mathbf{r}^\mathrm{LAH} \): the coordinates of the atom that has been removed by creating the
--     new layer
--   * \( \mathbf{r}^\mathrm{LAC} \): the coordinates of the atom, that will be capped by the link atom
--   * \( g \): the scaling factor for the position of the link atom.
createLinkAtom ::
  MonadThrow m =>
  -- | Scaling factor \(g\) for the position of the link atom.
  Maybe Double ->
  -- | Chemical element for link atom to create.
  Maybe Element ->
  -- | Textual label of the link atom.
  Maybe Text ->
  -- | Force field type of the link atom.
  Maybe FFType ->
  -- | 'Atom' and its key to cap with the link atom.
  (Int, Atom) ->
  -- | 'Atom' and its key that has been cut away and needs to be replaced by the
  --   link atom, that will be created.
  (Int, Atom) ->
  -- | Created link atom.
  m Atom
createLinkAtom gScaleOption linkElementOption label' fftype (cappedKey, cappedAtom) (removedKey, removedAtom) =
  do
    let linkElement = fromMaybe H linkElementOption
        cappedElement = cappedAtom ^. #element
        cappedCoords = Massiv.delay . getVectorS $ cappedAtom ^. #coordinates
        cappedCovRadius = covalentRadii Map.!? cappedElement
        removedElement = removedAtom ^. #element
        removedCoords = Massiv.delay . getVectorS $ removedAtom ^. #coordinates
        removedCovRadius = covalentRadii Map.!? removedElement
        linkCovRadius = covalentRadii Map.!? linkElement
        gFactorDefault :: Maybe Double
        gFactorDefault = do
          cappedCovRadius' <- cappedCovRadius
          removedCovRadius' <- removedCovRadius
          linkCovRadius' <- linkCovRadius
          return $ (cappedCovRadius' + linkCovRadius') / (removedCovRadius' + cappedCovRadius')

    gScaleToUse <- case (gScaleOption, gFactorDefault) of
      (Just gScale, _) -> return gScale
      (Nothing, Just gScale) -> return gScale
      (Nothing, Nothing) ->
        throwM
          . MolLogicException "createLinkAtom"
          $ "Can not find covalent radius of one of these elements "
            <> show (linkElement, cappedElement, removedElement)
            <> " and no scaling factor g has been given."

    linkCoordinates <- calcLinkCoords cappedCoords removedCoords gScaleToUse

    let newLinkAtom =
          Atom
            { element = linkElement,
              label = fromMaybe "" label',
              isLink =
                IsLink
                  LinkAtomInfo
                    { linkModelPartner = cappedKey,
                      linkRealPartner = removedKey,
                      linkGFactor = gScaleToUse
                    },
              isDummy = False,
              ffType = fromMaybe FFXYZ fftype,
              coordinates = VectorS . computeS $ linkCoordinates,
              multipoles = def,
              formalCharge = 0,
              formalExcEl = 0
            }

    return newLinkAtom

----------------------------------------------------------------------------------------------------

-- | Calculate the new coordinates of a link atom as:
-- \[
--     \mathbf{r}^\mathrm{LA} =
--     \mathbf{r}^\mathrm{LAC} + g ( \mathbf{r}^\mathrm{LAH} - \mathbf{r}^\mathrm{LAC})
-- \]
-- With:
--
--   * \( \mathbf{r}^\mathrm{LA} \): the coordinates of the created link atom
--   * \( \mathbf{r}^\mathrm{LAH} \): the coordinates of the atom that has been removed by creating
--     the new layer
--   * \( \mathbf{r}^\mathrm{LAC} \): the coordinates of the atom, that will be capped by the link
--     atom
calcLinkCoords ::
  (MonadThrow m, Numeric r a) =>
  -- | Coordinates of the atom being capped.
  Vector r a ->
  -- | Coordinates of the atom being removed.
  Vector r a ->
  -- | The scaling factor g.
  a ->
  m (Vector r a)
calcLinkCoords cappedAtomCoords removedAtomCoords gScale = do
  diffVec <- removedAtomCoords .-. cappedAtomCoords
  let scaledDiffVec = gScale *. diffVec
  cappedAtomCoords .+. scaledDiffVec

----------------------------------------------------------------------------------------------------

-- | Adds an 'Atom' to a specified 'Molecule' layer within the full molecular system. The
-- 'Int' key of the new atom will be maximum index used in the full system + 1. The atom will also
-- be added to all deeper layers in the tree. The new atom will __not__ be added to the
-- 'Molecule.neighbourlist', 'Molecule.bonds' and 'Molecule.groups'. __Therefore, this function
-- produces invalid molecules__.
--
-- The 'Int' returned is the 'IntMap' key of the newly added atom, which help to keep track of
-- maximum atom index, when adding atoms.
{-# WARNING addAtom "This function produces inconsistent molecules. The atom won't be added to the neighbourlist, nor the groups." #-}
addAtom :: MonadThrow m => Molecule -> MolID -> Atom -> m (Int, Molecule)
addAtom fullMol molID' atom = do
  -- Before doing anything, make sure the whole molecule is sane.
  _ <- checkMolecule fullMol

  -- The largest atom index in the complete molecule.
  maxAtomIndex <- case IntSet.maxView . getAtomIndices $ fullMol of
    Nothing -> throwM $ MolLogicException "addAtom" "Your molecule appears to have no atoms"
    Just (maxKey, _) -> return maxKey

  -- Get the layer from which to start adding the atom by a MolID.
  mol <- case fullMol ^? molIDLensGen molID' of
    Nothing ->
      throwM
        . MolLogicException "addAtom"
        $ ( "Requested to add an atom to layers below layer with ID "
              <> show molID'
              <> " but no layer with this id could be found."
          )
    Just m -> return m

  -- Find the largest index of this layer.
  let newAtomIndex = maxAtomIndex + 1

  -- Insert the new atom recursively into all layers below and including the selected one.
  newMol <- goInsert mol newAtomIndex atom

  -- Return the full molecular system with the sublayers updated.
  let newFullMol = fullMol & molIDLensGen molID' .~ newMol

  return (newAtomIndex, newFullMol)
  where
    goInsert :: MonadThrow m => Molecule -> Int -> Atom -> m Molecule
    goInsert mol' newInd' atom' = do
      let atoms' = mol' ^. #atoms

      -- Check if the key for the new atom would be new/unique.
      when (newInd' `IntMap.member` atoms')
        . throwM
        . MolLogicException "addAtom"
        $ "Cannot add atom with key "
          <> show newInd'
          <> " to the current molecule layer. The key already exists."

      -- Add the atom with the given index to the current layer.
      let thisLayerMolAtomAdded = mol' & #atoms %~ IntMap.insert newInd' atom'
          subMols = thisLayerMolAtomAdded ^. #subMol

      -- Add recursively also to all deeper layers.
      if IntMap.null subMols
        then return thisLayerMolAtomAdded
        else do
          updatedSubMols <- traverse (\m -> goInsert m newInd' atom') subMols
          return $ thisLayerMolAtomAdded & #subMol .~ updatedSubMols

----------------------------------------------------------------------------------------------------

-- | This function adds an atom with a given key to all layers below the 'Molecule' which was given
-- as input. This can cause problems if the key, that is given is already present. If it is already
-- present in the layers visible to this function (current one and below), an exception will be
-- thrown. But if some sublayer is given to this function and you specify a key to this function,
-- which is present in layers above (not visible to this function) but not in the layers visible,
-- you will end up with an inconsistent molecule.
--
-- The new atom will be added to a group to keep consistent with 'Molecule' data structure
-- assumptions. The group ID needs to be specified or a new group will be added.
addAtomWithKeyLocal ::
  MonadThrow m =>
  -- | Molecule to which an atom will be added.
  Molecule ->
  -- | The key the newly added atom shall have. Must not be used in any layer yet.
  Int ->
  -- | A group ID to which the atom should be added. If none is given, a new group will be created.
  Maybe Int ->
  Atom ->
  m Molecule
addAtomWithKeyLocal mol key groupID atom = do
  -- Before doing anything, make sure that the input is sane.
  _ <- checkMolecule mol

  -- Find the largest group ID, that is in use yet.
  let groupKeys = IntMap.keysSet $ mol ^. #group
      unwrapKey = maybe2MThrow (localExc "No groups in the top layer.") . fmap fst
  groupIDTopMax <- unwrapKey . IntSet.maxView $ groupKeys
  groupIDMax <-
    molFoldl
      ( \maxGroupID thisMol -> do
          thisGroupIDMax <- unwrapKey . IntMap.lookupMax $ thisMol ^. #group
          maxGroupID >>= \accID -> return $ max thisGroupIDMax accID
      )
      (pure groupIDTopMax)
      mol

  -- Construct the modified molecule recursively.
  newMol <- goInsert mol key (fromMaybe groupIDMax groupID) atom
  return newMol
  where
    localExc = MolLogicException "addAtomWithKeyLocal"

    goInsert :: MonadThrow m => Molecule -> Int -> Int -> Atom -> m Molecule
    goInsert mol' newKey' groupID' atom' = do
      let atoms' = mol' ^. #atoms

      when (newKey' `IntMap.member` atoms')
        . throwM
        . MolLogicException "addAtomWithKeyLocal"
        $ "Cannot add atom with key "
          <> show newKey'
          <> " to the current molecule layer. The key already exists."

      -- Add the atom with the given index to the current layer and invalidate all data, that are
      -- not valid anymore.
      let singularGroup =
            IntMap.singleton groupID' $
              Group
                { label = mempty,
                  chain = Nothing,
                  atoms = IntSet.singleton newKey',
                  charge = atom ^. #formalCharge,
                  excEl = atom ^. #formalExcEl
                }
          thisLayerMolAtomAdded =
            mol
              -- Invalidate data.
              & #energyDerivatives .~ def
              & #jacobian .~ Nothing
              & #neighbourlist .~ mempty
              -- Add data.
              & #atoms %~ IntMap.insert newKey' atom'
              & #group
                %~ IntMap.unionWith
                  ( \orig new ->
                      orig & #atoms %~ IntSet.union (new ^. #atoms)
                        & #charge %~ (+ (new ^. #charge))
                        & #excEl %~ (+ (new ^. #excEl))
                  )
                  singularGroup

          subMols = thisLayerMolAtomAdded ^. #subMol

      -- Add recursively also to all deeper layers.
      if IntMap.null subMols
        then return thisLayerMolAtomAdded
        else do
          updateSubMols <- traverse (\m -> goInsert m newKey' groupID' atom') subMols
          return $ thisLayerMolAtomAdded & #subMol .~ updateSubMols

----------------------------------------------------------------------------------------------------

-- | Removes an 'Atom' specified by its index key from the 'Molecule' and all deeper layers.
{-# WARNING removeAtom "This function produces inconsistent molecules. The atom won't be removed from the neighbourlist, nor the groups." #-}
removeAtom :: MonadThrow m => Molecule -> Int -> m Molecule
removeAtom mol atomInd = do
  -- Before doing anything check if the molecule is sane.
  _ <- checkMolecule mol

  -- Get information about the atom of interest.
  let atoms' = mol ^. #atoms
      atomExists = atomInd `IntMap.member` atoms'
      atomIsLink = case atoms' IntMap.!? atomInd of
        Just a -> isAtomLink $ a ^. #isLink
        Nothing -> False

  unless atomExists
    . throwM
    . MolLogicException "removeAtom"
    $ "Cannot remove atom with key "
      <> show atomInd
      <> " from the top molecule layer. The key does not exist."

  return $ goRemove mol (atomInd, atomIsLink)
  where
    goRemove :: Molecule -> (Int, Bool) -> Molecule
    goRemove mol' (atomInd', wasLink) =
      -- Get information about the atom to remove in the current layer.
      let atoms' = mol' ^. #atoms
          atomIsLink = case atoms' IntMap.!? atomInd' of
            Just a -> isAtomLink $ a ^. #isLink
            Nothing -> False

          -- If it did not change if this atom is a link atom, remove it from this layer.
          -- Otherwise it must be a different atom now and we dont touch it.
          atomsUpdated = if atomIsLink == wasLink then atomInd' `IntMap.delete` atoms' else atoms'

          -- Remove bonds involving this atom if there was no change wether this atom was a
          -- link atom..
          bonds' = mol' ^. #bonds
          bondsUpdated = removeBondsByAtomIndsFromBondMat bonds' (IntSet.singleton atomInd')

          -- Update the current layer with bonds and atoms cleaned.
          thisLayerUpdated = mol' & #bonds .~ bondsUpdated & #atoms .~ atomsUpdated

          -- Get the submolecules and update them lazily.
          subMols = thisLayerUpdated ^. #subMol
          subMolsUpdated = IntMap.map (\m -> goRemove m (atomInd', wasLink)) subMols
       in if IntMap.null subMols
            then thisLayerUpdated
            else thisLayerUpdated & #subMol .~ subMolsUpdated

----------------------------------------------------------------------------------------------------

-- | Adds\/removes a bond recursively to\/from a 'Molecule' and its deeper layers. Two atom keys are
-- specified, to indicate between which 'Atom's a new bond shall be inserted/removed. If these
-- 'Atom's do not exist in the top layer, this function will fail. If the atoms do not exist in
-- deeper layers, no bond will be inserted/removed in the deeper layer. If a bond already
-- existed between those atoms, it will be updated with the new bond type.
--
-- If one of the two atoms was a link atom in the top layer and still is in the deeper layers, bonds
-- will be inserted/removed normally. If one the atoms was not a link atom in the top layer but
-- becomes a link atom in a deeper layer, bonds involving those atoms will not be touched.
changeBond :: MonadThrow m => BondOrder -> Molecule -> (Int, Int) -> m Molecule
changeBond bondOrder mol (at1, at2) = do
  -- Check sanity of the molecule before doin anything else.
  _ <- checkMolecule mol

  -- Check if origin and target key are present as atoms in the molecule at all.
  let atoms' = mol ^. #atoms
      atom1Exists = at1 `IntMap.member` atoms'
      atom2Exists = at2 `IntMap.member` atoms'
      -- These lookups are safe as long as they are lazy and the check if the atoms exist happens
      -- before those information are used.
      atom1IsLink = isAtomLink $ (atoms' IntMap.! at1) ^. #isLink
      atom2IsLink = isAtomLink $ (atoms' IntMap.! at2) ^. #isLink

  unless (atom1Exists && atom2Exists)
    . throwM
    . MolLogicException "changeBond"
    $ "Wanted to add a bond between atoms"
      <> show at1
      <> " and "
      <> show at2
      <> " but at least one of those atoms does not exist in the top layer."

  -- If checks are passed, start with updating the current layer and work all the way down.
  return $ goDeepAddRemove bondOrder mol ((at1, atom1IsLink), (at2, atom2IsLink))
  where
    -- Update the deeper layers with a different check.
    goDeepAddRemove :: BondOrder -> Molecule -> ((Int, Bool), (Int, Bool)) -> Molecule
    goDeepAddRemove bondOrder' mol' ((at1', wasLink1), (at2', wasLink2)) =
      -- Check if both origin and target exist and if they are link atom.
      let atoms' = mol' ^. #atoms
          originExitst = at1' `IntMap.member` atoms'
          targetExists = at2' `IntMap.member` atoms'
          originIsLink = case atoms' IntMap.!? at1' of
            Just a -> isAtomLink $ a ^. #isLink
            Nothing -> False
          targetIsLink = case atoms' IntMap.!? at2' of
            Just a -> isAtomLink $ a ^. #isLink
            Nothing -> False

          -- Choose the update function depending wether to remove or add bonds.
          updateFunction = case bondOrder' of
            None -> removeBondFromBondMat
            bo -> \bm ot -> addBondToBondMat bm ot bo

          -- Update the bonds of this layer if everything is fine. Keep them as is otherwise.
          thisLayerBonds = mol' ^. #bonds
          thisLayerBondsUpdated =
            if all
              (== True)
              [originExitst, targetExists, wasLink1 == originIsLink, wasLink2 == targetIsLink]
              then updateFunction thisLayerBonds (at1', at2')
              else thisLayerBonds

          -- Update bonds data and invalidate the ones, that make no sense after bond updates.
          molUpdated =
            mol' & #bonds .~ thisLayerBondsUpdated
              & #jacobian .~ Nothing

          -- Lazily update the submolecules recursively.
          subMols = molUpdated ^. #subMol
          subMolsUpdated =
            IntMap.map (\m -> goDeepAddRemove bondOrder' m ((at1', wasLink1), (at2', wasLink2))) subMols
       in if IntMap.null subMols -- Update deeper layers only if required to end the recursion.
            then molUpdated
            else molUpdated & #subMol .~ subMolsUpdated

----------------------------------------------------------------------------------------------------

-- | Get the coordinates of the current molecule layer as matrix with \(n \times 3\), where the
-- cartesian coordinates for each atom are along the rows of the matrix. Fails if some of the
-- coordinate vectors have the wrong size.
{-# INLINE getCoordinatesAs3NMatrix #-}
getCoordinatesAs3NMatrix :: (Mutable r Double, MonadThrow m, Foldable f) => f Atom -> m (Matrix r Double)
getCoordinatesAs3NMatrix atoms = do
  let nAtoms = length atoms
  getCoordinatesAs' atoms (Sz (nAtoms :. 3))

-- | Get the coordinates of the current molecule layer as a linear vector.
{-# INLINE getCoordinatesAsVector #-}
getCoordinatesAsVector :: (Mutable r Double, MonadThrow m, Foldable f) => f Atom -> m (Vector r Double)
getCoordinatesAsVector atoms = do
  let nAtoms = length atoms
  getCoordinatesAs' atoms (Sz $ 3 * nAtoms)

-- | Primitive version of the @getCoordinatesAs@ functions.
{-# INLINE getCoordinatesAs' #-}
getCoordinatesAs' ::
  (Mutable r Double, MonadThrow m, Massiv.Index ix, Foldable f) =>
  f Atom ->
  Sz ix ->
  m (Array r ix Double)
getCoordinatesAs' atoms sz =
  let atomCoords = foldl' (\acc atom -> acc `sappend` getVectorS (atom ^. #coordinates)) Massiv.empty atoms
   in Massiv.resizeM sz . convert . setComp Par $ atomCoords

----------------------------------------------------------------------------------------------------

-- | Calculates a distance matrix from a linearised vector of cartesian coordinates.
{-# INLINE distMat #-}
distMat :: (MonadThrow m) => Molecule -> m (Matrix D Double)
distMat mol = do
  -- Reshapes the 3N vector into a 3 x N matrix. The x-axis represents atom indices and y-axis has
  -- cartesian coordinates.
  n3Vec <- getCoordinatesAs3NMatrix @S (mol ^. #atoms)
  let -- Get the number of atoms.
      nAtoms :: Int
      Sz (nAtoms :. _) = Massiv.size n3Vec

      -- The x-Axis is now a repetition of the atoms on the y-Axis (which were previously
      -- the x-axis) and z now stores the 3 compotents of the coordinates.
      xVec :: Array D Ix3 Double
      xVec = Massiv.expandOuter (Sz1 nAtoms) const n3Vec

      -- Swap x and y axsis and also have numbers of atoms ox x again.
      yVec :: Array D Ix3 Double
      yVec = Massiv.transposeInner xVec

      -- The xVec is now a structure with repetition of all atom coordinates along the x-Axis, the
      -- index of the atom on the y axis and the cartesian components on the z axis. The yVec has x-
      -- and y-axes transposed. Now overlap these two matrices and for each x and y components, a
      -- pair of R3 vectors on the z-axis is obtained. Those are folded to distances between the
      -- atoms pairs represented by x and y index.
      distances :: Matrix D Double
      distances =
        Massiv.map sqrt . Massiv.foldlInner (+) 0.0 . Massiv.map (** 2) $ Massiv.zipWith (-) xVec yVec
  return distances

----------------------------------------------------------------------------------------------------

-- | Distance matrix between two arbitrary sets of atoms.
arbDistMat :: (Traversable t, MonadThrow m) => t Atom -> t Atom -> m (Matrix D Double)
arbDistMat a b = do
  a3N <- compute @U <$> reshapeCoords a
  b3N <- compute @U <$> reshapeCoords b
  let a3Nb = Massiv.expandOuter @U @Ix3 (Sz1 $ length b) const a3N
      b3Na = Massiv.transposeInner . Massiv.expandOuter @U @Ix3 (Sz1 $ length a) const $ b3N
      distances =
        Massiv.map sqrt
          . Massiv.foldlInner (+) 0.0
          . Massiv.map (** 2)
          $ Massiv.zipWith (-) a3Nb b3Na
  return distances
  where
    reshapeCoords x = do
      flatVec <- Massiv.concatM 1 . fmap (getVectorS . coordinates) $ x
      Massiv.resizeM (Sz $ length x :. 3) flatVec

----------------------------------------------------------------------------------------------------

-- | Get mapping from dense arrays as in Massiv (starting at 0) to the sparse indexing used in the
-- 'Molecule' type with 'IntMap' and 'HashMap'.
{-# INLINE getDenseSparseMapping #-}
getDenseSparseMapping :: Manifest r Int => IntSet -> Vector r Int
getDenseSparseMapping atomIndices = Massiv.fromList Par . IntSet.toAscList $ atomIndices

----------------------------------------------------------------------------------------------------

-- | The opposite of 'getDenseSparseMapping'. Contains mapping from the original atom indices to the
-- dense indices.
{-# INLINE getSparseDenseMapping #-}
getSparseDenseMapping :: IntSet -> IntMap Int
getSparseDenseMapping atomIndices = IntMap.fromList $ S.zip (IntSet.toAscList atomIndices) [0 ..]

----------------------------------------------------------------------------------------------------

-- | Get the atoms of the current layer in a dense Massiv vector.
{-# INLINE getAtomsAsVector #-}
getAtomsAsVector :: Manifest r Atom => Molecule -> Vector r Atom
getAtomsAsVector mol = Massiv.fromList Par . fmap snd . IntMap.toAscList . (^. #atoms) $ mol

----------------------------------------------------------------------------------------------------

-- | Generate a neighbour list in a given search distance. This is an set of association of one
-- atom with all the ones, which are within a certain distance. The neighbours are free of
-- self-interaction. NOTE: contains a morally pure use of 'unsafePerformIO' in order to parallelise
-- the operation.
{-# INLINE neighbourList #-}
{-# SCC neighbourList #-}
neighbourList :: (MonadThrow m) => Double -> IntMap Atom -> m NeighbourList
neighbourList maxNeighbourDist atms = do
  let atIxs = IntMap.keysSet atms
  -- Gets the atom coordinates in Nx3 matrix representation.
  atomCoords <- getCoordinatesAs3NMatrix @S atms

  -- Find maximum and minumum value in each dimension.
  xCoords <- atomCoords <!? 0
  yCoords <- atomCoords <!? 1
  zCoords <- atomCoords <!? 2
  xMin <- Massiv.minimumM xCoords
  xMax <- Massiv.maximumM xCoords
  yMin <- Massiv.minimumM yCoords
  yMax <- Massiv.maximumM yCoords
  zMin <- Massiv.minimumM zCoords
  zMax <- Massiv.maximumM zCoords

  let -- Mapping from dense 0-based coordinates to the sparse indexing in the Molecule.
      atomIndexDenseToSparseMapping :: Vector P Int
      atomIndexDenseToSparseMapping = getDenseSparseMapping atIxs

      -- Mapping from the sparse indices as used in the mol to the dense ones as used in arrays.
      atomIndexSparseToDenseMapping :: IntMap Int
      atomIndexSparseToDenseMapping = getSparseDenseMapping atIxs

      -- Convert the original sparse atom IntMap to the dense representation as used in arrays.
      atomDenseMap :: IntMap Atom
      atomDenseMap = intReplaceMapKeys atomIndexSparseToDenseMapping atms

      -- Definition of the cell dimensions of an orthorhombic cell around the atoms
      _orthorhombicCellSize :: (Double, Double, Double)
      _orthorhombicCellSize@(cellSizeX, cellSizeY, cellSizeZ) =
        (xMax - xMin, yMax - yMin, zMax - zMin)

      -- Define a bin size for linear scaling neighbour search, which defines the side length of the
      -- cubic bin cells. It must be at least as big as the maximum neighbour distance but a minimum
      -- of 3 angstrom cubes will be used.
      binSize :: Double
      binSize = max maxNeighbourDist 3

      -- Calculate the maximum bin index in each direction. (0-based)
      nBinsMaxIx :: (Int, Int, Int)
      nBinsMaxIx@(nBinsMaxIxX, nBinsMaxIxY, nBinsMaxIxZ) =
        let f cellSz = floor $ cellSz / binSize in (f cellSizeX, f cellSizeY, f cellSizeZ)

      -- The number of bins per direction.
      nBinsDim :: (Int, Int, Int)
      nBinsDim@(nBinsX, nBinsY, nBinsZ) = (nBinsMaxIxX + 1, nBinsMaxIxY + 1, nBinsMaxIxZ + 1)

      -- Linearised index for the bin cells.
      ixOrigin = (0, 0, 0)
      binLinearIxRange = (ixOrigin, nBinsMaxIx)

      -- Sort the atoms into bins now. This is basically the conversion from the cartesian
      -- coordinates to bin indices in R3.
      atomBinAssignment :: Matrix D Int
      atomBinAssignment =
        Massiv.imap
          ( \((_atom :. cartComponent)) el -> case cartComponent of
              0 -> floor $ (el - xMin) / binSize
              1 -> floor $ (el - yMin) / binSize
              2 -> floor $ (el - zMin) / binSize
              _ -> -1
          )
          atomCoords

      -- Linearise the bin assignment of the atoms with respect to the bin index.
      atomBinAssignmentLinear :: Massiv.Vector D Int
      atomBinAssignmentLinear =
        Massiv.ifoldlInner
          ( \(_atom :. cartComponent) accBinLIx el -> case cartComponent of
              0 -> accBinLIx + Data.Ix.index binLinearIxRange (el, 0, 0)
              1 -> accBinLIx + Data.Ix.index binLinearIxRange (0, el, 0)
              2 -> accBinLIx + Data.Ix.index binLinearIxRange (0, 0, el)
              _ -> Data.Ix.index binLinearIxRange nBinsDim - 1
          )
          0
          atomBinAssignment

      -- Sort the atoms into bins (bin characterised by first index) and keep their index around in a
      -- tuple as (assignedBin, atomIndex).
      atomBinAssignmentLinearSort :: Massiv.Vector U (Int, Int)
      atomBinAssignmentLinearSort =
        Massiv.quicksort
          . Massiv.computeAs Massiv.U
          . Massiv.setComp Par
          . Massiv.imap (\atomIx binLIx -> (binLIx, atomIx))
          $ atomBinAssignmentLinear

  -- A HashMap from scalar bin index to the atoms a bin contains.
  binAtomMap <- do
    -- Create the equally sized rows of the bin (rows) to atom indices (column).
    let binGroups = vectorToGroups fst atomBinAssignmentLinearSort

    -- The bin indices (only bins that actually contain atoms)
    binIndices <-
      traverse
        ( \aGroup -> do
            groupHead <- case List.headMaybe aGroup of
              Nothing ->
                throwM $
                  DataStructureException
                    "neighbouhrList"
                    "Found an empty atom group for a bin,\
                    \ but only non-empty groups should be specified in this step."
              Just h -> return h
            let groupBinIx = fst groupHead
            return groupBinIx
        )
        binGroups
        `using` rpar

    -- The atom vectors already expanded to matrices for easier concatenation.
    let binAtomSets :: [IntSet]
        binAtomSets = List.map (IntSet.fromList . List.map snd) binGroups
        mappingGroups = HashMap.fromList $ List.zip binIndices binAtomSets
    return mappingGroups

  let -- Create the array of ALL bins to the atoms they contain. This is a 3D array, indexed by the
      -- bins.
      binAtomMatrix :: Array B Ix3 IntSet
      binAtomMatrix =
        Massiv.makeArrayLinear
          Par
          (Sz (nBinsX :> nBinsY :. nBinsZ))
          (\lBinIx -> HashMap.lookupDefault IntSet.empty lBinIx binAtomMap)

      -- Create a stencil over the 3 bin dimensions, which combines all sets of the neighbouring bins
      -- with the set of this bin.
      {-# INLINE neighbourCollectionStencil #-}
      neighbourCollectionStencil :: Stencil Ix3 IntSet IntSet
      neighbourCollectionStencil =
        Massiv.makeStencil (Sz (3 :> 3 :. 3)) (1 :> 1 :. 1) $ \get ->
          let validIndRange = [-1, 0, 1]
              allStencilIndices =
                fmap
                  toIx3
                  [(x, y, z) | x <- validIndRange, y <- validIndRange, z <- validIndRange]
              allStencilGetters = fmap get allStencilIndices
           in foldl' (<>) IntSet.empty allStencilGetters

      -- Apply the stencil to the bins and collect in each bin all the atoms, that need to be checked
      -- against each other.
      collectedAtomSetInBins :: Array DW Ix3 IntSet
      collectedAtomSetInBins =
        Massiv.mapStencil (Fill IntSet.empty) neighbourCollectionStencil binAtomMatrix

      -- Within each collected bin, calculate all distances now of all possible combinations and
      -- keep those, which fullfill the distance criterion.
      neighboursInBins :: Array D Ix3 (IntMap IntSet)
      neighboursInBins =
        Massiv.map (correlateIntSet atomDenseMap maxNeighbourDist)
          . Massiv.computeAs Massiv.B
          . Massiv.setComp Par
          $ collectedAtomSetInBins

      -- Join all neighbours from the individual bins together.
      foldingF :: IntMap IntSet -> IntMap IntSet -> IntMap IntSet
      foldingF = IntMap.unionWith IntSet.union
      neighboursInDenseNumbering = unsafePerformIO $ Massiv.foldlP foldingF IntMap.empty foldingF IntMap.empty neighboursInBins

  -- Remap the neighbour list back to the sparse mapping, that has been originally used.
  let neighboursInSparseNumbering =
        ( IntMap.map
            ( IntSet.map
                (\denseKey -> atomIndexDenseToSparseMapping Massiv.! denseKey)
            )
            . IntMap.mapKeys (\denseKey -> atomIndexDenseToSparseMapping Massiv.! denseKey)
            $ neighboursInDenseNumbering
        )
          `using` parTraversable rpar

  return neighboursInSparseNumbering
  where
    -- Calculate the distances between all atoms in the set and keep only those pairs, which have a
    -- distance less or equal than the supplied one. If an atom cannot be found in the atom map,
    -- it will no be a neighbour of anything. Therefore this function never fails.
    correlateIntSet :: IntMap Atom -> Double -> IntSet -> IntMap IntSet
    correlateIntSet denseAtomMap maxDist atomsInBin =
      let atomList = IntSet.toAscList atomsInBin
          unfilteredPotentialNeigbhours =
            IntMap.fromAscList $ List.zip atomList (List.repeat atomsInBin)
          neighbours =
            IntMap.filter (not . IntSet.null) $
              IntMap.mapWithKey
                (checkDistancesFromOriginAtom denseAtomMap maxDist)
                unfilteredPotentialNeigbhours
       in neighbours

    -- Checks for a set of target atoms, if they are within a given distance of the origin atom.
    -- This never fails. If an atom that was looked up by its index is not in the IntMap of atoms,
    -- it will not be a neighbour of the origin. If the origin cannot be found, there will be no
    -- neighbours.
    checkDistancesFromOriginAtom :: IntMap Atom -> Double -> Int -> IntSet -> IntSet
    checkDistancesFromOriginAtom denseAtomMap maxDist originInd targetInds =
      let originCoords =
            Massiv.delay . getVectorS . coordinates <$> denseAtomMap IntMap.!? originInd
       in IntSet.filter
            ( \tIx ->
                let targetCoords =
                      Massiv.delay . getVectorS . coordinates <$> denseAtomMap IntMap.!? tIx
                    dist = join $ distance <$> originCoords <*> targetCoords
                 in case dist of
                      Nothing -> False
                      Just d -> d <= maxDist && tIx /= originInd
            )
            targetInds

----------------------------------------------------------------------------------------------------

-- | Guess the bond matrix in a two step procedure: guess the topology first and then the bond
-- orders. Follows the rules from [SMF](https://doi.org/10.1021/ar500088d) with modifications for
-- metals.
{-# INLINE guessBondMatrix #-}
{-# SCC guessBondMatrix #-}
guessBondMatrix ::
  (MonadThrow m) =>
  -- | Optional tolerance (in Angstrom) for the detection of (single, multiple) bonds.
  Maybe (Double, Double) ->
  -- | Atoms whose topology is to be determined.
  IntMap Atom ->
  -- | Previous neighbourlists. May be empty; in that case a new one will be calculated.
  Map Double NeighbourList ->
  -- | Bond Matrix for the system.
  m BondMatrix
guessBondMatrix bondDistAdds atms nls = do
  let (singleTol, multiTol) = fromMaybe (defCovAddSingle, defCovAddMultiple) bondDistAdds

  -- Guess the bonding topology.
  topologyMat <- guessTopology (Just singleTol) atms nls

  -- Get direct bond partners of all atoms.
  bondPartnerDists <- getDirectBondPartners topologyMat

  -- Construct the bond matrix with bond orders and return.
  return $ getBondOrders multiTol bondPartnerDists
  where
    localExc = MolLogicException "guessBondMatrix"

    -- Obtain a mapping from origin keys to their chemical element and their bonding targets with
    -- chemical elements and the distances to the targets.
    getDirectBondPartners :: MonadThrow m => HashMap (Int, Int) Bool -> m (IntMap (Element, IntMap (Element, Double)))
    getDirectBondPartners topoMat =
      maybe2MThrow (localExc "An atom from the bond matrix could not be found in the map of atoms") $
        HashMap.foldlWithKey'
          ( \acc' (o, t) val -> do
              acc <- acc'
              atomO <- atms IntMap.!? o
              atomT <- atms IntMap.!? t
              let coordO = getVectorS $ atomO ^. #coordinates
                  coordT = getVectorS $ atomT ^. #coordinates
                  elementO = atomO ^. #element
                  elementT = atomT ^. #element
              distOT <- distance coordO coordT
              let targetMap = IntMap.singleton t (elementT, distOT)
              return $
                if val
                  then IntMap.insertWith (\(oE, tM) (_, accTM) -> (oE, tM <> accTM)) o (elementO, targetMap) acc
                  else acc
          )
          (pure mempty)
          topoMat

    -- Check the valencies of an atom and its direct bond partners as well as the distance. If an
    -- atom has too few valencies, some of its neighbours has too few valencies and their distance
    -- is below a threshold, some bond order > 1 will be assumed. Otherwise will keep a single bond.
    -- This function will construct the bond matrix unidirectorial for each atom, but as each atom
    -- will be processed (both origin and targets will be processed by the same rules), the final
    -- bond matrix will be bidirectorial again.
    -- In case of doubt and multiple targets exist, that could take multiple bonds, we use them all,
    -- even this exceeds the valencies of the origin atom. This makes sure never ever to cut through
    -- a POSSIBLY conjugated system (and it also makes the check in the reverse direction easier).
    getBondOrders :: Double -> IntMap (Element, IntMap (Element, Double)) -> BondMatrix
    getBondOrders multipleTol bpMap = runIdentity
      .
      -- Iterate over the origins
      foldHelper bpMap
      $ \acc' oIx (oEl, targets) -> do
        acc <- acc'
        let covRadO = covalentRadii Map.!? oEl
            nTargetMetals = IntMap.size . IntMap.filter (== Metal) <$> traverse (\(tEl, _) -> metallicity Map.!? tEl) targets
            originValRaw = IntMap.size targets
            originMetallicity = metallicity Map.!? oEl
            originVal = (originValRaw -) <$> nTargetMetals

        -- For each origin; iterate over its targets.
        bondMatO <- foldHelper targets $ \localAcc' tIx (tEl, dist) -> do
          localAcc <- localAcc'

          let -- Get the maximum distance between origin and target to consider it a bond order > 1
              covRadT = covalentRadii Map.!? tEl
              maxMultipleDist = (+ multipleTol) <$> ((+) <$> covRadO <*> covRadT)

              -- The distance criterion: is the distance between origin and target <= the sum of the
              -- covalent radii + tolerance?
              distCrit = fromMaybe False $ (dist <=) <$> maxMultipleDist

              -- Determine the bond order by the following rules:
              --   - If any of the bond partners is a metal, declare a coordinative bond
              --   - For both origin and target of a bond, remove bonds to metals from the number of
              --     valences, as they are most likely a coordinative bond. This is considered the
              --     "true" valence of an atom. If both origin and target are subvalent and close
              --     enough, they get a multi-bond.
              bondOrder = fromMaybe Multiple $ do
                targetMetallicity <- metallicity Map.!? tEl
                targetValRaw <- IntMap.size . snd <$> (bpMap IntMap.!? tIx)
                targetTargets <- snd <$> bpMap IntMap.!? tIx
                nTargetTargetMetals <- IntMap.size . IntMap.filter (== Metal) <$> traverse (\(ttEl, _) -> metallicity Map.!? ttEl) targetTargets
                let targetVal = targetValRaw - nTargetTargetMetals
                    targetNextPlausibleVal = getNextMatchingVal targetVal =<< (possibleValences Map.!? tEl)

                originVal' <- originVal
                originMetallicity' <- originMetallicity
                let originNextPlausibleVal = getNextMatchingVal originVal' =<< (possibleValences Map.!? oEl)

                let isPairSubvalent = do
                      originNextPlausibleVal' <- originNextPlausibleVal
                      targetNextPlausibleVal' <- targetNextPlausibleVal
                      return $ originVal' < originNextPlausibleVal' && targetVal < targetNextPlausibleVal'

                return $ case (originMetallicity', targetMetallicity) of
                  (Metal, _) -> Coordinative
                  (_, Metal) -> Coordinative
                  (_, _) -> case (distCrit, isPairSubvalent) of
                    (True, Just True) -> Multiple
                    (True, Just False) -> Single
                    (True, Nothing) -> Coordinative
                    (False, Just _) -> Single
                    (False, Nothing) -> Coordinative

          return $ localAcc <> HashMap.singleton (oIx, tIx) bondOrder

        return $ acc <> bondMatO
      where
        -- FoldlWithKey with more convenient order of arguments.
        foldHelper :: (Monoid a) => IntMap b -> (a -> IntMap.Key -> b -> a) -> a
        foldHelper intmap foldf = IntMap.foldlWithKey' foldf mempty intmap

        -- Go over possible valencies and compare to the current valency: take the first possible
        -- valency, that is >= the current valency.
        getNextMatchingVal :: Int -> IntSet -> Maybe Int
        getNextMatchingVal currVal possibleVal = fmap fst . IntSet.minView . IntSet.filter (>= currVal) $ possibleVal

----------------------------------------------------------------------------------------------------

-- | Efficiently guess the topology of a molecule by utilising the neighbourlist. Only guesses
-- whether atoms are bonded, but does not guess the bond order yet.
guessTopology ::
  (MonadThrow m) =>
  -- | Optional tolerance (in Angstrom) for the detection of any bond. The term is additive to the
  -- sum of covalent radii of the involved atoms.
  Maybe Double ->
  -- | Atoms whose topology is to be determined.
  IntMap Atom ->
  -- | Previous neighbourlists. May be empty; in that case a new one will be calculated.
  Map Double NeighbourList ->
  -- | All 'Bool's in this will be True for ~*reasons*~
  m (HashMap (Int, Int) Bool)
guessTopology bondDistTol' atms nls = do
  let bondDistTol = fromMaybe defCovAddSingle bondDistTol'
      atomElements = fmap element atms
      maxCovAtomRadius = foldl' max 0 . IntMap.mapMaybe (covalentRadii Map.!?) $ atomElements
      maxNeighbourDist = maxCovAtomRadius * 2 + bondDistTol
      matchingNeighbours = Map.minView . Map.filterWithKey (\k _ -> k >= maxNeighbourDist) $ nls
  neighbours <- case matchingNeighbours of
    Nothing -> neighbourList maxNeighbourDist atms
    Just (bestNL, _) -> return bestNL
  let topologyMatrix =
        IntMap.foldlWithKey'
          ( \accBM originIx targetSet ->
              let newRow = makeScreenedTopologyRow bondDistTol originIx targetSet atms
               in accBM <> newRow
          )
          HashMap.empty
          neighbours
          `using` parTraversable rpar
  return topologyMatrix
  where
    -- Create a row of a bond matrix for a given origin atom and a pre-filtered set of potential
    -- target atoms. Elements, for which a covalent radius cannot be found will have no bonds.
    makeScreenedTopologyRow :: Double -> Int -> IntSet -> IntMap Atom -> HashMap (Int, Int) Bool
    makeScreenedTopologyRow singleTol originInd targetInds originalAtoms =
      IntSet.foldl'
        ( \topologyMatrixRowAcc targetInd ->
            let hasBond = do
                  originAtom <- originalAtoms IntMap.!? originInd
                  targetAtom <- originalAtoms IntMap.!? targetInd
                  let originCoords = Massiv.delay . getVectorS . coordinates $ originAtom
                      targetCoords = Massiv.delay . getVectorS . coordinates $ targetAtom
                      originElement = element originAtom
                      targetElement = element targetAtom
                  targetRadius <- covalentRadii Map.!? targetElement
                  originRadius <- covalentRadii Map.!? originElement
                  realDistance <- distance originCoords targetCoords
                  let sumCovRadii = originRadius + targetRadius
                  return $ realDistance <= sumCovRadii + singleTol
             in case hasBond of
                  Just True -> HashMap.insert (originInd, targetInd) True topologyMatrixRowAcc
                  _ -> topologyMatrixRowAcc
        )
        HashMap.empty
        targetInds

----------------------------------------------------------------------------------------------------

-- | Group detection in a molecule. The group detection works by following the bond matrix.
-- Sets of atoms, which do not have any covalent connections to atoms outside of their own set are
-- considered a group.
{-# INLINE groupDetectionDetached #-}
{-# SCC groupDetectionDetached #-}
groupDetectionDetached :: MonadThrow m => Molecule -> m (IntMap IntSet)
groupDetectionDetached mol = do
  -- Make sure the molecule is sane, otherwise the groups will potentially be garbage.
  _ <- checkMolecule mol

  -- Detect all groups by following the bond matrix.
  let bonds' = mol ^. #bonds
      atoms' = mol ^. #atoms
      allGroups = findAllGroups bonds' atoms' IntMap.empty
  return allGroups
  where
    -- Takes a starting atom and the bond matrix of the molecule and an (initially empty) set of atoms
    -- that are already part of the group. Starting from this atom follow the bonds through the
    -- molecule. until no new atoms can be found anymore.
    findFromStartingAtom ::
      -- | Index of the atom currently checked
      Int ->
      -- | The bond matrix of the molecule.
      BondMatrix ->
      -- | The atom indices in the groups. Accumulates during the recursion.
      IntSet ->
      -- | The bond matrix after all bonds involving the current atom have been
      --   removed and the next bound neighbours from this atom.
      IntSet
    findFromStartingAtom atomIndex bondMatrix groupAcc =
      let -- All bonding partners of the currently inspected atom.
          bondsTargetsOfThisAtoms =
            IntSet.fromList . fmap snd . HashMap.keys $
              HashMap.filterWithKey
                (\(oIx, _) val -> oIx == atomIndex && isBond val)
                bondMatrix

          -- Only the bond targets, that are not yet part of the group.
          newTargets = bondsTargetsOfThisAtoms IntSet.\\ groupAcc
       in if IntSet.null newTargets
            then -- If no new targets were found, terminate the recursive search here.
              let groupAccNew = groupAcc <> IntSet.singleton atomIndex in groupAccNew
            else -- If new targets were found, recursively also follow the new targets.

              let groupAccNew = groupAcc <> newTargets <> IntSet.singleton atomIndex
               in IntSet.foldl'
                    (\gAcc newTarget -> findFromStartingAtom newTarget bondMatrix gAcc)
                    groupAccNew
                    newTargets

    -- Move through a complete molecule until all groups have been found.
    findAllGroups ::
      -- | Bond matrix of the complete molecule.
      BondMatrix ->
      -- | An accumulator of atoms, which have not yet been assigned to a group.
      IntMap Atom ->
      -- | Growing accumulator of groups, that have been found yet.
      IntMap IntSet ->
      IntMap IntSet
    findAllGroups bondMat atomMapAcc groupAcc =
      let -- Use the first atom of the rest of the whole molcule, which has not yet been consumed into
          -- groups as a starting atom for the next group search.
          atomHead = fst <$> IntMap.lookupMin atomMapAcc
          newGroup = findFromStartingAtom <$> atomHead <*> pure bondMat <*> pure IntSet.empty

          -- Remove the atoms of the new group from the whole system for the next iteration.
          atomMapAccNew = IntMap.withoutKeys atomMapAcc <$> newGroup

          -- Get the highest key of the group IntMap before insertion of the new group.
          highestKey = case IntMap.lookupMax groupAcc of
            Just (k, _) -> k
            Nothing -> 0

          -- Add the new group with an incremented key to the IntMap of groups.
          newGroupAcc = IntMap.insert (highestKey + 1) <$> newGroup <*> pure groupAcc
       in case (atomMapAccNew, newGroupAcc) of
            -- A new group was found
            (Just remainingAtoms, Just groupsFoundYet) ->
              findAllGroups bondMat remainingAtoms groupsFoundYet
            -- The atoms already have been fully consumed and therefore no new groups could be
            -- found.
            _ -> groupAcc

----------------------------------------------------------------------------------------------------

-- | This function adds multipole centres to a given molecule layer. In the context of ONIOM, this
-- means, that the multipoles of a real system are used as a polarisation cloud of a model system.
-- Polarisation centres, that are real atoms in the model system, will be removed. A sequence of
-- values gives scaling factors for the multipoles in bond distances.
--
-- If the atoms of the real system above do not contain multipole information, you will have useless
-- dummy atoms in the model system.
--
-- Be aware of a nasty corner case:
--
-- @
--       b--c--d
--      /       \\
--     A         E
-- @
-- The atoms @A@ and @E@ belong to the model system, which shall be polarised, while @b@, @c@ and
-- @d@ belong to the real system, which provides the polarisation cloud. If scaling factors for up
-- to three bonds are given, atom @b@ has a distance of 1 from atom @A@ but a distance of 3 from
-- atom @E@. This introduces some ambiguity to the scaling. This will use the smallest distance
-- found to select the scaling factor. Therefore @b@ and @d@ will both be treated as being in a
-- distance of 1 bond, not @b@ having a distance of 1 with respect to @A@ and a distance of 3 to
-- @E@.
--
-- The function obtains a representation suitable for
-- 'Spicy.Molecule.Multipoles.molToPointCharges', where the atoms of the layer above are
-- dummy atoms but still connected in the bond matrix, which is important for definition of the
-- local axes system of the multipole point charges.
--
-- It will furthermore join all neighbour lists top down, so that also for dummy atoms neighbours
-- are available.
-- TODO - Possibly the function is slightly incorrect: when using already collected multipoles,
-- information from multi-model-centres on the same layer propagate to the real system, which then
-- polarises other model centres. This introduces a dependency of the second layer on the first
-- layer and therefore changes results, depending on which layer is defined first. A workaround
-- would be to use the high level calculation outputs instead and build the polarisation cloud like
-- this.
{-# SCC getPolarisationCloudFromAbove #-}
getPolarisationCloudFromAbove ::
  (MonadThrow m) =>
  -- | The whole molecular system with the multipoles in all layers above the specified one.
  Molecule ->
  -- | Specificaiton of the molecule layer, which should get a polarisation cloud from all layers
  -- above.
  MolID ->
  -- | Scaling factors for the multipole moments in bond distances. The first value in the sequence
  -- would be used to scale the multipoles of atoms 1 bond away from set 1 atoms, the second value
  -- in the sequence multipoles 2 bonds away and so on.
  Seq Double ->
  -- | The specified layer of the molecule only with polarisation centres added.
  m OniomLayer
getPolarisationCloudFromAbove mol layerID poleScalings = do
  -- Check for the sanity of the molecule, as otherwise the assumptions regarding bonds make no
  -- sense.
  _ <- checkMolecule mol

  go mol layerID
  where
    go ::
      (MonadThrow m) =>
      -- | The whole molecular system with the multipoles at least in the layer above the
      --   specified one.
      Molecule ->
      -- | Specificaiton of the molecule layer, which should get a polarisation cloud from
      --   the layer above. The layer above is also determined from the MolID by removing
      --   its last element.
      MolID ->
      -- | The specified layer of the molecule only with polarisation centres added. All
      --   submolecules or groups this layer may have had are removed.
      m OniomLayer
    -- The system has been reduced to a single layer and is definitely done. Return it.
    go currentSubTree Empty = return . OniomLayer $ currentSubTree & #subMol .~ mempty
    -- There are more layers above the current one which shall be polarised. Use the layer directly
    -- above the current model system for polarisation and join.
    go currentSubTree currentMolID@(polID :|> _modelID) = do
      -- Obtain the local model system and the local real system, which is being used for
      -- polarisation of the model.
      localModel <- getMolByID currentSubTree currentMolID
      localReal <- getMolByID currentSubTree polID

      -- Deconstruct the real system and do some adjustements.
      let --  - Dummy atoms are removed from the real system.
          --  - All remaining atoms will become dummy atoms.
          realAtoms =
            IntMap.map (\a -> a & #isDummy .~ True)
              . IntMap.filter (\a -> not $ a ^. #isDummy)
              $ localReal ^. #atoms
          realAtomsKeys = IntMap.keysSet realAtoms
          -- - The bond matrix will be updated only to contain the filtered atoms.
          realBonds = cleanBondMatByAtomInds (localReal ^. #bonds) realAtomsKeys
          -- - The neighbourlist will be updated to contain only filtered atoms.
          realNeighbours =
            fmap
              ( \nlAtDist ->
                  let keysRestricted = IntMap.restrictKeys nlAtDist realAtomsKeys
                      targetsRestricted = IntMap.map (IntSet.intersection realAtomsKeys) keysRestricted
                   in targetsRestricted
              )
              $ localReal ^. #neighbourlist
          -- - The groups will be updated only to contain the filtered atoms.
          realGroups =
            IntMap.map
              (\f -> f & #atoms %~ IntSet.intersection realAtomsKeys)
              $ localReal ^. #group
          -- Combine the groups of the model and the real system. After combination, the groups
          -- will be updated to reflect the properties of the non-dummy atoms. I.e. if a group now
          -- only contains non-dummy atoms, that have zero charge in sum and a dummy atom, that just
          -- became a dummy with charge -1, the charge of the group would still be -1. But this does
          -- not reflect the physical model. The charge of the group must only be built over the #
          -- non-dummy atoms and will be updated to be the sum of the non-dummy atoms, therefore 0
          -- in this example.
          physicalModelGroups =
            let localModelGroups = localModel ^. #group
                localModelAtoms = IntMap.filter (not . isDummy) $ localModel ^. #atoms
                joinedGroups =
                  IntMap.unionWith
                    (\fM fR -> fM & #atoms %~ (<> fR ^. #atoms))
                    localModelGroups
                    realGroups
             in joinedGroups <&> \group@Group {atoms = grAtoms} ->
                  let physicalGroupAtoms = IntMap.restrictKeys localModelAtoms grAtoms
                      physicalCharge = sum . fmap (^. #formalCharge) $ physicalGroupAtoms
                      phyiscalExcEl = sum . fmap (^. #formalExcEl) $ physicalGroupAtoms
                   in group & #charge .~ physicalCharge
                        & #excEl .~ phyiscalExcEl

      -- Get the link atoms of the model system and groups of their bonding partners in the real
      -- system in a given distance of bonds. The first entry is dropped as those are distance 0
      -- atoms (therefore the capped atoms themselves).
      let modelCappedAtoms = IntMap.keys . getCappedAtoms $ localModel ^. #atoms
          searchDistance = fromIntegral . Seq.length $ poleScalings
      distanceGroups <-
        combineDistanceGroups
          <$> mapM
            (\cappedStartAtom -> Seq.drop 1 <$> bondDistanceGroups (OniomLayer localReal) cappedStartAtom searchDistance)
            modelCappedAtoms

      -- Scale the polarisation centres of the real system according to their distance to the capped
      -- atoms of the model system.
      let realAtomsScaled =
            Seq.foldlWithIndex
              ( \atomsAcc bondDist atomsAtDist ->
                  let scaleFactor = fromMaybe 0 $ poleScalings Seq.!? bondDist
                   in scaleMultipoles atomsAcc atomsAtDist scaleFactor
              )
              realAtoms
              distanceGroups

      -- Combine the model system (and its already existing polarisation centres) with the modified
      -- stuff of the real layer.
      let bottomMolPol =
            localModel
              & #atoms %~ (<> realAtomsScaled)
              & #bonds %~ HashMap.unionWith (\boM _ -> boM) realBonds
              & #neighbourlist %~ combineNLs realNeighbours
              & #group .~ physicalModelGroups
              & #subMol .~ mempty

      go (currentSubTree & molIDLensGen polID .~ bottomMolPol) polID

    -- Function to scale all Multipoles of the atoms in the input set with a factor.
    scaleMultipoles :: IntMap Atom -> IntSet -> Double -> IntMap Atom
    scaleMultipoles atoms selection factor =
      IntSet.foldl'
        (\accAtoms sel -> IntMap.adjust (& #multipoles %~ modifyMultipole (* factor)) sel accAtoms)
        atoms
        selection

    -- Combines given neighbour lists at a distance.
    combineNLAtDist :: NeighbourList -> NeighbourList -> NeighbourList
    combineNLAtDist nlReal nlModel = IntMap.unionWith (<>) nlModel nlReal

    -- Combine all neighbour lists at all distances.
    combineNLs :: Map Double NeighbourList -> Map Double NeighbourList -> Map Double NeighbourList
    combineNLs nlsModel nlsReal = Map.unionWith combineNLAtDist nlsReal nlsModel

----------------------------------------------------------------------------------------------------

-- | Getting the distance of atoms of the real system to the capped atoms of the model system, gives
-- multiple distances of a given real system atom to the model system atoms in bond distances. This
-- function joins the real system atoms, which are grouped by bond distance from a given capped
-- model system atom, in a way to ensure that a real system atom always only appears at the lowest
-- possible distance to the model system.
combineDistanceGroups :: [Seq IntSet] -> Seq IntSet
combineDistanceGroups distGroups =
  let -- Join all the distance groups of different capped atoms. An atom of the real system might
      -- appear here mutliple times with different distances.
      combinationWithAmbigousAssignments =
        List.foldl'
          ( \acc thisGroup ->
              let lengthAcc = Seq.length acc
                  lengthThis = Seq.length thisGroup

                  -- Adjust the length of the accumulator and this group, to match the longer one of both.
                  lengthAdjAcc =
                    if lengthThis > lengthAcc
                      then acc Seq.>< Seq.replicate (lengthThis - lengthAcc) IntSet.empty
                      else acc
                  lengthAdjThis =
                    if lengthAcc > lengthThis
                      then thisGroup Seq.>< Seq.replicate (lengthAcc - lengthThis) IntSet.empty
                      else thisGroup

                  -- Join this group with the accumulator.
                  newAcc = Seq.zipWith IntSet.union lengthAdjAcc lengthAdjThis
               in newAcc
          )
          Seq.empty
          distGroups
      -- Make sure, that a given atom always only appears at the lower distance and remove its
      -- occurence at a higher distance, if already found at a lower distance.
      combinationUnambigous =
        foldl
          ( \(atomsAtLowerDist, unambDistSeqAcc) thisDistanceAtoms ->
              let thisWithLowerRemoved = thisDistanceAtoms IntSet.\\ atomsAtLowerDist
                  newAtomsAtLowerDist = thisWithLowerRemoved `IntSet.union` atomsAtLowerDist
                  newUnambDistSeqAcc = unambDistSeqAcc |> thisWithLowerRemoved
               in (newAtomsAtLowerDist, newUnambDistSeqAcc)
          )
          (IntSet.empty, Seq.empty)
          combinationWithAmbigousAssignments
   in snd combinationUnambigous

----------------------------------------------------------------------------------------------------

-- | This function takes a starting atom in given layer and starts moving away from it along the
-- bonds. Atoms of the same bond distance will be grouped. Therefore all atoms 1 bond away from the
-- start will form a group, all bonds 2 bonds away from the starting atom will form a group and so
-- on. Atoms will only belong to one group always, which is relevant for cyclic structures. They
-- will always be assigned to the group with the lowest distance.
--
-- The function takes a maximum distance in bonds to search. The search ends, if either no new atoms
-- can be found or the search would extend beyond the maximum search distance.
{-# SCC bondDistanceGroups #-}
bondDistanceGroups ::
  MonadThrow m =>
  -- | The molecule layer which to use for the search. Sublayers are ignored.
  OniomLayer ->
  -- | Key of the starting atom.
  Int ->
  -- | The maximum distance for search in bonds. Must therefore be equal or greater than 0.
  Natural ->
  -- | A sequence of atoms in a given distance. At the sequence index 0 will be
  --   the starting atom index. At sequence index n will be the set of atom
  --   indices n bonds away from the start.
  m (Seq IntSet)
bondDistanceGroups (OniomLayer mol) startAtomInd maxBondSteps = do
  -- Check the molecule, as the bond matrix could otherwise be damaged and create strange results.
  _ <- checkMolecule mol

  let bondMat = mol ^. #bonds
      groups = stepAndGroup bondMat (fromIntegral maxBondSteps) (Seq.singleton . IntSet.singleton $ startAtomInd)
  return groups
  where
    stepAndGroup ::
      -- | The bond matrix through which to step. During the recursion this will
      --   shrink, as the atoms already assigned will be removed as origins and targets.
      BondMatrix ->
      -- | The maximum distance in bonds.
      Int ->
      -- | Accumulator of the groups.
      Seq IntSet ->
      Seq IntSet
    stepAndGroup bondMat' maxDist groupAcc
      | currentDist > maxDist = groupAcc
      | otherwise =
        let -- The currently most distant atoms from the start and therefore the origin for the next
            -- search.
            searchOrigins = case groupAcc of
              Empty -> IntSet.singleton startAtomInd
              _ :|> lastGroup -> lastGroup

            -- Make sure the group accumulator always contains the start atom at distance 0.
            groupAccAdjusted = case groupAcc of
              Empty -> Seq.singleton . IntSet.singleton $ startAtomInd
              _ -> groupAcc

            -- The next more distant sphere of atoms. Potentially, in case of a ring closure, the new
            -- sphere might contain atoms of this sphere, due to how the bond matrix is updated in the
            -- recursion. To avoid this, remove the origins from the next sphere.
            nextSphereAtoms =
              (`IntSet.difference` searchOrigins) . IntSet.unions $
                fmap
                  (getAtomBondPartners bondMat')
                  (IntSet.toList searchOrigins)

            -- The bond matrix for the next iteration will have this iterations search origins removed
            -- both as origin as well as target. This should make sure, that stepping back is not
            -- possible.
            newBondMat =
              HashMap.filterWithKey
                ( \(ixO, ixT) val ->
                    not (ixO `IntSet.member` searchOrigins)
                      && not (ixT `IntSet.member` searchOrigins)
                      && isBond val
                )
                bondMat'

            -- The new accumulator will contain the results as a new entry to the sequence.
            newGroupAcc = groupAccAdjusted |> nextSphereAtoms
         in stepAndGroup newBondMat maxDist newGroupAcc
      where
        currentDist = Seq.length groupAcc

    -- Get all bond partners of an atom.
    getAtomBondPartners :: BondMatrix -> Int -> IntSet
    getAtomBondPartners bondMat' atom =
      IntSet.fromList
        . fmap snd
        . HashMap.keys
        . HashMap.filterWithKey (\(ixO, _) val -> ixO == atom && isBond val)
        $ bondMat'

----------------------------------------------------------------------------------------------------

-- | Given a molecule, build the associations of this layers atoms, with the groups of this layers.
-- Therefore, if groups represent the whole molecule, this will assign Just Group to each atom.
-- This is the group to which the atom belongs.
getAtomAssociationMap ::
  MonadThrow m =>
  -- | The current molecule layer, for which to build
  --   the association map.
  OniomLayer ->
  -- | The original 'IntMap' of atoms, but the atoms are
  --   now associated with a group and the groups
  --   'Int' key.
  m (IntMap (Atom, Maybe (Int, Group)))
getAtomAssociationMap (OniomLayer layer) = do
  let atoms' = layer ^. #atoms
      groups = layer ^. #group
  atomToGroupAssociations <-
    IntMap.traverseWithKey
      ( \atomKey atom -> do
          -- Find all groups, to which an atom is assigned.
          let matchingGroups =
                IntMap.filter (\group -> atomKey `IntSet.member` (group ^. #atoms)) groups

          case IntMap.size matchingGroups of
            0 -> return (atom, Nothing)
            1 -> return (atom, IntMap.lookupMin matchingGroups)
            _ ->
              throwM
                . MolLogicException "getAtomAssociationMap"
                $ "Assignment of atom "
                  <> show atomKey
                  <> " to a group failed, as the assignment is ambigous. \
                     \This means an invalid data structure in the groups."
      )
      atoms'
  return atomToGroupAssociations

----------------------------------------------------------------------------------------------------

-- | Parser easily obtain lists of 'GroupAtomInfo', which need to be converted to proper groups and
-- atoms for the molecule. This function builds the data types for 'Molecule.atoms' and
-- 'Molecule.group'.
groupAtomInfo2AtomsAndGroups :: [GroupAtomInfo] -> (IntMap Atom, IntMap Group)
groupAtomInfo2AtomsAndGroups info =
  let atoms' = IntMap.fromList . fmap (\i -> (faiAtomIndex i, faiAtom i)) $ info
      groups = IntMap.fromListWith groupUnion . fmap (\i -> (faiGroupIndex i, faiGroup i)) $ info
   in (atoms', groups)
  where
    -- Joins two groups. The @label@ and @chain@ should be identical but this
    -- is not strictly required by this functions. Instead this functions uses a left bias for the
    -- groups.
    -- It accumulates charges and excess electrons.
    groupUnion :: Group -> Group -> Group
    groupUnion a b =
      a & #atoms %~ IntSet.union (b ^. #atoms)
        & #charge %~ (+ (b ^. #charge))
        & #excEl %~ (+ (b ^. #excEl))

----------------------------------------------------------------------------------------------------

-- | Obtains the Jacobian matrix for the transformation from the basis of the model system to the
-- basis of a real system as used in the equation for ONIOM gradients:--
-- \[
--     \nabla E^\mathrm{ONIOM2} = \nabla E^\mathrm{real} - \nabla E^\mathrm{model, low} \mathbf{J}
--                              + \nabla E^\mathrm{model, high} \mathbf{J}
-- \]--
-- The gradient of the real system, which has \(N\) atoms is a row vector with \(3 N\) elements. The
-- gradient of the model system, which has \(M\) atoms is a row vector, with \(3 M\) elements. The
-- Jacobian matrix therefore has a size of \( 3 M \times 3 N\).
--
-- To transform from the basis of the model system to the basis of the real system, the Jacobian
-- does the following
--
--   * Distribute the gradient that is acting on a link atom @LA@ (which connects the model system
--     atom @LAC@ to the real system atom @LAH@) according to the factor \(g\), by which the
--     position of "LA" depends on @LAH@ and @LAC@ as
--     \( \mathbf{r}^\mathrm{LA} = \mathbf{r}^\mathrm{LAC} + g (\mathbf{r}^\mathrm{LAH} - \mathbf{r}^\mathrm{LAC}) \)
--     as
--     \( \partial r_a^\mathrm{LA} / \partial r_b^\mathrm{LAH} = g \delta_{a,b} \)
--     ,
--     \(\partial r_a^\mathrm{LA} / \partial r_b^\mathrm{LAC} = (1 - g) \delta_{a,b}\)
--     , where \(a\) and \(b\) are the cartesian components \(x\), \(y\) or \(z\) of the gradient.
--   * Atoms of the real system, that are not part of the model system (set 4 atoms) get a
--     contribution of 0 from the model system.
--   * Atoms of the real system, that are part of the model system, get a full contribution from the
--     model system.
--   * To obtain an element of the Jacobian all contributions are summed up.

-- TODO (phillip|p=5|#Improvement #ExternalDependency) - As soon as Massiv supports sparse arrays, use them! https://github.com/lehins/massiv/issues/50
{-# SCC getJacobian #-}
getJacobian ::
  MonadThrow m =>
  -- | Atoms of the real system.
  IntMap Atom ->
  -- | Atoms of the model system.
  IntMap Atom ->
  m (Matrix D Double)
getJacobian realAtoms modelAtoms = do
  let nModelAtoms = IntMap.size modelAtoms
      nRealAtoms = IntMap.size realAtoms
      realLinkAtoms = IntMap.filter (isAtomLink . isLink) realAtoms
      realLinkAtomKeys = IntMap.keysSet realLinkAtoms
      modelLinkAtoms =
        flip IntMap.withoutKeys realLinkAtomKeys
          . IntMap.filter (isAtomLink . isLink)
          $ modelAtoms
      -- Translations of different mapping schemes:
      --   - Global dense mapping means, that the real system atoms are treated as if they were dense
      --      and 0-based (array like)
      --   - Local dense mapping means, that the model system atoms are treated as if they were dense
      --     and 0-based
      modelAtomKeys = IntMap.keys modelAtoms
      allAtomKeys = IntMap.keys $ IntMap.union realAtoms modelAtoms
      sparse2DenseGlobal = IntMap.fromAscList $ S.zip allAtomKeys [0 ..]
      dense2SparseGlobal = Massiv.fromList Par allAtomKeys :: Vector U Int
      sparse2DenseLocal = IntMap.fromAscList $ S.zip modelAtomKeys [0 ..]
      -- This is the mapping from global dense indices to local dense indices. This means if you ask
      -- for a real system atom, you will get 'Just' the dense index of the same atom in the model
      -- system or 'Nothing' if this atom is not part of the model system.
      global2Local :: IntMap (Maybe Int)
      global2Local =
        let globalKeys = [0 .. nRealAtoms - 1]
            global2LocalAssoc =
              fmap
                ( \globalKey ->
                    let sparseKey = dense2SparseGlobal Massiv.!? globalKey :: Maybe Int
                        localKey = flip IntMap.lookup sparse2DenseLocal =<< sparseKey
                     in (globalKey, localKey)
                )
                globalKeys
         in IntMap.fromAscList global2LocalAssoc

  -- Create a Matrix like HashMap, similiar to the BondMat stuff, that contains for pairs of
  -- dense (local modelKey, global realKey) scaling factors g. There should be one pair per link.
  -- atom.
  -- This is close to the final jacobian but not there yet.
  ((linkToModelG, linkToRealG) :: (HashMap (Int, Int) Double, HashMap (Int, Int) Double)) <-
    IntMap.foldlWithKey'
      ( \hashMapAcc' linkKey linkAtom -> do
          (linkToModelAcc, linkToRealAcc) <- hashMapAcc'

          let denseLinkLocalKey = sparse2DenseLocal IntMap.!? linkKey
              sparseModelKey = linkAtom ^? #isLink % _IsLink % #linkModelPartner
              denseModelGlobalKey = (sparse2DenseGlobal IntMap.!?) =<< sparseModelKey
              sparseRealKey = linkAtom ^? #isLink % _IsLink % #linkRealPartner
              denseRealGlobalKey = (sparse2DenseGlobal IntMap.!?) =<< sparseRealKey
              gFactor' = linkAtom ^? #isLink % _IsLink % #linkGFactor

          (linkIx, modelIx, realIx) <-
            case (denseLinkLocalKey, denseModelGlobalKey, denseRealGlobalKey) of
              (Just l, Just m, Just r) -> return (l, m, r)
              _ ->
                throwM $
                  MolLogicException
                    "getJacobian"
                    "While checking for atoms, that are influenced by link atoms, either a model system\
                    \ atom or a real system atom, that must be associated to the link atom, could not be\
                    \ found."

          gFactor <- case gFactor' of
            Nothing ->
              throwM $
                MolLogicException
                  "getJacobian"
                  "The scaling factor g for this link atom could not be found."
            Just g -> return g

          return
            ( HashMap.insert (linkIx, modelIx) (1 - gFactor) linkToModelAcc,
              HashMap.insert (linkIx, realIx) gFactor linkToRealAcc
            )
      )
      (return (HashMap.empty, HashMap.empty))
      modelLinkAtoms

  let jacobian' :: Matrix D Double
      jacobian' =
        Massiv.makeArray
          Par
          (Sz (3 * nModelAtoms :. 3 * nRealAtoms))
          ( \(modelCartIx :. realCartIx) ->
              let -- The indices of the matrix are the expanded atom indices, meaning that actually 3
                  -- values per atom (x, y, z) are present. These are the atom indices again now.
                  modelIx = modelCartIx `div` 3
                  realIx = realCartIx `div` 3

                  -- This is the cartesian component of the gradient of an atom. 0:x, 1:y, 2:z
                  modelComponent = modelCartIx `mod` 3
                  realComponent = realCartIx `mod` 3

                  -- Check wether the current real atom is also in the model system.
                  realInModel = join $ global2Local IntMap.!? realIx

                  -- If the real atom is present in the model system, the gradient of the model system
                  -- for this atom is used instead. Only use the gradient for the appropriate cartesian
                  -- components (x component provides x component, but not x provided y).
                  defaultValue = case realInModel of
                    Nothing -> 0
                    Just localModelKey ->
                      if modelIx == localModelKey && modelComponent == realComponent then 1 else 0

                  -- Check if the current pair belogns to a link atom in the model system. Keep it only
                  -- if the cartesian components match.
                  (link2ModelGValue, link2RealGValue) =
                    let link2ModelG = fromMaybe 0 $ HashMap.lookup (modelIx, realIx) linkToModelG
                        link2RealG = fromMaybe 0 $ HashMap.lookup (modelIx, realIx) linkToRealG
                     in if modelComponent == realComponent then (link2ModelG, link2RealG) else (0, 0)
               in defaultValue + link2ModelGValue + link2RealGValue
          )
  return jacobian'

----------------------------------------------------------------------------------------------------

-- | Applies a function that somehow combines the two set of multipoles.
combineMultipoles :: (Double -> Double -> Double) -> Multipoles -> Multipoles -> Multipoles
combineMultipoles f a b =
  let newMonopole = do
        mA <- a ^. #monopole
        mB <- b ^. #monopole
        return $
          Monopole
            { q00 = f (mA ^. #q00) (mB ^. #q00)
            }
      newDipole = do
        dA <- a ^. #dipole
        dB <- b ^. #dipole
        return $
          Dipole
            { q10 = f (dA ^. #q10) (dB ^. #q10),
              q11c = f (dA ^. #q11c) (dB ^. #q11c),
              q11s = f (dA ^. #q11s) (dB ^. #q11s)
            }
      newQuadrupole = do
        qA <- a ^. #quadrupole
        qB <- b ^. #quadrupole
        return $
          Quadrupole
            { q20 = f (qA ^. #q20) (qB ^. #q20),
              q21c = f (qA ^. #q21c) (qB ^. #q21c),
              q21s = f (qA ^. #q21s) (qB ^. #q21s),
              q22c = f (qA ^. #q22c) (qB ^. #q22c),
              q22s = f (qA ^. #q22s) (qB ^. #q22s)
            }
      newOctopole = do
        oA <- a ^. #octopole
        oB <- b ^. #octopole
        return $
          Octopole
            { q30 = f (oA ^. #q30) (oB ^. #q30),
              q31c = f (oA ^. #q31c) (oB ^. #q31c),
              q31s = f (oA ^. #q31s) (oB ^. #q31s),
              q32c = f (oA ^. #q32c) (oB ^. #q32c),
              q32s = f (oA ^. #q32s) (oB ^. #q32s),
              q33c = f (oA ^. #q33c) (oB ^. #q33c),
              q33s = f (oA ^. #q33s) (oB ^. #q33s)
            }
      newHexadecapole = do
        hA <- a ^. #hexadecapole
        hB <- b ^. #hexadecapole
        return $
          Hexadecapole
            { q40 = f (hA ^. #q40) (hB ^. #q40),
              q41c = f (hA ^. #q41c) (hB ^. #q41c),
              q41s = f (hA ^. #q41s) (hB ^. #q41s),
              q42c = f (hA ^. #q42c) (hB ^. #q42c),
              q42s = f (hA ^. #q42s) (hB ^. #q42s),
              q43c = f (hA ^. #q43c) (hB ^. #q43c),
              q43s = f (hA ^. #q43s) (hB ^. #q43s),
              q44c = f (hA ^. #q44c) (hB ^. #q44c),
              q44s = f (hA ^. #q44s) (hB ^. #q44s)
            }
   in Multipoles
        { monopole = newMonopole,
          dipole = newDipole,
          quadrupole = newQuadrupole,
          octopole = newOctopole,
          hexadecapole = newHexadecapole
        }

----------------------------------------------------------------------------------------------------

-- | Applies a function to multipoles.
modifyMultipole :: (Double -> Double) -> Multipoles -> Multipoles
modifyMultipole f a =
  let newMonopole = do
        mA <- a ^. #monopole
        return $
          Monopole
            { q00 = f (mA ^. #q00)
            }
      newDipole = do
        dA <- a ^. #dipole
        return $
          Dipole
            { q10 = f (dA ^. #q10),
              q11c = f (dA ^. #q11c),
              q11s = f (dA ^. #q11s)
            }
      newQuadrupole = do
        qA <- a ^. #quadrupole
        return $
          Quadrupole
            { q20 = f (qA ^. #q20),
              q21c = f (qA ^. #q21c),
              q21s = f (qA ^. #q21s),
              q22c = f (qA ^. #q22c),
              q22s = f (qA ^. #q22s)
            }
      newOctopole = do
        oA <- a ^. #octopole
        return $
          Octopole
            { q30 = f (oA ^. #q30),
              q31c = f (oA ^. #q31c),
              q31s = f (oA ^. #q31s),
              q32c = f (oA ^. #q32c),
              q32s = f (oA ^. #q32s),
              q33c = f (oA ^. #q33c),
              q33s = f (oA ^. #q33s)
            }
      newHexadecapole = do
        hA <- a ^. #hexadecapole
        return $
          Hexadecapole
            { q40 = f (hA ^. #q40),
              q41c = f (hA ^. #q41c),
              q41s = f (hA ^. #q41s),
              q42c = f (hA ^. #q42c),
              q42s = f (hA ^. #q42s),
              q43c = f (hA ^. #q43c),
              q43s = f (hA ^. #q43s),
              q44c = f (hA ^. #q44c),
              q44s = f (hA ^. #q44s)
            }
   in Multipoles
        { monopole = newMonopole,
          dipole = newDipole,
          quadrupole = newQuadrupole,
          octopole = newOctopole,
          hexadecapole = newHexadecapole
        }

----------------------------------------------------------------------------------------------------

-- | Redistributes link moments from the link atoms over the other atoms.
redistributeLinkMoments :: Molecule -> Molecule
redistributeLinkMoments mol@Molecule {atoms} =
  let nonDummyPoles = fmap (^. #multipoles) nonDummy
      redistRealPoles = redistributeLinkMoments' nonDummyPoles (IntMap.keysSet realAtoms)
      updatedReals = IntMap.intersectionWith (\mp a -> a & #multipoles .~ mp) redistRealPoles realAtoms
      zeroMPlinks = fmap (\a -> a & #multipoles .~ def) linkAtoms
      allAtomsUpdated = IntMap.unions [updatedReals, zeroMPlinks, dummyAtoms]
   in mol {atoms = allAtomsUpdated}
  where
    (dummyAtoms, nonDummy) = IntMap.partition isDummy atoms
    (linkAtoms, realAtoms) = IntMap.partition (isAtomLink . isLink) nonDummy

-- | Redistributes the multipole moments of some link atoms homogenously over the other atoms.
redistributeLinkMoments' ::
  -- | A map of multipoles as obtained from a calculation for example. The keys must correspond to
  -- the keys of the atoms.
  IntMap Multipoles ->
  -- | The set of "real" atoms (not dummy and not link).
  IntSet ->
  -- | New multipoles moments, restricted to the set of real atoms.
  IntMap Multipoles
redistributeLinkMoments' allMoments realAtoms =
  let -- Link atoms of this layer.
      linkAtoms = IntMap.keysSet allMoments IntSet.\\ realAtoms
      linkMoments = IntMap.restrictKeys allMoments linkAtoms
      realMomentsOrig = IntMap.restrictKeys allMoments realAtoms

      -- The sum of all moments on the link atoms.
      nRealAtoms = IntSet.size realAtoms
      sumOfLinkMoments = IntMap.foldl' (combineMultipoles (+)) zeroMoment linkMoments
      linkMomentsScaled = modifyMultipole (/ fromIntegral nRealAtoms) sumOfLinkMoments

      -- Distribute the link atom multipoles homogenously over the real atoms.
      realMultipolesDist = fmap (combineMultipoles (+) linkMomentsScaled) realMomentsOrig
   in realMultipolesDist
  where
    zeroMoment =
      Multipoles
        { monopole = Just $ Monopole 0,
          dipole = Just $ Dipole 0 0 0,
          quadrupole = Just $ Quadrupole 0 0 0 0 0,
          octopole = Just $ Octopole 0 0 0 0 0 0 0,
          hexadecapole = Just $ Hexadecapole 0 0 0 0 0 0 0 0 0
        }

----------------------------------------------------------------------------------------------------

-- | This function takes a local MC-ONIOM2 setup and removes link tag of atoms from the model
-- systems, if they were already link atoms in the real system.
removeRealLinkTagsFromModel ::
  -- | The real system.
  Molecule ->
  -- | A model system directly below the real system.
  Molecule ->
  Molecule
removeRealLinkTagsFromModel realMol modelCentre =
  let realLinkAtoms =
        IntMap.keysSet . IntMap.filter (isAtomLink . isLink) $ realMol ^. #atoms

      -- The model centres, but all atoms, that were already a link atom in the real system, do not
      -- longer have the link tag.
      newModel =
        let modelAtoms = modelCentre ^. #atoms
            modelAtomsClean =
              IntMap.mapWithKey
                ( \atomKey atom ->
                    if atomKey `IntSet.member` realLinkAtoms
                      then atom & #isLink .~ NotLink
                      else atom
                )
                modelAtoms
         in modelCentre & #atoms .~ modelAtomsClean
   in newModel

----------------------------------------------------------------------------------------------------

-- | Obtain all calculations on layers, that need to be performed on a molecule in hierarchical
-- order (depth-first).
getAllLayerCalcIDsHierarchically :: Molecule -> Seq LayerCalcID
getAllLayerCalcIDsHierarchically mol =
  molFoldlWithMolID
    ( \idAcc currentMolID currentMol ->
        let originalCntxt = currentMol ^? #calculations % _Just % #original
            inheritedCntxt = currentMol ^? #calculations % _Just % #inherited % _Just
            oID = case originalCntxt of
              Nothing -> mempty
              Just cntxt ->
                let frags = Map.keys <$> cntxt ^? #gmbe % _Just
                 in pure . fromMaybe (Monolithic currentMolID Original) $
                      Fragments currentMolID Original <$> frags
            iID = case inheritedCntxt of
              Nothing -> mempty
              Just cntxt ->
                let frags = Map.keys <$> cntxt ^? #gmbe % _Just
                 in pure . fromMaybe (Monolithic currentMolID Inherited) $
                      Fragments currentMolID Inherited <$> frags
         in idAcc <> iID <> oID
    )
    mempty
    mol

----------------------------------------------------------------------------------------------------

-- | Get all layer IDs of a molecule in ONIOM hierarchical order (depth first order).
getAllMolIDsHierarchically :: Molecule -> Seq MolID
getAllMolIDsHierarchically mol =
  molFoldlWithMolID
    ( \idAcc currentMolID _ -> idAcc |> currentMolID
    )
    Empty
    mol

----------------------------------------------------------------------------------------------------

-- | Generic update of some atomic positions. The updated atoms need to be specified by an 'IntSet'
-- but the position vector represents an update to all real top layer atoms. The coordinates in the
-- vector need to be in the same (strictly ascending) order as the atoms in the 'IntSet' (always
-- strictly ascending).
{-# SCC updatePositionsPosVec #-}
updatePositionsPosVec :: MonadThrow m => Vector S Double -> IntSet -> Molecule -> m Molecule
updatePositionsPosVec pos sel mol = do
  -- Obtain all top level atoms.
  let molAtomsSel = IntMap.keysSet $ mol ^. #atoms

  -- Associate new positions with individual atoms.
  pos3 <- Massiv.map (VectorS . compute @S) . outerSlices <$> resizeM (Sz $ IntSet.size molAtomsSel :. 3) pos
  atomPosMap <- associate pos3 molAtomsSel mempty

  -- Update all atoms with new positions.
  updatePositions (atomPosMap `IntMap.restrictKeys` sel) mol
  where
    -- A zipper for vectors with sets to construct an IntMap
    associate :: (Source r e, MonadThrow m) => Vector r e -> IntSet -> IntMap e -> m (IntMap e)
    associate vec selSet acc
      | sizeV /= sizeS = throwM $ MolLogicException "updatePositionsGeneric" "mismatch between number of atoms and coordinates"
      | sizeV == 0 && sizeS == 0 = pure acc
      | sizeV == 1 && sizeS == 1 = IntMap.insert <$> headSet <*> headVec <*> pure acc
      | otherwise =
        let newAcc = IntMap.insert <$> headSet <*> headVec <*> pure acc
         in join $ associate tailVec <$> tailSet <*> newAcc
      where
        localExc = SpicyIndirectionException "updatePositionsPosVec"
        Sz sizeV = Massiv.size vec
        sizeS = IntSet.size selSet
        splitSet = maybe2MThrow (localExc "no atoms to update left") $ IntSet.minView selSet
        headSet = fst <$> splitSet
        tailSet = snd <$> splitSet
        headVec = headM vec
        tailVec = Massiv.tail vec

----------------------------------------------------------------------------------------------------

-- | Recursively update the coordinates of all real and link atoms in all layers. Link atoms that
-- were found get new coordinates on this layer and then their new coordinates are added to the
-- 'IntMap' of new atom coordinates. This allows to safely update link atom coordinates in
-- deeper layers, that depend on link atoms of higher layers. Dummy atoms in the layer will be
-- removed. But this is what we would want anyway as also the layers above are probably now
-- displaced and the dummies need to be regenerated by another function.
updatePositions :: MonadThrow m => IntMap (VectorS Double) -> Molecule -> m Molecule
updatePositions updateCoords molLayer = do
  -- Update all real atoms of this layer excluding Dummy atoms.
  let oldCoords = fmap (^. #coordinates) $ molLayer ^. #atoms
      newCoords = updateCoords `IntMap.union` oldCoords
      atoms = molLayer ^. #atoms
      linkAtoms = IntMap.filter (isAtomLink . isLink) atoms
      updatedRealAtoms =
        IntMap.intersectionWith
          ( \newC oldA -> oldA & #coordinates .~ newC
          )
          newCoords
          atoms
  updateLinkAtoms <-
    maybe2MThrow (localExc "atom key not found for coordinate update") $
      traverse
        ( \lA -> case isLink lA of
            IsLink LinkAtomInfo {..} -> do
              coordsMP <- getVectorS <$> newCoords IntMap.!? linkModelPartner
              coordsRP <- getVectorS <$> newCoords IntMap.!? linkRealPartner
              newLinkCoords <- VectorS <$> calcLinkCoords coordsMP coordsRP linkGFactor
              return $ lA & #coordinates .~ newLinkCoords
            NotLink -> return lA
        )
        linkAtoms

  -- Updates of this layers data and inputs for next recursion steps.
  let updateNonDummies = updateLinkAtoms <> updatedRealAtoms
      thisLayerUpdated = molLayer & #atoms .~ updateNonDummies
      newLinkCoords = fmap (^. #coordinates) updateLinkAtoms
      newCoordsUpdated = newLinkCoords <> newCoords

  -- Recursively update all sublayers.
  let subMols = thisLayerUpdated ^. #subMol
  subMolsUpdated <- traverse (updatePositions newCoordsUpdated) subMols

  return $ thisLayerUpdated & #subMol .~ subMolsUpdated
  where
    localExc = MolLogicException "updatePositions"

----------------------------------------------------------------------------------------------------

-- | Reduces the search radius of a neighbour list to a smaller distance of neighbours. This should
-- be more efficient than building a new neighbourlist with a different radius.
shrinkNeighbourList ::
  MonadThrow m =>
  IntMap Atom ->
  Map Double NeighbourList ->
  Double ->
  m (Map Double NeighbourList)
shrinkNeighbourList atoms oldNLs newDist
  | isNothing maxDist = throwM . localExc $ "No neighbourlists available."
  | maxDist < Just newDist = throwM . localExc $ "New distance of the neighbour list must be smaller than the old one"
  | otherwise = do
    -- Obtain the neighbourlist which has a larger search distance but is closest to the new one.
    -- This saves time in computing distances.
    bestMatchNL <-
      maybe2MThrow
        ( localExc
            "no matching neighbourlist with a distance larger\
            \ than the new search distance could be found"
        )
        . fmap fst
        . Map.minView
        . Map.filterWithKey (\k _ -> k > newDist)
        $ oldNLs

    -- Check all distances and remove those atoms, which have a higher distance than the search
    -- distance
    newNL <- IntMap.traverseWithKey (filterNeigbours atoms newDist) bestMatchNL

    -- Return the old neighbourlists together with the new shrinked one.
    return $ oldNLs <> Map.singleton newDist newNL
  where
    localExc = MolLogicException "shrinkNeighbourList"
    maxDist = fmap (fst . fst) . Map.maxViewWithKey $ oldNLs
    filterNeigbours :: MonadThrow m => IntMap Atom -> Double -> Int -> IntSet -> m IntSet
    filterNeigbours atoms' dist origin targets = do
      originCoord <-
        maybe2MThrow (localExc "Origin atom does not exist in the set of atoms.")
          . fmap getVectorS
          $ (atoms' IntMap.!? origin ^? _Just % #coordinates)
      let filteredTargets =
            IntSet.filter
              ( \t ->
                  let targetCoord = getVectorS <$> (atoms' IntMap.!? t ^? _Just % #coordinates)
                   in case targetCoord of
                        Nothing -> False
                        Just coord -> case distance originCoord coord of
                          Nothing -> False
                          Just d -> d <= dist
              )
              targets
      return filteredTargets

----------------------------------------------------------------------------------------------------

-- | Adds information of the link atom to the neighbour list. If the atom is a link atom, the
-- following changes to the neigbhourlist will be performed:
--
--  * The neighbours of the link atom will be the neighbours of the capped model atom.
--  * All atoms, that are neighbours of the capped model atom, will get the link atom as additional
--    neighbour.
--  * The link atom and the capped model atom will be neighbours to each other.
--
-- Atoms of the real layer will possibly be contained, if they are still in the neighbour list.
--
-- If the atom is not a link atom, the neighbourlist is returned unchanged.
addLinksToNeighbourList :: MonadThrow m => (Int, Atom) -> NeighbourList -> m NeighbourList
addLinksToNeighbourList (k, Atom {isLink}) oldNL = case isLink of
  IsLink LinkAtomInfo {..} -> do
    nlHostNeighbours <-
      maybe2MThrow (MolLogicException "addLinksToNeighbourList" "Host atom neighbours could not be found in the neighbour list.") $
        oldNL IntMap.!? linkModelPartner
    let linkAdded = IntMap.insert k (nlHostNeighbours <> IntSet.singleton linkModelPartner) oldNL
        localUpdated = IntSet.foldl (\acc a -> IntMap.adjust (<> IntSet.singleton k) a acc) linkAdded nlHostNeighbours
    return localUpdated
  NotLink -> return oldNL

----------------------------------------------------------------------------------------------------

-- | Isolate a molecule layer, usually in preparation for printing. Cleaning involves:
--
--  * Remove all dummy atoms
--  * Removing all sub-molecules
--  * Update the bond matrix and the groups
isolateMoleculeLayer :: Molecule -> OniomLayer
isolateMoleculeLayer mol =
  OniomLayer $
    mol
      & #atoms %~ flip IntMap.restrictKeys realAtomInds
      & #group % each % #atoms %~ IntSet.intersection realAtomInds
      & #bonds %~ flip cleanBondMatByAtomInds realAtomInds
      & #subMol .~ mempty
  where
    realAtomInds = IntMap.keysSet . IntMap.filter (\a -> not $ a ^. #isDummy) $ mol ^. #atoms

----------------------------------------------------------------------------------------------------

-- | Spicy tree notation string for identifying an ONIOM node in the tree.
molID2OniomHumanID :: MolID -> Text
molID2OniomHumanID Seq.Empty = "0"
molID2OniomHumanID molID =
  let depth = Seq.length molID
      idTree =
        foldr'
          ( \currentID textAcc ->
              let offSet = fromEnum 'A'
                  idLetter = toEnum $ currentID + offSet
               in textAcc `Text.snoc` idLetter
          )
          (tshow depth)
          molID
   in idTree

----------------------------------------------------------------------------------------------------

-- | Spicy tree notation string for identifying an ONIOM node in the tree.
calcID2Human :: CalcID -> Text
calcID2Human CalcID {..} =
  let oniomString = molID2OniomHumanID molID
      keyString = tshow calcK
      fragmentString = case frag of
        Nothing -> "Groups: none"
        Just AuxMonomer {constGroups} -> "Groups: " <> (tshow . IntSet.toAscList $ constGroups)
   in oniomString <> " (" <> keyString <> ") (" <> fragmentString <> ")"

----------------------------------------------------------------------------------------------------

-- | Horizontal slices through the ONIOM tree. Gets all layers of the ONIOM finger tree that are on
-- the same depth, no matter if they are in the same branch of the tree or not. The outer sequence
-- is the hierarchical level.
horizontalSlices :: Molecule -> Seq (Seq Molecule)
horizontalSlices mol =
  let allMolIDs = getAllMolIDsHierarchically mol
      idHierarchyGroups = groupBy (\a b -> Seq.length a == Seq.length b) allMolIDs
      molHierarchyGroups = traverse (\molID -> getMultipleMols molID mol) idHierarchyGroups
   in fromMaybe mempty molHierarchyGroups
  where
    -- Apply multiple MolID lenses to a molecule and get all results. No lens must fail.
    getMultipleMols :: Traversable t => t MolID -> Molecule -> Maybe (t Molecule)
    getMultipleMols ids m = traverse (\i -> m ^? molIDLensGen i) ids

----------------------------------------------------------------------------------------------------

-- | Transforms a dense gradient vector to the sparse representation as used by the corresponding
-- atoms.
gradDense2Sparse :: MonadThrow m => Molecule -> m (IntMap (Vector S Double))
gradDense2Sparse mol = do
  let atomKeys = IntMap.keysSet . IntMap.filter (not . isDummy) $ mol ^. #atoms
  denseGrad <-
    maybe2MThrow (localExc "Gradient transformation was requested but not gradient is available") $
      getVectorS <$> mol ^. #energyDerivatives % #gradient
  denseGrad3 <- toList . outerSlices <$> resizeM (Sz $ IntSet.size atomKeys :. 3) denseGrad
  return . IntMap.fromAscList $ S.zip (IntSet.toAscList atomKeys) denseGrad3
  where
    localExc = MolLogicException "gradDense2Sparse"

----------------------------------------------------------------------------------------------------

-- | Calculate geometry optimisation convergence criteria by comparing the molecule before and after
-- displacement. A selection must be specified, which atoms are optimised. Gradients and
-- displacements will be checked only for those. The gradients are checked before displacement, the
-- displacement is checked between old and new.
calcGeomConv ::
  MonadThrow m =>
  -- | 'Atom' keys, whose positions are optimised. Geometry convergence criteria will only be
  -- calculated on this set.
  IntSet ->
  -- | The system, which is being optimised before the displacement step.
  Molecule ->
  -- | The same system but after a displacement step.
  Molecule ->
  m GeomConv
calcGeomConv sel molOld molNew = do
  let eOld = molOld ^. #energyDerivatives % #energy
      eNew = molNew ^. #energyDerivatives % #energy

  let selAtomsOld = flip IntMap.restrictKeys sel $ molOld ^. #atoms
      selAtomsNew = flip IntMap.restrictKeys sel $ molNew ^. #atoms
  cOld <- fmap (compute @U) . concatM 1 . fmap getVectorS $ selAtomsOld ^.. each % #coordinates
  cNew <- fmap (compute @U) . concatM 1 . fmap getVectorS $ selAtomsNew ^.. each % #coordinates
  gNewIM <- flip IntMap.restrictKeys sel <$> gradDense2Sparse molNew
  gNew <- fmap (compute @U) . concatM 1 $ gNewIM

  disp <- Massiv.map abs <$> (cOld .-. cNew)
  maxForce <- Massiv.maximumM . Massiv.map abs $ gNew
  maxDisp <- Massiv.maximumM . Massiv.map abs $ disp
  return
    GeomConv
      { rmsForce = Just . rms $ gNew,
        maxForce = Just maxForce,
        rmsDisp = Just . rms $ disp,
        maxDisp = Just maxDisp,
        eDiff = (-) <$> eNew <*> eOld
      }

----------------------------------------------------------------------------------------------------

-- | Multipole transfer from an 'Original' calculation output to the atoms of the layer.
multipoleTransfer :: MonadThrow m => Molecule -> m Molecule
multipoleTransfer mol@Molecule {atoms, calculations} = do
  -- Obtain multipoles as by the calculation output.
  mPolesOut <-
    maybe2MThrow (localExc "No multipoles available from output") $
      calculations ^? _Just % #original % #output % _Just % #multipoles

  -- Redistribute link moments from the calculation output to the other atoms.
  let mPolesOutRedis = redistributeLinkMoments' mPolesOut (IntMap.keysSet atomsNeedingPoles)

  -- Check if all multipoles, that are necessary are available.
  unless (IntMap.keysSet atomsNeedingPoles == IntMap.keysSet mPolesOutRedis) . throwM . localExc $
    "Mismatch between available and required multipoles"

  -- Update the atoms with the multipoles from the output.
  let updatedAtomsWithPoles =
        IntMap.intersectionWith
          (\a p -> a & #multipoles .~ p)
          atomsNeedingPoles
          mPolesOutRedis
      allAtomsUpdated = updatedAtomsWithPoles `IntMap.union` atoms
  return $ mol & #atoms .~ allAtomsUpdated
  where
    atomsNeedingPoles =
      IntMap.filter (\a -> not $ (isAtomLink . isLink $ a) || isDummy a) atoms
    localExc = MolLogicException "multipoleTransfer"

----------------------------------------------------------------------------------------------------

-- | Get the coordinates of the real system atoms. __The coordinates will be converted to Bohr!__
getCoordsNetVec :: MonadThrow m => Molecule -> m NetVec
getCoordsNetVec mol = do
  coordVecMassiv <-
    compute @S . Massiv.map angstrom2Bohr . flatten
      <$> getCoordinatesAs3NMatrix @S (mol ^. #atoms)

  return . NetVec . Massiv.toVector $ coordVecMassiv

----------------------------------------------------------------------------------------------------

-- | Form a new 'Group' from a group of atoms.
groupFromAtomKeys :: IntMap Atom -> Group
groupFromAtomKeys atoms =
  let initGroup =
        Group
          { label = mempty,
            chain = Nothing,
            atoms = IntMap.keysSet atoms,
            charge = 0,
            excEl = 0
          }
      finalGroup =
        IntMap.foldl'
          ( \gAcc Atom {formalCharge, formalExcEl} ->
              gAcc & #charge %~ (+ formalCharge)
                & #excEl %~ (+ formalExcEl)
          )
          initGroup
          atoms
   in finalGroup

----------------------------------------------------------------------------------------------------

-- | Conversion of a molecule to a graph. Looses a lot of information. Only the information in
-- 'Molecule.atoms' and 'Molecule.bonds' is kept.
mol2Graph :: Molecule -> MolGraph
mol2Graph Molecule {atoms, bonds} =
  let atomNodes = IntMap.toAscList atoms
      bondEdges =
        fmap
          ( \((o, t), bo) ->
              (o, t, bo)
          )
          . HashMap.toList
          . HashMap.filter (/= None)
          $ bonds
   in Graph.undir $ Graph.mkGraph atomNodes bondEdges

{-
====================================================================================================
-}

-- | Check multipole convergence by RMS between two iterations. This requires atomic multipole maps
-- to be identical between iterations.
checkMultipoleConvergence ::
  MonadThrow m =>
  Double ->
  IntMap Multipoles ->
  IntMap Multipoles ->
  m (Bool, Double)
checkMultipoleConvergence convThresh oldMPs newMPs = do
  -- Check sanity of multipole maps.
  unless (IntMap.keys oldMPs == IntMap.keys newMPs) . throwM $ DataStructureException "checkMultipoleConvergence" "Multipole maps to compare do not match in their keys."

  let oldMPv = Massiv.sfromList . IntMap.elems $ oldMPs
      newMPv = Massiv.sfromList . IntMap.elems $ newMPs
      mpRMS = rms $ Massiv.szipWith mpAbsDiff oldMPv newMPv

  return (mpRMS <= abs convThresh, mpRMS)
  where
    -- Absolute differences between all multipole components.
    mpAbsDiff mpA mpB = sumMPDiffs $ combineMultipoles (\a b -> abs $ a - b) mpA mpB

    -- Sum of all multipole components. Only meaningful for differences.
    sumMPDiffs mp =
      sum . fmap (fromMaybe 0) $
        [ -- Monopole
          mp ^? #monopole % _Just % #q00,
          -- Dipole
          mp ^? #dipole % _Just % #q10,
          mp ^? #dipole % _Just % #q11c,
          mp ^? #dipole % _Just % #q11s,
          -- Quadrupole
          mp ^? #quadrupole % _Just % #q20,
          mp ^? #quadrupole % _Just % #q21c,
          mp ^? #quadrupole % _Just % #q21s,
          mp ^? #quadrupole % _Just % #q22c,
          mp ^? #quadrupole % _Just % #q22s,
          -- Octopole
          mp ^? #octopole % _Just % #q30,
          mp ^? #octopole % _Just % #q31c,
          mp ^? #octopole % _Just % #q31s,
          mp ^? #octopole % _Just % #q32c,
          mp ^? #octopole % _Just % #q32s,
          mp ^? #octopole % _Just % #q33c,
          mp ^? #octopole % _Just % #q33s,
          -- Hexadecapole
          mp ^? #hexadecapole % _Just % #q40,
          mp ^? #hexadecapole % _Just % #q41c,
          mp ^? #hexadecapole % _Just % #q41s,
          mp ^? #hexadecapole % _Just % #q42c,
          mp ^? #hexadecapole % _Just % #q42s,
          mp ^? #hexadecapole % _Just % #q43c,
          mp ^? #hexadecapole % _Just % #q43s,
          mp ^? #hexadecapole % _Just % #q44c,
          mp ^? #hexadecapole % _Just % #q44s
        ]
