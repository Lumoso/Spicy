-- |
-- Module      : Spicy.Molecule.Internal.Computational
-- Description : Computational details of calculations
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This molecule provides types and functions to express the details of a calculation.
--
-- This module is an internal module. It is not intended for direct import; consider importing
-- 'Spicy.Molecule.Computational' instead.
module Spicy.Molecule.Internal.Computational
  ( -- * Programs
    Program (..),
    program,
    ProgramData (..),
    _XTB,
    _Psi4,
    _Turbomole,
    _QCHamiltonian,
    _GFN,
    GFN (..),
    renderGFN,

    -- * Generic information for all QC programs
    QCHamiltonian (..),
    BasisSet (..),
    SCF (..),
    Damp (..),
    RI (..),
    _OtherRI,
    RefWfn (..),
    DFT (..),
    _RKS,
    _UKS,
    Correlation (..),
    Excitations (..),

    -- * Optimiser Settings
    Optimisation (..),
    CoordType (..),
    HessianUpdate (..),
    MicroStep (..),
    OptType (..),
    _Minimum,
    _SaddlePoint,
    TSOptAlg (..),
    MinOptAlg (..),
    GeomConv (..),

    -- * Calculations
    Calculations (..),
    CalcContext (..),
    GMBEmap,
    CalcInput (..),
    CalcOutput (..),

    -- * ONIOM Embedding
    Embedding (..),
    _Mechanical,
    _Electronic,

    -- * Energy Derivatives
    EnergyDerivatives (..),

    -- * Wrapper Context
    WrapperTask (..),
    QMContext (..),
    MMContext (..),
    QMMMSpec (..),
    _QM,
    _MM,
  )
where

import Data.Aeson
import Data.Default
import Spicy.Aeson
import Spicy.Common
import Spicy.Molecule.Internal.Fragment
import Spicy.Molecule.Internal.Physical
import Spicy.Prelude
import Spicy.Wavefunction.Basis (Basis)
import Spicy.Wavefunction.Wavefunction (Wavefunction)
import Spicy.Wrapper.IPI.Types hiding (hessian, input)

-- | Identifier for one of the supported quantum chemistry programs.
data Program
  = Psi4
  | XTB
  | Turbomole
  deriving (Eq, Show, Generic)

instance ToJSON Program where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Program where
  parseJSON = genericParseJSON spicyJOption

program :: Getter ProgramData Program
program = to $ \pd -> case pd of
  Psi4Data _ -> Psi4
  XTBData _ -> XTB
  TurbomoleData _ -> Turbomole

----------------------------------------------------------------------------------------------------

-- | Program-specific description of the calculation niveau. Most quantum chemistry programs use
-- the 'QCHamiltonian' type, which is generic enough to express nearly all calculations.
data ProgramData
  = Psi4Data QCHamiltonian
  | XTBData GFN
  | TurbomoleData QCHamiltonian
  deriving (Eq, Show, Generic)

instance ToJSON ProgramData where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON ProgramData where
  parseJSON = genericParseJSON spicyJOption

_XTB :: Prism' ProgramData GFN
_XTB = prism' XTBData $ \s -> case s of
  XTBData b -> Just b
  _ -> Nothing

_Psi4 :: Prism' ProgramData QCHamiltonian
_Psi4 = prism' Psi4Data $ \s -> case s of
  Psi4Data info -> Just info
  _ -> Nothing

_Turbomole :: Prism' ProgramData QCHamiltonian
_Turbomole = prism' TurbomoleData $ \s -> case s of
  TurbomoleData info -> Just info
  _ -> Nothing

-- | Extra 'AffineFold', that gets the 'QCHamiltonian' regardless of constructor (if it supplies
-- ...).
_QCHamiltonian :: AffineFold ProgramData QCHamiltonian
_QCHamiltonian = afolding $ \s -> case s of
  TurbomoleData h -> Just h
  Psi4Data h -> Just h
  XTBData _ -> Nothing

-- | 'AffineFold' to retrieve the 'GFN' from a 'ProgramData' regardless of constuctor.
_GFN :: AffineFold ProgramData GFN
_GFN = afolding $ \s -> case s of
  XTBData g -> Just g
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Version of the GFN-Hamiltonian in XTB calculations.
data GFN
  = -- | Non-self-consistent tight binding hamiltonian GFN-0. Provides only monopoles.
    GFNZero
  | -- | Original version of the GFN-XTB semiempirical method GFN-1
    GFNOne
  | -- | Updated version of the semiempirical tight binding method GFN-2
    GFNTwo
  deriving (Eq, Show, Generic)

instance ToJSON GFN where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON GFN where
  parseJSON = genericParseJSON spicyJOption

-- | Convert the enumeration type representation to the digit expected by XTB.
renderGFN :: GFN -> Text
renderGFN GFNZero = "0"
renderGFN GFNOne = "1"
renderGFN GFNTwo = "2"

----------------------------------------------------------------------------------------------------

-- | Program agnostic information for quantum chemistry programs.
data QCHamiltonian = QCHamiltonian
  { -- | Basis set specifications, including auxiliary basis sets.
    basis :: BasisSet,
    -- | SCF convergence settings and thresholds
    scf :: Maybe SCF,
    -- | Reference wavefunction settings (e.g. Hartree-Fock or DFT)
    ref :: RefWfn,
    -- | Settings for resolution of identity
    ri :: Maybe RI,
    -- | Correlation of the wavefunction and excited states
    corr :: Maybe Correlation,
    -- | Excited states
    exc :: Maybe Excitations,
    -- | Other keywords to use verbatim. This allows for basically arbitrary customisations at the
    -- cost of easily producing inconsistent input files. Will usually be serialised last, so that
    -- in a stateful input file, those keywords may override others, that have been serialised by
    -- the other keyword fields.
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance ToJSON QCHamiltonian where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON QCHamiltonian where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Specifications of various basis sets for quantum chemistry packages. Most fields are restricted
-- to the builtin basis sets of a program and refer to a basis set by its name, e.g. it is not
-- possible to inline a custom basis set with these keywords. See:
--
--   * [Psi4](https://psicode.org/psi4manual/master/basissets.html#sec-basisbuiltin)
--   * [Turbomole](https://cosmologic-services.de/basis-sets/basissets.php)
--
-- If a custom basis set is required, the @other@ field can be used.
data BasisSet = BasisSet
  { -- | Name of a builtin primary orbital basis set
    basis :: Text,
    -- | Name of a builtin auxiliary coulomb-fitting basis
    jbas :: Maybe Text,
    -- | Name of a builtin auxiliary JK-fitting basis
    jkbas :: Maybe Text,
    -- | Name of a builtin effective core potential from the library
    ecp :: Maybe Text,
    -- | Name of a builtin correlation fitting basis from the library
    cbas :: Maybe Text,
    -- | Name of a builtin complementary auxiliary basis for explicitly correlated calculations
    cabs :: Maybe Text,
    -- | Other keywords to write verbatim. May be used to add custom basis sets, override previous
    -- settings, assign a different basis to specific atoms, etc.
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance FromJSON BasisSet where
  parseJSON = genericParseJSON spicyJOption

instance ToJSON BasisSet where
  toEncoding = genericToEncoding spicyJOption

----------------------------------------------------------------------------------------------------

-- | SCF convergence and threshold settings.
data SCF = SCF
  { -- | SCF convergence threshold as exponent, e.g. converges to \( 10^{-x} \) (usually orbital
    -- gradient norm), where you give \(x\).
    conv :: Natural,
    -- | Maximum number of iterations of the SCF
    iter :: Natural,
    -- | Damping control
    damp :: Maybe Damp,
    -- | Orbital level shift in Hartree
    shift :: Maybe Double,
    -- | Other SCF settings to be used verbatim
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance FromJSON SCF

instance ToJSON SCF

instance Default SCF where
  def = SCF {conv = 8, iter = 200, damp = def, shift = Nothing, other = Nothing}

----------------------------------------------------------------------------------------------------

-- | Settings of the SCF damping
data Damp = Damp
  { -- | Amount of old fock matrix to be mixed in. The exact meaning may depend on the program. This
    -- can be percent, but it could also be a fraction between \( [ 0 \dots 1 ] \).
    start :: Double,
    -- | On good convergece how much to reduce the mixing amount in each SCF step. Not used by all
    -- programs.
    step :: Double,
    -- | Minimum amount of the old fock matrix to be used in damping. Will not drop below this. Not
    -- used by all programs.
    lower :: Double
  }
  deriving (Eq, Show, Generic)

instance FromJSON Damp

instance ToJSON Damp

instance Default Damp where
  def = Damp {start = 0.75, step = 0.05, lower = 0.1}

----------------------------------------------------------------------------------------------------

-- | Type of RI approximation for the reference wavefunction.
data RI
  = -- | Coulomb fitting
    RIJ
  | -- | Coulomb and exchange fitting
    RIJK
  | -- | Any other RI method, e.g. Marij or RIJCOSX, identified by a string.
    OtherRI Text
  deriving (Eq, Show, Generic)

instance FromJSON RI

instance ToJSON RI

_OtherRI :: Prism' RI Text
_OtherRI = prism' OtherRI $ \ri -> case ri of
  OtherRI t -> Just t
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Reference wavefunction specification.
data RefWfn
  = -- | Restricted Hartree-Fock
    RHF
  | -- | Unrestricted Hartree-Fock. Needs a multiplicity, which it takes from the description of the
    -- system.
    UHF
  | -- | Restricted Kohn-Sham DFT
    RKS DFT
  | -- | Unrestricted Kohn-Sham DFT. Needs a multiplicity, which it takes from the description of
    -- the system.
    UKS DFT
  deriving (Eq, Show, Generic)

instance FromJSON RefWfn

instance ToJSON RefWfn

_RKS :: Prism' RefWfn DFT
_RKS = prism' RKS $ \s -> case s of
  RKS d -> Just d
  _ -> Nothing

_UKS :: Prism' RefWfn DFT
_UKS = prism' UKS $ \s -> case s of
  UKS d -> Just d
  _ -> Nothing

-- | Settings for density functional theory.
data DFT = DFT
  { -- | Density functional to be used
    functional :: Text,
    -- | DFT grid specification
    grid :: Maybe Text,
    -- | Dispersion correction
    disp :: Maybe Text,
    -- | Other keywords to put verbatim in the DFT input
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance FromJSON DFT

instance ToJSON DFT

----------------------------------------------------------------------------------------------------

-- | Correlation on top of the reference wavefunction.
data Correlation = Correlation
  { -- | The program module, which to use for correlation calculation. Not used in all programs.
    corrModule :: Maybe Text,
    -- | Wavefunciton correlation model to be used
    method :: Text,
    -- | Maximum number of iterations in convergence of correlation
    iter :: Maybe Natural,
    -- | Other keywords to put verbatim in the input
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance FromJSON Correlation

instance ToJSON Correlation

----------------------------------------------------------------------------------------------------

-- | Excitations. Either on top of the correlated wavefunction (if used) or ontop of the reference
-- wavefunction, if no correlation is given.
data Excitations = Excitations
  { -- | Number of states to be calculated.
    states :: Natural,
    -- | Possibly a target root, for which to calculate properties like gradients or densities
    target :: Maybe Natural,
    -- | Other information, that will be put verbatim into the input file.
    other :: Maybe [Text]
  }
  deriving (Eq, Show, Generic)

instance FromJSON Excitations

instance ToJSON Excitations

{-
====================================================================================================
-}

-- | Settings for optimisers. Those are specific for a given layer if microiterations are used.
-- If only macroiterations are used, the settings of the real system will be used.
data Optimisation = Optimisation
  { -- | The coordinate system, that is being used to calculate steps.
    coordType :: CoordType,
    -- | Maximum number of geometry optimisation iterations.
    maxCycles :: Int,
    -- | Recalculate a true hessian every n steps. If this is 'Nothing' no hessians will be
    -- calculated.
    hessianRecalc :: Maybe Int,
    -- | How the hessian is updated through the curse of the optimisation.
    hessianUpdate :: HessianUpdate,
    -- | Initial trust radius of the optimisation.
    trustRadius :: Double,
    -- | Maximum of the trust radius in optimisation.
    maxTrust :: Double,
    -- | Minimum of the trust radius in optimisation.
    minTrust :: Double,
    -- | Whether to perform a line search.
    lineSearch :: Bool,
    -- | Optimisation to either minima or saddle points.
    optType :: OptType,
    -- | Convergence criteria.
    convergence :: GeomConv,
    -- | Atom indices to freeze.
    freezes :: IntSet,
    -- | Pysisyphus connection settings.
    pysisyphus :: IPI,
    -- | Settings in case micro-optimisers are used.
    microStep :: MicroStep
  }
  deriving (Generic)

instance DefaultIO Optimisation where
  defIO = do
    defPysis <- defIO
    return
      Optimisation
        { coordType = DLC,
          maxCycles = 50,
          hessianRecalc = Nothing,
          hessianUpdate = BFGS,
          trustRadius = 0.3,
          minTrust = 0.1,
          maxTrust = 1.0,
          lineSearch = True,
          optType = Minimum RFO,
          pysisyphus = defPysis,
          convergence = def,
          freezes = mempty,
          microStep = LBFGS
        }

instance ToJSON Optimisation where
  toJSON
    Optimisation
      { coordType,
        maxCycles,
        hessianRecalc,
        hessianUpdate,
        trustRadius,
        maxTrust,
        minTrust,
        lineSearch,
        optType,
        convergence,
        freezes,
        microStep
      } =
      object
        [ "coordType" .= coordType,
          "maxCycles" .= maxCycles,
          "hessianRecalc" .= hessianRecalc,
          "hessianUpdate" .= hessianUpdate,
          "trustRadius" .= trustRadius,
          "maxTrust" .= maxTrust,
          "minTrust" .= minTrust,
          "lineSearch" .= lineSearch,
          "optType" .= optType,
          "convergence" .= convergence,
          "freezes" .= freezes,
          "microStep" .= microStep
        ]

----------------------------------------------------------------------------------------------------

-- | Type of coordinates used for optimisation.
data CoordType
  = -- | [Delocalised internal coordinates](https://pysisyphus.readthedocs.io/en/latest/coordinate_systems.html?highlight=DLC#delocalized-internal-coordinates-dlc)
    DLC
  | -- | [Cartesian coordinates](https://pysisyphus.readthedocs.io/en/latest/coordinate_systems.html?highlight=DLC#cartesian-coordinates)
    Cart
  | -- | [Redundant internal coordinates](https://pysisyphus.readthedocs.io/en/latest/coordinate_systems.html?highlight=DLC#redundant-internal-coordinates)
    Redund
  deriving (Eq, Show)

instance FromJSON CoordType where
  parseJSON v = case v of
    String "dlc" -> pure DLC
    String "cart" -> pure Cart
    String "redund" -> pure Redund
    _ -> fail "encountered unknown field for CoordType"

instance ToJSON CoordType where
  toJSON v = case v of
    DLC -> toJSON @Text "dlc"
    Cart -> toJSON @Text "cart"
    Redund -> toJSON @Text "redund"

----------------------------------------------------------------------------------------------------

-- | Algorithm to keep an approximate hessian up to date between optimisation steps, see
-- <https://pysisyphus.readthedocs.io/en/latest/_modules/pysisyphus/optimizers/hessian_updates.html>
data HessianUpdate
  = BFGS
  | FlowChart
  | DampedBFGS
  | Bofill
  deriving (Eq, Show)

instance FromJSON HessianUpdate where
  parseJSON v = case v of
    String "bfgs" -> pure BFGS
    String "flowchart" -> pure FlowChart
    String "damped_bfgs" -> pure DampedBFGS
    String "bofill" -> pure Bofill
    _ -> fail "encountered unknown field for TSOptAlg"

instance ToJSON HessianUpdate where
  toJSON v = case v of
    BFGS -> toJSON @Text "bfgs"
    FlowChart -> toJSON @Text "flowchart"
    DampedBFGS -> toJSON @Text "damped_bfgs"
    Bofill -> toJSON @Text "bofill"

----------------------------------------------------------------------------------------------------

-- | Available step types for steps with the micro-cycles.
data MicroStep
  = -- | Limited memory version of BFGS
    LBFGS
  | -- | Steepest descent
    SD
  | -- | Conjugate gradient with line searches
    CG
  deriving (Eq, Show, Generic)

instance FromJSON MicroStep where
  parseJSON v = case v of
    String "lbfgs" -> pure LBFGS
    String "sd" -> pure SD
    String "cg" -> pure CG
    _ -> fail "encountered unknown field for MicroStep"

instance ToJSON MicroStep where
  toJSON v = case v of
    LBFGS -> toJSON @Text "lbfgs"
    SD -> toJSON @Text "sd"
    CG -> toJSON @Text "cg"

----------------------------------------------------------------------------------------------------

-- | Optimisation type.
data OptType
  = -- | Optimisation to a local minimum
    Minimum MinOptAlg
  | -- | Optimisation to a transition state (saddle point of first order)
    SaddlePoint TSOptAlg
  deriving (Eq, Show, Generic)

instance FromJSON OptType

instance ToJSON OptType

-- Prisms
_Minimum :: Prism' OptType MinOptAlg
_Minimum = prism' Minimum $ \s -> case s of
  Minimum b -> Just b
  _ -> Nothing

_SaddlePoint :: Prism' OptType TSOptAlg
_SaddlePoint = prism' SaddlePoint $ \s -> case s of
  SaddlePoint b -> Just b
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Optimisation algorithms for transition state/saddle point search, see
-- <https://pysisyphus.readthedocs.io/en/latest/_modules/pysisyphus/optimizers/hessian_updates.html>
data TSOptAlg
  = RS_I_RFO
  | RS_P_RFO
  | TRIM
  deriving (Eq, Show)

instance FromJSON TSOptAlg where
  parseJSON v = case v of
    String "rsirfo" -> pure RS_I_RFO
    String "rsprfo" -> pure RS_P_RFO
    String "trim" -> pure TRIM
    _ -> fail "encountered unknown field for TSOptAlg"

instance ToJSON TSOptAlg where
  toJSON v = case v of
    RS_I_RFO -> toJSON @Text "rsirfo"
    RS_P_RFO -> toJSON @Text "rsprfo"
    TRIM -> toJSON @Text "trim"

----------------------------------------------------------------------------------------------------

-- | Optimisation algorithm for geometry optimisation to minima.
data MinOptAlg
  = RFO
  deriving (Eq, Show)

instance FromJSON MinOptAlg where
  parseJSON v = case v of
    String "rfo" -> pure RFO
    _ -> fail "encountered unknown field for MinOptAlg"

instance ToJSON MinOptAlg where
  toJSON RFO = toJSON @Text "rfo"

----------------------------------------------------------------------------------------------------

-- | Convergence criteria of a geometry optimisation. 'Nothing' values are not checked for
-- convergce. Units are Hartree and Angstrom (__not__ Bohr).
data GeomConv = GeomConv
  { rmsForce :: Maybe Double,
    maxForce :: Maybe Double,
    rmsDisp :: Maybe Double,
    maxDisp :: Maybe Double,
    eDiff :: Maybe Double
  }
  deriving (Eq, Show, Generic)

-- | 'Ord' instance, that can be used to check convergence. The left side is the value and the right
-- side the criterion.
instance Ord GeomConv where
  a `compare` b
    | a == b = EQ
    | all (== LT) allChecks = LT
    | otherwise = GT
    where
      allChecks =
        [ rmsForce a `compareMaybe` rmsForce b,
          maxForce a `compareMaybe` maxForce b,
          rmsDisp a `compareMaybe` rmsDisp b,
          maxDisp a `compareMaybe` maxDisp b,
          eDiff a `compareMaybe` eDiff b
        ]
      compareMaybe x y = case y of
        Nothing -> LT
        _ -> x `compare` y

instance ToJSON GeomConv where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON GeomConv where
  parseJSON = genericParseJSON spicyJOption

instance Default GeomConv where
  def =
    GeomConv
      { maxForce = Just $ 0.00045 * 1.88973,
        rmsForce = Just $ 0.00030 * 1.88973,
        maxDisp = Just $ 0.00180 * 1.88973,
        rmsDisp = Just $ 0.00120 * 1.88973,
        eDiff = Just 0.00001
      }

----------------------------------------------------------------------------------------------------

-- | Calculations to be performed on a given layer. For ONIOM those are two calculations per layer,
-- except the lowest level theory layer (real system), which is the phyiscally largest system.
data Calculations = Calculations
  { -- | The original calculation on a system. Usually the high-level calculation.
    original :: CalcContext,
    -- | Possibly a inherited calculation, usually the low-level calculation. Present in all layers
    -- except the real system.
    inherited :: Maybe CalcContext
  }
  deriving (Generic)

instance ToJSON Calculations where
  toEncoding = genericToEncoding spicyJOption

----------------------------------------------------------------------------------------------------

-- | The context of a calculation is the combination of its input and output values. This can both
-- describe monolitic (normal) calculations, or fragment-based GMBE calculations. A GMBE calculation
-- is present, if the 'gmbe' field has a 'Just' value. In the GMBE case, the expected 'AuxMonomer's
-- are described as a 'Map' to 'CalcOutput's, which are expected to be filled. A complete GMBE
-- output map can then be transformed by GMBE collectors to a monolithic calculation context,
-- therefore also populating the 'output' field.
data CalcContext = CalcContext
  { -- | Generic input for this level. In case of GMBE calculations values such as charges,
    -- multiplicities, directories, etc, are updated for each fragment and this specification always
    -- refers to the full system, not to the fragments.
    input :: CalcInput,
    -- | A monolithic output for this layer. If the 'gmbe' field is 'Nothing', this is a normal
    -- result from a single calculation. If the 'gmbe' field is a 'Just' value, then this layer has
    -- been processed by the GMBE fragment method. Still, this is field can be set to a 'Just' and
    -- should then hold the transformed GMBE results as obtained by GMBE transformers/collectors.
    output :: Maybe CalcOutput,
    -- | The GMBE sum and outputs. It maps from auxiliary monomers, that are required to be
    -- calculated to its factor in the sum and a calculation output.
    gmbe :: Maybe GMBEmap
  }
  deriving (Generic)

instance ToJSON CalcContext where
  toEncoding = genericToEncoding spicyJOption

----------------------------------------------------------------------------------------------------

-- | A mapping from auxiliary monomers to a factor which they contribute to the sum, and potentially
-- the calculation output for the 'AuxMonomer'.
type GMBEmap = Map AuxMonomer (Int, Maybe CalcOutput)

----------------------------------------------------------------------------------------------------

-- | The context for a wrapper calculation. This information is used by the system call to the
-- wrapper and some Spicy internals.
data CalcInput = CalcInput
  { -- | A 'Task' the wrapper needs to perform.
    task :: !WrapperTask,
    -- | Potentially a restart file (wavefunction), which might me present.
    restartFile :: !(Maybe JFilePathAbs),
    -- | A program and its specific input, which performs the actual calculation.
    software :: !ProgramData,
    -- | Some prefix, which can be used to name output files. Consider this a project name.
    prefixName :: !String,
    -- | Directory, where permanent output of the calculation shall be stored. This must be a shared
    -- directory in case of distributed calculations.
    permaDir :: !JDirPathAbs,
    -- | Directory, with fast access for scratch files of the software. In case of distributed
    -- calculations, this might be a replicated directory.
    scratchDir :: !JDirPathAbs,
    -- | Number of (MPI) processes to run in parallel for a software.
    nProcs :: !Int,
    -- | Number of threads per MPI process or OpenMP threads.
    nThreads :: !Int,
    -- | Memory per process in MiB for the program.
    memory :: !Int,
    -- | Information specific to either a QM or MM calculation.
    qMMMSpec :: !QMMMSpec,
    -- | The embedding type for this calculation part.
    embedding :: !Embedding,
    -- | Settings for geometry optimisations on this layer. Always given and if not specified in the
    -- input using defaults.
    optimisation :: !Optimisation,
    -- | Specification of fragment methods in terms of GMBE.
    fragmentMethods :: !(Maybe FragmentMethods),
    -- | Additional input text, which will be inserted into the input file.
    additionalInput :: !(Maybe Text)
  }
  deriving (Generic)

instance ToJSON CalcInput where
  toEncoding = genericToEncoding spicyJOption

----------------------------------------------------------------------------------------------------

-- | Results from a wrapper calculation on the given niveau. If for example multiple computations
-- are performed on the same 'Molecule' layer as in ONIOM (high and low level calculation on the
-- model system), this type allows to store the intermediate information before combining them and
-- putting them back to the '_molecule_EnergyDerivatives' field.
data CalcOutput = CalcOutput
  { -- | Energy, gradient and hessian from the calculation. The hessian matrix must be non-projected
    -- and no degrees of freedom must have been removed.
    energyDerivatives :: !EnergyDerivatives,
    -- | Mapping 'Atom' indices to the 'Multipoles', that were calculated by a wrapper calculation.
    multipoles :: !(IntMap Multipoles),
    -- | Basis set used in this calculation (primary orbital basis).
    basis :: !(Maybe Basis),
    -- | Wavefunction resulting from the calculation.
    wavefunction :: !Wavefunction
  }
  deriving (Eq, Show, Generic)

instance ToJSON CalcOutput where
  toEncoding = genericToEncoding spicyJOption

----------------------------------------------------------------------------------------------------

-- | A task the wrapper needs to perform. When serialising the wrapper input, we may decide to
-- actually carry out a different calculation type, if necessary. E.g. a CCSD single point
-- calculation may still request a gradient calculation in reality, if required for relaxed
-- properties such as multipole moments. However, this choice is specific to the wrapper and
-- serialiser and we usually request exactly the type we expect.
data WrapperTask
  = -- | Single point energy calculation.
    WTEnergy
  | -- | Gradient calculation.
    WTGradient
  | -- | Hessian calculation.
    WTHessian
  deriving (Eq, Show, Generic)

instance ToJSON WrapperTask where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON WrapperTask where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Context specific to a QM calculation.
data QMContext = QMContext
  { -- | Electronic charge of the (sub)system.
    charge :: !Int,
    -- | Multiplicity of the (sub)system.
    mult :: !Int
  }
  deriving (Show, Eq, Generic)

instance ToJSON QMContext where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON QMContext where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Context specific to a MM calculation.
data MMContext = MMContext
  deriving (Show, Eq, Generic)

instance ToJSON MMContext where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON MMContext where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Information needed either by a QM or MM calculation.
data QMMMSpec
  = -- | Information only needed for QM calculations.
    QM !QMContext
  | -- | Information only needed for MM calculations.
    MM !MMContext
  deriving (Show, Eq, Generic)

instance ToJSON QMMMSpec where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON QMMMSpec

-- Prisms
_QM :: Prism' QMMMSpec QMContext
_QM = prism' QM $ \s -> case s of
  QM b -> Just b
  _ -> Nothing

_MM :: Prism' QMMMSpec MMContext
_MM = prism' MM $ \s -> case s of
  MM b -> Just b
  _ -> Nothing

----------------------------------------------------------------------------------------------------

-- | Derivatives of the potential energy, that can be calculated for a 'Molecule' or molecular layer.
data EnergyDerivatives = EnergyDerivatives
  { -- | The potential energy.
    energy :: !(Maybe Double),
    -- | The cartesian nuclear gradient as a vector.
    gradient :: !(Maybe (VectorS Double)),
    -- | The cartesian nuclear hessian. The hessian matrix must __not__ be projected, e.g. no
    -- degrees of freedom must be removed.
    hessian :: !(Maybe (MatrixS Double))
  }
  deriving (Eq, Show, Generic)

instance ToJSON EnergyDerivatives where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON EnergyDerivatives where
  parseJSON = genericParseJSON spicyJOption

instance Default EnergyDerivatives where
  def =
    EnergyDerivatives
      { energy = Nothing,
        gradient = Nothing,
        hessian = Nothing
      }

-- Lenses (those are not sufficiently type safe via generics)
instance (k ~ A_Lens, a ~ Maybe Double, b ~ a) => LabelOptic "energy" k EnergyDerivatives EnergyDerivatives a b where
  labelOptic = lens energy $ \s b -> s {energy = b}

instance (k ~ A_Lens, a ~ Maybe (VectorS Double), b ~ a) => LabelOptic "gradient" k EnergyDerivatives EnergyDerivatives a b where
  labelOptic = lens gradient $ \s b -> s {gradient = b}

instance (k ~ A_Lens, a ~ Maybe (MatrixS Double), b ~ a) => LabelOptic "hessian" k EnergyDerivatives EnergyDerivatives a b where
  labelOptic = lens hessian $ \s b -> s {hessian = b}

----------------------------------------------------------------------------------------------------

-- | Available embedding methods for layer interaction in ONIOM.
data Embedding
  = -- | Mechanical embedding.
    Mechanical
  | -- | Electronic embedding. Scaling factors of charges in a given distance to a capped atom can
    -- be given.
    Electronic (Maybe (Seq Double))
  deriving (Eq, Show, Generic)

instance ToJSON Embedding where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Embedding where
  parseJSON = genericParseJSON spicyJOption

-- Prisms
_Mechanical :: Prism' Embedding ()
_Mechanical = prism' (const Mechanical) $ \s -> case s of
  Mechanical -> Just ()
  _ -> Nothing

_Electronic :: Prism' Embedding (Maybe (Seq Double))
_Electronic = prism' Electronic $ \s -> case s of
  Electronic b -> Just b
  _ -> Nothing
