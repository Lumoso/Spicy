-- |
-- Module      : Spicy.Molecule.Internal.Multipoles
-- Description : Operations and transformations of multipole data
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides operations for multipole data, including transformation to local frames and
-- conversion to point charges. For most of the definitions and conversion used see
-- [A Novel, Computationally Efficient Multipolar Model Employing Distributed Charges for Molecular Dynamics Simulations](https://pubs.acs.org/doi/10.1021/ct500511t).
--
-- The multipole moments up to the quadrupoles are defined as spherical tensors. Those multipoles
-- can be exactly represented as an octahedral point charge model in the axis system of the
-- quadrupole tensor, see 'toOctrahedralModel'.
--
-- This octahedral model defined in the coordinate system of the quadrupole tensor must be rotated
-- to the actual orientation of the molecule. For this purpose a local, orthogonal axis system is
-- defined. The original literature defines only for completely bonded systems of at least 3
-- non-colinear atoms how to define the axes system. In our case we do not treat completely bonded
-- molecular systems, but whatever is given in a 'group' field of a 'Molecule', which potentially
-- involves non-bonded atoms, only colinear atoms and other nasty cases that complicate finding a
-- triple of three non-bonded atoms. Those cases are dealt with in 'selectCases'. To be able to
-- treat corner cases correctly we also allow definition of groups of no or one patner atoms only,
-- if no other atom is close by to define a proper reference system. In the case of no bond partner
-- available the octahedron will not be rotated and the charges will be on the cartesian axes of the
-- atom centred cartesian coordinate system. In the case of two binding partners only the z-axis
-- will be defined and x and y will be chosen arbitrarily. This is legitimate if no other
-- interacting partners are close and the charge distribution is spherical (no partner) or
-- rotational symmetric (one partner).
module Spicy.Molecule.Multipoles (module Spicy.Molecule.Internal.Multipoles) where

import Spicy.Molecule.Internal.Multipoles
  ( OctahedralModel (..),
    molToPointCharges,
    sphericalToLocal,
    toOctrahedralModel,
  )
