-- |
-- Module      : Spicy.Data
-- Description : Data to lookup for chemical systems
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides scientific constants, conversion factors and physical data.
module Spicy.Data
  ( covalentRadii,
    possibleValences,
    Metallicity (..),
    metallicity,
    defCovAddSingle,
    defCovAddMultiple,
    defElectronicScalingFactors,
    angstrom2Bohr,
    bohr2Angstrom,
  )
where

import qualified Data.IntSet as IntSet
import RIO
import qualified RIO.List as List
import qualified RIO.Map as Map
import Spicy.Molecule.Internal.Physical

-- | Covalent radii of elements in Angstrom taken from
-- <https://pubs.rsc.org/en/content/articlelanding/2008/DT/b801115j Covalent radii revisited>
covalentRadii :: Map Element Double
covalentRadii =
  Map.fromList
    [ (H, 0.31),
      (He, 0.28),
      (Li, 1.28),
      (Be, 0.96),
      (B, 0.84),
      (C, 0.76),
      (N, 0.71),
      (O, 0.66),
      (F, 0.57),
      (Ne, 0.58),
      (Na, 1.66),
      (Mg, 1.41),
      (Al, 1.21),
      (Si, 1.11),
      (P, 1.07),
      (S, 1.05),
      (Cl, 1.02),
      (Ar, 1.06),
      (K, 2.03),
      (Ca, 1.76),
      (Sc, 1.70),
      (Ti, 1.60),
      (V, 1.53),
      (Cr, 1.39),
      (Mn, 1.61),
      (Fe, 1.52),
      (Co, 1.50),
      (Ni, 1.24),
      (Cu, 1.32),
      (Zn, 1.22),
      (Ga, 1.22),
      (Ge, 1.20),
      (As, 1.19),
      (Se, 1.20),
      (Br, 1.20),
      (Kr, 1.16),
      (Rb, 2.20),
      (Sr, 1.95),
      (Y, 1.90),
      (Zr, 1.75),
      (Nb, 1.64),
      (Mo, 1.54),
      (Tc, 1.47),
      (Ru, 1.46),
      (Rh, 1.42),
      (Pd, 1.39),
      (Ag, 1.45),
      (Cd, 1.44),
      (In, 1.42),
      (Sn, 1.39),
      (Sb, 1.39),
      (Te, 1.38),
      (I, 1.39),
      (Xe, 1.40),
      (Cs, 2.44),
      (Ba, 2.15),
      (Lu, 1.87),
      (Hf, 1.75),
      (Ta, 1.70),
      (W, 1.62),
      (Re, 1.51),
      (Os, 1.44),
      (Ir, 1.41),
      (Pt, 1.36),
      (Au, 1.36),
      (Hg, 1.32),
      (Tl, 1.45),
      (Pb, 1.46),
      (Bi, 1.48),
      (Po, 1.40),
      (At, 1.50),
      (Rn, 1.50),
      (Fr, 2.60),
      (Ra, 2.21),
      (La, 2.07),
      (Ce, 2.04),
      (Pr, 2.03),
      (Nd, 2.01),
      (Pm, 1.99),
      (Sm, 1.98),
      (Eu, 1.98),
      (Gd, 1.96),
      (Tb, 1.50),
      (Dy, 1.92),
      (Ho, 1.92),
      (Er, 1.89),
      (Tm, 1.90),
      (Yb, 1.87),
      (Ac, 1.92),
      (Th, 2.06),
      (Pa, 2.00),
      (U, 1.96),
      (Np, 1.90),
      (Pu, 1.87),
      (Am, 1.80),
      (Cm, 1.69)
    ]

----------------------------------------------------------------------------------------------------

-- | Common valences of elements. Taken from an evalation with the Radium library (which is
-- unfortunately not compatible with recent containers library) for elements up to Rn.
possibleValences :: Map Element IntSet
possibleValences =
  fmap IntSet.fromAscList . Map.fromList $
    [ (H, [1]),
      (He, [0]),
      (Li, [1]),
      (Be, [0, 2]),
      (B, [1, 3]),
      (C, [2, 4]),
      (N, [3]),
      (O, [2]),
      (F, [1]),
      (Ne, [0]),
      (Na, [1]),
      (Mg, [0, 2]),
      (Al, [1, 3]),
      (Si, [2, 4]),
      (P, [3, 5]),
      (S, [2, 4, 6]),
      (Cl, [1, 3, 5, 7]),
      (Ar, [0, 2, 4, 6, 8]),
      (K, [1]),
      (Ca, [0, 2]),
      (Sc, [3]),
      (Ti, [4]),
      (V, [2, 3, 4, 5]),
      (Cr, [2, 3, 6]),
      (Mn, [2, 3, 4, 6, 7]),
      (Fe, [2, 3]),
      (Co, [2, 3]),
      (Ni, [2]),
      (Cu, [1, 2]),
      (Zn, [2]),
      (Ga, [1, 3]),
      (Ge, [2, 4]),
      (As, [3, 5]),
      (Se, [2, 4, 6]),
      (Br, [1, 3, 5, 7]),
      (Kr, [0, 2, 4, 6, 8]),
      (Rb, [1]),
      (Sr, [0, 2]),
      (Y, [3]),
      (Zr, [4]),
      (Nb, [5]),
      (Mo, [4, 6]),
      (Tc, [4, 7]),
      (Ru, [3, 4]),
      (Rh, [3]),
      (Pd, [2, 4]),
      (Ag, [1]),
      (Cd, [2]),
      (In, [1, 3]),
      (Sn, [2, 4]),
      (Sb, [3, 5]),
      (Te, [2, 4, 6]),
      (I, [1, 3, 5, 7]),
      (Xe, [0, 2, 4, 6, 8]),
      (Cs, [1]),
      (Ba, [0, 2]),
      (La, [3]),
      (Ce, [3, 4]),
      (Pr, [3]),
      (Nd, [3]),
      (Pm, [3]),
      (Sm, [3]),
      (Eu, [2, 3]),
      (Gd, [3]),
      (Tb, [3]),
      (Dy, [3]),
      (Ho, [3]),
      (Er, [3]),
      (Tm, [3]),
      (Yb, [3]),
      (Lu, [3]),
      (Hf, [4]),
      (Ta, [5]),
      (W, [4, 6]),
      (Re, [4]),
      (Os, [4]),
      (Ir, [3, 4]),
      (Pt, [2, 4]),
      (Au, [1, 3]),
      (Hg, [2]),
      (Tl, [1, 3]),
      (Pb, [2, 4]),
      (Bi, [3, 5]),
      (Po, [2, 4, 6]),
      (At, [1, 3, 5, 7]),
      (Rn, [0, 2, 4, 6, 8])
    ]

----------------------------------------------------------------------------------------------------

-- | If an element is metallic. Relevant in the perspective of coordination complexes.
data Metallicity = NonMetal | SemiMetal | Metal deriving (Eq, Show)

-- | If elements are metallic.
metallicity :: Map Element Metallicity
metallicity =
  Map.fromList (List.zip nonMetals $ List.repeat NonMetal)
    <> Map.fromList (RIO.zip metals $ List.repeat Metal)
    <> Map.fromList (RIO.zip semiMetals $ List.repeat SemiMetal)
  where
    nonMetals = [H, He] <> [C .. Ne] <> [P .. Ar] <> [Br, Kr, I, Xe, Rn]
    metals = [Li, Be, Na, Mg, Al] <> [K .. Ga] <> [Rb .. Sn] <> [Cs .. Bi] <> [Fr .. Og]
    semiMetals = [B, Si, Ge, As, Se, Sb, Te, Po, At]

----------------------------------------------------------------------------------------------------

-- | Additional distance in angstrom, that can be added to the sum of covalent radii to consider two
-- atoms still bonded.
defCovAddSingle :: Double
defCovAddSingle = 0.4

-- | Additional distance in Angstrom, that can be added to the sum of covalent radii to consider two
-- atoms to have a bond order > 1.
defCovAddMultiple :: Double
defCovAddMultiple = 0.07

----------------------------------------------------------------------------------------------------

-- | Default scaling factors for electronic embedding in ONIOM. The first value in the sequence will
-- be used to scale the multipoles one bond away from a capped atom, the second entry multipoles two
-- bonds away and so on.

-- TODO (phillip|p=50|#Wrong) - Those values are just made up. Maybe the list should be empty, maybe the first three values should be zero. We haven't tested yet for DMA embedding
defElectronicScalingFactors :: Seq Double
defElectronicScalingFactors = mempty

----------------------------------------------------------------------------------------------------

-- | Conversion from Angstrom to Bohr.
angstrom2Bohr :: Double -> Double
angstrom2Bohr lengthInAngstrom = lengthInAngstrom * 1.889725989

----------------------------------------------------------------------------------------------------

-- | Conversion from Bohr to Angstrom.
bohr2Angstrom :: Double -> Double
bohr2Angstrom lengthInBohr = lengthInBohr / angstrom2Bohr 1
