-- |
-- Module      : Spicy.Massiv
-- Description : Utility functions around Massiv arrays.
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Utility funcitons around massiv arrays.
module Spicy.Massiv
  ( -- * Operations on Massiv Types
    -- $massivOperations
    takeWhileV',
    takeWhileV,
    vectorToVectorGroups,
    vectorToGroups,
    matrixFromGroupVector,
    innerChunksOfN,
    intoMeasuredChunksOf,
    partitionIntoVectors,
    reshapeCoords3M,
  )
where

import Data.Massiv.Array as Massiv
import qualified RIO.Seq as Seq
import Spicy.Prelude

-- $massivOperations

-- | takeWhile function for Massiv's vectors, but also returns the part of the vector that has not
-- been consumed yet.
{-# INLINE takeWhileV' #-}
takeWhileV' :: (Source r a, Stream r Ix1 a) => (a -> Bool) -> Vector r a -> ([a], Vector r a)
takeWhileV' cond !vec = go cond vec []
  where
    go :: (Source r a, Stream r Ix1 a) => (a -> Bool) -> Vector r a -> [a] -> ([a], Vector r a)
    go cond' !vec' foundYet' =
      if Massiv.isNull vec'
        then (foundYet', vec')
        else
          let headOfVec = Massiv.head' vec'
              tailOfVec = Massiv.tail vec'
           in if cond' headOfVec
                then let newFoundYet = headOfVec : foundYet' in go cond' tailOfVec newFoundYet
                else (foundYet', vec')

----------------------------------------------------------------------------------------------------

-- | Equivalent of takeWhile for vectors.
{-# INLINE takeWhileV #-}
takeWhileV :: (Source r a, Stream r Ix1 a) => (a -> Bool) -> Vector r a -> [a]
takeWhileV cond !vec = fst $ takeWhileV' cond vec

----------------------------------------------------------------------------------------------------

-- | Takes a sorted vector and groups its consecutive elements. Given a maximum size of elements, all
-- groups will be padded with a given default value.
{-# INLINE vectorToVectorGroups #-}
vectorToVectorGroups ::
  (Source r a, Stream r Ix1 a, MonadThrow m, Show a, Eq b) =>
  -- | An accessor to the values of the vector, which allows to compare on specific constructors of
  -- the vector values.
  (a -> b) ->
  -- | The number of elements a vector must have.
  Int ->
  -- | The default element to use for padding.
  a ->
  -- | A vector of elements, potentially presorted, which forms groups.
  Vector r a ->
  m (Seq (Vector B a))
vectorToVectorGroups accessorF' elemsPerRow' defElem' vec' =
  go
    accessorF'
    elemsPerRow'
    defElem'
    vec'
    Seq.empty
  where
    go ::
      (Source r1 a, Stream r1 Ix1 a, Mutable r2 a, MonadThrow m, Show a, Eq b) =>
      -- | An accessor to the values of the vector, which allows to compare on
      --   specific constructors of the vector values.
      (a -> b) ->
      -- | The number of elements a vector must have.
      Int ->
      -- | The default value to use with padding, if a group does not have enough
      --   elements.
      a ->
      -- | A vector of elements, potentially presorted, which forms groups.
      Vector r1 a ->
      -- | A sequence f groups, built from the vector.
      Seq (Vector r2 a) ->
      m (Seq (Vector r2 a))
    go accessorF elemsProRow defElem !vec !groupAcc = do
      let currentGroupVal = Massiv.headM vec
          currentCondition = accessorF <$> currentGroupVal

      case currentCondition of
        -- Vector must have been fully consumed.
        Nothing -> return groupAcc
        -- The start of the next group.
        Just accessorCond -> do
          let (currentGroup, restOfVec) = takeWhileV' (\a -> accessorF a == accessorCond) vec
              groupSize = length currentGroup

          -- Cannot continue, if more elements are in a group than the number of columns in the matrix
          -- is.
          when (groupSize > elemsProRow)
            . throwM
            . DataStructureException "groupedVectorToMatrix"
            $ "Taking the current group from the vector (current head: "
              <> show currentGroupVal
              <> ") and got "
              <> show groupSize
              <> "elements, but there must not be more than "
              <> show elemsProRow
              <> " elements per group."

          -- Pad the group with the default element.
          let numberOfPaddingElems = elemsProRow - groupSize
              paddingVec = Massiv.sreplicate (Sz numberOfPaddingElems) defElem
              nonDefGroupVec = Massiv.sfromListN (Sz groupSize) currentGroup
              groupVec =
                Massiv.compute . Massiv.setComp Par $ Massiv.sappend nonDefGroupVec paddingVec

          -- Add the new group vector to the sequence of groups.
          let newGroupAcc = groupVec <| groupAcc

          -- Another recursion for the next part of the vector.
          go accessorF elemsProRow defElem restOfVec newGroupAcc

----------------------------------------------------------------------------------------------------

-- | Takes a sorted vector and groups its consecutive elements. Given a maximum size of elements, all
-- groups will be padded with a given default value.
{-# INLINE vectorToGroups #-}
vectorToGroups ::
  (Source r1 a, Stream r1 Ix1 a, Show a, Eq b) =>
  -- | An accessor to the values of the vector, which allows to compare on specific
  --   constructors of the vector values.
  (a -> b) ->
  -- | A vector of elements, potentially presorted, which forms groups.
  Vector r1 a ->
  [[a]]
vectorToGroups accessorF' vec' = go accessorF' vec' []
  where
    go ::
      (Source r1 a, Stream r1 Ix1 a, Show a, Eq b) =>
      -- | An accessor to the values of the vector, which allows to compare on
      --   specific constructors of the vector values.
      (a -> b) ->
      -- | A vector of elements, potentially presorted, which forms groups.
      Vector r1 a ->
      -- | A sequence f groups, built from the vector.
      [[a]] ->
      [[a]]
    go accessorF !vec !groupAcc =
      let currentGroupVal = Massiv.headM vec
          currentCondition = accessorF <$> currentGroupVal
       in case currentCondition of
            -- Vector must have been fully consumed
            Nothing -> groupAcc
            -- The start of the next group.
            Just accessorCond ->
              let (currentGroup, restOfVec) = takeWhileV' (\a -> accessorF a == accessorCond) vec
                  newGroupAcc = currentGroup : groupAcc
               in -- Another recursion to consume the next group in the vector.
                  go accessorF restOfVec newGroupAcc

----------------------------------------------------------------------------------------------------

-- | Build a matrix from a vector of grouped elements. Groups are of fixed size and will be padded
-- with a default argument. The function fails if one of the groups exceeds the maximum size.
matrixFromGroupVector ::
  (Source r a, Stream r Ix1 a, MonadThrow m, Eq b, Show a) =>
  (a -> b) ->
  Int ->
  a ->
  Vector r a ->
  m (Matrix DL a)
matrixFromGroupVector accessorF nColumns defElem vec = do
  groups <- vectorToVectorGroups accessorF nColumns defElem vec
  let groupsExpandedToMatrix1 = fmap (Massiv.expandOuter (Sz 1) const) groups
  matrix <- Massiv.concatM 2 groupsExpandedToMatrix1
  return matrix

----------------------------------------------------------------------------------------------------

-- | Takes up to N columns from a matrix and groups them. Behaves otherwise similiar to
-- 'Data.List.Split.chunksOf'.
innerChunksOfN :: Massiv.Index ix => Int -> Array D ix e -> Seq (Array D ix e)
innerChunksOfN n matrix = go n matrix Seq.Empty
  where
    go n' restMatrix groupAcc = case Massiv.splitAtM (Dim 1) n' restMatrix of
      Nothing ->
        let (_, Sz nColsRemaining) = Massiv.unsnocSz . Massiv.size $ restMatrix
         in if nColsRemaining == 0 then groupAcc else groupAcc |> restMatrix
      Just (thisChunk, restMinusThisChunk) -> go n' restMinusThisChunk (groupAcc |> thisChunk)

----------------------------------------------------------------------------------------------------

-- | Partition a vector into several vectors of possibly uneven length.
intoMeasuredChunksOf ::
  (Stream s Ix1 Int, MonadThrow m, Source r e) =>
  -- | Vector to be split up.
  Vector r e ->
  -- | Vector giving the lengths of the resulting vectors in order.
  Vector s Int ->
  -- | Vector containing the partitioned results.
  m (Vector DL (Vector r e))
intoMeasuredChunksOf things lengths = fst <$> sfoldlM f (Massiv.empty, things) lengths
  where
    f (vecs, vec) i = do
      (this, next) <- sliceAtM (Sz i) vec
      return (Massiv.snoc vecs this, next)

----------------------------------------------------------------------------------------------------

-- | Partition an arbitrary 'Foldable' into two 'Vector's by examining the contents with a
-- predicate. Inspired by 'Data.IntMap.Lazy.partition'.
partitionIntoVectors ::
  Foldable f =>
  -- | A predicate.
  (e -> Bool) ->
  -- | A foldable to be partitioned.
  f e ->
  -- | First field contains the entries for which the predicate holds true,
  -- second field the entries for which it does not.
  (Vector DL e, Vector DL e)
partitionIntoVectors f arr = foldr decide (mempty, mempty) arr
  where
    decide e (tru, fls) =
      if f e
        then (e `Massiv.cons` tru, fls)
        else (tru, e `Massiv.cons` fls)

----------------------------------------------------------------------------------------------------

-- | Reshapes an arbitrary array into a matrix-like representation of the coordinates, one atom/3
-- coordinates per row.
reshapeCoords3M :: (MonadThrow m, Load r ix e, Size r) => Int -> Array r ix e -> m (Matrix r e)
reshapeCoords3M n arr
  | n < 0 = throwM $ MolLogicException "reshapeCoords3M" "Cannot have less than 0 atoms."
  | otherwise = Massiv.resizeM (Sz $ n :. 3) arr
