-- |
-- Module      : Spicy.Containers
-- Description : Utility functions around the types of containers
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Utility funcitons around the types of the containers package, i.e.
-- 'Set', 'Map', 'IntSet', 'IntMap' and 'Seq'. These are commonly used in the logic of molecules.
module Spicy.Containers
  ( -- * Sequence
    -- $sequenceOperations
    groupBy,

    -- * Map and Set Structures
    -- $mapSetOperations
    mapSetIsBidirectorial,
    intMapSetIsBidirectorial,
    mapDisjoint,
    intMapDisjoint,
    isRepMapCompleteForSet,
    intIsRepMapCompleteForSet,
    isRepMapCompleteForMap,
    intIsRepMapCompleteForMap,
    isRepMapCompleteForMapSet,
    intIsRepMapCompleteForMapSet,
    replaceSet,
    intReplaceSet,
    replaceMapKeys,
    intReplaceMapKeys,
    replaceMapSet,
    intReplaceMapSet,
    groupTupleSeq,
    intGroupTupleSeq,
    mapSetFromGroupedSequence,
    intMapSetFromGroupedSequence,
    makeMapSetUnidirectorial,
    intMakeMapSetUnidirectorial,
    removeInverseFromMapSet,
    intRemoveInverseFromMapSet,
    removeEmptyMapSet,
    intRemoveEmptyMapSet,
    mapSetAddConnectionBidirectorial,
    intMapSetAddConnectionBidirectorial,
    mapSetRemoveConnectionBidirectorial,
    intMapSetRemoveConnectionBidirectorial,

    -- * Bond Matrix Operations
    -- $bondMatrixOperations
    isBondMatrixBidirectorial,
    isRepMapCompleteforBondMatrix,
    replaceBondMatrixInds,
    cleanBondMatByAtomInds,
    removeBondsByAtomIndsFromBondMat,
    removeBondFromBondMat,
    addBondToBondMat,
    bondMat2ImIs,
    makeBondMatUnidirectorial,
  )
where

import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import qualified RIO.HashMap as HashMap
import qualified RIO.Map as Map
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import qualified RIO.Set as Set
import Spicy.Molecule.Internal.Physical
import Spicy.Prelude

{-
====================================================================================================
-}

-- $sequenceOperations
-- Operations on 'Seq'uneces.

-- | This function implements
-- [groupBy](http://hackage.haskell.org/package/base-4.16.0.0/docs/Data-List.html#v:groupBy)
-- as in [Data.List](https://hackage.haskell.org/package/base-4.16.0.0/docs/Data-List.html):
-- "The group function takes a list and returns a list of lists such that the concatenation of the
-- result is equal to the argument. Moreover, each sublist in the result contains only equal
-- elements."
{-# INLINE groupBy #-}
groupBy :: (a -> a -> Bool) -> Seq a -> Seq (Seq a)
groupBy _ Seq.Empty = Seq.empty
groupBy f (x :<| xs) = (x :<| ys) :<| groupBy f zs where (ys, zs) = Seq.spanl (f x) xs

{-
====================================================================================================
-}

-- $mapSetOperations
-- Operations on 'Map' and 'Set' data structures, as they are commonly used in Spicy. For every
-- function a general version is implemented and a version which does the same on 'IntSet' and
-- 'IntMap' structures. Those functions specific to the 'Int' types are prefixed with Int, but
-- otherwise behave the same.

-- | Check wether a 'Map' 'Set' structure is bidirectorial, e.g. A 'Map' key that points to a target
-- in the 'Set' also has the reversed appearance. Examples:
--
-- >>> mapSetIsBidirectorial $ Map.fromList [ ('A', Set.fromList [ 'B', 'C' ]), ('B', Set.fromList [ 'A' ]), ('C', Set.fromList [ 'A' ])]
-- True
--
-- >>> mapSetIsBidirectorial $ Map.fromList [ ('A', Set.fromList [ 'B', 'C' ]), ('B', Set.fromList [ 'A' ]), ('C', Set.fromList [ 'B' ])]
-- False
{-# INLINE mapSetIsBidirectorial #-}
mapSetIsBidirectorial :: Ord a => Map a (Set a) -> Bool
mapSetIsBidirectorial mapSet =
  Map.foldrWithKey'
    ( \key valSet testBool ->
        let -- Look for the IntSet, that can be found when looking up all values from an IntSet of Keys.
            targetSet =
              Set.foldr'
                ( \k acc -> case mapSet Map.!? k of
                    Nothing -> acc :|> Set.empty
                    Just tIS -> acc :|> tIS
                )
                Seq.empty
                valSet
            -- Check for all in the Seq of found IntSet, if the current key is also a member.
            keyInTargets :: Seq Bool
            keyInTargets = fmap (key `Set.member`) targetSet
         in -- If the current key is a member of all target IntSet, we are fine. If not, we have a
            -- problem.
            all (== True) keyInTargets && testBool
    )
    True
    mapSet

{-# INLINE intMapSetIsBidirectorial #-}
intMapSetIsBidirectorial :: IntMap IntSet -> Bool
intMapSetIsBidirectorial mapSet =
  IntMap.foldrWithKey'
    ( \key valSet testBool ->
        let -- Look for the IntSet, that can be found when looking up all values from an IntSet of Keys.
            targetSet =
              IntSet.foldr'
                ( \k acc -> case mapSet IntMap.!? k of
                    Nothing -> acc :|> IntSet.empty
                    Just tIS -> acc :|> tIS
                )
                Seq.empty
                valSet
            -- Check for all in the Seq of found IntSet, if the current key is also a member.
            keyInTargets :: Seq Bool
            keyInTargets = fmap (key `IntSet.member`) targetSet
         in -- If the current key is a member of all target IntSet, we are fine. If not, we have a
            -- problem.
            all (== True) keyInTargets && testBool
    )
    True
    mapSet

----------------------------------------------------------------------------------------------------

-- | Check if 2 'Map's are disjoint in their keys.
{-# INLINE mapDisjoint #-}
mapDisjoint :: Ord a => Map a b -> Map a c -> Bool
mapDisjoint a b = Map.null $ a `Map.intersection` b

{-# INLINE intMapDisjoint #-}
intMapDisjoint :: IntMap a -> IntMap a -> Bool
intMapDisjoint a b = IntMap.null $ a `IntMap.intersection` b

----------------------------------------------------------------------------------------------------

-- | Check if a 'Map' is complete to replace all values in an 'Set' (any old key in the 'Set' can be
-- replaced by a new key).  Gives 'True' if complete, 'False' if the replacement 'Map' has holes.
{-# INLINE isRepMapCompleteForSet #-}
isRepMapCompleteForSet ::
  Ord a =>
  -- | 'Map' containing the mapping from old keys to new keys of the set.
  Map a b ->
  -- | 'Set' in which values should be replaced.
  Set a ->
  -- | Result.
  Bool
isRepMapCompleteForSet repMap st =
  let -- Keys, that can be replaced
      repKeys = Map.keysSet repMap
      -- Keys that shall be replaced minus keys that can be replaced
      lostKeys = st Set.\\ repKeys
   in Set.null lostKeys

{-# INLINE intIsRepMapCompleteForSet #-}
intIsRepMapCompleteForSet ::
  -- | 'IntMap' containing the mapping from old keys to new keys of the set.
  IntMap b ->
  -- | 'IntSet' in which values should be replaced.
  IntSet ->
  -- | Result.
  Bool
intIsRepMapCompleteForSet repMap st =
  let -- Keys, that can be replaced
      repKeys = IntMap.keysSet repMap
      -- Keys that shall be replaced minus keys that can be replaced
      lostKeys = st IntSet.\\ repKeys
   in IntSet.null lostKeys

----------------------------------------------------------------------------------------------------

-- | Check if a 'Map' is complete to replace all keys from the old 'Map' by new keys. Gives
-- 'True' if the 'Map' with replacements is complete and 'False' otherwise.
{-# INLINE isRepMapCompleteForMap #-}
isRepMapCompleteForMap ::
  Ord a =>
  -- | 'Map' containing the mapping from old key to new key.
  Map a b ->
  -- | 'Map' in which keys should be replaced.
  Map a c ->
  -- | Result.
  Bool
isRepMapCompleteForMap repMap map' =
  let -- Keys, that can be replaced
      repKeys = Map.keysSet repMap
      -- Keys that shall be replaced
      oldKeys = Map.keysSet map'
      -- Keys that cannot be replaced
      lostKeys = oldKeys Set.\\ repKeys
   in Set.null lostKeys

{-# INLINE intIsRepMapCompleteForMap #-}
intIsRepMapCompleteForMap ::
  -- | 'IntMap' containing the mapping from old key to new key.
  IntMap a ->
  -- | 'IntMap' in which keys should be replaced.
  IntMap b ->
  -- | Result.
  Bool
intIsRepMapCompleteForMap repMap map' =
  let -- Keys, that can be replaced
      repKeys = IntMap.keysSet repMap
      -- Keys that shall be replaced
      oldKeys = IntMap.keysSet map'
      -- Keys that cannot be replaced
      lostKeys = oldKeys IntSet.\\ repKeys
   in IntSet.null lostKeys

----------------------------------------------------------------------------------------------------

-- | Check if a 'Map' is complete to replace all values in a 'Map a Set a' type construction
-- (replacing both the lookup keys in the 'Map', as well as all values in the 'Set'). Gives
-- 'True' if complete and 'False' otherwise.
{-# INLINE isRepMapCompleteForMapSet #-}
isRepMapCompleteForMapSet ::
  Ord a =>
  -- | 'IntMap' containing the mapping from old keys to new keys.
  Map a b ->
  -- | 'IntMap' 'IntSet' in which values and keys should be replaced.
  Map a (Set a) ->
  -- | Result.
  Bool
isRepMapCompleteForMapSet repMap mapSet =
  let -- All values, that appear in the union of all IntSet
      oldSets = Set.unions mapSet
   in isRepMapCompleteForMap repMap mapSet && isRepMapCompleteForSet repMap oldSets

{-# INLINE intIsRepMapCompleteForMapSet #-}
intIsRepMapCompleteForMapSet ::
  -- | 'IntMap' containing the mapping from old keys to new keys.
  IntMap Int ->
  -- | 'IntMap' 'IntSet' in which values and keys should be replaced.
  IntMap IntSet ->
  -- | Result.
  Bool
intIsRepMapCompleteForMapSet repMap mapSet =
  let -- All values, that appear in the union of all IntSet
      oldSets = IntSet.unions mapSet
   in intIsRepMapCompleteForMap repMap mapSet && intIsRepMapCompleteForSet repMap oldSets

----------------------------------------------------------------------------------------------------

-- | Replace all keys from a @'Set' a@ according to mappings from an @'Map' a b@. Entries, that
-- cannot be found in the 'Map' will no be changed.
{-# INLINE replaceSet #-}
replaceSet ::
  Ord a =>
  -- | 'Map' containing the mapping from old keys to new keys.
  Map a a ->
  -- | 'Set' to be modified.
  Set a ->
  -- | Resulting new 'IntSet'.
  Set a
replaceSet repMap st = Set.map (\oK -> let nK = repMap Map.!? oK in fromMaybe oK nK) st

{-# INLINE intReplaceSet #-}
intReplaceSet ::
  -- | 'IntMap' containing the mapping from old keys to new keys.
  IntMap Int ->
  -- | 'IntSet' to be modified.
  IntSet ->
  -- | Resulting new 'IntSet'.
  IntSet
intReplaceSet repMap st =
  IntSet.map (\oK -> let nK = repMap IntMap.!? oK in fromMaybe oK nK) st

----------------------------------------------------------------------------------------------------

-- | Replace all keys in a 'Map' according to mappings from a different 'Map'. Entries that
-- cannot be found in the 'Map' with the replacements will not be changed.
{-# INLINE replaceMapKeys #-}
replaceMapKeys ::
  Ord a =>
  -- | 'Map' containing the mapping from old keys to new keys.
  Map a a ->
  -- | 'Map' in which keys shall be replaced.
  Map a b ->
  -- | Resulting new 'Map' with replaced keys.
  Map a b
replaceMapKeys repMap map' =
  Map.mapKeys (\oK -> let nK = repMap Map.!? oK in fromMaybe oK nK) map'

{-# INLINE intReplaceMapKeys #-}
intReplaceMapKeys ::
  -- | 'IntMap' containing the mapping from old keys to new keys.
  IntMap Int ->
  -- | 'IntMap' in which keys shall be replaced.
  IntMap a ->
  -- | Resulting new 'Map' with replaced keys.
  IntMap a
intReplaceMapKeys repMap map' =
  IntMap.mapKeys (\oK -> let nK = repMap IntMap.!? oK in fromMaybe oK nK) map'

----------------------------------------------------------------------------------------------------

-- | Replace all lookup-keyss of a @'Map' a ('Set' a)@ and all values in the 'Set' by a given
-- mapping from a different 'Map'. Entries that cannot be found in the replacement-'Map' will not be
-- changed.
{-# INLINE replaceMapSet #-}
replaceMapSet ::
  Ord a =>
  -- | 'Map' containing the mapping from old keys to new keys.
  Map a a ->
  -- | Original structure, which to replace both keys and values.
  Map a (Set a) ->
  -- | Modified structure.
  Map a (Set a)
replaceMapSet repMap mapSet =
  -- Replace all values in all IntSet
  Map.map (replaceSet repMap)
    -- Replace all lookup keys
    . replaceMapKeys repMap
    $ mapSet

{-# INLINE intReplaceMapSet #-}
intReplaceMapSet ::
  -- | 'IntMap' containing the mapping from old keys to new keys.
  IntMap Int ->
  -- | Original structure, which to replace both keys and values.
  IntMap IntSet ->
  -- | Modified structure.
  IntMap IntSet
intReplaceMapSet repMap mapSet =
  -- Replace all values in all IntSet
  IntMap.map (intReplaceSet repMap)
    -- Replace all lookup keys
    . intReplaceMapKeys repMap
    $ mapSet

----------------------------------------------------------------------------------------------------

-- | Group by the first tuple element and within this group build an 'Set' of the the second tuple
-- elements.
{-# INLINE groupTupleSeq #-}
groupTupleSeq :: Ord a => Seq (a, a) -> Map a (Set a)
groupTupleSeq a =
  let -- Build groups of tuples with same keys.
      --keyValGroups :: Seq (Seq (a, a))
      keyValGroups =
        groupBy (\b c -> fst b == fst c) . Seq.sortBy (\b c -> fst b `compare` fst c) $ a

      -- Transform the grouped key value structures to a Seq (IntMap IntSet), where each IntMap has
      -- just one key.
      -- atomicIntMaps :: MonadThrow m => m (Seq (Map Int (Set Int)))
      atomicIntMaps = traverse mapSetFromGroupedSequence keyValGroups
      -- Fold all atom IntMap in the sequence into one.
      completeMap = foldl' (<>) Map.empty <$> atomicIntMaps
   in -- The only way this function can fail, is if keys would not properly be groupled. This cannot
      -- happen if 'groupBy' is called correclty before 'mapSetFromGroupedSequence'. Therefore
      -- default to the empty Map if this case, that cannot happen, happens.
      case completeMap of
        Left _ -> Map.empty
        Right map' -> map'

{-# INLINE intGroupTupleSeq #-}
intGroupTupleSeq :: Seq (Int, Int) -> IntMap IntSet
intGroupTupleSeq a =
  let -- Build groups of tuples with same keys.
      --keyValGroups :: Seq (Seq (a, a))
      keyValGroups =
        groupBy (\b c -> fst b == fst c) . Seq.sortBy (\b c -> fst b `compare` fst c) $ a

      -- Transform the grouped key value structures to a Seq (IntMap IntSet), where each IntMap has
      -- just one key.
      -- atomicIntMaps :: MonadThrow m => m (Seq (Map Int (Set Int)))
      atomicIntMaps = traverse intMapSetFromGroupedSequence keyValGroups
      -- Fold all atom IntMap in the sequence into one.
      completeMap = foldl' (<>) IntMap.empty <$> atomicIntMaps
   in -- The only way this function can fail, is if keys would not properly be groupled. This cannot
      -- happen if 'groupBy' is called correclty before 'mapSetFromGroupedSequence'. Therefore
      -- default to the empty Map if this case, that cannot happen, happens.
      case completeMap of
        Left _ -> IntMap.empty
        Right map' -> map'

----------------------------------------------------------------------------------------------------

-- | Create the @'Map' a ('Set' a)@ structure from a group of key value pairs. This means, that the
-- first elements of the tuple, all need to be the same key. If they are not the assumptions of this
-- function are not met and an error will be returned. The result will be a 'Map' with a single
-- keys.
{-# INLINE mapSetFromGroupedSequence #-}
mapSetFromGroupedSequence :: (MonadThrow m, Ord a) => Seq (a, a) -> m (Map a (Set a))
mapSetFromGroupedSequence group
  | Seq.null group = return Map.empty
  | keyCheck = case headKey of
    Nothing -> return Map.empty
    Just k -> return $ Map.fromList [(k, values)]
  | otherwise =
    throwM $
      DataStructureException "mapSetFromGroupedSequence" "The keys are not all the same."
  where
    headGroup = group Seq.!? 0
    keys = fst <$> group
    headKey = fst <$> headGroup
    keyCheck = all (== headKey) (pure <$> keys)
    values = Set.fromList . toList . fmap snd $ group

{-# INLINE intMapSetFromGroupedSequence #-}
intMapSetFromGroupedSequence :: MonadThrow m => Seq (Int, Int) -> m (IntMap IntSet)
intMapSetFromGroupedSequence group
  | Seq.null group = return IntMap.empty
  | keyCheck = case headKey of
    Nothing -> return IntMap.empty
    Just k -> return $ IntMap.fromList [(k, values)]
  | otherwise =
    throwM $
      DataStructureException "mapSetFromGroupedSequence" "The keys are not all the same."
  where
    headGroup = group Seq.!? 0
    keys = fst <$> group
    headKey = fst <$> headGroup
    keyCheck = all (== headKey) (pure <$> keys)
    values = IntSet.fromList . toList . fmap snd $ group

----------------------------------------------------------------------------------------------------

-- | The bond structure, which is defined bidirectorially can be reduced to be defined unidirectorial. If
-- you imagine this structure as the bond matrix, this is the same as taking just the upper right
-- triangular matrix without the main diagonal.
{-# INLINE makeMapSetUnidirectorial #-}
makeMapSetUnidirectorial :: Ord a => Map a (Set a) -> Map a (Set a)
makeMapSetUnidirectorial mapSet =
  removeEmptyMapSet $
    Map.foldrWithKey
      (\key valSet acc -> Map.update (\_ -> Just $ Set.filter (> key) valSet) key acc)
      mapSet
      mapSet

{-# INLINE intMakeMapSetUnidirectorial #-}
intMakeMapSetUnidirectorial :: IntMap IntSet -> IntMap IntSet
intMakeMapSetUnidirectorial mapSet =
  intRemoveEmptyMapSet $
    IntMap.foldrWithKey
      (\key valSet acc -> IntMap.update (\_ -> Just $ IntSet.filter (> key) valSet) key acc)
      mapSet
      mapSet

----------------------------------------------------------------------------------------------------

-- | This function takes a 'Map a (Set a)' structure and a single update tuple. All values from the 'Set'
-- in the tuple will be looked up in the 'Map' as key, and the keys from the tuple will be removed from
-- the so obtained pairs. Example:
--
-- >>> valMap = Map.fromList [ (1, Set.fromList [1] ), (2, Set.fromList [1,2,3]), (3, Set.fromList [1,2,3]) ]
-- >>> removeInverseFromMapSet valMap (1, Set.fromList [1,2])
-- Map.fromList [ (1, Set.fromList [] ), (2, Set.fromList [2,3]), (3, Set.fromList [1,2,3]) ]
{-# INLINE removeInverseFromMapSet #-}
removeInverseFromMapSet ::
  (Ord a, Ord b) =>
  -- | Original structure.
  Map a (Set b) ->
  -- | The update tuple. @b@ is the value to be removed from the 'Map's 'Set'
  --   values, that are found, when looking up all values from the 'Set' in the
  --   original 'Map'.
  (b, Set a) ->
  -- | Updated structure.
  Map a (Set b)
removeInverseFromMapSet mapSet (val2Rem, keys) =
  Map.foldrWithKey'
    ( \key _ acc ->
        if key `Set.member` keys then Map.update (Just <$> Set.delete val2Rem) key acc else acc
    )
    mapSet
    mapSet

{-# INLINE intRemoveInverseFromMapSet #-}
intRemoveInverseFromMapSet ::
  -- | Original structure.
  IntMap IntSet ->
  -- | The update tuple. @fst@ is the value to be removed from the 'IntMap's
  --   'IntSet' values, that are found, when looking up all values from the 'Set'
  --   in the original 'IntMap'.
  (Int, IntSet) ->
  -- | Updated structure.
  IntMap IntSet
intRemoveInverseFromMapSet mapSet (val2Rem, keys) =
  IntMap.foldrWithKey'
    ( \key _ acc ->
        if key `IntSet.member` keys
          then IntMap.update (Just <$> IntSet.delete val2Rem) key acc
          else acc
    )
    mapSet
    mapSet

----------------------------------------------------------------------------------------------------

-- | Remove key value pairs from the 'Map', where the 'Set' is empty.
{-# INLINE removeEmptyMapSet #-}
removeEmptyMapSet :: (Ord a) => Map a (Set b) -> Map a (Set b)
removeEmptyMapSet mapSet =
  Map.foldrWithKey'
    (\key is acc -> Map.update (\_ -> if Set.null is then Nothing else Just is) key acc)
    mapSet
    mapSet

{-# INLINE intRemoveEmptyMapSet #-}
intRemoveEmptyMapSet :: IntMap IntSet -> IntMap IntSet
intRemoveEmptyMapSet mapSet =
  IntMap.foldrWithKey'
    (\key is acc -> IntMap.update (\_ -> if IntSet.null is then Nothing else Just is) key acc)
    mapSet
    mapSet

----------------------------------------------------------------------------------------------------

-- | Given two keys, this function will add the bidirectorially as origin ('Map' key) and target
-- ('Set' key) to the data structure. If they already exists, they will be added to the existing
-- structure.
{-# INLINE mapSetAddConnectionBidirectorial #-}
mapSetAddConnectionBidirectorial :: Ord a => Map a (Set a) -> (a, a) -> Map a (Set a)
mapSetAddConnectionBidirectorial mapSet (a, b) =
  let firstDirectionAdded =
        if a `Map.member` mapSet
          then Map.adjust (Set.insert b) a mapSet
          else Map.insert a (Set.singleton b) mapSet
      secondDirectionAdded =
        if b `Map.member` mapSet
          then Map.adjust (Set.insert a) b firstDirectionAdded
          else Map.insert b (Set.singleton a) firstDirectionAdded
   in secondDirectionAdded

{-# INLINE intMapSetAddConnectionBidirectorial #-}
intMapSetAddConnectionBidirectorial :: IntMap IntSet -> (Int, Int) -> IntMap IntSet
intMapSetAddConnectionBidirectorial mapSet (a, b) =
  let firstDirectionAdded =
        if a `IntMap.member` mapSet
          then IntMap.adjust (IntSet.insert b) a mapSet
          else IntMap.insert a (IntSet.singleton b) mapSet
      secondDirectionAdded =
        if b `IntMap.member` mapSet
          then IntMap.adjust (IntSet.insert a) b firstDirectionAdded
          else IntMap.insert b (IntSet.singleton a) firstDirectionAdded
   in secondDirectionAdded

----------------------------------------------------------------------------------------------------

-- | Given two keys, this function will remove the connection between them. If they are not found,
-- nothing will be done.
{-# INLINE mapSetRemoveConnectionBidirectorial #-}
mapSetRemoveConnectionBidirectorial :: Ord a => Map a (Set a) -> (a, a) -> Map a (Set a)
mapSetRemoveConnectionBidirectorial mapSet (a, b) =
  let firstDirectionRemoved =
        if a `Map.member` mapSet
          then Map.adjust (Set.delete b) a mapSet
          else mapSet
      secondDirectionRemoved =
        if b `Map.member` mapSet
          then Map.adjust (Set.delete a) b firstDirectionRemoved
          else firstDirectionRemoved
   in secondDirectionRemoved

{-# INLINE intMapSetRemoveConnectionBidirectorial #-}
intMapSetRemoveConnectionBidirectorial :: IntMap IntSet -> (Int, Int) -> IntMap IntSet
intMapSetRemoveConnectionBidirectorial mapSet (a, b) =
  let firstDirectionRemoved =
        if a `IntMap.member` mapSet
          then IntMap.adjust (IntSet.delete b) a mapSet
          else mapSet
      secondDirectionRemoved =
        if b `IntMap.member` mapSet
          then IntMap.adjust (IntSet.delete a) b firstDirectionRemoved
          else firstDirectionRemoved
   in secondDirectionRemoved

{-
====================================================================================================
-}

-- | Check if the bond matrix is defined bidirectorial.
isBondMatrixBidirectorial :: BondMatrix -> Bool
isBondMatrixBidirectorial bondMat =
  HashMap.foldlWithKey'
    ( \accBool (ixO, ixT) val ->
        let mirrorVal = HashMap.lookup (ixT, ixO) bondMat
         in case mirrorVal of
              Just mVal -> mVal == val && accBool
              Nothing -> False
    )
    True
    bondMat

----------------------------------------------------------------------------------------------------

-- | Check if a replacement map is complete to replace all index components in the bond matrix. Also
-- checks, that no values are lost because multiple old keys are mapped to a single new one.
isRepMapCompleteforBondMatrix :: IntMap Int -> BondMatrix -> Bool
isRepMapCompleteforBondMatrix repMap bondMat =
  let replacementAttempts = IntMap.keysSet repMap
      replacementNewKeys = IntMap.foldl' (flip IntSet.insert) IntSet.empty repMap
      indexComponentsToReplace =
        let bondInds = HashMap.keys bondMat
            bondOrigins = IntSet.fromList . fmap fst $ bondInds
            bondTargets = IntSet.fromList . fmap snd $ bondInds
         in bondOrigins <> bondTargets
      nothingLostCheck = IntSet.size replacementNewKeys == IntSet.size replacementAttempts
      completenessCheck = IntSet.null $ indexComponentsToReplace IntSet.\\ replacementAttempts
   in nothingLostCheck && completenessCheck

----------------------------------------------------------------------------------------------------

-- | Replace the index components of a bond matrix with mappings from another replacement map. Index
-- components, that can not be found in the replacement map will not be changed.
replaceBondMatrixInds :: IntMap Int -> BondMatrix -> BondMatrix
replaceBondMatrixInds repMap bondMat =
  HashMap.foldlWithKey'
    ( \accBondMat (ixO, ixT) val ->
        let ixONew = IntMap.findWithDefault ixO ixO repMap
            ixTNew = IntMap.findWithDefault ixT ixT repMap
         in HashMap.insert (ixONew, ixTNew) val accBondMat
    )
    HashMap.empty
    bondMat

----------------------------------------------------------------------------------------------------

-- | Removes all bonds from a given bond matrix, that involve atoms, that are no longer present. The
-- 'IntSet' contains the indices of the atoms, which are kept.
cleanBondMatByAtomInds :: BondMatrix -> IntSet -> BondMatrix
cleanBondMatByAtomInds bondMat atomInds =
  HashMap.foldlWithKey'
    ( \accBondMat (ixO, ixT) val ->
        if (ixO `IntSet.member` atomInds) && (ixT `IntSet.member` atomInds)
          then HashMap.insert (ixO, ixT) val accBondMat
          else accBondMat
    )
    HashMap.empty
    bondMat

----------------------------------------------------------------------------------------------------

-- | Removes all bonds in which an atom is involded. The atom is specified by its index. If the atom is
-- involed in no bond, the bond matrix will not be changed. Multiple atoms can be specified by giving
-- the indices in an 'IntSet'.
removeBondsByAtomIndsFromBondMat :: BondMatrix -> IntSet -> BondMatrix
removeBondsByAtomIndsFromBondMat bondMat atomInds =
  HashMap.foldlWithKey'
    ( \accBondMat inx@(ixO, ixT) _ ->
        if ixO `IntSet.member` atomInds || ixT `IntSet.member` atomInds
          then HashMap.delete inx accBondMat
          else accBondMat
    )
    bondMat
    bondMat

----------------------------------------------------------------------------------------------------

-- | Removes a bond between two atoms bidirectorially from the bond matrix. If the bond does not exist,
-- the bond matrix is unaltered.
removeBondFromBondMat :: BondMatrix -> (Int, Int) -> BondMatrix
removeBondFromBondMat bondMat (ixO, ixT) =
  HashMap.delete (ixT, ixO) . HashMap.delete (ixO, ixT) $ bondMat

----------------------------------------------------------------------------------------------------

-- | Adds a bond between two atoms bidirectorially. This function can add bonds between non-existing
-- atoms and destroy the molecule data consistency.
addBondToBondMat :: BondMatrix -> (Int, Int) -> BondOrder -> BondMatrix
addBondToBondMat bondMat (ixO, ixT) bondOrder =
  HashMap.insert (ixT, ixO) bondOrder . HashMap.insert (ixO, ixT) bondOrder $ bondMat

----------------------------------------------------------------------------------------------------

-- | Convert a bond matrix data structure to the IntMap IntSet structure as an alternative
-- representation.
bondMat2ImIs :: BondMatrix -> IntMap IntSet
bondMat2ImIs bondMat =
  HashMap.foldlWithKey'
    ( \accIntMap (ixO, ixT) val ->
        if isBond val then IntMap.insertWith (<>) ixO (IntSet.singleton ixT) accIntMap else accIntMap
    )
    IntMap.empty
    bondMat

----------------------------------------------------------------------------------------------------

-- | Makes the bond matrix unidirectorial by only taking the lower left triangular part of it. This means
-- that atoms only bind to those with higher index then their own.
makeBondMatUnidirectorial :: BondMatrix -> BondMatrix
makeBondMatUnidirectorial bondMat =
  HashMap.filterWithKey (\(ixO, ixT) val -> ixO <= ixT && isBond val) bondMat
