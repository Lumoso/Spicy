-- |
-- Module      : Spicy.Wavefunction.Basis
-- Description : GTO Basis functions
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Types and definitions to handle Gaussian-type orbital basis functions in Spicy. This module is
-- only concerned with the primary orbital basis and does not care about fitting basis sets.
module Spicy.Wavefunction.Basis
  ( Basis (..),
    Shell (..),
    ShellType (..),
    Gauss (..),
    shellTypeToGaussian,
    basisFunctionsInShellType,
    mkShellType,
    getAngularMomentum,
    spCoefficients,
    BasisShell (..),
    erdNormaliseBasis,
  )
where

import Data.Aeson
import Data.Massiv.Array as Massiv
import qualified Data.Vector.Generic as VG
import qualified Data.Vector.Generic.Mutable as VGM
import qualified Data.Vector.Unboxed as VU
import Spicy.Aeson
import Spicy.Math.Util
import Spicy.Prelude as S hiding (exponent)

-- | Basis functions used in some calculation.
data Basis = Basis
  { -- | Name of the basis set. Can be empty.
    name :: !Text,
    -- | Shells in the basis, in order.
    shells :: !(VectorG B Shell)
  }
  deriving (Generic, Show, Eq)

instance ToJSON Basis where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Basis where
  parseJSON = genericParseJSON spicyJOption

instance Semigroup Basis where
  a <> b =
    Basis
      { name = name a <> name b,
        shells = VectorG . compute . concat' (Dim 1) . fmap (getVectorG . shells) $ [a, b]
      }

----------------------------------------------------------------------------------------------------

-- $shells

-- | Shells are groups of basis functions with the same angular momentum \(l\). Those are the
-- contracted GTOs of the basis.
data Shell = Shell
  { -- | 'ShellType' of this shell, containing information about the angular momentum.
    shellType :: ShellType,
    -- | Primitives of this calculation.
    primitives :: VectorG U Gauss,
    -- | Coordinates of this 'Shell' and all functions in it.
    coordinates :: VectorS Double,
    -- | If this shell is assigned to an atom, this contains the atom index. Note that this is
    -- unlikely to correspond to the atom index in the 'Molecule'.
    atomIndex :: Maybe Int
  }
  deriving (Generic, Show, Eq)

instance ToJSON Shell where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Shell where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Type of a 'Shell': It is either cartesian, spherical, or a SP hybrid shell. Cartesian and
-- spherical shells include the angular momentum \(l\); SP shells include the P contraction
-- coefficients.
data ShellType
  = CartesianShell !Natural
  | SphericalShell !Natural
  | -- | This shell is a combinded S (\(l = 0\)) and P (\(l = 1\)) shell. Contains the contraction
    -- coefficients for the p orbitals.
    SPShell !(VectorS Double)
  deriving (Generic, Show, Eq)

instance ToJSON ShellType where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON ShellType where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | A single, primitive Gauss function. Should always be part of a contraction. Coordinates are
-- specified at the contraction level.
data Gauss = Gauss
  { exponent :: !Double,
    coefficient :: !Double
  }
  deriving (Generic, Show, Eq)

newtype instance VU.Vector Gauss = V_Gauss (VU.Vector (Double, Double))

newtype instance VU.MVector s Gauss = MV_Gauss (VU.MVector s (Double, Double))

instance VGM.MVector VU.MVector Gauss where
  basicLength (MV_Gauss v) = VGM.basicLength v
  basicUnsafeSlice i n (MV_Gauss v) = MV_Gauss $ VGM.basicUnsafeSlice i n v
  basicOverlaps (MV_Gauss v1) (MV_Gauss v2) = VGM.basicOverlaps v1 v2
  basicUnsafeNew n = MV_Gauss <$> VGM.basicUnsafeNew n
  basicInitialize (MV_Gauss v) = VGM.basicInitialize v
  basicUnsafeRead (MV_Gauss v) i = uncurry Gauss <$> VGM.basicUnsafeRead v i
  basicUnsafeWrite (MV_Gauss v) i x = VGM.basicUnsafeWrite v i (exponent x, coefficient x)

instance VG.Vector VU.Vector Gauss where
  basicLength (V_Gauss v) = VG.basicLength v
  basicUnsafeFreeze (MV_Gauss v) = V_Gauss <$> VG.basicUnsafeFreeze v
  basicUnsafeThaw (V_Gauss v) = MV_Gauss <$> VG.basicUnsafeThaw v
  basicUnsafeSlice i n (V_Gauss v) = V_Gauss $ VG.basicUnsafeSlice i n v
  basicUnsafeIndexM (V_Gauss v) i = uncurry Gauss <$> VG.basicUnsafeIndexM v i

instance Unbox Gauss

instance ToJSON Gauss where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Gauss where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Get the angular momentum \(l\) of a shell.
getAngularMomentum :: ShellType -> Natural
getAngularMomentum st = case st of
  CartesianShell n -> n
  SphericalShell n -> n
  SPShell _ -> 1

-- | FChks represent the type of shell with an integer number. Negative integers represent spherical
-- shells, with the exception of -1, which stands for SP combined shells.
shellTypeToGaussian :: ShellType -> Int
shellTypeToGaussian st = case st of
  CartesianShell n -> fromIntegral n
  SphericalShell n | n == 1 -> 1
  SphericalShell n -> negate . fromIntegral $ n
  SPShell _ -> -1

----------------------------------------------------------------------------------------------------

-- | Construct a 'ShellType' from a FChk-encoded integer (see 'shellTypeToGaussian') and a vector of
-- contraction coefficients in case of a SP shell.
mkShellType :: Load r Ix1 Double => Int -> Vector r Double -> ShellType
mkShellType i v
  | i == -1 = SPShell . VectorS . convert $ v
  | i <= 1 = SphericalShell . fromIntegral . abs $ i
  | otherwise = CartesianShell . fromIntegral . abs $ i

----------------------------------------------------------------------------------------------------

-- | How many basis functions are in a shell of a given \(l\)?
basisFunctionsInShellType :: ShellType -> Int
basisFunctionsInShellType (CartesianShell n) = numCartesian . fromIntegral $ n
basisFunctionsInShellType (SphericalShell n) = numSpherical . fromIntegral $ n
basisFunctionsInShellType (SPShell _) = 4

-- | Number of cartesian basis functions in a shell of a given angular momentum \(l\)
numCartesian :: Int -> Int
numCartesian l = (l + 1) * (l + 2) `div` 2

-- | Number of spherical basis functions in a shell of a given angular momentum \(l\)
numSpherical :: Int -> Int
numSpherical l = 2 * l + 1

----------------------------------------------------------------------------------------------------

-- | Get the P contraction coefficients from SP shells. This isn't valid for most shells; in those
-- cases zeroes will be returned (since that's what FChks want)
spCoefficients :: Shell -> VectorS Double
spCoefficients Shell {..} = case shellType of
  SPShell vec -> vec
  _ -> VectorS . compute @S . Massiv.map (const 0) . getVectorG $ primitives

{-
====================================================================================================
-}

-- $basissets

-- | A prototype 'Shell'. Unlike 'Shell', a 'BasisShell' is not yet localised
-- to a point in space, nor is it cartesian/spherical.
data BasisShell = BasisShell
  { angularMomentum :: Natural,
    primitives :: Vector U Gauss
  }
  deriving (Show, Generic)

{-
====================================================================================================
-}

-- | Normalisation factor for a spherical-harmonic gauss function. The normalisation factor is given
-- by \(\frac{1}{\sqrt{N}}\), where
-- \[
-- N = \frac{(2l - 1)!!}{(4\alpha)^l} {\frac{\pi}{2\alpha}}^(3/2)
-- \]
-- which is the self-overlap of a gaussian function.
nGauss ::
  -- | Angular momentum
  Natural ->
  -- | Exponent
  Double ->
  Double
nGauss l e = sqrt . recip $ selfOverlap
  where
    selfOverlap =
      pi ** 1.5
        * (2 * e) ** (-1.5)
        * dFac (2 * fromIntegral l - 1)
        * (4 * e) ** (negate . fromIntegral $ l)

-- | Apply the normalisation factor (see 'nGauss') to a 'Gauss'. The new coefficient will be
-- normalisation factor \(\times\) old coefficient
normaliseGauss :: Natural -> Gauss -> Gauss
normaliseGauss i g = g & #coefficient %~ (* n)
  where
    n = nGauss i (g ^. #exponent)

-- | Normalise all primitives in a 'Shell'. This does not mean the Shell itself will be normalised!
normaliseAllGaussInShell :: Shell -> Shell
normaliseAllGaussInShell s =
  let i = getAngularMomentum $ s ^. #shellType
      f = VectorG . convert . Massiv.map (normaliseGauss i) . getVectorG
   in s & #primitives %~ f

----------------------------------------------------------------------------------------------------

-- | Normalise a Shell according to the Electron Repulsion Direct (ERD) convention. Unfortunately, I
-- was not able to find a detailed description for this convention, and so this funciton has been
-- reverse engineered from other source code.
--
-- Note: this is not the full ERD normalisation, which includes two extra factors depending on the
-- angular momentum of the shell and exponent the GTO. Psi4 removes these factors before writing to
-- FChk and they are thus not even considered here.
erdNormaliseShell :: Shell -> Shell
erdNormaliseShell s@Shell {..} = s & #primitives %~ map' adjustGauss
  where
    primitives' = getVectorG primitives
    Sz1 len = size primitives'
    l = fromIntegral . getAngularMomentum $ shellType
    map' f = VectorG . convert . Massiv.map f . getVectorG
    adjustGauss = #coefficient %~ (* n)
    contribution :: Gauss -> Gauss -> Double
    contribution g1 g2 =
      let e1 = g1 ^. #exponent
          e2 = g2 ^. #exponent
          t = 2 * sqrt (e1 * e2) / (e1 + e2)
          t2 = t ** (l + 1.5)
          c = (g1 ^. #coefficient) * (g2 ^. #coefficient)
       in c * t2
    totalContribution = S.sum $ do
      i <- [0 .. len - 1]
      j <- [0 .. i]
      let x = contribution (primitives' ! i) (primitives' ! j)
      if i == j
        then return x
        else return $ 2 * x
    n = recip . sqrt $ totalContribution

----------------------------------------------------------------------------------------------------

-- | Normalise a 'Basis' according to the ERD convention. See 'erdNormaliseShell' for more
-- information.
erdNormaliseBasis :: Basis -> Basis
erdNormaliseBasis = #shells %~ fmap erdNormaliseShell
