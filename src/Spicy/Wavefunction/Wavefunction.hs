-- |
-- Module      : Spicy.Wavefunction.Wavefunction
-- Description : The Wavefunction data type
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Wavefunction type, containing all important information about a wavefunction. This handles
-- single-determinant wavefunctions and 1 particle density matrices.
module Spicy.Wavefunction.Wavefunction
  ( Wavefunction (..),
    MOCoefficients (..),
    densityFromCoefficients,
    densityFromUnrestricted,
  )
where

import Data.Aeson
import Data.Default
import Data.Massiv.Array
import Spicy.Aeson
import Spicy.Prelude

-- | Wavefunction of a calculation. Rather meaningless when not paired with a basis.
data Wavefunction = Wavefunction
  { -- | MO coefficients of a converged calculation. Column indices are AO indices, row indices are
    -- MO indices.
    mocoefficients :: !(Maybe MOCoefficients),
    -- | Density matrix.
    density :: !(Maybe (MatrixS Double))
  }
  deriving (Show, Eq, Generic)

instance Default Wavefunction

instance FromJSON Wavefunction where
  parseJSON = genericParseJSON spicyJOption

instance ToJSON Wavefunction where
  toEncoding = genericToEncoding defaultOptions

-- | MO coefficients resulting from some calculation. Rather meaningless when not paired with a basis.
data MOCoefficients
  = -- | These coefficients belong to an unresticted wavefunction. The first field contains the
    --  alpha coefficients, the second field has the beta coefficients.
    Unrestricted (MatrixS Double) (MatrixS Double)
  | -- | These coefficients belong to a restricted wavefunction.
    Restricted (MatrixS Double)
  deriving (Show, Eq, Generic)

instance FromJSON MOCoefficients where
  parseJSON = genericParseJSON spicyJOption

instance ToJSON MOCoefficients where
  toEncoding = genericToEncoding spicyJOption

-- | Build a RHF density matrix from the MO coefficients and the number of electrons, which
-- obviously must be even. The elements of the matrix are given by:
-- \( P_{\mu\nu} = 2 \sum_a^{N/2} C_{\mu a} C_{\nu a} \)
-- TODO - one can also use a RHF reference for systems that are not singlets. What now?
densityFromCoefficients ::
  forall r m.
  (MonadThrow m, Source r Double) =>
  -- | Number of electrons
  Int ->
  -- | MO coefficients
  Matrix r Double ->
  m (MatrixS Double)
densityFromCoefficients n coeffs = do
  unless (even n)
    . throwM
    . DataStructureException "densityFromCoefficients"
    $ "uneven number of electrons in restricted calculation"
  (m, _) <- splitAtM (Dim 1) (n `div` 2) coeffs
  let m' = convert @P m
  dens <- m' `multiplyMatricesTransposed` m'
  return . MatrixS . convert $ dens .* 2

-- | Build an UHF density matrix from the alpha and beta MO coefficients and the numbers of alpha
-- and beta electrons. Elements of the matrix are given by:
-- \( P^T_{\mu\nu} = P^{\alpha}_{\mu\nu} + P^{\beta}_{\mu\nu} \)
densityFromUnrestricted ::
  (MonadThrow m, Source r Double) =>
  -- | Number of \(\alpha\)-electrons
  Int ->
  -- | Number of \(\beta\)-electrons
  Int ->
  -- | \(\alpha\)-MO coefficients
  Matrix r Double ->
  -- | \(\beta\)-MO coefficients
  Matrix r Double ->
  m (MatrixS Double)
densityFromUnrestricted nAlpha nBeta acoeffs bcoeffs = do
  (a, _) <- splitAtM (Dim 1) nAlpha acoeffs
  (b, _) <- splitAtM (Dim 1) nBeta bcoeffs
  let a' = convert @P a
      b' = convert @P b
  densAlpha <- a' `multiplyMatricesTransposed` a'
  densBeta <- b' `multiplyMatricesTransposed` b'
  densTotal <- densAlpha .+. densBeta
  return . MatrixS . convert $ densTotal
