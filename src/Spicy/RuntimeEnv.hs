{-# OPTIONS_GHC -Wno-incomplete-record-updates #-}
{-# OPTIONS_GHC -Wno-partial-fields #-}

-- |
-- Module      : Spicy.RuntimeEnv
-- Description : Definition of a runtime environment in Spicy.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Spicy conceptionally runs on the 'Reader' 'IO' pattern. This module defines the runtime reader
-- environment for Spicy. The runtime environment most importantly contains a global state of the
-- 'Molecule', the 'InputFile', and a bunch of global settings.
module Spicy.RuntimeEnv
  ( SpicyEnv (..),
    WrapperConfigs (..),
    HasWrapperConfigs (..),
    Motion (..),
    HasMotion (..),
    CalcSlot (..),
    HasCalcSlot (..),
    IPI (..),
  )
where

import Data.Aeson
import RIO.Process (HasProcessContext (..), ProcessContext)
import Spicy.Aeson
import Spicy.Common
import Spicy.InputFile hiding (MD, molecule)
import Spicy.Molecule.Computational hiding (pysisyphus)
import Spicy.Molecule.Molecule
import Spicy.Outputter
import Spicy.Prelude
import Spicy.Wrapper.IPI.Types

----------------------------------------------------------------------------------------------------

-- | Definition of the current 'State' in the execution of Spicy.
data SpicyEnv = SpicyEnv
  { -- | Global state of the 'Molecule'. The state is only touched by very few modules
    -- (most importantly "Spicy.ONIOM.Driver", "Spicy.ONIOM.Layout", and "Spicy.JobDriver"), and
    -- only by the main thread.
    molecule :: !(TVar Molecule),
    -- | This is the input file and contains the definition of which kind of calculation to do. This
    -- should be set in the beginning and never change.
    calculation :: !InputFile,
    -- | Configuration files with Maps of environment variables to set before launching a program.
    wrapperConfigs :: !WrapperConfigs,
    -- | Counters, quantifiers and history of the motion.
    motion :: !(TVar (Seq Motion)),
    -- | A logging function for RIO for on-screen event printing.
    logFunc :: !LogFunc,
    -- | A process context for RIO.
    procCntxt :: !ProcessContext,
    -- | Provides the interface for Spicy to talk with external computational chemistry software.
    calcSlot :: !CalcSlot,
    -- | Output file writer context.
    outputter :: !Outputter
  }
  deriving (Generic)

-- Reader Classes
instance HasMotion SpicyEnv where
  motionL = #motion

instance HasInputFile SpicyEnv where
  inputFileL = #calculation

instance HasMolecule SpicyEnv where
  moleculeL = #molecule

instance HasWrapperConfigs SpicyEnv where
  wrapperConfigsL = #wrapperConfigs

instance HasLogFunc SpicyEnv where
  logFuncL = toLensVL #logFunc

instance HasProcessContext SpicyEnv where
  processContextL = toLensVL #procCntxt

instance HasCalcSlot SpicyEnv where
  calcSlotL = #calcSlot

instance HasOutputter SpicyEnv where
  outputterL = #outputter

----------------------------------------------------------------------------------------------------

-- | The command to use for launching the wrapped programs. Alls arguments are meant to be passed to
-- those wrappers.
data WrapperConfigs = WrapperConfigs
  { -- | Path to the @psi4@ executable; quantum chemistry program
    psi4 :: Maybe JFilePath,
    -- | Path to the @nwchem@ executable (matching @mpiexec@ must also be in @$PATH@); quantum
    -- chemistry program
    nwchem :: Maybe JFilePath,
    -- | Path to the @gdma@ executable; wavefunction analysis, required for any calculation, that is
    -- not purely XTB
    gdma :: Maybe JFilePath,
    -- | Path to the @ipi@ executable; required for molecular dynamics
    ipi :: Maybe JFilePath,
    -- | Path to the @pysis@ executable; required for geometry optimisations
    pysisyphus :: Maybe JFilePath,
    -- | Path to the @xtb@ executable; quantum chemistry program
    xtb :: Maybe JFilePath,
    -- | Path to the directory, in which the Turbomole executables of the correct architecture live.
    -- If Turbomole is not handled by Nix, Turbomole's environment variables and setup scripts have
    -- to be set up correctly before starting Spicy.
    turbomole :: Maybe JDirPath
  }
  deriving (Show, Generic)

instance ToJSON WrapperConfigs where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON WrapperConfigs

-- Reader Classes
class HasWrapperConfigs env where
  wrapperConfigsL :: Lens' env WrapperConfigs

instance HasWrapperConfigs WrapperConfigs where
  wrapperConfigsL = castOptic simple

----------------------------------------------------------------------------------------------------

-- | Defining the curent state of Motion in either an Optimisation or MD run.
data Motion = Motion
  { -- | The counter of outer optimisation steps (whole system as one with transformed gradients).
    outerCycle :: Int,
    -- | Geometry changes from the previous step.
    geomChange :: GeomConv,
    -- | Counter for microiterations. Only incremented on micro-cycle driven updates, (0,0)
    -- otherwise. Contains the depth of the current cycle, and the cycle counter at that depth.
    microCycle :: (Int, Int),
    -- | The current 'Molecule' might be saved at this step. Be aware that this potentially becomes
    -- very very memory demanding.
    molecule :: Maybe Molecule
  }
  deriving (Generic)

-- Reader classes.
class HasMotion env where
  motionL :: Lens' env (TVar (Seq Motion))

----------------------------------------------------------------------------------------------------

-- | Configuration settings for the calculation slot, that executes the wrapper calculations. It has
-- an input and an output variable.
data CalcSlot = CalcSlot
  { -- | Queue for calculations, that can be done in parallel. Provide a 'OniomLayer' to be
    -- calculated together with the associated 'CalcInput' and a 'CalcID', from which those
    -- information were obtained.
    input :: TQueue (OniomLayer, CalcInput, CalcID),
    -- | The finished result. Should be taken and emptied by the consumer.
    output :: TQueue (CalcID, CalcOutput)
  }
  deriving (Generic)

-- Default Class
instance DefaultIO CalcSlot where
  defIO = do
    calcSlotIn <- newTQueueIO
    calcSlotOut <- newTQueueIO
    return CalcSlot {input = calcSlotIn, output = calcSlotOut}

-- Reader Classes
class HasCalcSlot env where
  calcSlotL :: Lens' env CalcSlot

instance HasCalcSlot CalcSlot where
  calcSlotL = castOptic simple
