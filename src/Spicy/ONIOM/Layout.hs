-- |
-- Module      : Spicy.ONIOM.Layout
-- Description : Layouting the molecule for ONIOM calculations.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module takes care of initial preparation of the 'Molecule' type from a simple structured
-- input molecule to the ONIOM setup used in subsequent steps. It employs the anamorphism of trees
-- and the theory layers from the input file to do so.
module Spicy.ONIOM.Layout
  ( mcOniomNLayout,
  )
where

import Data.Foldable
import qualified Data.IntMap.Strict as IntMap
import qualified Data.IntSet as IntSet
import Data.Text (replace)
import qualified Network.Socket as Net
import RIO.Char (isAlphaNum, isDigit)
import qualified RIO.Map as Map
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import RIO.State
import qualified RIO.Text as Text
import Spicy.Attoparsec
import Spicy.Common
import Spicy.FragmentMethods.GMBE
import Spicy.FragmentMethods.SMF
import Spicy.InputFile
import Spicy.Molecule.Computational hiding (program)
import Spicy.Molecule.Fragment
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Prelude
import Spicy.SelectionLanguage (parseSelection, runSelection)
import System.Path as Path

-- | This modifies the molecule and unfolds the top layer into a full ONIOM tree, by using
-- information from the 'InputFile' (namely 'TheoryLayer'). It modifies the 'HasMolecule' state and
-- will expose the full ONIOM tree. It should be applied after changes to the topology (re-grouping,
-- bond modifications, ...).
mcOniomNLayout :: (HasInputFile env, HasMolecule env) => RIO env ()
mcOniomNLayout = do
  -- Obtain the environemt.
  InputFile {..} <- view inputFileL
  molT <- view moleculeL
  originalMolecule <- readTVarIO molT

  -- Get the initial state
  maxAtomKey <- getMaxAtomIndex originalMolecule

  -- Make the paths absolute.
  scratchDir <- liftIO . Path.genericMakeAbsoluteFromCwd . getDirPath $ scratch
  permaDir <- liftIO . Path.genericMakeAbsoluteFromCwd . getDirPath $ permanent
  let globalSettings = GlobalSettings {scratchDir, permaDir}

  -- Run the layout monad to unfold the input molecule into the full ONIOM tree.
  oniomTree <-
    liftIO . flip runReaderT globalSettings . flip evalStateT maxAtomKey $
      unfoldMol (model ^. #theoryLayer) layoutUnfoldF originalMolecule

  -- Expose the layouted molecule as the new reader state
  atomically . writeTVar molT $ oniomTree

----------------------------------------------------------------------------------------------------

-- | Static, global settings for layout creation.
data GlobalSettings = GlobalSettings
  { scratchDir :: AbsDir,
    permaDir :: AbsDir
  }

----------------------------------------------------------------------------------------------------

-- | Unfold an input molecule into an ONIOM tree by recursive application of its 'TheoryLayer's.
unfoldMol ::
  (Monad m) =>
  -- | Top-most 'TheoryLayer' from the 'InputFile'
  TheoryLayer ->
  -- | Function that creates a new layer from the current 'Molecule' and the current 'TheoryLayer'
  (MolID -> Molecule -> TheoryLayer -> m Molecule) ->
  -- | Molecule as read from file, that is to be layouted for an ONIOM calculation.
  Molecule ->
  m Molecule
unfoldMol theoryLayer f inpMol = go Empty theoryLayer inpMol
  where
    -- Recursive unfolding with accesss to the mol-id
    go idAcc tl mol = do
      -- Create this this ONIOM layer from a theory layer.
      thisLayer <- f idAcc mol tl

      -- Create the deeper layers.
      subMol <-
        IntMap.fromAscList . toList
          <$> traverse (\(k, sl) -> go (idAcc |> k) sl thisLayer <&> \sm -> (k, sm)) annSubLayers

      return thisLayer {subMol}
      where
        annSubLayers = Seq.fromList . zip [0 ..] $ (tl ^. #deeperLayer)

----------------------------------------------------------------------------------------------------

-- | The unfold function to create a valid ONIOM tree.
layoutUnfoldF ::
  (MonadThrow m, MonadState Int m, MonadIO m, MonadReader GlobalSettings m) =>
  MolID ->
  Molecule ->
  TheoryLayer ->
  m Molecule
layoutUnfoldF molID parentMol tl = do
  -- Isolate a new sublayer, with empty calculation context
  newNoCntxt <- isolateSubLayer molID parentMol tl

  -- Obtain calculation contexts for the new layer.
  original <- makeOrigCalcInput molID parentMol newNoCntxt tl
  inherited <- makeInheritedCalc molID parentMol newNoCntxt tl

  -- Combine the new molecule with its contexts.
  return $ newNoCntxt {calculations = Just Calculations {original, inherited}}

----------------------------------------------------------------------------------------------------

-- | Create an empty sublayer. Calculation contexts are not set, yet.
isolateSubLayer ::
  (MonadThrow m, MonadState Int m) =>
  -- | 'MolID' of the layer that is being created by this function.
  MolID ->
  -- | Layer from which to isolate the new one (parent)
  Molecule ->
  TheoryLayer ->
  m Molecule
isolateSubLayer molID inpMol TheoryLayer {selection} = do
  -- Parse and run the selection program for this layer.
  sel <- parse' parseSelection selection <&> runSelection inpMol

  -- Maximum atom key, that has been used up to now.
  maxKey <- get

  -- Create the new sublayer
  OniomLayer subLayer <- newSubLayer maxKey (OniomLayer inpMol) sel Nothing Nothing

  -- Set the comment string for this layer
  let comment = "OniomLayer_" <> molID2OniomHumanID molID
      subLayerCommented = subLayer {comment}

  -- Expose the new maximum atom key to the state.
  getMaxAtomIndex subLayer >>= put

  return subLayerCommented

----------------------------------------------------------------------------------------------------

-- | Construct the GMBE information for a new layer. If neither the original parent calculation, nor
-- the original calculation of the current layer are GMBE calculations, it will return a 'Nothing'.
makeGMBEInfo ::
  MonadThrow m =>
  -- | The parent molecule, from which the current layer was isolated. Needed to judge whether to
  -- build GMBE expressions by inheritance, even if the new layer itself did not specify GMBE
  -- calculations.
  Molecule ->
  -- | The newly isolated layer, that is to be filled with context.
  Molecule ->
  TheoryLayer ->
  -- | Specify whether to build the GMBE terms for the inherited calculation (as in the parent
  -- layer), or for the original calculation (as in the new layer).
  CalcK ->
  -- | If at least one of the original parent calculation, or this layers original calculation
  -- is of GMBE type, the function will 'Just' return a tuple of the 'GMBESum' and GMBE 'CalcOutput'
  -- 'Map's to be used on this layer (both for original, as well as inherited calculations).
  m GMBEmap
makeGMBEInfo parentMol newMol@Molecule {atoms, group} TheoryLayer {fragments} calcK = case calcK of
  Inherited -> do
    -- Obtain the auxiliary monomers from the parent layer and restrict them to groups of this layer
    parentGMBEMap <-
      maybe2MThrow
        (localExc "Inheritance of GMBE from the parent layer has been requested, but the parent layer does not have GMBE context.")
        $ parentMol ^? #calculations % _Just % #original % #gmbe % _Just
    let parentOutMap = fmap snd parentGMBEMap
    thisAuxMonomers <- fmap Map.keysSet . flip Map.traverseMaybeWithKey parentOutMap $ \am _ -> do
      let amRestrictedGroups = am & #constGroups %~ IntSet.intersection newGroupKeys
      newAMAtomKeys <- getAuxMonomerAtoms group amRestrictedGroups
      let newAMAtoms = IntMap.restrictKeys atoms newAMAtomKeys
          newAMCharge = sum . fmap (^. #formalCharge) $ newAMAtoms
          newAMExcEl = sum . fmap (^. #formalExcEl) $ newAMAtoms
      return $
        if IntSet.null (amRestrictedGroups ^. #constGroups)
          then Nothing
          else
            Just $
              amRestrictedGroups
                & #charge .~ newAMCharge
                & #excEl .~ newAMExcEl

    -- Make the GMBE sum for the inherited auxiliary monomers.
    return $ makeGmbeSum group thisAuxMonomers
  Original -> do
    -- Create the nmers by a given fragmentation pattern.
    FragmentMethods {nmer, fragtype, distCutoff} <-
      maybe2MThrow
        (localExc "Cannot setup a GMBE calculation without input from the theory layer")
        fragments
    let molGraph = mol2Graph newMol
        fragPattern = case fragtype of
          SMFA o -> smfa o molGraph group
          Overlap o -> systematicOverlapFragmentation o molGraph group
          Exclusion o -> systematicOverlapFragmentation o molGraph group
    nMers <-
      makeNmers newMol fragPattern nmer (fromMaybe 10e10 distCutoff) >>= \allAM -> case allAM of
        (_ :|> nmerAMs) -> return nmerAMs
        _ -> throwM . localExc $ "Couldn't form nmers for layer."

    return $ makeGmbeSum group nMers
  where
    localExc = MolLogicException "makeGMBEInfo"

    -- Keys of groups in this newly created layer. Strictly a subset of those of the parent layer.
    newGroupKeys = IntMap.keysSet group

----------------------------------------------------------------------------------------------------

-- | Construct the original calculation context for the given layer.
makeOrigCalcInput ::
  (MonadReader GlobalSettings m, MonadIO m, MonadThrow m) =>
  -- | 'MolID' of the newly created sub-layer.
  MolID ->
  -- | The parent molecule, from which this layer was created.
  Molecule ->
  -- | Newly created sub-layer (from 'isolateSubLayer'), that is to be filled with information.
  Molecule ->
  -- | 'TheoryLayer', that describes the new layer.
  TheoryLayer ->
  m CalcContext
makeOrigCalcInput molID parentMol newMol tl@TheoryLayer {execution, program, charge, mult, embedding, additionalInput, fragments} = do
  -- Get the global settings.
  GlobalSettings {scratchDir, permaDir} <- ask

  -- Create and update default optimisation settings.
  opt <- optSettings <$> defIO

  -- Prepare a pysisyphus instance.
  let pysisSocket = dirByIDAndCalc scratchDir molID Original </> Path.relFile "pysis.socket"
      pysisDir = dirByIDAndCalc scratchDir molID Original </> Path.relDir "pysis"
  pysis <-
    defIO >>= \p ->
      return $
        p
          & #socketAddr .~ (Net.SockAddrUnix . Path.toString $ pysisSocket)
          & #workDir .~ Path.toAbsRel pysisDir
          & #initCoords .~ (pysisDir </> Path.relFile "InitCoords.xyz")

  -- Prepare a generic calculation input.
  let input =
        CalcInput
          { task = WTEnergy,
            restartFile = Nothing,
            software = program,
            prefixName = Text.unpack . sanitiseIdentifier $ "OniomLayer_" <> molID2OniomHumanID molID <> "_high",
            permaDir = JDirPathAbs $ dirByIDAndCalc permaDir molID Original,
            scratchDir = JDirPathAbs $ dirByIDAndCalc scratchDir molID Original,
            nProcs = execution ^. #nProcesses,
            nThreads = execution ^. #nThreads,
            memory = execution ^. #memory,
            qMMMSpec = QM QMContext {charge, mult},
            embedding,
            optimisation = opt & #pysisyphus .~ pysis,
            additionalInput,
            fragmentMethods = fragments
          }

  -- If this an GMBE calculation, create the GMBE context, if not a Monolith context.
  case fragments of
    Nothing -> return $ CalcContext {input, output = Nothing, gmbe = Nothing}
    Just _ -> do
      gmbeMap <- makeGMBEInfo parentMol newMol tl Original
      return $
        CalcContext
          { input,
            output = Nothing,
            gmbe = Just gmbeMap
          }
  where
    defUp ::
      -- | Default 'Optimisation' values to be updated
      Optimisation ->
      -- | Tuple of lenses. Pairs the value to be updated with the corresponding value from the
      -- 'InputFile' settings in 'Opt'.
      (Optic' A_Lens NoIx Optimisation a, Optic' A_Lens NoIx Opt (Maybe a)) ->
      -- | Updated values.
      Optimisation
    defUp opt (targetL, originL) = case tl ^? #optimisation % _Just % originL % _Just of
      Nothing -> opt
      Just v -> opt & targetL .~ v
    optSettings :: Optimisation -> Optimisation
    optSettings optS =
      optS
        -- Simple updates
        `defUp` (#coordType, #coords)
        `defUp` (#maxCycles, #iterations)
        `defUp` (#hessianUpdate, #hessianUpdate)
        `defUp` (#maxTrust, #trustMax)
        `defUp` (#minTrust, #trustMin)
        `defUp` (#minTrust, #trustMin)
        `defUp` (#convergence, #conv)
        `defUp` (#microStep, #microStep)
        -- Other updates without direct match.
        & #optType
          .~ ( case tl ^? #optimisation % _Just % #target % _Just of
                 Nothing -> Minimum RFO
                 Just Min -> Minimum RFO
                 Just TS -> SaddlePoint RS_I_RFO
             )
        & #hessianRecalc .~ fromMaybe Nothing (tl ^? #optimisation % _Just % #hessianRecalc)

----------------------------------------------------------------------------------------------------

-- | Construct the inherited calculation. The embedding type is modified to match that of the
-- original layer at this level in the ONIOM tree, and will ignore what was given in the parent
-- layer.
makeInheritedCalc ::
  (MonadReader GlobalSettings m, MonadThrow m) =>
  -- | 'MolID' of this layer, which is the one to be filled with context.
  MolID ->
  -- | Parent molecule, from which the calculation needs to be inherited.
  Molecule ->
  -- | Newly created sub-layer, that is to be filled with context.
  Molecule ->
  -- | 'TheoryLayer', that describes the new layer.
  TheoryLayer ->
  m (Maybe CalcContext)
makeInheritedCalc molID parentMol newMol tl@TheoryLayer {charge, mult} = do
  -- Get the global settings
  GlobalSettings {..} <- ask

  -- Look for an original calculation on the parent molecule.
  let parentOrigCalc = parentMol ^? #calculations % _Just % #original
      thisOrigCalcEmbedding = tl ^. #embedding

  -- Compare the original calculation from above.
  case parentOrigCalc of
    -- No calculation above, nothing to inherit.
    Nothing -> return Nothing
    -- The parent had a monolithic calculation, therefore inherit a monolithic calculation.
    Just CalcContext {input, gmbe = Nothing} ->
      return . Just $
        CalcContext
          { output = Nothing,
            gmbe = Nothing,
            input =
              input
                { permaDir = JDirPathAbs $ dirByIDAndCalc permaDir molID Inherited,
                  scratchDir = JDirPathAbs $ dirByIDAndCalc scratchDir molID Inherited,
                  qMMMSpec = QM QMContext {charge, mult},
                  prefixName = Text.unpack . sanitiseIdentifier $ "OniomLayer_" <> molID2OniomHumanID molID <> "_low",
                  embedding = thisOrigCalcEmbedding
                }
          }
    -- The parent had a GMBE calculation, therefore adapt the GMBE expression to this layer and
    -- inherit the GMBE calculation.
    Just CalcContext {input, gmbe = Just _} -> do
      gmbeMap <- makeGMBEInfo parentMol newMol tl Inherited
      return . Just $
        CalcContext
          { output = Nothing,
            gmbe = Just gmbeMap,
            input =
              input
                { permaDir = JDirPathAbs $ dirByIDAndCalc permaDir molID Inherited,
                  scratchDir = JDirPathAbs $ dirByIDAndCalc scratchDir molID Inherited,
                  qMMMSpec = QM QMContext {charge, mult},
                  prefixName = Text.unpack . sanitiseIdentifier $ "OniomLayer_" <> molID2OniomHumanID molID <> "_low",
                  embedding = thisOrigCalcEmbedding
                }
          }

----------------------------------------------------------------------------------------------------

-- | Creat a directory hierarchy based on the ONIOM layer and the calculation key
dirByIDAndCalc :: Path.AbsDir -> MolID -> CalcK -> Path.AbsDir
dirByIDAndCalc dir' molID calcK = calcDir
  where
    idDir' = foldl (\acc i -> acc </> (Path.relDir . show $ i)) dir' molID
    calcDir =
      idDir'
        </> Path.relDir
          ( case calcK of
              Original -> "high"
              Inherited -> "low"
          )

----------------------------------------------------------------------------------------------------

-- | Sanitiser for the prefix. Ensures to only have valid identifiers (as accepted by Python's
-- @isidentifier@). Therefore contains only alpha-numeric characters and underscores and does not
-- start with a number.
sanitiseIdentifier :: Text -> Text
sanitiseIdentifier t =
  let bodyCleaned =
        Text.filter (\c -> isAlphaNum c || c == '_')
          . replace " " "_"
          . replace "-" "_"
          $ t
      firstDigitIx = Text.findIndex isDigit bodyCleaned
   in case firstDigitIx of
        Just 0 -> Text.cons 'a' bodyCleaned
        _ -> bodyCleaned
