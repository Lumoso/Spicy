-- |
-- Module      : Spicy.ONIOM.Collector
-- Description : Transforming individual information from calculations to an ONIOM result
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Collector functions to gather the results from the 'CalcOutput's of a molecule and build the
-- ONIOM result from it. It relies on the monolithic calculation context of each layer and fragment
-- based calculations must be transformed first.
module Spicy.ONIOM.Collector
  ( multicentreOniomNCollector,
    collector,
    collectorDepth,
  )
where

import qualified Data.IntMap.Strict as IntMap
import Data.Massiv.Array as Massiv hiding
  ( mapM,
    sum,
  )
import qualified RIO.Seq as Seq
import Spicy.FragmentMethods.GMBE
import Spicy.Molecule.Computational
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.Prelude

-- | Collector for multicentre ONIOM-N calculations.
{-# SCC multicentreOniomNCollector #-}
multicentreOniomNCollector :: HasMolecule env => RIO env ()
multicentreOniomNCollector = do
  molT <- view moleculeL
  mol <- readTVarIO molT
  molEDeriv <- collector mol
  atomically . writeTVar molT $ molEDeriv

----------------------------------------------------------------------------------------------------

-- | MC-ONIOMn collector for the full ONIOM tree. Transforms the energy and its derivatives to the
-- the ONIOM representation. Collects all derivatives as far as possible from bottom to top.
-- \[
--     E^\mathrm{MC-ONIOM2} = E^\mathrm{real} + \sum\limits_c E^\mathrm{model, high}_c - \sum\limits_c E^\mathrm{model, low}_c
-- \]
-- \[
--     \nabla E^\mathrm{MC-ONIOM2} = E^\mathrm{real} + \sum\limits_c \nabla E_c^\mathrm{model, high} \mathbf{J}_c - \sum\limits_c \nabla E_c^\mathrm{model, low} \mathbf{J}_c
-- \]
-- \[
--     \mathbf{H}^\mathrm{MC-ONIOM2} = \mathbf{H}^\mathrm{real}
--                                   - \sum\limits_c \mathbf{J}_c^T \mathbf{H}_c^\mathrm{model, low} \mathbf{J}_c
--                                   + \sum\limits_c \mathbf{J}_c^T \mathbf{H}_c^\mathrm{model, high} \mathbf{J}_c
-- \]
-- The idea is to express the full ONIOM tree as a recursion over local multi-centre ONIOM2 setups,
-- where the deepest layer of each branch forms the base case of the recursion.
{-# SCC collector #-}
collector :: MonadThrow m => Molecule -> m Molecule
collector mol = collectorDepth maxDepth mol
  where
    maxDepth =
      Seq.length $
        molFoldlWithMolID
          (\iAcc i _ -> if Seq.length i > Seq.length iAcc then i else iAcc)
          mempty
          mol

----------------------------------------------------------------------------------------------------

-- | Energy derivative collector up to a given depth of the ONIOM tree. If the maximum depth or
-- larger is given, this will collect the full ONIOM tree.
{-# SCC collectorDepth #-}
collectorDepth :: MonadThrow m => Int -> Molecule -> m Molecule
collectorDepth depth molecule
  | depth < 0 = throwM . localExc $ "Cannot collect for negative depth."
  | otherwise = go 0 molecule
  where
    localExc = MolLogicException "collectorDepth"

    -- Recursive bottom up collector.
    go :: MonadThrow m => Int -> Molecule -> m Molecule
    go currDepth mol = do
      -- If any calculation is a GMBE context in the current layer, transform the GMBE calculations
      -- in a monolithic calculation. Do this even if the monolithic output context has already been
      -- set, just to ensure they fit.
      molMono <- case mol ^? #calculations % _Just of
        Nothing -> throwM . localExc $ "No calculation to transform present on this layer."
        Just Calculations {original, inherited} -> do
          origMono <- do
            let CalcContext {gmbe} = original
            case gmbe of
              Nothing -> return $ original ^. #output
              Just _ -> Just <$> gmbeToMono (OniomLayer mol) Original

          inhMono <- case inherited of
            Nothing -> return Nothing
            Just inh -> do
              let CalcContext {gmbe} = inh
              case gmbe of
                Nothing -> return $ inh ^. #output
                Just _ -> Just <$> gmbeToMono (OniomLayer mol) Inherited

          return $
            mol
              & #calculations % _Just % #original % #output .~ origMono
              & #calculations % _Just % #inherited % _Just % #output .~ inhMono

      -- Model centres below this layer.
      let modelCentres = molMono ^. #subMol

      -- If we are at the maximum collection depth or nothing deeper exists we found the model
      -- system and assign the high level calculation output as the true energy of this model.
      -- Otherwise collect deeper layes first.
      if IntMap.null modelCentres || currDepth >= depth
        then eDerivTransfer Original molMono >>= multipoleTransformation IntMap.empty
        else do
          -- Recursion into deeper layers happens first. The collector works bottom up.
          modelsCollected <- mapM (go (currDepth + 1)) modelCentres

          -- Get the transformed derivatives of all model centres.
          let eModelHigh = fmap (^. #energyDerivatives) modelsCollected
          eModelLow <- mapM (getEDeriv Inherited) modelsCollected
          modelJacobian <-
            maybe2MThrow (localExc "Missing Jacobian for molecule") $
              mapM (^? #jacobian % _Just) modelCentres
          let modelEDs =
                IntMap.intersectionWith (\j (h, l) -> (h, l, j)) modelJacobian $
                  IntMap.intersectionWith (,) eModelHigh eModelLow

          -- Get the high level energy of this system.
          eReal <- getEDeriv Original molMono

          -- Perform the transformations of the energy derivatives.
          eDerivs <- eDerivTransformation modelEDs eReal

          -- Collect the multipoles.
          molMPoles <- multipoleTransformation modelCentres molMono

          return $
            molMPoles
              & #energyDerivatives .~ eDerivs
              & #subMol .~ modelsCollected

----------------------------------------------------------------------------------------------------

-- | Transformer for energy derivatives of local MC-ONIOM2 setups.
{-# SCC eDerivTransformation #-}
eDerivTransformation ::
  ( MonadThrow m,
    Traversable t
  ) =>
  -- | High level energy derivatives, low level energy derivatives and the Jacobian matrix per model
  -- centre.
  t (EnergyDerivatives, EnergyDerivatives, MatrixS Double) ->
  -- | Energy derivatives of the real system.
  EnergyDerivatives ->
  m EnergyDerivatives
eDerivTransformation eModelHighLow eReal = ff (pure eReal) eModelHighLow
  where
    ff = foldl' $ \accM (eDHigh, eDLow, MatrixS j) -> do
      -- Unwrap the accumulator.
      acc <- accM

      -- Transform the energy.
      let newEAcc :: Maybe Double
          newEAcc = do
            accE <- acc ^. #energy
            eHigh <- eDHigh ^. #energy
            eLow <- eDLow ^. #energy
            return $ accE + eHigh - eLow

      -- Transform the gradient.
      let newGAcc :: Maybe (VectorS Double)
          newGAcc = do
            (VectorS accG) <- acc ^. #gradient
            gHighT <- (eDHigh ^. #gradient) >>= \(VectorS g) -> g ><. j
            gLowT <- (eDLow ^. #gradient) >>= \(VectorS g) -> g ><. j
            VectorS <$> (accG .+. gHighT >>= (.-. gLowT))

      -- Transform the hessian.
      let newHAcc :: Maybe (MatrixS Double)
          newHAcc = do
            (MatrixS accH) <- acc ^. #hessian
            let jT = compute . transpose $ j
            hHighT <- (eDHigh ^. #hessian) >>= \(MatrixS h) -> jT .><. h >>= (.><. j)
            hLowT <- (eDLow ^. #hessian) >>= \(MatrixS h) -> jT .><. h >>= (.><. j)
            MatrixS . compute <$> (accH .+. hHighT >>= (.-. hLowT))

      return $
        EnergyDerivatives
          { energy = newEAcc,
            gradient = newGAcc,
            hessian = newHAcc
          }

----------------------------------------------------------------------------------------------------

-- | Energy derivative transfer from a high or low level calculation to the real system. Works on
-- the current top layer.
eDerivTransfer :: MonadThrow m => CalcK -> Molecule -> m Molecule
eDerivTransfer oniomKey mol =
  getEDeriv oniomKey mol >>= \eD -> return $ mol & #energyDerivatives .~ eD

----------------------------------------------------------------------------------------------------

-- | Get the energy derivatives of the current layer from the given calculation key.
getEDeriv :: MonadThrow m => CalcK -> Molecule -> m EnergyDerivatives
getEDeriv calcK mol =
  maybe2MThrow localExc $
    mol ^? #calculations % _Just % hierarchyOptic % #output % _Just % #energyDerivatives
  where
    localExc =
      MolLogicException "getEDeriv" $
        "Energy derivatives for the calculation key " <> show calcK <> " could not be found."

    hierarchyOptic = case calcK of
      Original -> castOptic #original
      Inherited -> #inherited % _Just

----------------------------------------------------------------------------------------------------

-- | Collector for multipole moments. Uses a charge/moment redistribution scheme. Moments of a link
-- atom in the layer will normally get the multipole moment from the calculation output. But if this
-- layer is later used to provide multipoles for a real system, the link atom moments will first be
-- distributed to the real atoms.
{-# SCC multipoleTransformation #-}
multipoleTransformation :: (Foldable t, Functor t, MonadThrow m) => t Molecule -> Molecule -> m Molecule
multipoleTransformation models real = do
  -- Get the multipoles of the real system from the high level calculation on this layer.
  mpRealOut <-
    maybe2MThrow (localExc "High level calculation with multipole information for this layer not found") $
      real ^? #calculations % _Just % #original % #output % _Just % #multipoles

  let -- Atoms of the real system with poles from the high level calculation on the real system.
      atomsRealWithOutPoles = updateAtomsWithPoles atomsReal mpRealOut
      -- Update again with the poles from the model centres.
      atomsRealWithModelPoles = updateAtomsWithPoles atomsRealWithOutPoles mpModels
      -- Reconstruct all atoms of the real system with highest level poles that are available.
      atomsUpdated = atomsRealWithModelPoles `IntMap.union` (atomsRealWithOutPoles `IntMap.union` atomsReal)

  return $ real & #atoms .~ atomsUpdated
  where
    localExc = MolLogicException "multipoleTransformation"
    atomsReal = real ^. #atoms

    -- Redistribute link atom moments of the model systems
    modelsRedis = fmap redistributeLinkMoments models

    -- Redistributed but otherwise collected multipole moments of the model systems.
    mpModels = IntMap.unions . fmap (\m -> fmap (^. #multipoles) $ m ^. #atoms) $ modelsRedis

----------------------------------------------------------------------------------------------------

-- | Update atoms with available multipoles. Atoms, that have no multipoles assigned to, will be
-- kept unchanged.
updateAtomsWithPoles :: IntMap Atom -> IntMap Multipoles -> IntMap Atom
updateAtomsWithPoles atoms poles =
  let atomsWithPoles = IntMap.intersectionWith (\a p -> a & #multipoles .~ p) atoms poles
   in atomsWithPoles `IntMap.union` atoms
