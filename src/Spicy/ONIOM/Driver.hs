-- |
-- Module      : Spicy.ONIOM.Driver
-- Description : Preparation, running an analysis of ONIOM jobs on layouted systems
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- The driver method for ONIOM applications. This modules uses the layouted molecules and performs
-- an atomic task with them (energy, gradient, hessian or property calculation). The drivers operate
-- stateful on the runtime environment, which keeps a global state of the molecule.
--
--   * Updates all calculation contexts to perform the correct task
--   * Call wrappers to perform the calculations specified, get their output and update all
--     'CalcOutput's
--   * Combine all data from 'CalcOutput' to a final ONIOM result, according to the task. Also see
--     "Spicy.ONIOM.Collector".
module Spicy.ONIOM.Driver
  ( multicentreOniomNDriver,
    geomMacroDriver,
    geomMicroDriver,
  )
where

import Control.Concurrent.STM.TQueue (flushTQueue)
import Data.Default
import Data.Foldable
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import Data.Massiv.Array as Massiv hiding (forM, forM_, loop, mapM)
import Data.Massiv.Array.Manifest.Vector as Massiv
import Network.Socket
import qualified RIO.HashSet as HashSet
import RIO.Process
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import qualified RIO.Vector.Storable as VectorS
import Spicy.Common
import Spicy.Data
import Spicy.FragmentMethods.GMBE
import Spicy.InputFile
import Spicy.Molecule.Computational
import Spicy.Molecule.Fragment
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.ONIOM.Collector
import Spicy.Outputter as Out
import Spicy.Prelude
import Spicy.RuntimeEnv
import Spicy.Wrapper.IPI.Protocol
import Spicy.Wrapper.IPI.Pysisyphus
import Spicy.Wrapper.IPI.Types
import System.Path ((</>))
import qualified System.Path as Path

-- | A primitive driver for individual calculations. It takes a single 'LayerCalcID' and ensures
-- correct execution of the associated calculation(s). A monolithic calculation will be performed
-- as is, while gmbe fragment calculation can be performed concurrently by a scheduling backend.
-- The calculations are atomic: the calculations are provided and only a molecule with fully updated
-- calculation outputs will be made available. A layer calculation will never produce partial
-- results for its fragments. A GMBE calculation will also populate the monolithic output field.
layerBatchDriver ::
  ( HasMolecule env,
    HasLogFunc env,
    HasCalcSlot env
  ) =>
  LayerCalcID ->
  WrapperTask ->
  RIO env ()
layerBatchDriver layerCalcID wTask = do
  -- Obtain infos from the environment.
  molT <- view moleculeL
  CalcSlot {input, output} <- view calcSlotL

  -- Logging
  logInfoS logSource $ "Running calculations for layer: " <> (displayShow . fmap calcID2Human $ calcIDs)

  -- Prepare all individual calculations and request them from the scheduling backend.
  atomically . forM_ calcIDs $ \calcID -> do
    cleanOutputOfCalc molT calcID
    assignTaskToCalc molT calcID wTask
    prepMol <- readTVar molT
    (calcInput, _, calcLayer) <- getCalcByID prepMol calcID
    writeTQueue input (calcLayer, calcInput, calcID)

  -- Logging
  logInfoS logSource "Polarised layer and scheduled all calculations, waiting for all results to finish now ..."

  -- Wait for all calculations to finish and then update the global molecule state with the results
  -- from the output queue.
  atomically $ do
    -- Check if all outputs are present yet.
    calcOutputs <- flushTQueue output
    checkSTM $ fmap fst calcOutputs == calcIDs

    -- Update the calculation output of the fragments.
    currMol <- readTVar molT
    let updatedMol = flipFoldl' currMol calcOutputs $ \accMol (calcID, calcOutput) -> case calcID of
          CalcID _ _ Nothing -> accMol & calcIDLensGen calcID % _Just % #output ?~ calcOutput
          CalcID _ _ (Just am) -> accMol & calcIDLensGen calcID % _Just % #gmbe % _Just % ix am % _2 ?~ calcOutput

    -- Write the updated molecule to the shared environment. GMBE collection happens in the GMBE
    -- driver.
    writeTVar molT updatedMol

  logInfoS logSource "Finished all calculations on layer and updated the corresponding outputs."
  where
    (molID, calcK, ams) = case layerCalcID of
      Monolithic mid ck -> (mid, ck, Nothing)
      Fragments mid ck f -> (mid, ck, Just f)
    logSource = "layerBatchDriver"
    calcIDs = case ams of
      Nothing -> pure $ CalcID molID calcK Nothing
      Just f -> fmap (CalcID molID calcK . Just) f

----------------------------------------------------------------------------------------------------

-- | Run all calculations on a given layer and calculation niveau. For a 'Monolithic' this will be a
-- single calculation, but in case of a fragment based calculation, it will run all of them and
-- populate both the fragment output map __and__ the collected output. Therefore transformation
-- from GMBE outputs to layer's monolithic output happens here implicitly. The 'Molecule' state will
-- be updated.
layerCalcDriver ::
  (HasMolecule env, HasLogFunc env, HasCalcSlot env) =>
  LayerCalcID ->
  WrapperTask ->
  RIO env ()
layerCalcDriver layerCalcID wTask = case layerCalcID of
  Monolithic molID calcK -> do
    -- Logging
    logInfoS logSource $
      "Running a Monolithic calculation on "
        <> (display . molID2OniomHumanID $ molID)
        <> " ("
        <> displayShow calcK
        <> ")"

    -- Run the monolithic calculation.
    layerBatchDriver layerCalcID wTask

  -- Multiple fragment calculations, can all be processed in parallel.
  Fragments molID calcK _ -> do
    logInfoS logSource $
      "Running GMBE calculations on "
        <> (display . molID2OniomHumanID $ molID)
        <> " ("
        <> displayShow calcK
        <> ")"
    gmbeDriver layerCalcID wTask
  where
    logSource = "CalcDriver(Layer)"

----------------------------------------------------------------------------------------------------

-- | A driver for GMBE calculations. It will process all the GMBE calculations of a layer and a
-- given niveau. Depending on the embedding scheme of fragments in GMBE, it might execute the GMBE
-- calculations of a layer multiple times, e.g. for parallel domain ONIOM until convergence.
-- It will only return after convergence. Should only be executed on actual domain calculations and
-- will fail otherwise.
gmbeDriver ::
  ( HasMolecule env,
    HasLogFunc env,
    HasCalcSlot env
  ) =>
  LayerCalcID ->
  WrapperTask ->
  RIO env ()
gmbeDriver (Monolithic _ _) _ = throwM $ SpicyIndirectionException "gmbeDriver" "This driver may only be used for fragment based calculations."
gmbeDriver layerCalcID@(Fragments molID calcK _auxMonos) wTask = do
  -- Logging
  logInfoS logSource $
    "Starting domain calculations on "
      <> (display . molID2OniomHumanID $ molID)
      <> " "
      <> displayShow calcK

  -- Obtain initial information
  molT <- view moleculeL
  mol <- readTVarIO molT
  layer <-
    maybe2MThrow (localExc "Could not find the specified layer for this calculation ID.") $
      mol ^? layerLens
  calcCntxt <-
    maybe2MThrow (localExc "Could not find a calculation for the given layer.") $
      layer ^? #calculations % _Just % niveauLensGen calcK

  -- Check if multiple iterations will be required by looking for the inter-fragment embedding.
  let fragEmb = calcCntxt ^? #input % #fragmentMethods % _Just % #embedding % _Just
      lastMPs = calcCntxt ^? #output % _Just % #multipoles

  -- Perform the necessary number of iterations until embedding has converged (if it was requested).
  case fragEmb of
    -- No embedding has been requested and the multipoles therefore do not need to converged.
    -- Every gets just embedded in as described for the ONIOM layer.
    Nothing -> do
      logInfoS logSource "No inter-fragment embedding. Doing single iteration."
      runGMBE layerCalcID wTask
    -- Static embedding has been requested, which means, that the multipoles of non-interacting
    -- fragments are calculated and they are then used to provide a polarisation cloud for just a
    -- single iteration in the field of the gas phase fragments.

    Just Static -> do
      logInfoS logSource "Static inter-fragment embedding. Doing one gas phase iteration to obtain multipoles."
      -- Make sure the old embedding has been removed, before attempting the gas phase calculation.
      atomically . writeTVar molT $
        mol
          & layerLens % #calculations % _Just % niveauLensGen calcK % #output % _Just % #multipoles .~ def
          & layerLens % #calculations % _Just % niveauLensGen calcK % #gmbe % _Just % each %~ second (\co -> co & _Just % #multipoles .~ def)

      -- Run the gas phase calculation. Populates the monolithic output field on this layer.
      runGMBE layerCalcID WTEnergy

      -- Run the calculation with gas phase embedding charges.
      logInfoS logSource "Using gas-phase multipoles for static inter-fragment embedding."
      runGMBE layerCalcID wTask

    -- Parallel domain ONIOM iterates until the charges converge in the field of the previous
    -- iteration and should therefore provide fully consistent embedding.
    Just (ParallelDomain convThresh) -> do
      logInfoS logSource "Self-consistent inter-fragment embedding requested. Iterating ..."
      -- If multipoles from the last iteration are present, use them to polarise the fragments for
      -- this iteration. This happens implicitly when calling the calcIdDriver (and therefore also
      -- getCalcByID).
      runGMBE layerCalcID WTEnergy

      -- Obtain the multipoles, that have been obtained by this iteration and check convergence.
      molPost <- readTVarIO molT
      thisMPs <-
        maybe2MThrow (localExc "GMBE iteration has not produced collected multipoles") $
          molPost ^? layerLens % #calculations % _Just % niveauLensGen calcK % #output % _Just % #multipoles
      mpConverged <- sequence $ checkMultipoleConvergence convThresh thisMPs <$> lastMPs

      -- If the multipoles have converged, perform one more GMBE iteration with the appropriate
      -- task. If not, reiterate.
      case mpConverged of
        Nothing -> do
          logInfoS logSource "No multipoles from previous iteration. Generating multipoles ..."
          gmbeDriver layerCalcID WTEnergy
        Just (True, diff) -> do
          logInfoS logSource $ "Multipoles converged: RMS = " <> displayShow diff
          logInfoS logSource "Performing final iteration ..."
          runGMBE layerCalcID wTask
        Just (False, diff) -> do
          logInfoS logSource $ "Multipoles no converged, reiterating ...: RMS = " <> displayShow diff
          gmbeDriver layerCalcID WTEnergy
  where
    logSource = "CalcDriver(domain)"
    localExc = MolLogicException "gmbeDriver"
    layerLens = molIDLensGen molID

    runGMBE ::
      (HasMolecule env, HasLogFunc env, HasCalcSlot env) =>
      LayerCalcID ->
      WrapperTask ->
      RIO env ()
    runGMBE (Monolithic _ _) _ = throwM $ SpicyIndirectionException "runGMBE" "This driver may only be used for fragment based calculations."
    runGMBE lCID@(Fragments mid ck _) wt = do
      -- Run all calculations.
      layerBatchDriver lCID wt

      -- Prepare GMBE result transformation to a monolithic result.
      molT <- view moleculeL
      mol <- readTVarIO molT
      layer <- getMolByID mol mid

      -- Populate the monolithic output field with collected and transformed GMBE results.
      collGMBEOutput <- gmbeToMono (OniomLayer layer) ck
      let molWithCollGMBEoutput =
            mol & layerLens' % #calculations
              % _Just
              % niveauLens'
              % #output
              ?~ collGMBEOutput
      atomically . writeTVar molT $ molWithCollGMBEoutput
      where
        layerLens' = molIDLensGen mid
        niveauLens' = case ck of
          Original -> castOptic #original
          Inherited -> #inherited % _Just

----------------------------------------------------------------------------------------------------

-- | A driver function for an atomic step in Multicentre-ONIOM-n methods. Performs a single point
-- energy calculation, a gradient calculation or a hessian calculation on a given layout and builds
-- the ONIOM result from the individual results of the calculations on the layers.
multicentreOniomNDriver ::
  ( HasMolecule env,
    HasLogFunc env,
    HasInputFile env,
    HasCalcSlot env
  ) =>
  WrapperTask ->
  RIO env ()
multicentreOniomNDriver atomicTask = do
  logInfoS logSource $
    "Full ONIOM traversal for " <> case atomicTask of
      WTEnergy -> "energy."
      WTGradient -> "gradient."
      WTHessian -> "hessian."

  -- Obtain environment information
  molT <- view moleculeL
  mol <- readTVarIO molT
  inputFile <- view inputFileL

  let modelOK = case inputFile ^. #model of
        ONIOMn {} -> True

  -- Check if this driver is suitable for the layout.
  unless modelOK . throwM . localExc $
    "This driver assumes a multicentre ONIOM-n layout,\
    \ but the input file specifies a different layout type."

  -- Obtain all calculations IDs.
  let allLayerCalcIDs = getAllLayerCalcIDsHierarchically mol

  -- Iterate over all layers and calculate all of them. After each high level calculation the
  -- multipoles will be collected and transfered from the high level calculation output to the
  -- actual multipole fields of the atoms, to allow for electronic embedding in deeper layers.
  --
  -- Run the original and the inherited calculation on this layer. For the original high level
  -- calculation transfer the multipoles from the calculation otuput to the corresponding fields
  -- of the atoms.
  forM_ allLayerCalcIDs $ \layerCalcID -> do
    -- Generate a layer lens from this ID.
    let (molID, calcK) = case layerCalcID of
          Monolithic mi ck -> (mi, ck)
          Fragments mi ck _ -> (mi, ck)
        layerLens = molIDLensGen molID

    -- Perform the current calculation on the current calculation of the molecule.
    layerCalcDriver layerCalcID atomicTask

    -- If this was a high level original calculation, transfer the multipoles to the atoms and
    -- recalculate energy derivatives.
    when (calcK == Original) $ do
      molWithPolOutput <- readTVarIO molT
      layerWithPolTrans <- getMolByID molWithPolOutput molID >>= multipoleTransfer
      let molWithPolTrans = molWithPolOutput & layerLens .~ layerWithPolTrans
      atomically . writeTVar molT $ molWithPolTrans

  -- After full traversal collect all results.
  multicentreOniomNCollector
  where
    localExc = MolLogicException "multicentreOniomNDriver"
    logSource = "multi-centre ONIOM driver"

----------------------------------------------------------------------------------------------------

-- | A driver for geometry optimisations without microiteration. Does one full traversal of the
-- ONIOM molecule before a geometry displacement step. Updates can happen by an arbitrary i-PI
-- server.
geomMacroDriver ::
  ( HasMolecule env,
    HasLogFunc env,
    HasInputFile env,
    HasCalcSlot env,
    HasProcessContext env,
    HasWrapperConfigs env,
    HasOutputter env,
    HasMotion env
  ) =>
  RIO env ()
geomMacroDriver = do
  -- Logging.
  logInfoS logSource "Starting a direct geometry optimisation on the full ONIOM tree."
  optStartPrintEnv <- getCurrPrintEnv
  printSpicy txtDirectOpt
  printSpicy . renderBuilder . spicyLog optStartPrintEnv $
    spicyLogMol (HashSet.fromList [Always, Task Start]) All
  printSpicy $ "\n\n" <> optTableHeader True

  -- Obtain the Pysisyphus IPI settings and convergence treshold for communication.
  mol <- view moleculeL >>= readTVarIO
  optSettings <-
    maybe2MThrow (localExc "Optimisation settings not found on the top layer.") $
      mol ^? #calculations % _Just % #original % #input % #optimisation
  let pysisIPI = optSettings ^. #pysisyphus
      convThresh = optSettings ^. #convergence

  -- Launch a Pysisyphus server and an i-PI client for the optimisation.
  logDebugS logSource "Launching i-PI server for optimisations."
  (pysisServer, pysisClient) <- providePysis
  link pysisServer
  link pysisClient

  -- Start the loop that provides the i-PI client thread with data for the optimisation.
  let allTopAtoms = IntMap.keysSet $ mol ^. #atoms
  logDebugS logSource "Entering the optimisation recursion."
  loop pysisIPI convThresh allTopAtoms

  -- Final logging
  logInfoS logSource "Finished geometry optimisation."
  optEndPrintEnv <- getCurrPrintEnv
  printSpicy . renderBuilder . spicyLog optEndPrintEnv $
    spicyLogMol (HashSet.fromList [Always, Task End]) All
  where
    localExc = MolLogicException "geomMacroDriver"
    logSource = "MacroGeometryDriver"

    -- The optimisation loop.
    loop ::
      ( HasMolecule env,
        HasLogFunc env,
        HasInputFile env,
        HasCalcSlot env,
        HasProcessContext env,
        HasWrapperConfigs env,
        HasOutputter env,
        HasMotion env
      ) =>
      IPI ->
      GeomConv ->
      IntSet ->
      RIO env ()
    loop pysisIPI convThresh selAtoms = do
      -- Get communication variables with the i-PI client and Spicy.
      let ipiDataIn = pysisIPI ^. #input
          ipiPosOut = pysisIPI ^. #output
      molT <- view moleculeL

      -- Check if the client is still runnning and expects us to provide new data.
      ipiServerWants <- atomically . takeTMVar $ pysisIPI ^. #status
      case ipiServerWants of
        -- Terminate the loop in case the i-PI server signals convergence
        Done -> logInfoS logSource "i-PI server signaled convergence. Exiting."
        -- Pysisyphus extensions. Performs a client -> server position update and then loops back.
        WantPos -> do
          logWarnS
            logSource
            "Unusual i-PI server request for a spicy -> i-PI position update. Providing current atomic positions ..."

          -- Consume an old geometry from the server and discard.
          void . atomically . takeTMVar $ ipiPosOut

          -- Obtain current molecule geometry and update the server with those.
          updateCoords <- readTVarIO molT >>= getCoordsNetVec
          atomically . putTMVar ipiDataIn . PosUpdateData $ updateCoords

          -- Reiterating
          loop pysisIPI convThresh selAtoms

        -- Standard i-PI behaviour with server -> client position updates and client -> sever force/
        -- hessian updates.
        _ -> do
          logInfoS logSource "New geometry from i-PI server. Preparing to calculate new energy derivatives."

          -- Obtain the molecule before i-PI modifications.
          molOld <- readTVarIO molT
          posData <- atomically . takeTMVar $ ipiPosOut
          posVec <- case posData ^. #coords of
            NetVec vec -> Massiv.fromVectorM Par (Sz $ VectorS.length vec) vec
          molNewStruct <- updatePositionsPosVec posVec selAtoms molOld
          atomically . writeTVar molT $ molNewStruct

          -- Do a full traversal of the ONIOM tree and obtain the full ONIOM gradient.
          ipiData <- case ipiServerWants of
            WantForces -> do
              logInfoS logSource "Calculating new gradient."
              multicentreOniomNDriver WTGradient
              multicentreOniomNCollector
              molWithForces <- readTVarIO molT
              molToForceData molWithForces
            WantHessian -> do
              logInfoS logSource "Calculating new hessian."
              multicentreOniomNDriver WTHessian
              multicentreOniomNCollector
              molWithHessian <- readTVarIO molT
              molToHessianData molWithHessian
            Done -> do
              logErrorS
                logSource
                "The macro geometry driver should be done but has entered an other\
                \ calculation loop."
              throwM $
                SpicyIndirectionException "geomMacroDriver" "Data expected but not calculated?"
            WantPos -> do
              logErrorS
                logSource
                "The i-PI server wants a position update, but position updates must not occur here."
              throwM $
                SpicyIndirectionException
                  "geomMacroDriver"
                  "Position update requested but must not happen here."

          -- Get the molecule in the new structure with its forces or hessian.
          atomically . putTMVar ipiDataIn $ ipiData

          -- If converged terminate the i-PI server by putting a "converged" file in its working
          -- directory
          molNewWithEDerivs <- readTVarIO molT
          geomChange <- calcGeomConv (IntMap.keysSet $ molOld ^. #atoms) molOld molNewWithEDerivs
          let isConverged = geomChange < convThresh
          when isConverged $ do
            logInfoS logSource "Optimisation has converged. Telling i-PI server to EXIT in the next loop."
            writeFileUTF8 (pysisIPI ^. #workDir </> Path.relFile "converged") mempty

          -- Update the Motion history types.
          motionT <- view motionL
          motionHist <- readTVarIO motionT
          let newMotion = case motionHist of
                Empty ->
                  Spicy.RuntimeEnv.Motion
                    { geomChange,
                      molecule = Nothing,
                      outerCycle = 0,
                      microCycle = (0, 0)
                    }
                _ :|> Spicy.RuntimeEnv.Motion {outerCycle} ->
                  Spicy.RuntimeEnv.Motion
                    { geomChange,
                      molecule = Nothing,
                      outerCycle = outerCycle + 1,
                      microCycle = (0, 0)
                    }
              nextMotion = motionHist |> newMotion
              step = outerCycle newMotion
          atomically . writeTVar motionT $ nextMotion

          -- Opt loop logging.
          optLoopPrintEnv <- getCurrPrintEnv
          let molInfo =
                renderBuilder . spicyLog optLoopPrintEnv $
                  spicyLogMol (HashSet.fromList [Always, Out.Motion Out.Macro, FullTraversal]) All
          printSpicy $
            sep <> "\n\n"
              <> "@ Geometry Convergence\n\
                 \----------------------\n"
              <> optTableHeader False
              <> optTableLine step Nothing geomChange
              <> "\n\n"
              <> molInfo

          -- Reiterating
          loop pysisIPI convThresh selAtoms

{-
====================================================================================================
-}

-- | A driver for geometry optimisations with microiterations. The scheme is a so called adiabatic
-- one. Let's say we focus on a horizontal slice of the ONIOM tree, and look at all layers that are
-- on this same hierarchy in the ONIOM layout, no matter if they are in the same branch or not.
-- Those are optimised together by a single optimiser (pysisyphus instance). We define a coordinate
-- system for this hierarchy, that does not influence the coordinates of atoms of any other
-- hierarchy. These coordinates are \(q^l\) and \(\partial E_\text{model} / \partial q^l = 0\).
-- Therefore, \(q^l\) contains the coordinates of all atoms of this horizontal slice, minus the
-- atoms of all models one layer below and also minus the atoms that were replaced by links in the
-- models (real parent atoms). Now, we fully converge these coordinates to a minimum. After
-- convergence we do a single step in the model systems with coordinate system \(q^m\), which
-- contains the coordinates of model atoms that are not part of even deeper models and not link
-- atoms of the model layer, as well as the real parents from one layer above. In the next recursion
-- \(q^m\) will become \(q^l\).
geomMicroDriver ::
  ( HasMolecule env,
    HasInputFile env,
    HasLogFunc env,
    HasProcessContext env,
    HasWrapperConfigs env,
    HasCalcSlot env,
    HasMotion env,
    HasOutputter env
  ) =>
  RIO env ()
geomMicroDriver = do
  -- Logging.
  logInfoS logSource "Starting a geometry optimisation with micro cycles."
  optStartPrintEnv <- getCurrPrintEnv
  printSpicy txtMicroOpt
  printSpicy . renderBuilder . spicyLog optStartPrintEnv $
    spicyLogMol (HashSet.fromList [Always, Task Start]) All

  -- Get intial information.
  molT <- view moleculeL
  mol <- readTVarIO molT

  -- Setup the pysisyphus optimisation servers per horizontal slice.
  logDebugS logSource "Launching client and server i-PI companion threads per horizontal slice ..."
  microOptHierarchy <- setupPysisServers mol

  -- Perform the optimisations steps in a mindblowing recursion ...
  -- Spawn an optimiser function at the lowest depth and this will spawn the other optimisers bottom
  -- up. When converged, the function will terminate.
  logInfoS logSource $
    "Starting the micro-cycle recursion at the most model slice of the ONIOM tree ("
      <> display (Seq.length microOptHierarchy - 1)
      <> ")."
  optAtDepth (Seq.length microOptHierarchy - 1) microOptHierarchy
  logInfoS logSource "Optimisation has converged!"

  -- Final optimisation logging
  optEndPrintEnv <- getCurrPrintEnv
  printSpicy . renderBuilder . spicyLog optEndPrintEnv $
    spicyLogMol (HashSet.fromList [Always, Task End]) All

  -- Terminate all the companion threads.
  logDebugS logSource "Terminating i-PI companion threads."
  forM_ microOptHierarchy $ \MicroOptSetup {ipiClientThread, pysisIPI} -> do
    -- Stop the pysisyphus server by gracefully by writing a magic file. Send once more gradients
    -- (dummy values more or less ...) to finish nicely and then reset all connections.
    let convFile = (pysisIPI ^. #workDir) </> Path.relFile "converged"
    writeFileUTF8 convFile mempty
    atomically . putTMVar (pysisIPI ^. #input) . dummyForces . IntMap.keysSet $ mol ^. #atoms

    -- Cancel the client and reset the communication variables to a fresh state.
    cancel ipiClientThread
    void . atomically . tryTakeTMVar $ pysisIPI ^. #input
    void . atomically . tryTakeTMVar $ pysisIPI ^. #output
    void . atomically . tryTakeTMVar $ pysisIPI ^. #status
  where
    logSource = "geomMicroDriver"
    dummyForces sel =
      ForceData
        { potentialEnergy = 0,
          forces = NetVec . VectorS.fromListN (3 * IntSet.size sel) $ [0 ..],
          virial = CellVecs (T 1 0 0) (T 0 1 0) (T 0 0 1),
          optionalData = mempty
        }

----------------------------------------------------------------------------------------------------

-- | Select the atoms that will be optimised on this hierarchy/horizontal slice.
-- @
-- selection = (real atoms of all centres in this horizontal slice)
--           - (link atoms of all centres in this horizontal slice)
--           - (atoms that also belong to deeper layers)
--           - (atoms that are real atoms in this layer but real parents (replaced by link atoms) in deeper layers)
--           + (the real parents of the link atoms of this layer)
-- @
getOptAtomsAtDepth ::
  -- | Full ONIOM tree, not just a sublayer
  Molecule ->
  -- | The depth at which to make a slice and optimise.
  Int ->
  -- | Atoms selected to be optimised at this depth.
  IntSet
getOptAtomsAtDepth mol depth =
  ( allAtomsAtDepthS -- All "real" atoms at given depth
      IntSet.\\ modelRealParentsS -- Minus atoms that are real partners with respect to deeper models
      IntSet.\\ allAtomsModelS -- Minus all atoms of deeper models.
      IntSet.\\ linkAtomsAtDepthS -- Minus all link atoms at this depth.
  )
    <> realRealPartnersS -- Plus all atoms in the layer above that are bound to link atoms of this layer
  where
    -- Obtain all non-dummy atoms that are at the given depth.
    depthSliceMol = fromMaybe mempty $ horizontalSlices mol Seq.!? depth
    allAtomsAtDepth =
      IntMap.filter (not . isDummy) . foldl' (\acc m -> acc <> m ^. #atoms) mempty $ depthSliceMol
    allAtomsAtDepthS = IntMap.keysSet allAtomsAtDepth
    linkAtomsAtDepthS = IntMap.keysSet . IntMap.filter linkF $ allAtomsAtDepth

    -- Obtain all atoms that are even deeper and belong to model systems and therefore need to be
    -- removed from the optimisation coordinates.
    modelSliceMol = fromMaybe mempty $ horizontalSlices mol Seq.!? (depth + 1)
    allAtomsModel = foldl' (\acc m -> acc <> m ^. #atoms) mempty modelSliceMol
    allAtomsModelS = IntMap.keysSet allAtomsModel
    modelRealParentsS = getRealPartners allAtomsModel

    -- Obtain the real partners one layer above of link atoms at this depth.
    realRealPartnersS = getRealPartners allAtomsAtDepth

    -- Filters to apply to remove atoms.
    linkF = isAtomLink . isLink

    -- Get real parent partner of a link atom.
    getLinkRP :: Atom -> Maybe Int
    getLinkRP a = a ^? #isLink % _IsLink % #linkRealPartner

    -- Get the real partners of the link atoms in a set of atoms.
    getRealPartners :: IntMap Atom -> IntSet
    getRealPartners =
      IntSet.fromList
        . fmap snd
        . IntMap.toList
        . fromMaybe mempty
        . traverse getLinkRP
        . IntMap.filter linkF

----------------------------------------------------------------------------------------------------

-- | Perform all calculations at a given depth. Allows to get gradients or hessians on a horizontal
-- slice hierarchically.
calcAtDepth ::
  ( HasMolecule env,
    HasLogFunc env,
    HasCalcSlot env
  ) =>
  Int ->
  WrapperTask ->
  RIO env ()
calcAtDepth depth task = do
  molT <- view moleculeL
  mol <- readTVarIO molT

  -- Get all layer calculation IDs at a given depth.
  let calcIDDepth =
        Seq.filter
          ( \layerCalcID ->
              let molID = case layerCalcID of
                    Monolithic mi _ -> mi
                    Fragments mi _ _ -> mi
               in depth == Seq.length molID
          )
          . getAllLayerCalcIDsHierarchically
          $ mol

  -- Perform all calculations at the given depth.
  forM_ calcIDDepth $ \cid -> layerCalcDriver cid task

----------------------------------------------------------------------------------------------------

-- | Do a single geometry optimisation step with a *running* pysisyphus instance at a given depth.
-- Takes care that the gradients are all calculated and that the microcycles above have converged.
optAtDepth ::
  ( HasMolecule env,
    HasLogFunc env,
    HasCalcSlot env,
    HasProcessContext env,
    HasMotion env,
    HasOutputter env
  ) =>
  Int ->
  Seq MicroOptSetup ->
  RIO env ()
optAtDepth depth' microOptSettings'
  | depth' > Seq.length microOptSettings' = throwM $ MolLogicException "optStepAtDepth" "Requesting optimisation of a layer that has no microoptimisation settings"
  | depth' < 0 = return ()
  | otherwise = do
    logInfoS logSMicroD $ "Starting micro-cyle optimisation on layer " <> display depth'
    untilConvergence depth' microOptSettings'
    logInfoS logSMicroD $ "Finished micro-cycles of layer " <> display depth'
  where
    logSMicroD :: LogSource
    logSMicroD = "geomMicroDriver"

    ------------------------------------------------------------------------------------------------
    (!??) :: MonadThrow m => Seq a -> Int -> m a
    sa !?? i = case sa Seq.!? i of
      Nothing -> throwM $ IndexOutOfBoundsException (Sz $ Seq.length sa) i
      Just v -> return v

    ------------------------------------------------------------------------------------------------
    coordinateSync ::
      (HasMolecule env, HasLogFunc env) =>
      -- | Current optimisation depth
      Int ->
      -- | The optimisation settings of this layer used to optimised this layer
      Seq MicroOptSetup ->
      RIO env ()
    coordinateSync depth mos = do
      logDebugS logSMicroD $
        "(slice " <> display depth <> ") synchronising coordinates with i-PI server (spicy -> i-PI)"

      -- Settings of this layer
      optSettings <- mos !?? depth
      let posOut = optSettings ^. #pysisIPI % #output
          dataIn = optSettings ^. #pysisIPI % #input

      -- Consume and discard the invalid step from the server.
      void . atomically . takeTMVar $ posOut

      -- Get the current state of the molecule and send it to the Pysisyphus instance, to update
      -- those coordinates before doing a step.
      molCurrCoords <- view moleculeL >>= readTVarIO >>= getCoordsNetVec
      atomically . putTMVar dataIn . PosUpdateData $ molCurrCoords

    ------------------------------------------------------------------------------------------------
    updateGeomFromPysis ::
      (HasMolecule env, HasLogFunc env) =>
      -- | Current optimisation depth
      Int ->
      Seq MicroOptSetup ->
      RIO env ()
    updateGeomFromPysis depth mos = do
      logDebugS logSMicroD $ "(slice " <> display depth <> ") new geometry for slice from i-PI."

      -- Initial information
      molT <- view moleculeL
      molPreStep <- readTVarIO molT
      optSettings <- mos !?? depth
      let posOut = optSettings ^. #pysisIPI % #output
          allRealAtoms = IntMap.keysSet $ molPreStep ^. #atoms

      -- Do the position update.
      posVec <- do
        d <- atomically . takeTMVar $ posOut
        let v = getNetVec $ d ^. #coords
        Massiv.fromVectorM Par (Sz $ VectorS.length v) v
      molNewCoords <- updatePositionsPosVec posVec allRealAtoms molPreStep
      atomically . writeTVar molT $ molNewCoords

    ------------------------------------------------------------------------------------------------
    recalcGradsAbove ::
      (HasMolecule env, HasLogFunc env, HasCalcSlot env) =>
      -- | Current optimisation depth
      Int ->
      RIO env ()
    recalcGradsAbove depth = do
      logDebugS logSMicroD $ "(slice " <> display depth <> ") Step invalidated gradients. Recalculating gradients above current slice."

      -- Initial information.
      molT <- view moleculeL

      -- For all layers above recalculate the gradients. Then collect everything possible again and
      -- transform into a valid ONIOM representation.
      forM_ (allAbove depth) $ \d -> calcAtDepth d WTGradient
      molInvalidG <- readTVarIO molT
      unless (depth == 0) $ collectorDepth (max 0 $ depth - 1) molInvalidG >>= atomically . writeTVar molT

      logDebugS logSMicroD $ "(slice " <> display depth <> ") Finished with gradient above."
      where
        allAbove d = if d >= 0 then [0 .. d - 1] else []

    ------------------------------------------------------------------------------------------------
    recalcGradsAtDepth ::
      (HasMolecule env, HasLogFunc env, HasCalcSlot env) =>
      -- | Current optimisation depth
      Int ->
      RIO env ()
    recalcGradsAtDepth depth = do
      logDebugS logSMicroD $ "(slice " <> display depth <> ") Recalculating gradients in current slice."

      -- Initial information.
      molT <- view moleculeL

      -- Recalculate all layers at this depth slice and update the molecule state with this information.
      calcAtDepth depth WTGradient
      molPostStep <- readTVarIO molT >>= collectorDepth depth
      atomically . writeTVar molT $ molPostStep

      logDebugS logSMicroD $ "(slice " <> display depth <> ") Finished with current slice's gradients."

    ------------------------------------------------------------------------------------------------
    provideForces ::
      (HasMolecule env, HasLogFunc env) =>
      -- | Current optimisation depth
      Int ->
      Seq MicroOptSetup ->
      RIO env ()
    provideForces depth mos = do
      -- Initial information.
      optSettings <- mos !?? depth
      let dataIn = optSettings ^. #pysisIPI % #input

      -- Obtain (already transformed!) ONIOM gradients.
      molWithGrads <- view moleculeL >>= readTVarIO
      transGradReal <-
        maybe2MThrow (SpicyIndirectionException "untilConvergence" "Gradients missing") $
          molWithGrads ^? #energyDerivatives % #gradient % _Just

      -- Construct the data for i-PI
      let forcesBohrReal = compute @S . Massiv.map (bohr2Angstrom . negate) . getVectorS $ transGradReal
          forceData =
            ForceData
              { potentialEnergy = fromMaybe 0 $ molWithGrads ^. #energyDerivatives % #energy,
                forces = NetVec . Massiv.toVector $ forcesBohrReal,
                virial = CellVecs (T 1 0 0) (T 0 1 0) (T 0 0 1),
                optionalData = mempty
              }

      -- Send forces to i-PI
      logDebugS logSMicroD $ "(slice " <> display depth <> ") Providing forces to i-PI."
      atomically . putTMVar dataIn $ forceData

    ------------------------------------------------------------------------------------------------
    calcConvergence ::
      (HasMotion env) =>
      -- | Current optimisation depth
      Int ->
      Seq MicroOptSetup ->
      -- | Molecule before the step
      Molecule ->
      -- | Molecule after the step.
      Molecule ->
      -- | The geometry change between steps and the new 'Motion' information constructed from it.
      RIO env (GeomConv, Motion)
    calcConvergence depth mos molPre molPost = do
      -- Initial information
      optSettings <- mos !?? depth
      let atomDepthSelection = optSettings ^. #atomsAtDepth
      motionT <- view motionL

      -- Calculate the values characterising the geometry changes.
      geomChange <- calcGeomConv atomDepthSelection molPre molPost

      -- Build the next value of the motion history and add it to the history.
      motionHist <- readTVarIO motionT
      let lastMotionAtDepth = getLastMotionOnLayer motionHist
          lastCounterAtDepth = fromMaybe 0 $ snd . microCycle <$> lastMotionAtDepth
          newMotion = case motionHist of
            Empty ->
              Spicy.RuntimeEnv.Motion
                { geomChange,
                  molecule = Nothing,
                  outerCycle = 0,
                  microCycle = (depth, 0)
                }
            _ :|> Spicy.RuntimeEnv.Motion {outerCycle} ->
              Spicy.RuntimeEnv.Motion
                { geomChange,
                  molecule = Nothing,
                  outerCycle,
                  microCycle = (depth, lastCounterAtDepth + 1)
                }
          nextMotion = motionHist |> newMotion
      atomically . writeTVar motionT $ nextMotion

      -- Return the calculated geometry change.
      return (geomChange, newMotion)
      where
        getLastMotionOnLayer :: Seq Motion -> Maybe Motion
        getLastMotionOnLayer Empty = Nothing
        getLastMotionOnLayer (ini :|> l) =
          let lastDepth = l ^. #microCycle % _1
           in if lastDepth == depth
                then Just l
                else getLastMotionOnLayer ini

    ------------------------------------------------------------------------------------------------
    logOptStep ::
      (HasMolecule env, HasOutputter env, HasLogFunc env) =>
      -- | Current optimisation depth
      Int ->
      -- | Geometry change between steps.
      GeomConv ->
      -- | 'Motion' information for this geometry optimisation step.
      Motion ->
      RIO env ()
    logOptStep depth geomChange motion = do
      logInfoS logSMicroD $ "(slice " <> display depth <> ") Finished micro-cycle " <> displayShow (motion ^. #microCycle)

      -- Initial information
      mol <- view moleculeL >>= readTVarIO

      microStepPrintEnv <- getCurrPrintEnv
      let -- Maximum depth of the ONIOM tree.
          maxDepth = (+ (- 1)) . Seq.length . horizontalSlices $ mol

          -- 'MolID's at the current given depth.
          molIDsAtSlice =
            fmap Layer
              . Seq.filter (\mid -> Seq.length mid == depth)
              . getAllMolIDsHierarchically
              $ mol

          -- Auto-generated output for each layer involved in this step.
          layerInfos =
            renderBuilder
              . foldl (<>) mempty
              . fmap
                ( spicyLog microStepPrintEnv
                    . spicyLogMol (HashSet.fromList [Always, Out.Motion Out.Micro])
                )
              $ molIDsAtSlice

          -- Auto-generated output for the full ONIOM tree.
          molInfo =
            renderBuilder . spicyLog microStepPrintEnv $
              spicyLogMol
                (HashSet.fromList [Always, Out.Motion Out.Micro, Out.Motion Out.Macro, FullTraversal])
                All

      -- Write information about the molecule to the output file.
      printSpicy $
        "\n\n"
          <> "@ Geometry Convergence\n\
             \----------------------\n"
          <> optTableHeader ((snd . microCycle $ motion) == 0 && depth == 0)
          <> optTableLine (snd . microCycle $ motion) (Just depth) geomChange
          <> "\n\n"
          <> if depth == maxDepth
            then molInfo
            else layerInfos

    ------------------------------------------------------------------------------------------------
    untilConvergence ::
      ( HasMolecule env,
        HasLogFunc env,
        HasCalcSlot env,
        HasProcessContext env,
        HasMotion env,
        HasOutputter env
      ) =>
      Int ->
      Seq MicroOptSetup ->
      RIO env ()
    untilConvergence depth microOptSettings = do
      -- Initial information.
      microSettingsAtDepth <- microOptSettings !?? depth
      let geomConvCriteria = microSettingsAtDepth ^. #geomConv
          ipiStatusVar = microSettingsAtDepth ^. #pysisIPI % #status

      -- Follow the requests of the i-PI server.
      ipiServerWants <- atomically . takeTMVar $ ipiStatusVar
      case ipiServerWants of
        -- Terminate the optimisation loop in case the i-PI server signals convergence.
        Done ->
          logWarnS logSMicroD $
            "(slice " <> display depth <> ") i-PI server signaled an exit. It should never terminate by itself ..."
        -- Provide new positions to the i-PI server.
        WantPos -> coordinateSync depth microOptSettings >> untilConvergence depth microOptSettings
        -- Micro-cycle optimisation cannot use Hessian information at the moment, as I haven't
        -- thought about sub-tree hessian updates, yet.
        WantHessian -> do
          logErrorS
            logSMicroD
            "The i-PI server wants a hessian update, but hessian updates subtrees are not implemented yet."
          throwM $ SpicyIndirectionException "geomMicroDriver" "Hessian updates not implemented yet."

        -- Normal i-PI update, where the client communicates current forces to the the server.
        WantForces -> do
          molPreStep <- view moleculeL >>= readTVarIO

          -- Apply the step as obtained from Pysisyphus.
          updateGeomFromPysis depth microOptSettings

          -- Recalculate the gradients above. The position update invalidates the old ones.
          recalcGradsAbove depth

          -- Optimise the layers above.
          logDebugS logSMicroD $ "(slice " <> display depth <> ") Recursively entering micro-cycles in slice above."
          optAtDepth (depth - 1) microOptSettings
          logDebugS logSMicroD $ "(slice " <> display depth <> ") Finished with micro-cycles in slices above."

          -- Calculate forces in the new geometry for the current layer only.
          recalcGradsAtDepth depth

          -- Molecule with valid gradients up to this depth after the step.
          molPostStep <- view moleculeL >>= readTVarIO

          -- Construct force data, that we send to i-PI for this slice and send it.
          -- A collector up to the current depth will collect all force data and transform the
          -- gradients of all layers up to current depth into the real system gradient, which is
          -- then completely sent to Pysisyphus.
          provideForces depth microOptSettings

          -- Calculate geometry convergence and update the motion environment.
          (geomChange, newMotion) <- calcConvergence depth microOptSettings molPreStep molPostStep

          -- Logging post step.
          logOptStep depth geomChange newMotion

          -- Decide if to do more iterations.
          if geomChange < geomConvCriteria
            then return ()
            else untilConvergence depth microOptSettings

----------------------------------------------------------------------------------------------------

-- | Per slice settings for the optimisation.
data MicroOptSetup = MicroOptSetup
  { -- | Moving atoms for this slice
    atomsAtDepth :: !IntSet,
    -- | The i-PI client thread. To be killed in the end.
    ipiClientThread :: Async (),
    -- | Pysisyphus i-PI server thread. To be gracefully terminated when done.
    ipiServerThread :: Async (),
    -- | IPI communication and process settings.
    pysisIPI :: !IPI,
    -- | Geometry convergence.
    geomConv :: GeomConv
  }
  deriving (Generic)

----------------------------------------------------------------------------------------------------

-- | Create one Pysisyphus i-PI instance per layer that takes care of the optimisations steps at a
-- given horizontal slice. It returns relevant optimisation settings for each layer to be used by
-- other functions, that actually do the optimisation.
setupPysisServers ::
  ( HasInputFile env,
    HasLogFunc env,
    HasProcessContext env,
    HasWrapperConfigs env
  ) =>
  Molecule ->
  RIO env (Seq MicroOptSetup)
setupPysisServers mol = do
  -- Get directories to work in from the input file.
  inputFile <- view inputFileL
  scratchDirAbs <- liftIO . Path.dynamicMakeAbsoluteFromCwd . getDirPath $ inputFile ^. #scratch

  -- Make per slice information.
  let molSlices = horizontalSlices mol
      optAtomsSelAtDepth = getOptAtomsAtDepth mol <$> Seq.fromList [0 .. Seq.length molSlices]
      allAtoms = molFoldl (\acc m -> acc <> (m ^. #atoms)) mempty mol
      optAtomsAtDepth = fmap (IntMap.restrictKeys allAtoms) optAtomsSelAtDepth

  -- Get an IntMap of atoms for Pysisyphus. These are the atoms of the real layer. Freezes to only
  -- optimise a subsystem are employed later.
  let allRealAtoms = mol ^. #atoms

  fstMolsAtDepth <-
    maybe2MThrow (localExc "a slice of the molecule seems to be empty") $
      traverse (Seq.!? 0) molSlices
  optSettingsAtDepthRaw <-
    maybe2MThrow (localExc "an original calculation context on some level is missing") $
      forM fstMolsAtDepth (^? #calculations % _Just % #original % #input % #optimisation)
  let optSettingsAtDepth =
        Seq.mapWithIndex
          ( \i opt ->
              let freeAtomsAtThisDepth = IntMap.keysSet . fromMaybe mempty $ optAtomsAtDepth Seq.!? i
                  frozenAtomsAtThisDepth = IntMap.keysSet $ allRealAtoms `IntMap.withoutKeys` freeAtomsAtThisDepth
               in opt
                    & #pysisyphus % #socketAddr .~ SockAddrUnix (mkScktPath scratchDirAbs i)
                    & #pysisyphus % #workDir .~ Path.toAbsRel (mkPysisWorkDir scratchDirAbs i)
                    & #pysisyphus % #initCoords .~ (mkPysisWorkDir scratchDirAbs i </> Path.relFile "InitCoords.xyz")
                    & #pysisyphus % #oniomDepth ?~ i
                    & #freezes %~ (<> frozenAtomsAtThisDepth)
          )
          optSettingsAtDepthRaw
  threadsAtDepth <- mapM (providePysisAbstract True allRealAtoms) optSettingsAtDepth

  return $
    Seq.zipWith3
      ( \a os (st, ct) ->
          MicroOptSetup
            { atomsAtDepth = IntMap.keysSet a,
              ipiClientThread = ct,
              ipiServerThread = st,
              pysisIPI = os ^. #pysisyphus,
              geomConv = os ^. #convergence
            }
      )
      optAtomsAtDepth
      optSettingsAtDepth
      threadsAtDepth
  where
    localExc = MolLogicException "setupPsysisServers"
    mkScktPath sd i = Path.toString $ sd </> Path.relFile ("pysis_slice_" <> show i <> ".socket")
    mkPysisWorkDir pd i = pd </> Path.relDir ("pysis_slice" <> show i)

{-
====================================================================================================
-}

-- | Cleans all outputs from the molecule.
cleanOutputs :: TVar Molecule -> STM ()
cleanOutputs molT = do
  mol <- readTVar molT
  let molWithEmptyOutputs =
        molMap
          ( \m ->
              m
                & #calculations % _Just % #original % #output .~ def
                & #calculations % _Just % #original % #gmbe % _Just % each %~ second (const Nothing)
                & #calculations % _Just % #inherited % _Just % #output .~ def
                & #calculations % _Just % #inherited % _Just % #gmbe % _Just % each %~ second (const Nothing)
          )
          mol
  writeTVar molT molWithEmptyOutputs

----------------------------------------------------------------------------------------------------

-- | Cleans the output of a single calculation.
cleanOutputOfCalc :: TVar Molecule -> CalcID -> STM ()
cleanOutputOfCalc molT calcID = do
  mol <- readTVar molT
  writeTVar molT $
    mol
      & calcLens % _Just % #output .~ def
      & calcLens % _Just % #gmbe % _Just % each %~ second (const Nothing)
  where
    calcLens = calcIDLensGen calcID

----------------------------------------------------------------------------------------------------

-- | Assigns the given task to each calculation in the molecule.
assignTask :: TVar Molecule -> CalcID -> WrapperTask -> STM ()
assignTask molT calcID task = do
  mol <- readTVar molT
  writeTVar molT $ mol & calcLens % _Just % #input % #task .~ task
  where
    calcLens = calcIDLensGen calcID

----------------------------------------------------------------------------------------------------

-- | Assigns the given task to the given calculation id.
assignTaskToCalc :: TVar Molecule -> CalcID -> WrapperTask -> STM ()
assignTaskToCalc molT calcID task = do
  mol <- readTVar molT
  let molWithTasks =
        molMap
          (& calcLens % _Just % #input % #task .~ task)
          mol
  writeTVar molT molWithTasks
  where
    calcLens = calcIDLensGen calcID
