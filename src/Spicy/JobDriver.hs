-- |
-- Module      : Spicy.JobDriver
-- Description : Combination of steps to full ONIOM jobs
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module implements the top level logic of Spicy, after the runtime environment has been
-- initialised. It roughly has the following tasks:
--
--   - launch most of the companion threads (optimisers, calculators, ...)
--   - prepare/manipulate molecular topology
--   - create the ONIOM tree by an anamorphism (via 'Spicy.ONIOM.Layout.mcOniomNLayout')
--   - perform all tasks with the molecule (optimisation, single points, MD, ...) (calling mostly
--     into "Spicy.ONIOM.Driver")
module Spicy.JobDriver
  ( spicyExecMain,
  )
where

import qualified Data.IntMap as IntMap
import Data.List.Split
import qualified Data.Map as Map
import Data.Yaml.Pretty
import RIO.ByteString (hPutStr)
import qualified RIO.HashMap as HashMap
import qualified RIO.HashSet as HashSet
import RIO.Process
import Spicy.Common
import Spicy.Data
import Spicy.FragmentMethods.SMF
import Spicy.InputFile
import Spicy.Molecule.Computational
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Internal.Writer
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical (BondOrder (..))
import Spicy.ONIOM.Collector
import Spicy.ONIOM.Driver
import Spicy.ONIOM.Layout
import Spicy.Outputter hiding (Macro, Micro)
import Spicy.Prelude
import Spicy.RuntimeEnv
import Spicy.Wrapper
import System.Path ((</>))
import qualified System.Path as Path

logSource :: LogSource
logSource = "JobDriver"

----------------------------------------------------------------------------------------------------
spicyExecMain ::
  ( HasMolecule env,
    HasInputFile env,
    HasLogFunc env,
    HasWrapperConfigs env,
    HasProcessContext env,
    HasCalcSlot env,
    HasOutputter env,
    HasMotion env
  ) =>
  RIO env ()
spicyExecMain = do
  -- Starting the output log.
  hPutStr stderr spicyLogoColour
  inputFile <- view inputFileL
  inputPrintEnv <- getCurrPrintEnv
  printSpicy $
    spicyLogo
      <> ("Spicy Version " <> versionInfo <> "\n\n\n")
      <> txtInput
      <> sep
      <> (displayBytesUtf8 . encodePretty defConfig $ inputFile)
      <> sep
      <> ( renderBuilder . spicyLog inputPrintEnv $ do
             printCoords ONIOM
             printTopology ONIOM
         )
  -- Writing the ONIOM tree to a file
  let initLayoutFile = getDirPath (inputFile ^. #permanent) </> Path.relFile "Input.mol2"
  logInfoS logSource $ "Writing input structure as parsed to: " <> displayShow (Path.toString initLayoutFile)
  writeFileUTF8
    (getDirPath (inputFile ^. #permanent) </> Path.relFile "Input.mol2")
    =<< writeONIOM (inputPrintEnv ^. #mol)

  -- Start the companion thread for calculations.
  calcSlotThread <- async provideCalcSlot
  link calcSlotThread

  -- Building an inital neighbourlist for large distances.
  logInfoS logSource "Constructing initial neighbour list for molecule ..."
  initNeighbourList

  -- Apply topology updates as given in the input file to the molecule.
  logInfoS logSource "Applying changes to the input topology ..."
  changeTopologyOfMolecule

  -- The molecule as loaded from the input file must be layouted to fit the current calculation
  -- type.
  logInfoS logSource "Preparing layout for a MC-ONIOMn calculation ..."
  layoutMoleculeForCalc

  -- setupPhase printing. Show the layouted molecules and topologies in hierarchical order.
  setupPrintEnv <- getCurrPrintEnv
  let molIDHierarchy = getAllMolIDsHierarchically $ setupPrintEnv ^. #mol
  printSpicy $
    txtSetup
      <> ( renderBuilder . spicyLog setupPrintEnv . forM_ molIDHierarchy $ \i -> do
             printCoords (Layer i)
             printTopology (Layer i)
         )
  writeFileUTF8
    (getDirPath (inputFile ^. #permanent) </> Path.relFile "Setup.mol2")
    =<< writeONIOM (setupPrintEnv ^. #mol)

  -- Perform the specified tasks on the input file.
  tasks <- view $ inputFileL % #task
  forM_ tasks $ \t -> do
    case t of
      Energy -> do
        -- Logging.
        energyStartPrintEnv <- getCurrPrintEnv
        printSpicy txtSinglePoint
        printSpicy . renderBuilder . spicyLog energyStartPrintEnv $
          spicyLogMol (HashSet.fromList [Always, Task Start]) All

        -- Actual calculation
        multicentreOniomNDriver WTEnergy *> multicentreOniomNCollector

        -- Final logging
        energyEndPrintEnv <- getCurrPrintEnv
        printSpicy . renderBuilder . spicyLog energyEndPrintEnv $
          spicyLogMol (HashSet.fromList [Always, Task End, FullTraversal]) All
      Optimise Macro -> geomMacroDriver
      Optimise Micro -> geomMicroDriver
      Frequency -> do
        -- Logging.
        hessStartPrintEnv <- getCurrPrintEnv
        printSpicy txtSinglePoint
        printSpicy . renderBuilder . spicyLog hessStartPrintEnv $
          spicyLogMol (HashSet.fromList [Always, Task Start]) All

        -- Actual calculation
        multicentreOniomNDriver WTHessian *> multicentreOniomNCollector

        -- Final logging.
        hessEndPrintEnv <- getCurrPrintEnv
        printSpicy txtSinglePoint
        printSpicy . renderBuilder . spicyLog hessEndPrintEnv $
          spicyLogMol (HashSet.fromList [Always, Task End, FullTraversal]) All
      MD -> do
        logErrorS logSource "A MD run was requested but MD is not implemented yet."
        throwM $ SpicyIndirectionException "spicyExecMain" "MD is not implemented yet."

  -- Final logging.
  finalPrintEnv <- getCurrPrintEnv
  printSpicy . renderBuilder . spicyLog finalPrintEnv $
    spicyLogMol (HashSet.fromList [Spicy End, Always]) All
  writeFileUTF8
    (getDirPath (inputFile ^. #permanent) </> Path.relFile "Final.mol2")
    =<< writeONIOM (finalPrintEnv ^. #mol)
  printSpicy $ sep <> "Spicy execution finished. May the sheep be with you and your results!"

  -- Kill the companion threads after we are done.
  cancel calcSlotThread

  -- Ensure the logging queue is empty and give some additional time to empty the log.
  logQueue <- view (outputterL % #outChan)
  atomically $ isEmptyTBQueue logQueue >>= checkSTM

  -- LOG
  logInfoS logSource "Spicy execution finished. May the sheep be with you and your results!"

----------------------------------------------------------------------------------------------------

-- | Construct an initial neighbour list with large distances for all atoms. Can be reduced to
-- smaller values efficiently.
initNeighbourList :: (HasMolecule env) => RIO env ()
initNeighbourList = do
  molT <- view moleculeL
  mol <- readTVarIO molT
  nL <- neighbourList 15 (mol ^. #atoms)
  atomically . writeTVar molT $ mol & #neighbourlist .~ Map.singleton 15 nL

----------------------------------------------------------------------------------------------------

-- | Application of the topology updates, that were requested in the input file. The order of
-- application is:
--
--   1. Guess a new bond matrix if requested.
--   2. Remove bonds between pairs of atoms.
--   3. Add bonds between pairs of atoms.
--   4. Add formal charges to atoms and their corresponding groups.
changeTopologyOfMolecule ::
  (HasInputFile env, HasMolecule env, HasLogFunc env) => RIO env ()
changeTopologyOfMolecule = do
  -- Get the molecule to manipulate
  molT <- view moleculeL
  mol <- readTVarIO molT

  -- Get the input file information.
  inputFile <- view inputFileL

  case inputFile ^. #topology of
    Nothing -> return ()
    Just topoChanges -> do
      -- Resolve the defaults to final values.
      let singleCovTol = fromMaybe defCovAddSingle $ topoChanges ^. #singleTol
          newBondMat = topoChanges ^. #guessBonds
          multipleCovTol = fromMaybe defCovAddMultiple $ topoChanges ^. #multipleTol
          covTolerances = (singleCovTol, multipleCovTol)
          removalPairs = fromMaybe [] $ topoChanges ^. #bondsToRemove
          additionPairs = fromMaybe [] $ topoChanges ^. #bondsToAdd
          assignedCharges = fromMaybe [] $ topoChanges ^. #formalCharges
          assignedExcEl = fromMaybe [] $ topoChanges ^. #formalExcEl
          regroupingAlg = topoChanges ^. #regroup

      -- Show what will be done to the topology:
      logInfoS logSource $
        "Guessing new bonds (single bond tolerance, multi-bond tolerance): "
          <> if newBondMat
            then displayShow covTolerances
            else "No"
      logInfoS logSource $ "Removing bonds between atom pairs: " <> displayShow (chunksOf 5 removalPairs)
      logInfoS logSource $ "Adding bonds between atom pairs: " <> displayShow (chunksOf 5 additionPairs)
      logInfoS logSource $ "Regrouping atoms with algorithm: " <> displayShow regroupingAlg
      logInfoS logSource $ "Assigning additional formal charges to atoms: " <> displayShow assignedCharges
      logInfoS logSource $ "Assigning additional formal excess electrons to atoms: " <> displayShow assignedExcEl
      case (regroupingAlg, newBondMat) of
        (Just SMF, False) ->
          logWarnS
            logSource
            "New groups are formed by the SMF algorithm but the bond matrix is not constructed by SMF rules.\
            \ This will in nearly all cases give wrong results."
        _ -> return ()

      -- Apply changes to the topology
      let bondMatrixFromInput = mol ^. #bonds
      unless (HashMap.null bondMatrixFromInput) $
        logWarnS
          logSource
          "The input file format contains topology information such as bonds\
          \ but manipulations were requested anyway."

      -- Completely guess new bonds if requested.
      molNewBondsGuess <-
        if newBondMat
          then do
            bondMatrix <- guessBondMatrix (Just covTolerances) (mol ^. #atoms) (mol ^. #neighbourlist)
            return $ mol & #bonds .~ bondMatrix
          else return mol

      -- Remove all bonds between atom pairs requested.
      molNewBondsRemoved <-
        foldl'
          ( \molAcc' atomPair -> do
              molAcc <- molAcc'
              molAccNew <- changeBond None molAcc atomPair
              return molAccNew
          )
          (return molNewBondsGuess)
          removalPairs

      -- Add all bonds between atom pairs requested.
      molNewBondsAdded <-
        foldl'
          ( \molAcc' (o, t, bo) -> do
              molAcc <- molAcc'
              molAccNew <- changeBond bo molAcc (o, t)
              return molAccNew
          )
          (return molNewBondsRemoved)
          additionPairs

      -- Regrouping of the atoms if requested.
      molRegrouped <- case regroupingAlg of
        Nothing -> return molNewBondsAdded
        Just SMF -> do
          let smfGroups = makeSMFgroups . mol2Graph $ molNewBondsAdded
          return $ molNewBondsAdded & #group .~ smfGroups
        Just Detached -> do
          groupSets <- groupDetectionDetached molNewBondsAdded
          let atoms = molNewBondsAdded ^. #atoms
              groups = fmap (groupFromAtomKeys . IntMap.restrictKeys atoms) groupSets
          return $ molNewBondsAdded & #group .~ groups

      -- Assign the charges and excess electrons to the atoms. To keep group electronic structure
      -- consistent, also recalculate the affected groups.
      molNewElectronic <- do
        let atoms = molRegrouped ^. #atoms
            newAtomCharges =
              foldl'
                (\atomAcc (aIx, aC) -> atomAcc & ix aIx % #formalCharge .~ aC)
                atoms
                assignedCharges
            newAtomExcEl =
              foldl'
                (\atomAcc (aIx, aEl) -> atomAcc & ix aIx % #formalExcEl .~ aEl)
                newAtomCharges
                assignedExcEl
            oldGroups = molRegrouped ^. #group
            combineF oG nG =
              nG
                & #label .~ (oG ^. #label)
                & #chain .~ (oG ^. #chain)
            newGroups = fmap (groupFromAtomKeys . IntMap.restrictKeys newAtomExcEl) (IntMap.map (^. #atoms) oldGroups)
        let finalGroups = IntMap.unionWith combineF oldGroups newGroups
            newMol =
              molRegrouped & #atoms .~ newAtomExcEl
                & #group .~ finalGroups
        return newMol

      -- The molecule after all changes to the topology have been applied.
      atomically . writeTVar molT $ molNewElectronic

----------------------------------------------------------------------------------------------------

-- | Perform transformation of the molecule data structure as obtained from the input to match the
-- requirements for the requested calculation type.
layoutMoleculeForCalc ::
  (HasInputFile env, HasMolecule env) => RIO env ()
layoutMoleculeForCalc = do
  inputFile <- view inputFileL
  case inputFile ^. #model of
    ONIOMn {} -> mcOniomNLayout
