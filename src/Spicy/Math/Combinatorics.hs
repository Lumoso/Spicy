-- |
-- Module      : Spicy.Math.Combinatorics
-- Description : Functions related to combinatorial problems
-- Copyright   : Phillip Seeber
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
module Spicy.Math.Combinatorics
  ( nCombinations,
    intNcombinations,
  )
where

import qualified Data.IntSet as IntSet
import qualified RIO.Set as Set
import Spicy.Prelude as P

-- | Function that builds up to the \(n\)-combinations of a given starting 'Set' of elements. The
-- result we be a trifold nested list.
--
--   * __outermost__: combinations of order \([n \dots 1]\). Therefore, the \(n\)-combinations come
--     first, the \((n-1)\)-combinations second, and the the 1-combinations (the original elements
--     of the set) come last.
--   * __middle__: a set of all combinations of a given order
--   * __inner__: one of the combinations at a given order
--
-- >>> nCombinations 3 (Set.fromList [1..5])
-- [[[1,2,3],[1,2,4],[1,2,5],[1,3,4],[1,3,5],[1,4,5],[2,3,4],[2,3,5],[2,4,5],[3,4,5]],[[1,2],[1,3],[1,4],[1,5],[2,3],[2,4],[2,5],[3,4],[3,5],[4,5]],[[1],[2],[3],[4],[5]]]
nCombinations :: Ord a => Natural -> Set a -> [[[a]]]
nCombinations n vals' = go 0 []
  where
    vals = Set.toAscList vals'

    go 0 _ = go 1 [fmap pure vals]
    go _ [] = go 0 []
    go m p@(x : _) =
      if m >= n
        then p
        else go (m + 1) $ combine x : p

    combine prev = [a : c | a <- vals, c@(x : _) <- prev, a < x]

----------------------------------------------------------------------------------------------------

-- | Same as 'nCombinations' but specialised for 'IntSet' and 'Int'.
intNcombinations :: Natural -> IntSet -> [[IntSet]]
intNcombinations n vals' = fmap IntSet.fromAscList <$> go 0 []
  where
    vals = IntSet.toAscList vals'

    go 0 _ = go 1 [fmap pure vals]
    go _ [] = go 0 []
    go m p@(x : _) =
      if m >= n
        then p
        else go (m + 1) $ combine x : p

    combine prev = [a : c | a <- vals, c@(x : _) <- prev, a < x]
