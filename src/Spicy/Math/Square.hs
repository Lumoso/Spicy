-- |
-- Module      : Spicy.Math.Square
-- Description : The Square type for typesafe squareity
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- A Type to encode a "squareness" property of a matrix, together with smart constructors that
-- guarantee that property.
module Spicy.Math.Square
  ( Square,
    getSquare,
    squareMatrix,
    squareMatrixS,
    squareMatrixG,
    squareSz,
  )
where

import Data.Massiv.Array
import Spicy.Prelude

-- | Newtype wrapper serving as a guarantee that the wrapped value is square. The 'UnsafeSquare'
-- constructor is not meant to be exported. Values of type @'Square' a@ are constructed by dedicated
-- safe functions.
newtype Square a = UnsafeSquare {getSquare :: a}
  deriving (Functor)

----------------------------------------------------------------------------------------------------

-- | Guarantee that a 'Matrix' is square. Will throw a 'DataStructureException' if it is not.
squareMatrix :: (Size r, MonadThrow m) => Matrix r e -> m (Square (Matrix r e))
squareMatrix matrix =
  let Sz (n :. m) = size matrix
   in if n == m
        then return . UnsafeSquare $ matrix
        else throwM $ DataStructureException "squareMatrix" "Matrix not square!"

-- | Guarantee that a 'MatrixS' is square.
-- Will throw a 'DataStructureException' if it is not.
squareMatrixS :: (MonadThrow m) => MatrixS e -> m (Square (MatrixS e))
squareMatrixS matrix = do
  let matrix' = getMatrixS matrix
  square <- squareMatrix matrix'
  return $ MatrixS <$> square

-- | Guarantee that a 'MatrixG' is square. Will throw a 'DataStructureException' if it is not.
squareMatrixG :: (Size r, MonadThrow m) => MatrixG r e -> m (Square (MatrixG r e))
squareMatrixG matrix = do
  let matrix' = getMatrixG matrix
  square <- squareMatrix matrix'
  return $ MatrixG <$> square

-- | Guarantee that a 2-dimensional size is square. Will throw a 'DataStructureException' if it is
-- not.
squareSz :: MonadThrow m => Sz Ix2 -> m (Square (Sz Ix2))
squareSz sz@(Sz (n :. m)) =
  if n == m
    then return . UnsafeSquare $ sz
    else throwM $ DataStructureException "squareSz" "Size not square!"
