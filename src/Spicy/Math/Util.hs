-- |
-- Module      : Spicy.Math
-- Description : Basic mathematical operations
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module defines basic algebraic operations used throughout the program. Numerical heavy and
-- most other operations are implemented using Accelerate, to provide parallel operations. Note that
-- all provided functions must be typed without typeclasses but by concrete types.
--
-- The operations here accept some insecurities (like not checking if both vectors of a dot product
-- have equal lenght) and trust the caller.
module Spicy.Math.Util
  ( -- * Utilities
    -- $utilities
    dFac,
    dFac',

    -- * Linear Algebra
    -- $linear
    -- $linearVector
    distance,
    magnitude,
    angle,
    cross3,

    -- * Conversion

    {-
    -- ** Massiv-HMatrix
    vecM2H,
    vecH2M,
    matM2H,
    matH2M,
    -}

    -- ** Matrix-Vector
    fromLT,
    toLT,
    fromLinear,
    toLinear,

    -- * Metrics
    rms,
  )
where

import Data.Massiv.Array as Massiv
import Data.Massiv.Core.Operations ()
import Math.NumberTheory.Roots
import Spicy.Math.Square
import Spicy.Prelude as S

newtype MathException = MathException String deriving (Show)

instance Exception MathException

-- $utilities
-- Various useful mathematical functions.

-- | Double factorial function: Product of all integers up to the argument that have the same parity
-- (odd or even). The double factorial is mathematically defined for negative arguments but can
-- produce non-integer results in that domain.
dFac :: (Fractional a) => Int -> a
dFac i
  | i < 0 = dFac (i + 2) / fromIntegral (i + 2)
  | otherwise = fromIntegral $ dFac' i

-- | Unsafe version of 'dFac', which is only defined for positive integrals. In this domain, results
-- are always integer numbers (which is not the case for negative arguments!). A special exception
-- is made for dFac (-1), which evaluates to 1 and occurs in the normalisation of basis functions.
dFac' :: Integral a => a -> a
dFac' i
  | i == (-1) = 1
  | i < 0 = impureThrow $ DataStructureException "dFac\'" "Negative argument!"
  | even i = S.product [0, 2 .. i]
  | otherwise = S.product [1, 3 .. i]
{-# SPECIALIZE dFac' :: Int -> Int #-}

{-
####################################################################################################
-}

-- $linear
-- Implements useful linear algebra functions.

{-
====================================================================================================
-}

-- $linearVector

----------------------------------------------------------------------------------------------------

-- | Calculate the distance between two vectors of same size.
distance :: (Source r a, NumericFloat r a, MonadThrow m) => Vector r a -> Vector r a -> m a
distance a b = do
  diffVec <- a .-. b
  let dist = sqrt . Massiv.sum . Massiv.map (** 2) $ diffVec
  return dist

----------------------------------------------------------------------------------------------------

-- | The magnitude/length of a vector.
magnitude :: (Floating e, Source r e) => Vector r e -> e
magnitude a = sqrt . Massiv.sum . Massiv.map (^ (2 :: Int)) $ a

----------------------------------------------------------------------------------------------------

-- | Numerically stable angle between two vectors, see <https://www.jwwalker.com/pages/angle-between-vectors.html>
-- \[
--   \alpha = 2 \operatorname{atan2} (\lVert \lVert \mathbf{v} \rVert \mathbf{u} - \lVert \mathbf{u} \rVert v \rVert,
--     \lVert \lVert \mathbf{v} \rVert \mathbf{u} + \lVert \mathbf{u} \rVert v \rVert )
-- \]
angle :: (RealFloat e, Numeric r e, Source r e, MonadThrow m) => Vector r e -> Vector r e -> m e
angle u v = do
  let magU = magnitude u
      magV = magnitude v
      magVxU = magV *. u
      magUxV = magU *. v
  l <- magVxU .-. magUxV
  r <- magVxU .+. magUxV
  return . (2 *) $ atan2 (magnitude l) (magnitude r)

----------------------------------------------------------------------------------------------------

-- | Cross product of two 3-dimensional vectors.
cross3 ::
  (Numeric r1 e, Manifest r1 e, Mutable r2 e, MonadThrow m) =>
  Vector r1 e ->
  Vector r1 e ->
  m (Vector r2 e)
cross3 a b = do
  let Sz nA = size a
      Sz nB = size b

  unless (nA == 3 && nB == 3) . throwM . MathException $ "Input vectors must both be of size 3"

  a1 <- a !? 0
  a2 <- a !? 1
  a3 <- a !? 2

  b1 <- b !? 0
  b2 <- b !? 1
  b3 <- b !? 2

  let c1 = a2 * b3 - a3 * b2
      c2 = a3 * b1 - a1 * b3
      c3 = a1 * b2 - a2 * b1
  return . Massiv.fromList Seq $ [c1, c2, c3]

{-
====================================================================================================
-}

-- | Convert a lower triangular matrix represented as a 'VS.Vector' in row major order (columns
-- index changes first) back to the square matrix. The main diagonal is supposed to be included. The
-- function fails if the number of elements in the vector is not suitable to represent the lower
-- triangular part of a square matrix.
--
-- Given a square matrix of dimension \(n \times n\), the number of elements of its lower triangular
-- part (including main diagonal) would be:
--
-- \[ n_\mathrm{t} = \frac{n^2 + n}{2} \]
--
-- Solving this equation for \( n \) with a given \( n_t \) gives:
--
-- \[ n_{1,2} = \frac{1}{2} \left( \pm \sqrt{8 n_t + 1} - 1 \right) \]
--
-- for which only the positive solution is meaningful for us. The index of the the \(n \times n\)
-- matrix \( \mathbf{S}_{ij} \) (with \( i \) being the row index and  \( j \) being the column index)
-- can be linearised to map in the lower triangular matrix, which is represented as a vector
-- \( \mathbf{T}_{k} \) as:
-- \[
-- k = \begin{cases}
--   \sum \limits_{m = 0}^i m + j, & i \geq j \\
--   \sum \limits_{m = 0}^j m + i, & \text{otherwise}
-- \end{cases}
-- \]
--
-- This index transformation can be used to reexpand the lower triangular matrix in vector form in row
-- major order back to the full symmetric square matrix.
fromLT :: (MonadThrow m, Source r e) => Vector r e -> m (Matrix D e)
fromLT lt = do
  sz <- maybe2MThrow (localExc "lower triangular matrix has invalid number of elements!") $ do
    root <- exactSquareRoot (8 * elemsCount lt + 1)
    return $ (root - 1) `div` 2
  return $ backpermute' (Sz $ sz :. sz) lT2LinearIndex lt
  where
    localExc = DataStructureException "fromLT"

-- TODO - I think this is the small gauss. There should be a non-iterative version for it
lT2LinearIndex :: Ix2 -> Ix1
lT2LinearIndex (ixR :. ixC) = case ixR `compare` ixC of
  LT -> S.sum [0 .. ixC] + ixR
  _ -> S.sum [0 .. ixR] + ixC

-- | Transform a symmetric matrix into a lower diagonal matrix, represented as a linear vector in
-- row major order. The main diagonal will be included. The precondition (symmetry) is not checked!
toLT :: (MonadThrow m, Source r e) => Matrix r e -> m (Vector D e)
toLT mat = do
  let Sz (n :. m) = size mat
  sz <- maybe2MThrow (localExc "matrix is not square!") $ do
    guard (n == m)
    return . Sz $ (n ^ (2 :: Int) + n) `div` 2
  let ixs = indexVec @B $ unSz sz
  return $ backpermute' sz (ixs !) mat
  where
    localExc = DataStructureException "toLT"

-- | Vector mapping the first l one-dimensional indices to two-dimensional lower triangular indices.
indexVec :: Mutable r Ix2 => Int -> Vector r Ix2
indexVec l = fromList Par $ S.take l [i :. j | i <- [0 :: Int ..], j <- [0 .. i]]

-- | Transform a vector with \(n^2\) elements into a \(n \times n\) Matrix in row major order.
-- Will throw a 'DataStructureException' if the number of elements in the vector is not a square
-- number.
fromLinear :: (MonadThrow m, Source r e) => Vector r e -> m (Square (Matrix D e))
fromLinear v = do
  let Sz1 nSquared = size v
  n <-
    maybe2MThrow
      (DataStructureException "fromLinear" "Vector has non-quadratic number of elements")
      (exactSquareRoot nSquared)
  squareMatrix $ backpermute' (Sz2 n n) (\(i :. j) -> n * i + j) v

-- | Inverse of fromLinear. Synonym for 'Data.Massiv.Array.flatten', specialized to 'Square'
-- matrices.
toLinear :: Size r => Square (Matrix r e) -> Vector r e
toLinear = flatten . getSquare

{-
====================================================================================================
-}

-- | Residual mean square value of an array.
rms :: (Floating e, Stream r ix e) => Array r ix e -> e
rms v =
  let Sz1 n = Massiv.linearSize v
   in sqrt . (/ fromIntegral n) . Massiv.ssum . Massiv.smap (** 2) $ v
