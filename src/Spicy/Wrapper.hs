-- |
-- Module      : Spicy.Wrapper
-- Description : Abstract routines to perform calculations with external software.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Abstract methods to perform computational chemistry tasks by calling external programs.
module Spicy.Wrapper
  ( provideCalcSlot,
  )
where

import RIO.Process
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Prelude
import Spicy.RuntimeEnv
import Spicy.Wrapper.Internal.Executor

logSource :: LogSource
logSource = "calc provider"

-- | Loop that provides the companion thread, which executes the wrapper calculations on demand,
-- when feed into the 'Spicy.RuntimeEnv.CalcSlot'.
provideCalcSlot ::
  ( HasLogFunc env,
    HasCalcSlot env,
    HasProcessContext env,
    HasWrapperConfigs env
  ) =>
  RIO env ()
provideCalcSlot = do
  -- Obtain initial information.
  calcSlotIn <- view $ calcSlotL % #input
  calcSlotOut <- view $ calcSlotL % #output

  -- Run the main loop
  forever $ do
    -- LOG
    logDebugS logSource "Waiting for calculation ..."

    -- Wait for a calculation to perform.
    (layer, calcInput, calcID@CalcID {molID, calcK}) <- atomically . readTQueue $ calcSlotIn

    -- LOG
    logDebugS logSource $
      "Got calculation:\n"
        <> ("  Layer: " <> (display . molID2OniomHumanID $ molID))
        <> ( "  Type : " <> case calcK of
               Original -> "high level calculation"
               Inherited -> "low level calculation"
           )

    -- Perform the given calculation.
    calcOutput <- runCalculation layer calcInput

    -- Mark calculation as ready to be consumed and return.
    atomically . writeTQueue calcSlotOut $ (calcID, calcOutput)
