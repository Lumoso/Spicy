-- |
-- Module      : Spicy.InputFile
-- Description : Definition of the input file
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module defines the structure of an input file for Spicy. The input file is the main source
-- for initialisation of the Spicy data structures, next to the molecule itself. In large parts it
-- directly uses the data structures, that are also used in the internal runtime environment
-- ('Spicy.RuntimeEnv.SpicyEnv') and therefore also 'Spicy.Molecule.Molecule.Molecule'.
-- The input file is (de)serialised as YAML and therefore structured hierarchically. The
-- serialisation conventions of the [aeson](https://hackage.haskell.org/package/aeson) and
-- [yaml](https://hackage.haskell.org/package/yaml) libraries apply.
-- All input options are documented here by construction, but for actual input examples refer to
-- @goldentests/input/calculations/@ and the Spicy manual.
module Spicy.InputFile
  ( InputFile (..),
    HasInputFile (..),
    Task (..),
    Style (..),
    InputMolecule (..),
    FileType (..),
    GroupingAlg (..),
    TopoChanges (..),
    Model (..),
    TheoryLayer (..),
    Execution (..),
    Opt (..),
    OptTarget (..),
  )
where

import Data.Aeson
import Spicy.Aeson
import Spicy.Common
import Spicy.Molecule.Computational (CoordType, Embedding, GeomConv, HessianUpdate, MicroStep, ProgramData)
import Spicy.Molecule.Fragment (FragmentMethods)
import Spicy.Molecule.Physical (BondOrder)
import Spicy.Outputter (Verbosity)
import Spicy.Prelude

-- | Definition of a complete calculation of arbitrary type.
data InputFile = InputFile
  { -- | A sequence of tasks to perform. They will be executed in
    --   order of the most current structure then. For example first
    --   'Optimise', then do a 'Frequency' calculation on the
    --   optimised structure and then run an 'MD' simulation.
    task :: Seq Task,
    -- | Molecular system input by an external file.
    molecule :: InputMolecule,
    -- | Changes in the topology.
    topology :: Maybe TopoChanges,
    -- | Calculation model that is being used (means a compound
    --   method, not a concrete type).
    model :: Model,
    -- | Fast directory used for scratch files of the wrapped
    --   software.
    scratch :: JDirPath,
    -- | Directory for permanent files.
    permanent :: JDirPath,
    -- | A print level for the output file.
    printLevel :: Maybe Verbosity
  }
  deriving (Eq, Show, Generic)

instance ToJSON InputFile where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON InputFile where
  parseJSON = genericParseJSON spicyJOption

-- Reader Class
class HasInputFile env where
  inputFileL :: Lens' env InputFile

instance HasInputFile InputFile where
  inputFileL = castOptic simple

----------------------------------------------------------------------------------------------------

-- | A task to perform with the molecule.
data Task
  = -- | Single point energy calculation. No change in structure
    Energy
  | -- | Optimisation of the molecular structure. Can do simple optimisations with macro iterarions
    -- only or advanced optimisations with micro iterations. Macroiterations are controlled by
    -- the optimisation settings on the real layer, optimisations with micro iterations are
    -- controlled by each layer individually.
    Optimise Style
  | -- | Frequency calculation with numerical or analytical hessian.
    Frequency
  | -- | Molecular dynamics simulation.
    MD
  deriving (Eq, Show, Generic)

instance ToJSON Task where
  toJSON Energy = toJSON @Text "energy"
  toJSON (Optimise Macro) = toJSON @Text "optimise_macro"
  toJSON (Optimise Micro) = toJSON @Text "optimise_micro"
  toJSON Frequency = toJSON @Text "frequency"
  toJSON MD = toJSON @Text "md"

instance FromJSON Task where
  parseJSON v =
    case v of
      String "energy" -> pure Energy
      String "frequency" -> pure Frequency
      String "md" -> pure MD
      String "optimise_macro" -> pure $ Optimise Macro
      String "optimise_micro" -> pure $ Optimise Micro
      o -> fail $ "encountered unknown field for task" <> show o

----------------------------------------------------------------------------------------------------

-- | Optimisation style. Either simple with macro iterations only or by using microiterations on
-- each layer.
data Style
  = Macro
  | Micro
  deriving (Eq, Show, Generic)

instance ToJSON Style where
  toJSON Macro = toJSON @Text "macro"
  toJSON Micro = toJSON @Text "micro"

instance FromJSON Style where
  parseJSON v = case v of
    String "macro" -> pure Macro
    String "micro" -> pure Micro
    _ -> fail "encountered unknown field for optimisation style"

----------------------------------------------------------------------------------------------------

-- | Specification of the input molecule, as read from a file, that represents a molecule in a
-- common file format.
data InputMolecule = InputMolecule
  { -- | Optionally a annotation of what file type is to be expected.
    fileType :: Maybe FileType,
    -- | A JSON enabled filepath to the file containing the overall
    --   system.
    path :: JFilePath
  }
  deriving (Eq, Show, Generic)

instance ToJSON InputMolecule where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON InputMolecule where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Filetypes for molecules, that can be read as input.
data FileType
  = -- | Molden XYZ file in angstroms.
    XYZ
  | -- | PDB as parsed by 'Spicy.Molecule.Internal.Parser.parsePDB'.
    PDB
  | -- | Sybyl MOL2 file as parsed by 'Spicy.Molecule.Internal.Parser.parseMOL2'.
    MOL2
  | -- | Tinker XYZ file as parsed by 'Spicy.Molecule.Internal.Parser.parseTXYZ'.
    TXYZ
  deriving (Eq, Show, Generic, Data, Typeable)

instance ToJSON FileType where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON FileType where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Different options for assigning atoms to groups.
data GroupingAlg
  = -- | The rules of systematic molecule fragmentation with additions for metals, see
    -- 'Spicy.FragmentMethods.SMF.makeSMFgroups'.
    SMF
  | -- | Detached groups of atoms will be considered separate fragments. E.g. a protein chain will
    -- be a single fragment, but every water molecule will get its own group.
    Detached
  deriving (Eq, Show, Generic)

instance ToJSON GroupingAlg where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON GroupingAlg where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Topology related tasks, such as bond definitions, assignements of electronic properties,
-- groupings of atoms, etc.
data TopoChanges = TopoChanges
  { -- | Ignore all old bonds and guess them from scratch with a distance criterion based on
    -- covalent radii. Makes only sense if the file did not provide bonds. This always uses the SMF
    -- bond criteria and will properly delocalise bonds. Basically a must have if SMF groups are to
    -- be used.
    guessBonds :: Bool,
    -- | Additional tolerance added to the sum of covalent radii, up to which atoms are considered
    -- to have a single bond ( \(d^\text{cov}_A + d^\text{cov}_B + d^\text{tol}\) ).
    singleTol :: Maybe Double,
    -- | Additional tolerance added to the sum of covalent radii, up to which atoms are considered
    -- to have multiple bonds ( \(d^\text{cov}_A + d^\text{cov}_B + d^\text{tol}\) ).
    multipleTol :: Maybe Double,
    -- | Pair of atoms, where any bond will be deleted.
    bondsToRemove :: Maybe [(Int, Int)],
    -- | Pairs of atoms between, between which a bond will be added.
    bondsToAdd :: Maybe [(Int, Int, BondOrder)],
    -- | Formal charges to assign to atoms. First number is the atom, second its formal charge.
    formalCharges :: Maybe [(Int, Int)],
    -- | Formal number of excess electrons to assign to an atom. First number is the atom, second
    -- its number of excess electrons (positive for alpha, negative for beta).
    formalExcEl :: Maybe [(Int, Int)],
    -- | Discard group information from the initial molecule and automatically regroup.
    regroup :: Maybe GroupingAlg
  }
  deriving (Eq, Show, Generic)

instance ToJSON TopoChanges where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON TopoChanges where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | A generic ONIOM-n calculation.
data Model = ONIOMn
  { -- | This defines the layer partitioning and levels of theory.
    --   This is a nested recursive data structure and the top
    --   theory layer can contain an arbitrary stack of deeper
    --   layers and on the same level multiple layers may exists, to
    --   describe multi-centre ONIOM.
    theoryLayer :: TheoryLayer
  }
  deriving (Eq, Show, Generic)

instance ToJSON Model where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Model where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Definition of a calculation for a computational chemistry program in a single layer.
data TheoryLayer = TheoryLayer
  { -- | Label of the layer, just for description.
    name :: Text,
    -- | The program to use for the computation.
    program :: ProgramData,
    -- | A selection made within the embedded language. See 'Spicy.SelectionLanguage' for details.
    selection :: Text,
    -- | Deeper layers of the calculation. If the list is empty, this is the highest level (with
    -- respect to computational cost) layer. Being a list, this structure allows to define
    -- multi-centre ONIOM.
    deeperLayer :: ![TheoryLayer],
    -- | Charge of the layer.
    charge :: Int,
    -- | Multiplicity of the layer.
    mult :: Int,
    -- | Information about the execution of the computational chemistry software, that is not
    -- relevant for chemical system description.
    execution :: Execution,
    -- | Defines the embedding type for the current layer.
    embedding :: Embedding,
    -- | Settings for the optimiser on a given layer.
    optimisation :: Maybe Opt,
    -- | Additional, arbitrary input string to be inserted into the input file (optional).
    additionalInput :: Maybe Text,
    -- | Whether and how to use fragment methods for this layer.
    fragments :: Maybe FragmentMethods
  }
  deriving (Eq, Show, Generic)

instance ToJSON TheoryLayer where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON TheoryLayer where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Information about how to execute a program, independent of system description.
data Execution = Execution
  { -- | Number of processes to launch (usually MPI).
    nProcesses :: Int,
    -- | Number of threads per (MPI) process to launch.
    nThreads :: Int,
    -- | The memory in MiB per process for the program.
    memory :: Int
  }
  deriving (Eq, Show, Generic)

instance ToJSON Execution where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Execution where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Settings for geometry optimisations. Either for macroiterations for the full system or for
-- a single layer if for microiterations. All values are optional and defaults for minima
-- optimisations on small and medium systems will be used.
data Opt = Opt
  { -- | Selects whether to optimise to a minimum or a saddle point.
    target :: Maybe OptTarget,
    -- | Coordinate system in which the optimisation is carried out.
    coords :: Maybe CoordType,
    -- | Maximum number of iterations. Only influences microiterations. Macroiterations are taken
    -- from somewhere else.
    iterations :: Maybe Int,
    -- | Option to recalculate the hessian every n steps. If not given, the hessian will never be
    -- recalculated.
    hessianRecalc :: Maybe Int,
    -- | Hessian update algorithm.
    hessianUpdate :: Maybe HessianUpdate,
    -- | Initial trust radius in optimisations.
    trust :: Maybe Double,
    -- | Maximum the trust radius can reach.
    trustMax :: Maybe Double,
    -- | Minimum the trust radius can reach.
    trustMin :: Maybe Double,
    -- | Convergence criteria.
    conv :: Maybe GeomConv,
    -- | Step type in micro-cycles
    microStep :: Maybe MicroStep
  }
  deriving (Eq, Show, Generic)

instance ToJSON Opt where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON Opt where
  parseJSON = genericParseJSON spicyJOption

----------------------------------------------------------------------------------------------------

-- | Optimise to a minimum or a transition state.
data OptTarget
  = Min
  | TS
  deriving (Eq, Show, Generic)

instance ToJSON OptTarget where
  toEncoding = genericToEncoding spicyJOption

instance FromJSON OptTarget where
  parseJSON = genericParseJSON spicyJOption
