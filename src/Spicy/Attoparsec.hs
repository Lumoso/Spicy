-- |
-- Module      : Spicy.Attoparsec
-- Description : Utility functions for parsing with attoparsec
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Utility functions around the attoparsec library.
module Spicy.Attoparsec
  ( -- * Parser Helper Functions
    parse',
    nextParse,
    lex,
    skipHorizontalSpace,
    getLine,
    skipLine,
    fortranDouble,
  )
where

import Data.Attoparsec.Text
import qualified RIO.Text as Text
import Spicy.Prelude hiding ((%))

-- | This is a wrapper around Attoparsec's 'parse' function. Contrary to 'parse', this function fails
-- with  an composable error type in 'MonadThrow'.
parse' :: MonadThrow m => Parser a -> Text -> m a
parse' p t = case parse p t of
  Done _ r -> return r
  Fail _ _ e -> throwM $ ParserException e
  Partial f -> case f "" of
    Done _ r -> return r
    Fail _ _ e -> throwM $ ParserException e
    Partial _ -> throwM $ ParserException "Obtained a Partial result twice. Cannot continue."

----------------------------------------------------------------------------------------------------

-- | Feed the result of one 'Parser', that obtains a text in the next 'Parser'.
nextParse :: Parser a -> Text -> Parser a
nextParse nextParser t = case parseOnly nextParser t of
  Left err -> fail err
  Right res -> return res

----------------------------------------------------------------------------------------------------

-- | "Lexeme": Skip all horizontal space before a parser.
lex :: Parser a -> Parser a
lex p = skipHorizontalSpace *> p

----------------------------------------------------------------------------------------------------

-- | As Attoparsec's 'skipSpace', but skips horizintal space only.
skipHorizontalSpace :: Parser ()
skipHorizontalSpace = do
  _ <- takeWhile (`elem` [' ', '\t', '\f', '\v'])
  return ()

----------------------------------------------------------------------------------------------------

-- | Consume the rest of the current line.
getLine :: Parser Text
getLine = takeWhile (not . isEndOfLine) <* endOfLine

----------------------------------------------------------------------------------------------------

-- | Skip the rest of the line.
skipLine :: Parser ()
skipLine = void getLine

----------------------------------------------------------------------------------------------------

-- | Haskell's 'read' and attoparsec's 'double' don't recognize the format for Fortrans's double
-- precision numbers, i.e. @1.00D-03@. This parser accepts all types of Fortran floats.
fortranDouble :: Parser Double
fortranDouble = do
  -- Potentially signed. But maybe also not ...
  sign <- optional $ char '-' <|> char '+'

  -- Potentially there is a number before the decimal point but you never know
  -- (looking at you, Turbomole! >:| )
  prefix <-
    ( do
        preDot <- optional digit
        dot <- char '.'
        postDot <- many1 digit
        let base' = Text.pack $ fromMaybe '+' sign : fromMaybe '0' preDot : dot : postDot
        return base'
      )
      >>= nextParse double

  -- Looking for an exponent. Might be there or not ...
  expoChar <- optional $ char 'd' <|> char 'D' <|> char 'e' <|> char 'E'

  case expoChar of
    Nothing -> return prefix
    Just _ -> do
      expo <- signed (decimal @Int)
      return $ prefix * 10 ^^ expo
