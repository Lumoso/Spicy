-- |
-- Module      : Spicy.Aeson
-- Description : Settings for JSON instance derivations.
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides settings to serialise Haskell values into Aeson's constructors. Those are
-- mainly convenience and styling settings for more convenient priting and parsing.
module Spicy.Aeson
  ( spicyJOption,
  )
where

import Data.Aeson.TH
import qualified RIO.Char as Char
import Spicy.Prelude

-- | Aeson options for nice parsing of JSON/YAML fields in combination with nice ability to still
-- create lenses.
spicyJOption :: Options
spicyJOption =
  defaultOptions
    { constructorTagModifier = map Char.toLower,
      omitNothingFields = True
    }
