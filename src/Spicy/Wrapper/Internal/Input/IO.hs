-- |
-- Module      : Spicy.Wrapper.Internal.Input.IO
-- Description : Writing external program input
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Program-specific preperation of calculation input files.
module Spicy.Wrapper.Internal.Input.IO
  ( writeInputs,
  )
where

import Spicy.Common
import Spicy.Molecule.Computational
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Internal.Writer
import Spicy.Molecule.Molecule
import Spicy.Prelude
import Spicy.Wrapper.Internal.Input.Language
import Spicy.Wrapper.Internal.Input.Representation
import System.Path

logSource :: LogSource
logSource = "wrapper-input"

-- | Write all necessary input files for the calculation specified by the input CalcID to the
-- directory specified in the input. Returns the path of the (main) input file.
writeInputs :: (HasLogFunc env) => OniomLayer -> CalcInput -> RIO env AbsFile
writeInputs layer@(OniomLayer mol) calcInput = do
  -- Get information
  let prog = calcInput ^. #software % program
      progStr p = case p of
        Psi4 -> "Psi4"
        XTB -> "XTB"
        Turbomole -> "Turbomole"
      permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      prefix = relFile . replaceProblematicChars $ calcInput ^. #prefixName
      inputFilePath = permanentDir </> prefix <.> ".inp"

  -- Write input file
  logDebugS logSource $
    "Writing input file for a " <> progStr prog <> " Calculation:\n"
      <> "Molecule comment: "
      <> displayShow (mol ^. #comment)
      <> ", File Path: "
      <> displayShow inputFilePath
  inputText <- runReaderT makeInput (layer, calcInput)
  writeFileUTF8 (toAbsRel inputFilePath) inputText

  -- If this is an XTB calculation, also write separate .xyz and .pc files
  when (prog == XTB) $ do
    -- .xyz file
    let OniomLayer realMol = isolateMoleculeLayer mol
        xyzFilePath = toAbsRel $ replaceExtension inputFilePath ".xyz"
    logDebugS logSource $
      "Writing .xyz (coordinate) file for XTB to: "
        <> displayShow xyzFilePath
    xyzText <- writeXYZ realMol
    writeFileUTF8 xyzFilePath xyzText
    -- .pc file
    let pcFilePath = toAbsRel $ xtbMultPath permanentDir prefix
    logDebugS logSource $
      "Writing .pc (point charge) file for XTB to: "
        <> displayShow pcFilePath
    pcText <- xtbMultipoleRep layer
    writeFileUTF8 pcFilePath pcText

  -- Return the input file path
  return inputFilePath
