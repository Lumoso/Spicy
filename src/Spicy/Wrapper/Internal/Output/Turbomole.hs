-- |
-- Module      : Spicy.Wrapper.Internal.Output.Turbomole
-- Description : Parsers for Turbomole output files
-- Copyright   : Phillip Seeber, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- This module provides parsers for Turbomole outputs.
module Spicy.Wrapper.Internal.Output.Turbomole
  ( tmEnergy,
    tmGradient,
    tmHessian,
    tmBasis,
    buildBasis,
    tmMolecularOrbitals,
  )
where

import Data.Attoparsec.Text
import qualified Data.IntMap as IntMap
import qualified Data.Map as Map
import Data.Massiv.Array as Massiv hiding (takeWhile)
import Math.NumberTheory.Roots
import RIO.Seq (Seq (..))
import qualified RIO.Seq as Seq
import Spicy.Attoparsec
import Spicy.Data
import Spicy.Molecule.Physical
import Spicy.Prelude hiding (exponent)
import Spicy.Wavefunction.Basis
import Spicy.Wrapper.Internal.Output.Generic

-- | @$energy@ block
tmEnergy :: Parser Double
tmEnergy = do
  void $ string "$energy" <* skipHorizontalSpace
  void $ string "SCF" <* skipHorizontalSpace
  void $ string "SCFKIN" <* skipHorizontalSpace
  void $ string "SCFPOT" <* skipHorizontalSpace
  _corrMethod <- optional (many1 $ letter <|> digit) <* skipHorizontalSpace
  endOfLine

  (_ :|> lastEnergy) <- fmap Seq.fromList . many1 $ do
    void $ skipHorizontalSpace *> decimal @Int <* skipHorizontalSpace
    scfEnergy <- double <* skipHorizontalSpace
    _kin <- double <* skipHorizontalSpace
    _pot <- double <* skipHorizontalSpace
    corr <- option 0 $ lex fortranDouble
    lex endOfLine
    return $ scfEnergy + corr

  return lastEnergy

----------------------------------------------------------------------------------------------------

-- | @$gradient@ block
tmGradient :: Parser (VectorS Double)
tmGradient = do
  -- Header
  void $ string "$grad" <* skipHorizontalSpace
  void $ string "cartesian gradients" <* endOfLine

  -- Summary stuff
  _cycle <- skipHorizontalSpace *> string "cycle =" *> skipHorizontalSpace *> decimal @Int <* skipHorizontalSpace
  _energyType <- takeWhile (/= '=') *> char '=' *> skipHorizontalSpace *> double <* skipHorizontalSpace
  _gradMag <- string "|dE/dxyz| =" *> skipHorizontalSpace *> double <* endOfLine

  -- @$coord@ style coordinates for which the gradient is printed
  void . many1 $ do
    _xyz <- count 3 (skipHorizontalSpace *> double) <* skipHorizontalSpace
    _element <- many1 letter <* endOfLine
    return ()

  -- Cartesian gradients, xyz component per atom and line
  grad <- fmap (Massiv.fromLists' @S @Ix2 Par) . many1 $ do
    gxyz <- count 3 $ skipHorizontalSpace *> fortranDouble
    endOfLine
    return gxyz

  return . VectorS . compute . flatten . Massiv.map bohr2Angstrom $ grad

----------------------------------------------------------------------------------------------------

-- | @$nprhessian@ block. This requires non-projected hessians, with translational and rotational
-- components still present.
tmHessian :: Parser (MatrixS Double)
tmHessian = do
  void $ string "$nprhessian" <* endOfLine

  -- Iterate over lines
  allElements <- fmap (compute @S . sconcat) . many1 $ do
    -- Indices. We don't care about them.
    void . count 2 $ skipHorizontalSpace *> decimal @Int

    -- Parse a single line. Might (!) contain up to 5 elements per line
    elements <- do
      els <- many1 $ skipHorizontalSpace *> fortranDouble
      endOfLine
      return . sfromList $ els

    return elements

  -- Reshape into a square hessian matrix.
  let Sz m = size allElements
  n <- case exactSquareRoot m of
    Nothing -> fail "The number of elements in the $hessian block is not a square number. Therefore no Hessian can be constructed."
    Just x -> return x
  let hess = resize' (Sz $ n :. n) allElements

  return . MatrixS $ hess

----------------------------------------------------------------------------------------------------

-- | Parser for the primary orbital basis in Turbomole format.
tmBasis :: Parser (Map Element (Vector B BasisShell))
tmBasis = do
  void $ string "$basis" <* endOfLine

  -- Parse basis sets per element.
  basisPerElement <- many1 elementBlock

  return . Map.fromList $ basisPerElement
  where
    -- Parser for the basis set of one element.
    elementBlock = do
      -- Start of a block.
      void $ char '*' <* endOfLine

      -- Element and name of the basis set
      element <- parseElementSymbol <* skipHorizontalSpace
      _name <- takeTill isEndOfLine <* endOfLine

      -- Comment. Often indicates contraction style
      _comment <- takeTill isEndOfLine <* endOfLine

      -- End of the header of a basis block.
      void $ char '*' <* endOfLine

      -- Contraction blocks.
      cGTOs <- Massiv.fromList Seq <$> many1 contractionBlock

      return (element, cGTOs)

    -- Parses a cGTO by parsing all pGTOs.
    contractionBlock = do
      -- Header of a cGTO. Indicates number of pGTOs and angular momentum.
      nPrim <- lex (decimal @Int)
      angularMomentum <- lex parseAngularMomentumAsLetter <* endOfLine

      -- Parse given number of pGTOs with exponent and contraction coefficient.
      primitives <- Massiv.fromList Seq <$> parsePrimitive nPrim

      return BasisShell {..}

    -- Parses n primitive GTOs.
    parsePrimitive n = count n $ do
      exponent <- lex double
      coefficient <- lex double <* endOfLine
      return Gauss {..}

----------------------------------------------------------------------------------------------------

-- | Simple parser for the 'Char' representation of angular momentum (s=0, p=1, d=2, etc.).
-- Works up to angular momentum j=7.
parseAngularMomentumAsLetter :: Parser Natural
parseAngularMomentumAsLetter =
  choice
    [ char 's' $> 0,
      char 'p' $> 1,
      char 'd' $> 2,
      char 'f' $> 3,
      char 'g' $> 4,
      char 'h' $> 5,
      char 'i' $> 6,
      char 'j' $> 7
    ]

----------------------------------------------------------------------------------------------------

-- | Construct an orbital basis for an 'IntMap' 'Atom' given a basis set. The keys of the 'IntMap'
-- will be used for the 'Shell.atomIndex' field of the orbital shells. Fails if any atom's
-- (chemical) element is not an element of the basis set.
buildBasis :: MonadThrow m => Map Element (Vector B BasisShell) -> IntMap Atom -> m Basis
buildBasis ebasis atoms = do
  let name = "Turbomole basis set constructed by Spicy"
  shells <- VectorG . convert <$> IntMap.foldlWithKey f (return Massiv.empty) atoms

  return Basis {..}
  where
    f :: MonadThrow m => m (Vector DS Shell) -> Int -> Atom -> m (Vector DS Shell)
    f acc' i atom = do
      acc <- acc'
      functions <- buildBasisHelper ebasis i atom
      return $ Massiv.sappend acc functions

-- | Helper function: Given a basis set and an index, construct the orbital basis for an 'Atom'.
-- Fails if the atom's 'Element' is not included in the basis set.
buildBasisHelper :: MonadThrow m => Map Element (Vector B BasisShell) -> Int -> Atom -> m (Vector B Shell)
buildBasisHelper ebasis i atom = do
  bshells <-
    maybe2MThrow
      (DataStructureException "buildBasis" "Element not found in basis set")
      (Map.lookup (atom ^. #element) ebasis)
  return $ toShellOf atom <$> bshells
  where
    toShellOf Atom {..} BasisShell {..} =
      Shell
        { shellType = SphericalShell angularMomentum, -- Cartesian shells not supported.
          primitives = VectorG primitives,
          coordinates = coordinates,
          atomIndex = Just i
        }

----------------------------------------------------------------------------------------------------

-- | Parser for the molecular orbitals in turbomole format.
tmMolecularOrbitals :: Parser (Matrix DL Double)
tmMolecularOrbitals = do
  void $ count 3 skipLine
  mMatrix <- stackInnerSlicesM @U @Ix2 <$> many1 parseMO
  case mMatrix of
    Nothing -> fail "Could not stack inner slices while parsing molecular orbitals"
    Just matrix -> return matrix
  where
    parseMO = do
      void $ lex double *> lex letter *> lex (string "eigenvalue=") *> fortranDouble
      nAO <- lex (string "nsaos=") *> decimal <* skipLine
      Massiv.fromList @U Seq <$> count nAO (p <* skipSpace)
    p = Data.Attoparsec.Text.take 20 >>= nextParse fortranDouble
