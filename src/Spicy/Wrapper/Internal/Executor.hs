-- |
-- Module      : Spicy.Wrapper.Internal.Executor
-- Description : System calls to the quantum chemistry software.
-- Copyright   : Phillip Seeber, 2020
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Provides the callers to quantum chemistry software.
module Spicy.Wrapper.Internal.Executor
  ( runCalculation,
  )
where

import qualified Data.ByteString.Lazy.Char8 as ByteStringLazy8
import qualified Data.IntMap as IntMap
import qualified Data.IntSet as IntSet
import qualified Data.Map as Map
import qualified Data.Massiv.Array as Massiv
import qualified RIO.ByteString.Lazy as BL
import qualified RIO.List as List
import RIO.Process
import Spicy.Attoparsec
import Spicy.Common
import Spicy.Formats.FChk
import Spicy.Formats.Molden
import Spicy.Molecule.Computational
import Spicy.Molecule.Internal.Util
import Spicy.Molecule.Molecule
import Spicy.Molecule.Physical
import Spicy.Prelude
import Spicy.RuntimeEnv
import Spicy.Wavefunction.Basis
import Spicy.Wavefunction.Wavefunction
import Spicy.Wrapper.Internal.Input.IO
import Spicy.Wrapper.Internal.Output.GDMA
import Spicy.Wrapper.Internal.Output.Generic
import Spicy.Wrapper.Internal.Output.Turbomole
import Spicy.Wrapper.Internal.Output.XTB
import System.Path
  ( (<++>),
    (<.>),
    (</>),
  )
import qualified System.Path as Path
import qualified System.Path.Directory as Dir

logSource :: LogSource
logSource = "Wrapper Executor"

----------------------------------------------------------------------------------------------------

-- | Run a given calculation of a molecule, described by a 'CalcInput'. This returns a 'CalcOutput'
-- for the calculation that has been requested.
runCalculation ::
  ( HasWrapperConfigs env,
    HasLogFunc env,
    HasProcessContext env
  ) =>
  OniomLayer ->
  CalcInput ->
  RIO env CalcOutput
runCalculation layer calcInput = do
  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      scratchDir = getDirPathAbs $ calcInput ^. #scratchDir
      software = calcInput ^. #software % program

  -- Create permanent and scratch directory
  liftIO $ Dir.createDirectoryIfMissing True permanentDir
  liftIO $ Dir.createDirectoryIfMissing True scratchDir

  -- Execute the wrapper on the input file.
  case software of
    Psi4 -> executePsi4 layer calcInput
    XTB -> executeXTB layer calcInput
    Turbomole -> executeTurbomole layer calcInput

  -- Parse the output, that has been produced by the wrapper.
  calcOutput <- case software of
    Psi4 -> analysePsi4 layer calcInput
    XTB -> analyseXTB layer calcInput
    Turbomole -> analyseTurbomole layer calcInput

  -- Write the updated molecule to the shared variable.
  return calcOutput

{-
####################################################################################################
-}

-- $executors
-- Functions to call computational chemistry software with the appropiate arguments. These functions
-- are not responsible for processing of the output.

-- | Run Psi4 on a given calculation. A Psi4 run will behave as follows:
--
--   * All permanent files will be in the 'CalcInput.permaDir'.
--   * The 'CalcInput.scratchDir' will be used for scratch files and keep temporary files after
--     execution.
--   * All files will be prefixed with the 'CalcInput.prefixName'.
--   * The output will be kept for analysis.
executePsi4 ::
  (HasWrapperConfigs env, HasLogFunc env, HasProcessContext env) =>
  -- | A single layer (or fragment), on which to perform a calculation. It's calculation context
  -- will be completely ignored.
  OniomLayer ->
  -- | A input context for the calculation on the given 'OniomLayer'.
  CalcInput ->
  RIO env ()
executePsi4 layer@(OniomLayer mol) calcInput = do
  -- Obtain the Psi4 wrapper.
  psi4Wrapper <-
    view (wrapperConfigsL % #psi4) >>= \wrapper -> case wrapper of
      Just w -> return . getFilePath $ w
      Nothing -> throwM $ WrapperGenericException "executePsi4" "Psi4 wrapper is not configured. Cannot execute."

  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      scratchDir = getDirPathAbs $ calcInput ^. #scratchDir
      software = calcInput ^. #software % program

  -- Check if this function is appropiate to execute the calculation at all.
  unless (software == Psi4) $ do
    logErrorS
      "psi4"
      "A calculation should be done with the Psi4 driver function,\
      \ but the calculation is not a Psi4 calculation."
    throwM $
      SpicyIndirectionException
        "executePsi4"
        ( "Requested to execute Psi4 on molecule with name "
            <> show (mol ^. #comment)
            <> " but this is not a Psi4 calculation."
        )

  -- Write the Psi4 input.
  inputFilePath <- writeInputs layer calcInput
  let outputFilePath = Path.replaceExtension inputFilePath ".out"

  -- Prepare the command line arguments to Psi4.
  let psi4CmdArgs =
        [ "--input=" <> Path.toString inputFilePath,
          "--output=" <> Path.toString outputFilePath,
          "--nthread=" <> show (calcInput ^. #nThreads),
          "--scratch=" <> Path.toString scratchDir,
          "--messy"
        ]

  -- Debug logging before execution of Psi4.
  logDebugS "psi4" $ "Starting Psi4 with command line arguments: " <> displayShow psi4CmdArgs

  -- Launch the Psi4 process and read its stdout and stderr.
  (exitCode, psi4Out, psi4Err) <-
    withWorkingDir (Path.toString permanentDir) $
      proc
        (Path.toString psi4Wrapper)
        psi4CmdArgs
        readProcess

  -- Provide some information if something went wrong.
  unless (exitCode == ExitSuccess) $ do
    logErrorS "psi4" $ "Execution terminated abnormally. Got exit code: " <> displayShow exitCode
    logErrorS "psi4" $ "Error messages:\n" <> (displayBytesUtf8 . toStrictBytes $ psi4Err)
    logErrorS "psi4" $ "Stdout messsages:\n" <> (displayBytesUtf8 . toStrictBytes $ psi4Out)
    throwM $ WrapperGenericException "executePsi4" "Psi4 execution terminated abnormally."

----------------------------------------------------------------------------------------------------

-- | Run a given XTB calculation. This function will write its own input files.
--
-- Unlike other software, xtb does not seem to support the use of a dedicated scratch directory.
-- Hence, all files are kept in the permanent directory.
executeXTB ::
  (HasWrapperConfigs env, HasLogFunc env, HasProcessContext env) =>
  -- | A single layer (or fragment), on which to perform a calculation. It's calculation context
  -- will be completely ignored.
  OniomLayer ->
  -- | A input context for the calculation on the given 'OniomLayer'.
  CalcInput ->
  RIO env ()
executeXTB layer@(OniomLayer mol) calcInput = do
  -- Obtain the XTB wrapper.
  xtbWrapper <-
    view (wrapperConfigsL % #xtb) >>= \wrapper -> case wrapper of
      Just w -> return . getFilePath $ w
      Nothing -> throwM $ WrapperGenericException "executeXTB" "XTB wrapper is not configured. Cannot execute."

  let thisSoftware = calcInput ^. #software % program

  -- Check if this function is appropiate to execute the calculation at all.
  unless (thisSoftware == XTB) $ do
    logErrorS
      "xtb"
      "A calculation should be done with the XTB driver function,\
      \ but the calculation is not a XTB calculation."
    throwM $
      SpicyIndirectionException
        "executeXTB"
        ( "Requested to execute XTB on a molecule with comment: "
            <> show (mol ^. #comment)
            <> ", but this is not a XTB calculation."
        )

  -- Write the input files for XTB
  inputFilePath <- writeInputs layer calcInput
  let geomFilePath = Path.replaceExtension inputFilePath ".xyz"

  -- Prepare the command line arguments to XTB.
  let cmdTask = case calcInput ^. #task of
        WTEnergy -> "--sp"
        WTGradient -> "--grad"
        WTHessian -> "--hess"
      xtbCmdArgs =
        [ "--json",
          "--molden",
          cmdTask,
          Path.toString geomFilePath,
          "--input", -- This...
          Path.toString inputFilePath -- And this need to be separate, for reasons unknown to mankind.
          --"--parallel " <> show (calcContext ^. #input % #nProc),
        ]

  -- Debug logging before execution of XTB.
  logDebugS "xtb" $ "Starting XTB with command line arguments: " <> displayShow xtbCmdArgs

  -- Launch the XTB process and read its stdout and stderr.
  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      nThreads = calcInput ^. #nThreads
  (exitCode, xtbOut, xtbErr) <-
    withModifyEnvVars (Map.insert "OMP_NUM_THREADS" (tshow nThreads <> ",1"))
      . withWorkingDir (Path.toString permanentDir)
      $ proc
        (Path.toString xtbWrapper)
        xtbCmdArgs
        readProcess

  -- Writing an output file of the XTB run.
  writeFileBinary (Path.toString $ permanentDir </> Path.relFile "xtb.log") . toStrictBytes $ xtbOut

  -- Provide some information if something went wrong.
  unless (exitCode == ExitSuccess) $ do
    logErrorS "xtb" $ "Execution terminated abnormally. Got exit code: " <> displayShow exitCode
    logErrorS "xtb" $ "Error messages:\n" <> (displayBytesUtf8 . toStrictBytes $ xtbErr)
    logErrorS "xtb" $ "Stdout messsages:\n" <> (displayBytesUtf8 . toStrictBytes $ xtbOut)
    throwM $ WrapperGenericException "executeXTB" "XTB execution terminated abnormally."

----------------------------------------------------------------------------------------------------

-- | Executes a given Turbomole calculation. Based on the associated QC Hamiltonian it will make
-- best attempts to figure out which of the various Turbomole executables need to be called.
executeTurbomole ::
  (HasWrapperConfigs env, HasLogFunc env, HasProcessContext env) =>
  -- | A single layer (or fragment), on which to perform a calculation. It's calculation context
  -- will be completely ignored.
  OniomLayer ->
  -- | A input context for the calculation on the given 'OniomLayer'.
  CalcInput ->
  RIO env ()
executeTurbomole layer@(OniomLayer mol) calcInput = do
  -- Obtain the turbomole installation bin directory, in which the executables are expected.
  turbomoleBinDir <-
    view (wrapperConfigsL % #turbomole) >>= \wrapper -> case wrapper of
      Just w -> return . getDirPath $ w
      Nothing -> throwM (localExc "Turbomole wrapper is not configured. Cannot execute.")
  turbomoleBindDirAbs <- liftIO . Path.genericMakeAbsoluteFromCwd $ turbomoleBinDir

  -- Look for all executables, that we need. If any cannot be found we must assume that the
  -- turbomole installation is broken and crash.
  let exes =
        [ "dscf",
          "escf",
          "ridft",
          "ricc2",
          "rdgrad",
          "egrad",
          "grad",
          "pnoccsd",
          "actual",
          "aoforce",
          "NumForce"
        ]
  exesPaths <- fmap Map.fromList . for exes $ \e -> do
    let exeAbsFile = turbomoleBindDirAbs </> Path.relFile e
    hasExe <- liftIO . Dir.doesFileExist $ turbomoleBinDir </> Path.relFile e
    unless hasExe $ do
      logErrorS logSourceS $ "Could not find Turbomole executable: " <> displayShow e
      throwM . localExc $ "Could not find executable " <> show exes
    return (e, exeAbsFile)

  let nThreads = calcInput ^. #nThreads
      permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      software = calcInput ^. #software
      task = calcInput ^. #task

  -- Check suitability and obtain the hamiltonian. The Hamiltonian is required
  hamiltonian <- case software of
    TurbomoleData h -> return h
    _ -> do
      logErrorS
        logSourceS
        "A calculation should be done with the Turbomole driver function,\
        \ but the calculation is not a Turbomole calculation."
      throwM . localExc $
        "Requested to execute Turbomole on molecule with comment "
          <> show (mol ^. #comment)
          <> " but this is not a Turbomole calculation."

  -- Write the Turbomole input file "control". Make a backup, which does not get modified.
  inputFilePath <- writeInputs layer calcInput
  liftIO $ Dir.copyFile inputFilePath (Path.takeDirectory inputFilePath </> Path.relFile "control.bak")
  liftIO $ Dir.renameFile inputFilePath (Path.takeDirectory inputFilePath </> Path.relFile "control")

  -- Step 1: Calculate reference wavefunctions.
  --   Figure out which turbomole executables to call. dscf and grad for HF and DFT without RI,
  --   ridft and rdgrad otherwise
  case hamiltonian ^. #ri of
    Nothing -> do
      commonCaller nThreads permanentDir exesPaths "dscf" mempty
      when (task /= WTEnergy) $ commonCaller nThreads permanentDir exesPaths "grad" mempty
    Just RIJ -> do
      commonCaller nThreads permanentDir exesPaths "ridft" mempty
      when (task /= WTEnergy) $ commonCaller nThreads permanentDir exesPaths "rdgrad" mempty
    Just RIJK -> do
      commonCaller nThreads permanentDir exesPaths "ridft" mempty
      when (task /= WTEnergy) $ commonCaller nThreads permanentDir exesPaths "rdgrad" mempty
    Just (OtherRI _) -> do
      logErrorS logSourceS "No other RI methods available in Turbomole."
      throwM . localExc $ "Unknown RI settings for Turbomole."

  -- Step 2: Calculate correlated wavefunctions if requested.
  case hamiltonian ^. #corr of
    Nothing -> return ()
    Just Correlation {corrModule} -> case corrModule of
      Nothing -> do
        logErrorS logSourceS "No correlation module specified for Turbomole, but is required."
        throwM . localExc $ "Correlation module for Turbomole not specified."
      Just "pnoccsd" -> commonCaller nThreads permanentDir exesPaths "pnoccsd" mempty
      Just "ricc" -> commonCaller nThreads permanentDir exesPaths "ricc2" mempty
      Just e -> do
        logErrorS logSourceS $ "Unknown correlation module " <> display e <> " for Turbomole."
        throwM . localExc $ "Unknown correlation module " <> show e <> " for Turbomole."

  -- Step 3: If excitations were requested and not already calculated by the correlation module,
  -- calculate them now.
  case (hamiltonian ^. #exc, hamiltonian ^. #corr) of
    (Just _, Nothing) -> commonCaller nThreads permanentDir exesPaths "escf" mempty
    _ -> return ()

  -- Step 4: Calculate frequencies on top of the RIDFT or HF wavefunction or numerically on the
  -- correlated wavefunctions.
  when (task == WTHessian) $ case hamiltonian ^. #corr of
    Just Correlation {corrModule = Just "ricc"} ->
      commonCaller nThreads permanentDir exesPaths "NumForce" ["-ri", "-level cc2", "-central"]
    Just Correlation {corrModule = Just "pnoccsd"} -> do
      logErrorS logSourceS "The pnoccsd module cannot calculate gradients and therefore also no hessians."
      throwM . localExc $ "Hessians not available with pnoccsd module"
    Just _ -> do
      logErrorS logSourceS "Unknown settings for calculation of hessians on a correlated wavefunction."
      throwM . localExc $ "Hessians not available with for unknown correlated wavefunction type."
    Nothing -> case hamiltonian ^. #ri of
      Just RIJ -> commonCaller nThreads permanentDir exesPaths "aoforce" ["-ri"]
      Just RIJK -> commonCaller nThreads permanentDir exesPaths "aoforce" ["-ri"]
      Just (OtherRI _) -> do
        logErrorS logSourceS "Unknown RI setting for Turbomole. Don't know how to calculate hessian."
        throwM . localExc $ "Unkown RI setting for Turbomole. Don't know how to calculate hessian."
      Nothing -> commonCaller nThreads permanentDir exesPaths "aoforce" mempty
  where
    localExc = WrapperGenericException "executeTurbomole"
    logSourceS = "Turbomole"
    commonCaller threads permanentDir exeMap exeName args = do
      logDebugS logSourceS $ "Calculating wavefunction with " <> displayShow exeName <> " ..."
      (exitCode, out, err) <-
        withWorkingDir (Path.toString permanentDir) $
          proc
            (Path.toString $ exeMap Map.! exeName)
            ("-smpcpus " <> show threads : args)
            readProcess
      unless (exitCode == ExitSuccess) $ do
        logErrorS logSourceS $ "Execution of " <> displayShow exeName <> " ended abnormally. Got exit code: " <> displayShow exitCode
        logErrorS logSourceS $ "Error messages:\n" <> (displayBytesUtf8 . toStrictBytes $ err)
        logErrorS logSourceS $ "Stdout messages:\n" <> (displayBytesUtf8 . toStrictBytes $ out)
        throwM . localExc $ "Turbomole dscf execution ended abnormally."
      writeFileBinary (Path.toString $ permanentDir </> Path.relFile (exeName <> ".out")) . toStrictBytes $ out

----------------------------------------------------------------------------------------------------

-- | Run GDMA on a given FChk file. The function takes also a set of link atom indices (in dense
-- mapping, suitable for GDMA), that are to be ignored in the multipole expansion. This replaces the
-- charge redistribution.
--
-- By the way GDMA expects its input file, to delete link atoms, the need to be named @L@ in the FChk
-- file. This conversion of the FChk happens within this function.
--
-- The function will return multipole moments in spherical tensor representation up to given order.
gdmaAnalysis ::
  (HasWrapperConfigs env, HasLogFunc env, HasProcessContext env) =>
  -- | The absolute path to the FChk file, which is the GDMA input.
  Path.AbsFile ->
  -- | The 'Atom's of the current molecule layer, which is being analysed.
  IntMap Atom ->
  -- | The order of the multipole expansion. >= 0 && <= 10
  Int ->
  RIO env (IntMap Multipoles)
gdmaAnalysis fchkPath atoms expOrder = do
  -- Obtain the GDMA wrapper.
  gdmaWrapper <-
    view (wrapperConfigsL % #gdma) >>= \wrapper -> case wrapper of
      Just w -> return . getFilePath $ w
      Nothing -> throwM $ WrapperGenericException "gdmaAnalysis" "The GDMA wrapper is not configured."

  -- Sanity Checks.
  unless (expOrder <= 10 && expOrder >= 0) . throwM $
    WrapperGenericException
      "gdmaAnalysis"
      "The multipole expansion order in GDMA must be >= 0 && <= 10."
  fchkExists <- liftIO . Dir.doesFileExist $ fchkPath
  unless fchkExists . throwM $
    WrapperGenericException "gdmaAnalysis" "The formatted checkpoint file does not exist."

  -- Read the FChk and reconstruct it with link atoms replaced by an uncommon element.
  let linkReplacement = Xe -- Xenon ist the heaviest element supported by GDMA.
      gdmaFChkPath = Path.takeDirectory fchkPath </> (Path.takeBaseName fchkPath <++> "_gdma" <++> ".fchk")
  fchkOrig <- readFileUTF8 (Path.toAbsRel fchkPath) >>= parse' fChk
  fchkLink <- relabelLinkAtoms linkReplacement atoms fchkOrig
  writeFileUTF8 (Path.toAbsRel gdmaFChkPath) . writeFChk $ fchkLink

  -- Construct the GDMA input.
  let gdmaInput =
        ByteStringLazy8.unlines
          [ "file " <> (ByteStringLazy8.pack . Path.toString $ gdmaFChkPath),
            "angstrom",
            "multipoles",
            "limit " <> (ByteStringLazy8.pack . show $ expOrder),
            "delete " <> (ByteStringLazy8.pack . show $ linkReplacement),
            "start"
          ]

  -- Execute GDMA on the input file now and pipe the input file into it.
  (exitCode, gdmaOut, gdmaErr) <-
    proc
      (Path.toString gdmaWrapper)
      mempty
      (readProcess . setStdin (byteStringInput gdmaInput))

  unless (exitCode == ExitSuccess) $ do
    logErrorS "gdma" $ "Execution terminated abnormally. Got exit code: " <> displayShow exitCode
    logErrorS "gdma" "Error messages:"
    logErrorS "gdma" . displayShow $ gdmaErr
    logErrorS "gdma" "Stdout messages:"
    logErrorS "gdma" . displayShow $ gdmaOut
    throwM . localExcp $ "Run uncessfull"

  -- Parse the GDMA output and rejoin them with the model atoms.
  modelMultipoleList <-
    getResOrErr
      . parse' gdmaMultipoles
      . decodeUtf8Lenient
      . BL.toStrict
      $ gdmaOut
  let modelAtomKeys =
        IntSet.toAscList
          . IntMap.keysSet
          . IntMap.filter (not . isAtomLink . isLink)
          . IntMap.filter (\a -> not $ a ^. #isDummy)
          $ atoms
      multipoleMap = IntMap.fromAscList $ zip modelAtomKeys modelMultipoleList

  -- Message if something is wrong with the number of poles expected and obtained.
  unless (List.length modelMultipoleList == List.length modelAtomKeys) $ do
    logErrorS
      "gdma"
      "Number of model atoms and number of multipole expansions centres obtained from GDMA\
      \ do not match."
    throwM . localExcp $ "Problematic behaviour in GDMA multipole analysis."

  return multipoleMap
  where
    localExcp = WrapperGenericException "gdmaAnalysis"

----------------------------------------------------------------------------------------------------

-- | Run GDMA on the results of a calculation. As 'gdmaAnalysis', but takes care of writing the FChk
-- itself. This function will return multipole moments in spherical tensor representation up to
-- order 4.
gdmaRun ::
  (HasLogFunc env, HasWrapperConfigs env, HasProcessContext env) =>
  Basis ->
  IntMap Atom ->
  Wavefunction ->
  CalcInput ->
  RIO env (IntMap Multipoles)
gdmaRun basis atoms waveFunction calcInput = do
  gdmaWrapper <-
    view (wrapperConfigsL % #gdma) >>= \wrapper -> case wrapper of
      Just w -> return . getFilePath $ w
      Nothing -> throwM $ WrapperGenericException "executeGDMA" "GDMA wrapper not configured."

  -- Create and write a correct FChk
  let linkReplacement = Xe
  fChkNoLink <- dataToFChkSimple basis atoms waveFunction calcInput
  fChkLink <- relabelLinkAtoms linkReplacement atoms fChkNoLink
  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      prefixName = calcInput ^. #prefixName
      fchkPath = permanentDir </> Path.relFile prefixName <.> ".fchk"
  writeFileUTF8 (Path.toAbsRel fchkPath) . writeFChk $ fChkLink

  -- Run gdma.
  let gdmaInput =
        ByteStringLazy8.unlines
          [ "file " <> (ByteStringLazy8.pack . Path.toString $ fchkPath),
            "angstrom",
            "multipoles",
            "limit " <> ByteStringLazy8.pack "4",
            "delete " <> (ByteStringLazy8.pack . show $ linkReplacement),
            "start"
          ]
  (exitCode, gdmaOut, gdmaErr) <-
    proc
      (Path.toString gdmaWrapper)
      mempty
      (readProcess . setStdin (byteStringInput gdmaInput))

  modelMultipoleList <-
    getResOrErr
      . parse' gdmaMultipoles
      . decodeUtf8Lenient
      . BL.toStrict
      $ gdmaOut
  let modelAtomKeys =
        IntSet.toAscList
          . IntMap.keysSet
          . IntMap.filter (not . isAtomLink . isLink)
          . IntMap.filter (not . isDummy)
          $ atoms
      multipoleMap = IntMap.fromAscList $ zip modelAtomKeys modelMultipoleList

  unless (exitCode == ExitSuccess) $ do
    logErrorS "gdma" $ "Execution terminated abnormally. Got exit code: " <> displayShow exitCode
    logErrorS "gdma" "Error messages:"
    logErrorS "gdma" . displayShow $ gdmaErr
    logErrorS "gdma" "Stdout messages:"
    logErrorS "gdma" . displayShow $ gdmaOut
    throwM . WrapperGenericException "gdma" $ "Run unsucessful"

  return multipoleMap

{-
####################################################################################################
-}

-- $analysers
-- Functions to process the outputs of computational chemistry software. This relies on consistent
-- file structure in the file system.

-- | Parse output produced by Psi4. The Psi4 analyser relies on a formatted checkpoint file for most
-- information, which must be at @permanentDir </> Path.relFile prefixName <.> ".fchk"@. If a
-- hessian calculation was requested, also a plain text hessian file (numpy style) is required,
-- which must be at @permanentDir </> Path.relFile prefixName <.> ".hess"@.
--
-- GDMA multipoles are obtained from this FCHK by an additional call to GDMA.
analysePsi4 ::
  (HasLogFunc env, HasProcessContext env, HasWrapperConfigs env) =>
  -- | The layer on which a calculation was run and whose output is to be analysed.
  OniomLayer ->
  -- | The calculation input, for which the calculation was run.
  CalcInput ->
  RIO env CalcOutput
analysePsi4 (OniomLayer mol) calcInput = do
  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      prefixName = calcInput ^. #prefixName
      fchkPath = permanentDir </> Path.relFile prefixName <.> ".fchk"
      hessianPath = permanentDir </> Path.relFile prefixName <.> ".hess"
      task' = calcInput ^. #task

  logDebugS "psi4" $ "Reading the formatted checkpoint file: " <> path2Utf8Builder fchkPath

  -- Read the formatted checkpoint file from the permanent directory.
  fChkOutput <- getResultsFromFChk =<< readFileUTF8 (Path.toAbsRel fchkPath)

  -- If the task was a hessian calculation, also parse the numpy array with the hessian.
  hessianOutput <- case task' of
    WTHessian -> do
      logDebugS "psi4" $ "Reading the hessian file: " <> path2Utf8Builder hessianPath
      hessianContent <- readFileUTF8 (Path.toAbsRel hessianPath)
      hessian <- parse' doubleSquareMatrix hessianContent
      return . Just $ hessian
    _ -> return Nothing

  -- Perform the GDMA calculation on the FChk file and obtain its results.
  multipoles <- gdmaAnalysis fchkPath (mol ^. #atoms) 4

  -- Combine the Hessian and multipole information into the main output from the FChk.
  let calcOutput =
        fChkOutput
          & #energyDerivatives % #hessian .~ hessianOutput
          & #multipoles .~ multipoles

  return calcOutput

----------------------------------------------------------------------------------------------------

-- | Analyse the output of an xtb calculation. Unlike Psi4, xtb output file names are independent of
--  the input (unless explicitly specified). This function is not reliant on gdma either.
analyseXTB ::
  (HasLogFunc env) =>
  -- | The layer on which a calculation was run and whose output is to be analysed.
  OniomLayer ->
  -- | The calculation input, for which the calculation was run.
  CalcInput ->
  RIO env CalcOutput
analyseXTB (OniomLayer mol) calcInput = do
  let permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      jsonPath = permanentDir </> Path.relFile "xtbout" <.> ".json"
      moldenPath = permanentDir </> Path.relFile "molden" <.> ".input"
      gradientPath = permanentDir </> Path.relFile "gradient"
      hessianPath = permanentDir </> Path.relFile "hessian"
      task' = calcInput ^. #task

  -- If required, get the gradient from the respective output file.
  gradientOutput <- case task' of
    WTEnergy -> return Nothing
    _ -> do
      logDebugS "xtb" $ "Reading gradient file " <> path2Utf8Builder gradientPath
      gradientContent <- readFileUTF8 $ Path.toAbsRel gradientPath
      gradient <- parse' parseXTBgradient gradientContent
      return . Just $ gradient

  -- If the task was a hessian calculation, also parse the array with the hessian.
  hessianOutput <- case task' of
    WTHessian -> do
      logDebugS "xtb" $ "Reading the hessian file: " <> path2Utf8Builder hessianPath
      hessianContent <- readFileUTF8 (Path.toAbsRel hessianPath)
      hessian <- parse' parseXTBhessian hessianContent
      return . Just $ hessian
    _ -> return Nothing

  -- Get multipoles from the output json.
  logDebugS "xtb" $ "Reading the XTB json file: " <> path2Utf8Builder jsonPath
  (energy, modelMultipoleList) <- fromXTBout =<< readFileBinary (Path.toString jsonPath)

  -- Get Wavefunction from the output molden file.
  logDebugS "xtb" $ "Reading the XTB molden file: " <> path2Utf8Builder moldenPath
  (wavefunction, basis, _) <- getResultsFromMolden =<< readFileUTF8 (Path.toAbsRel moldenPath)

  -- Assign multipoles to the correct keys
  let realAtoms = IntMap.filter (\a -> not $ a ^. #isDummy) $ mol ^. #atoms
      realAtomsKeys = IntMap.keys realAtoms
      multipoleMap = IntMap.fromAscList $ zip realAtomsKeys modelMultipoleList

  -- Finalize the output
  let enDeriv =
        EnergyDerivatives
          (pure energy)
          gradientOutput
          hessianOutput
      calcOutput =
        CalcOutput
          enDeriv
          multipoleMap
          (pure basis)
          wavefunction
  return calcOutput

----------------------------------------------------------------------------------------------------

-- | Analyse the output of a Turbomole calculation. Turbomole file names should always be the same
-- and follow a common layout, independent of the module.
analyseTurbomole ::
  (HasLogFunc env, HasProcessContext env, HasWrapperConfigs env) =>
  -- | The layer on which a calculation was run and whose output is to be analysed.
  OniomLayer ->
  -- | The calculation input, for which the calculation was run.
  CalcInput ->
  RIO env CalcOutput
analyseTurbomole (OniomLayer mol) calcInput = do
  let task = calcInput ^. #task
      permanentDir = getDirPathAbs $ calcInput ^. #permaDir
      energyFile = Path.toAbsRel $ permanentDir </> Path.relFile "energy"
      gradientFile = Path.toAbsRel $ permanentDir </> Path.relFile "gradient"
      hessianFile = Path.toAbsRel $ permanentDir </> Path.relFile "nprhessian"
      basisFile = Path.toAbsRel $ permanentDir </> Path.relFile "basis"
      mosFile = Path.toAbsRel $ permanentDir </> Path.relFile "mos"
      alphaMosFile = Path.toAbsRel $ permanentDir </> Path.relFile "alpha"
      betaMosFile = Path.toAbsRel $ permanentDir </> Path.relFile "beta"

  -- Start parsing files
  energy <- Just <$> (readFileUTF8 energyFile >>= parse' tmEnergy)
  gradient <- case task of
    WTEnergy -> return Nothing
    _ -> Just <$> (readFileUTF8 gradientFile >>= parse' tmGradient)
  hessian <- case task of
    WTHessian -> Just <$> (readFileUTF8 hessianFile >>= parse' tmHessian)
    _ -> return Nothing
  basisSet <- readFileUTF8 basisFile >>= parse' tmBasis

  let atoms = IntMap.filter (not . isDummy) $ mol ^. #atoms
      renumber = IntMap.fromAscList . zip [1 ..] . IntMap.elems
  basis <- buildBasis basisSet (renumber atoms)

  -- Figure out which MOs to use
  haveMOs <- liftIO $ Dir.doesFileExist mosFile
  haveAlpha <- liftIO $ Dir.doesFileExist alphaMosFile
  haveBeta <- liftIO $ Dir.doesFileExist betaMosFile
  let haveBothAlphaBeta = haveAlpha && haveBeta
      parseMOs = readFileUTF8 >=> parse' tmMolecularOrbitals

  mos <-
    case (haveMOs, haveBothAlphaBeta) of
      (True, True) -> throwM . localExc $ "Too many MO files!"
      (False, False) -> throwM . localExc $ "No MO files!"
      (True, False) ->
        Restricted . MatrixS . Massiv.convert
          <$> parseMOs mosFile
      (False, True) ->
        Unrestricted
          <$> (MatrixS . Massiv.convert <$> parseMOs alphaMosFile)
          <*> (MatrixS . Massiv.convert <$> parseMOs betaMosFile)
  charge <- maybe2MThrow (localExc "Not a QM calculation") (calcInput ^? #qMMMSpec % _QM % #charge)
  mult <- maybe2MThrow (localExc "Not a QM calculation") (calcInput ^? #qMMMSpec % _QM % #mult)
  let nElec = getNElectrons atoms charge
      nAlpha = (mult + nElec - 1) `div` 2 -- Relying on the convention that nAlpha > nBeta.
      nBeta = nElec - nAlpha
  let density = case mos of
        Restricted rm -> densityFromCoefficients nElec (getMatrixS rm)
        Unrestricted a b -> densityFromUnrestricted nAlpha nBeta (getMatrixS a) (getMatrixS b)
      waveFunction = Wavefunction (Just mos) density

  let basisERD = erdNormaliseBasis basis
  multipoles <- gdmaRun basisERD atoms waveFunction calcInput

  let calcOutput =
        CalcOutput
          { energyDerivatives = EnergyDerivatives {..},
            multipoles = multipoles,
            basis = Just basis,
            wavefunction = waveFunction
          }

  return calcOutput
  where
    localExc = WrapperGenericException "analyseTurbomole"
