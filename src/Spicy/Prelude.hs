{-# LANGUAGE StandaloneDeriving #-}

-- |
-- Module      : Spicy.Prelude
-- Description : A Prelude for Spicy.
-- Copyright   : Phillip Seeber, Sebastian Seidenath, 2021
-- License     : GPL-3
-- Maintainer  : phillip.seeber@uni-jena.de, sebastian.seidenath@uni-jena.de
-- Stability   : experimental
-- Portability : POSIX, Windows
--
-- Custom prelude for Spicy, shortening import lists and eliminating common import conflicts.
module Spicy.Prelude
  ( module Optics,

    -- * Exception Types
    -- $exceptionTypes
    DataStructureException (..),
    ParserException (..),
    MolLogicException (..),
    WrapperGenericException (..),
    SpicyIndirectionException (..),
    PysisException (..),
    IPIException (..),

    -- * RIO And Error Handling
    -- $rioAndErrors
    module RIO,
    view,
    maybe2MThrow,
    getResOrErr,

    -- * Massiv
    -- $massivWrapper
    module Data.Massiv.Array,
    VectorS (..),
    MatrixS (..),
    VectorG (..),
    MatrixG (..),

    -- * Text
    -- $textOperations
    readFileUTF8,
    writeFileUTF8,
    appendFileUTF8,
    text2Utf8Builder,
    removeWhiteSpace,
    tellN,

    -- * UTF8 Builder
    -- $utf8builderOperations
    appendFileUtf8,

    -- * ByteString
    -- $byteStringOperations
    byteStringLazy2Strict,
    byteStringStrict2Lazy,
    byteString2Utf8Builder,
  )
where

import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Builder as Builder
import qualified Data.ByteString.Lazy as BL
import Data.List.Split (chunksOf)
import Data.Massiv.Array
  ( Array,
    B,
    BL,
    BN,
    Comp (..),
    D,
    DI,
    DL,
    DS,
    DW,
    Matrix,
    P,
    S,
    U,
    Vector,
    compute,
    convert,
  )
import qualified Data.Massiv.Array as Massiv
import Data.Text.IO as T
import Data.Yaml hiding (Array)
import Optics hiding
  ( Empty,
    element,
    elements,
    view,
  )
import RIO hiding
  ( Lens,
    Lens',
    Vector,
    lens,
    over,
    preview,
    set,
    sets,
    takeWhile,
    to,
    view,
    (%~),
    (.~),
    (^.),
    (^..),
    (^?),
  )
import qualified RIO.Text as Text
import RIO.Writer
import System.IO
import qualified System.Path as Path
import qualified System.Path.IO as Path

{-
====================================================================================================
-}

-- $rioAndErrors
-- These are some simple helper functions for RIO and error handling patterns, commonly used in Spicy.

-- | A function as @view@ in RIO. It fetches the 'MonadReader' environment with an Optics' Lens.
-- view :: MonadReader env m => Lens' env a -> m a
view :: (Is k A_Getter, MonadReader s f) => Optic' k is s b -> f b
view lns = (^. lns) <$> ask

-- | Generalisation of a 'Maybe' value to an arbitrary monad, that is an instance of 'MonadThrow'. An
-- exception to throw must be provided, in case a 'Nothing' was given.
maybe2MThrow :: (MonadThrow m, Exception e) => e -> Maybe a -> m a
maybe2MThrow exc Nothing = throwM exc
maybe2MThrow _ (Just a) = return a

----------------------------------------------------------------------------------------------------

-- | Convenience function for a common RIO pattern. If some function returned an error, log this error
-- with RIO's logging system and then throw the error. If the result is fine, obtain it in the RIO
-- monad.
getResOrErr :: (HasLogFunc env, Exception e) => Either e a -> RIO env a
getResOrErr val = case val of
  Right res -> return res
  Left exc -> do
    logError . displayShow $ exc
    throwM exc

{-
====================================================================================================
-}

-- $exceptionTypes
-- Different types of exception used throughout Spicy.

-- | Exception type for operations on data structures, which are not meeting necessary criteria for the
-- operation to perform.
data DataStructureException = DataStructureException
  { -- | Function which is causing the exception.
    functionName :: String,
    -- | Description of the problem.
    description :: String
  }

instance Show DataStructureException where
  show (DataStructureException f e) = "DataStructureException in function \"" <> f <> "\":" <> e

instance Exception DataStructureException

----------------------------------------------------------------------------------------------------

-- | Exception type for textual or binary data, that could not be parsed.
newtype ParserException = ParserException String

instance Show ParserException where
  show (ParserException e) = "ParserException in parser: \"" <> e <> "\""

instance Exception ParserException

----------------------------------------------------------------------------------------------------

-- | Exception type for operations on 'Molecule's, which lead to a logical error. This can be caused
-- because some Spicy assumptions are not met, for example.
data MolLogicException = MolLogicException
  { functionName :: !String,
    description :: !String
  }

instance Show MolLogicException where
  show (MolLogicException f e) = "MoleculeLogicException in function \"" <> f <> "\": " <> e

instance Exception MolLogicException

----------------------------------------------------------------------------------------------------

-- | Exceptions for a computational chemistry wrapper, that are unspecific to a program.
data WrapperGenericException = WrapperGenericException
  { function :: !String,
    description :: !String
  }

instance Show WrapperGenericException where
  show (WrapperGenericException f e) = "WrapperGenericException: " <> f <> e

instance Exception WrapperGenericException

----------------------------------------------------------------------------------------------------

-- | An exception when the program control flow did a wrong turn and the information present are
-- inadequate to describe the program flow.
data SpicyIndirectionException = SpicyIndirectionException
  { functionName :: !String,
    description :: !String
  }

instance Show SpicyIndirectionException where
  show (SpicyIndirectionException f e) =
    "SpicyIndirectionException in function \"" <> f <> "\": " <> e

instance Exception SpicyIndirectionException

----------------------------------------------------------------------------------------------------

-- | Pysisyphus optimiser exceptions.
newtype PysisException = PysisException String
  deriving (Eq, Show)

instance Exception PysisException

----------------------------------------------------------------------------------------------------

-- | Problems in the communication with i-PI servers.
data IPIException = IPIException
  { functionName :: !String,
    description :: !String
  }

instance Show IPIException where
  show (IPIException f e) =
    "IPIException in function \"" <> f <> "\": " <> e

instance Exception IPIException

{-
====================================================================================================
-}

-- $massivWrapper
-- JSON enabled wrapper types around Massiv.

-- | Newtype wrapper for JSON serialisation around Massiv's unboxed 1D arrays.
newtype VectorS a = VectorS {getVectorS :: Vector Massiv.S a}
  deriving (Generic, Show, Eq)

instance (ToJSON a, Storable a) => ToJSON (VectorS a) where
  toJSON arr =
    let plainList = Massiv.toList . getVectorS $ arr
        Massiv.Sz dim1 = Massiv.size . getVectorS $ arr
     in object ["shape" .= dim1, "elements" .= plainList]

instance (FromJSON a, Storable a) => FromJSON (VectorS a) where
  parseJSON = withObject "VectorS" $ \arr -> do
    dim1 <- arr .: "shape"
    let sizeSupposed = Massiv.Sz dim1
    elements <- arr .: "elements"
    let parsedArr = Massiv.fromList Par elements
    if Massiv.size parsedArr == sizeSupposed
      then return . VectorS $ parsedArr
      else
        fail $
          "Size vs number of elements mismatch: Array has size: "
            <> (show . Massiv.size $ parsedArr)
            <> "and expected was: "
            <> show sizeSupposed

----------------------------------------------------------------------------------------------------

-- | Newtype wrapper for JSON serialisation around Massiv's unboxed 2D arrays.
newtype MatrixS a = MatrixS {getMatrixS :: Matrix Massiv.S a}
  deriving (Generic, Show, Eq)

instance (ToJSON a, Storable a) => ToJSON (MatrixS a) where
  toJSON arr =
    let plainList = Massiv.toList . getMatrixS $ arr
        Massiv.Sz (dim1 Massiv.:. dim2) = Massiv.size . getMatrixS $ arr
     in object ["shape" .= (dim1, dim2), "elements" .= plainList]

instance (FromJSON a, Storable a) => FromJSON (MatrixS a) where
  parseJSON = withObject "MatrixS" $ \arr -> do
    (dim1, dim2) <- arr .: "shape"
    let sizeSupposed = Massiv.Sz (dim1 Massiv.:. dim2)
    elements <- arr .: "elements"
    let parsedArr = Massiv.fromLists' Par . chunksOf dim2 $ elements
    if Massiv.size parsedArr == sizeSupposed
      then return . MatrixS $ parsedArr
      else
        fail $
          "Size vs number of elements mismatch: Array has size: "
            <> (show . Massiv.size $ parsedArr)
            <> " and expected was: "
            <> show sizeSupposed

----------------------------------------------------------------------------------------------------

-- | Vectors with arbitrary content, serialised to Lists.
newtype VectorG r a = VectorG {getVectorG :: Massiv.Vector r a}

deriving instance Functor (Massiv.Array r Massiv.Ix1) => Functor (VectorG r)

deriving instance Show (Vector r e) => Show (VectorG r e)

deriving instance Eq (Vector r e) => Eq (VectorG r e)

instance Semigroup (Massiv.Vector r a) => Semigroup (VectorG r a) where
  a <> b = VectorG $ getVectorG a <> getVectorG b

instance (ToJSON a, Massiv.Source r a) => ToJSON (VectorG r a) where
  toJSON arr = toJSON . Massiv.toList . getVectorG $ arr

instance (FromJSON a, Massiv.Mutable r a) => FromJSON (VectorG r a) where
  parseJSON v = do
    l <- parseJSON @[a] v
    return . VectorG $ Massiv.fromList Par l

----------------------------------------------------------------------------------------------------

-- | Matrices with arbitrary content, serialised to Lists of Lists.
newtype MatrixG r a = MatrixG {getMatrixG :: Massiv.Matrix r a}

instance (ToJSON a, Massiv.Source r a, Massiv.Shape r Massiv.Ix2) => ToJSON (MatrixG r a) where
  toJSON arr = toJSON . Massiv.toLists . getMatrixG $ arr

instance (FromJSON a, Massiv.Manifest r a) => FromJSON (MatrixG r a) where
  parseJSON v = do
    ll <- parseJSON @[[a]] v
    case Massiv.fromListsM Par ll of
      Nothing -> fail "Could not parse list of lists as matrix"
      Just arr -> return . MatrixG $ arr

{-
====================================================================================================
-}

-- $textOperations
-- Operations on strict 'Text'.

-- | Wrapper around RIO's writing of unicode formatted text to a file ('writeFileUtf8'), compatible with
-- typed paths.
writeFileUTF8 :: MonadIO m => Path.AbsRelFile -> Text -> m ()
writeFileUTF8 path' text' = writeFileUtf8 (Path.toString path') text'

----------------------------------------------------------------------------------------------------

-- | Wrapper around RIO's reading of unicode formatted text to a file ('readFileUtf8'), compatible with
-- typed paths.
readFileUTF8 :: MonadIO m => Path.AbsRelFile -> m Text
readFileUTF8 path' = readFileUtf8 (Path.toString path')

----------------------------------------------------------------------------------------------------

-- | Appending for UTF-8 encoded files in RIO's style of writing formatted text to a file,
-- compatible with typed paths.
appendFileUTF8 :: MonadIO m => Path.AbsRelFile -> Text -> m ()
appendFileUTF8 path' text' = liftIO . Path.withFile path' Path.AppendMode $ \h -> do
  hSetEncoding h utf8
  T.hPutStr h text'

----------------------------------------------------------------------------------------------------

-- | Convert a 'Text' to an Utf8Builder as used by RIO.
text2Utf8Builder :: Text -> Utf8Builder
text2Utf8Builder = Utf8Builder . Builder.byteString . Text.encodeUtf8

----------------------------------------------------------------------------------------------------

-- | Removes all white space, even between words, from a text.
removeWhiteSpace :: Text -> Text
removeWhiteSpace = Text.concat . Text.words

----------------------------------------------------------------------------------------------------

-- | 'tell' with an appended line break.
tellN :: (IsString w, MonadWriter w m) => w -> m ()
tellN c = tell $ c <> "\n"

{-
====================================================================================================
-}

-- $utf8builderOperations
-- Operation on and with RIO's 'Utf8Builder' strings.

-- | Append an 'Utf8Builder' to a file.
appendFileUtf8 :: MonadIO m => Path.AbsRelFile -> Utf8Builder -> m ()
appendFileUtf8 path' (Utf8Builder b) = liftIO . Path.withFile path' Path.AppendMode $ \h -> do
  hSetEncoding h utf8
  BB.hPutBuilder h b

{-
====================================================================================================
-}

-- $byteStringOperations
-- Operations on Bytestrings.

-- | Convert a lazy bytestring to a strict one.
byteStringLazy2Strict :: BL.ByteString -> ByteString
byteStringLazy2Strict = BL.toStrict

----------------------------------------------------------------------------------------------------

-- | Convert a strict bytestring to a lazy one.
byteStringStrict2Lazy :: ByteString -> BL.ByteString
byteStringStrict2Lazy = BL.fromStrict

----------------------------------------------------------------------------------------------------

-- | Converts a strict bytestring to an UTF8Builder.
byteString2Utf8Builder :: ByteString -> Utf8Builder
byteString2Utf8Builder = Utf8Builder . Builder.byteString
