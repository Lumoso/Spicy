let
  pkgs = import ./pkgs.nix;
  nixpkgs = pkgs.nixpkgs;
  haskellPkgs = pkgs.haskellPkgs;

  # Configuration File.
  spicyrc = with nixpkgs; writeTextFile {
    name = "spicyrc";
    text = lib.generators.toYAML {} ({
      "psi4" = "${qchem.psi4}/bin/psi4";
      "gdma" = "${qchem.gdma}/bin/gdma";
      "pysisyphus" = "${qchem.pysisyphus}/bin/pysis";
      "xtb" = "${qchem.xtb}/bin/xtb";
      "ipi" = "${qchem.i-pi}/bin/i-pi";
    } // lib.attrsets.optionalAttrs (qchem.turbomole != null) {"turbomole" = "${qchem.turbomole}/bin";}
    );
  };

in spicyrc
