let nixpkgs = (import ./pkgs.nix).nixpkgs;
in {
  spicy = nixpkgs.haskellPackages.spicy;

  spicyWrapped = nixpkgs.spicy;

  spicyStatic = nixpkgs.pkgsCross.musl64.haskellPackages.spicy;
}
