let
  sources = import ./sources.nix;
  qchemOvl = import "${sources.NixOS-QChem}/overlay.nix";
  # Adapt the set of non-free packages as you need.
  postOverlay = self: super:
    { qchem = super.qchem // {
        vmd = null;
        molpro = null;
        qdng = null;
        mesa = null;
        orca = null;
        cfour = null;
        gaussview = null;
        mctdh = null;
        turbomole = null;
        mrcc = null;
      };
    };

  # Fix the packages for nixkpkgs, to be able to build with nixpkgs directly. Also add spicy to the
  # package set.
  haskellFixOvl = self: super: {
    haskellPackages = super.haskellPackages.extend (hself: hsuper: {
       massiv_1_0_1_1 = hsuper.massiv_1_0_1_1.override  {
         scheduler = hself.scheduler_2_0_0_1;
       };

       optics_0_4 = hsuper.optics_0_4.override {
         optics-core = hself.optics-core_0_4;
         optics-extra = hself.optics-extra_0_4;
         optics-th = hself.optics-th_0_4;
       };

       spicy =
         let auxIgnores = [
               "notebooks/"
               "nix/"
               ".gitlab-ci.yml"
               ".hlint.yml"
               "hie.yaml"
               "README.md"
               "LICENSE"
             ];
          in (hsuper.callCabal2nix "spicy" (super.nix-gitignore.gitignoreSource auxIgnores ../.) {
               massiv = hself.massiv_1_0_1_1;
               optics = hself.optics_0_4;
             }).overrideAttrs (oldAttrs: rec {
               doCheck = true;
               doInstallCheck = true;
             });
        spicyNoCheck = hself.spicy.overrideAttrs (oldAttrs: {
          doCheck = false;
          doInstallCheck = false;
        });
     });

     spicy = super.callPackage ./spicy-wrapped.nix {
       spicy = self.haskellPackages.spicy;
     };
   };

  nixpkgs = import sources.nixpkgs {
    overlays = [ qchemOvl haskellFixOvl postOverlay ];
    config = {
      allowUnfree = true;
      qchem-config = {
        allowEnv = true;
        optAVX = false;
        optArch = null;
      };
    };
  };

  # Pysisyphus development version
  pysisyphus = nixpkgs.python3.pkgs.callPackage "${sources.pysisyphus}/nix" { };

in { inherit nixpkgs pysisyphus; }
