let
  pkgs = import ./pkgs.nix;
  nixpkgs = pkgs.nixpkgs;

  # Spicy runtime configuration setup.
  spicyrc = import ./spicyrc-generator.nix;

  haskellEnv = nixpkgs.haskellPackages.ghcWithPackages (p: with p; [
  ] ++ spicy.propagatedBuildInputs
    ++ spicy.buildInputs
  );

in with nixpkgs; mkShell {
  buildInputs = [
    # Spicy's Haskell dependencies
    haskellEnv

    # Haskell toolings
    cabal-install
    haskell-language-server
    hlint
    hpack
    niv
    ormolu

    # Quantum chemistry
    qchem.pysisyphus
    qchem.psi4
    qchem.gdma
    qchem.xtb
  ] ++ lib.optional (qchem.turbomole != null) qchem.turbomole
  ;

  shellHook = ''
    export SPICYRC=${spicyrc}
  '';
}
