{ stdenvNoCC, lib, makeWrapper, spicy }:

stdenvNoCC.mkDerivation {
  pname = "spicy";
  version = "dev";

  phases = [ "installPhase" ];

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin
    cp ${spicy}/bin/spicy $out/bin/
    wrapProgram $out/bin/spicy \
      --set SPICYRC ${import ./spicyrc-generator.nix}
  '';
}
