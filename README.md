# Spicy
[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)
[![pipeline status](https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy/badges/develop/pipeline.svg)](https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/Spicy/-/commits/develop)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

Spicy is a Haskell program for quantum chemistry and quantum dynamics and aims at providing a set of composable ONIOM methods, that are not widely available.
Spicy will not implement quantum chemistry calculations itself, but rather wrap different quantum chemistry programs.

## Installing / Building
### Nix
Spicy can be build with Nix.
This guarantees fully reproducible builds and saves you from the hurdles of installing *many* packages yourself.
Make sure to have the [Nix package manager installed on your system](https://nixos.org/download.html).
To save some time (aka many hours and gigabytes of RAM ...), you may want to add the [Cachix cache for NixOS-QChem](https://github.com/markuskowa/NixOS-QChem#binary-cache).
Have a look at `./nix/pkgs.nix`, which defines the package set, that is being used for all builds.
The `postOverlay` overrides NixOS-QChem packages and can `null` some proprietary packages, that could be used as build-dependencies.
If you do not have access to those proprietary packages, set them to `null`, or remove their nullification, if you would like to use them.

Change to the `nix/` directory (`cd nix`) and choose the build you want:


  * Spicy as a plain Haskell package and library
      ```
      nix-build -A spicy
      ```
  * A ready to use Spicy for command line usage with all available wrappers configured (depending on your settings in `nix/pkgs.nix`, especially the `postOverlay`)
      ```
      nix-build -A spicyWrapped
      ```
  * A fully statically linked Spicy, not requiring any runtime libraries, but without any quantum chemistry codes baked in.
    You will need to take care of those later by providing a `spicyrc` file.
    ```
    nix-build -A spicyStatic
    ```

### Manually
To build Spicy without Nix, you will need a [working Haskell toolchain](https://www.haskell.org/ghcup/) including a recent GHC (>= 8.10) and cabal.
The runtime dependencies (quantum chemistry software) need to be installed and configured manually.
- [Psi4](https://psicode.org/)
- [GDMA](https://gitlab.com/anthonyjs/gdma)
- [XTB](https://xtb-docs.readthedocs.io/en/latest/contents.html#)
- [Pysisyphus](https://pysisyphus.readthedocs.io/en/latest/)
- [Turbomole](https://www.turbomole.org/)

A YAML file is required to point Spicy to the executables (or wrapper scripts with same call convention), e.g.
```yaml
psi4: /opt/psi4/bin/psi4
xtb: /opt/xtb/bin/xtb
gdma: /opt/gdma/bin/gdma
pysisyphus: /opt/pysisyphus/bin/pysis
turbomole: /opt/turbomole/bin/emt64-unknown-linux-gnu_smp
```
Note that for Turbomole, the path points to the directory of the appropriate executables, instead of a single executable.
Also, for Turbomole its environment variables and setup scripts have to be loaded.
When building with Nix, you save yourself from these hurdles.
The environment variable `SPICYRC` should point to this YAML file.

## Using Jupyter Notebooks
The `notebooks` directory contains Jupyter notebooks, which are used for experiments and to work with different implementations and ideas.
Jupyterlab with all required dependencies can be started from within a nix-shell.

```bash
cd notebooks
nix-shell --run "jupyter lab"
```
