{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "890f3b62-334b-4f08-8f70-0abb78ed0f16",
   "metadata": {},
   "outputs": [],
   "source": [
    "-- Enable language extensions\n",
    "{-# LANGUAGE \n",
    "      OverloadedStrings,\n",
    "      NoImplicitPrelude,\n",
    "      FlexibleContexts,\n",
    "      ScopedTypeVariables,\n",
    "      OverloadedLabels,\n",
    "      FlexibleInstances,\n",
    "      MultiParamTypeClasses,\n",
    "      DuplicateRecordFields,\n",
    "      FlexibleInstances,\n",
    "      TypeApplications,\n",
    "      RecordWildCards,\n",
    "      TypeFamilies,\n",
    "      TupleSections\n",
    "  #-}\n",
    "\n",
    "-- Load modules\n",
    "import           Data.Bifunctor (second)\n",
    "import           Data.Graph.Inductive hiding ((&))\n",
    "import qualified Data.IntSet as IntSet\n",
    "import qualified Data.IntMap as IntMap\n",
    "import           Data.GraphViz\n",
    "import           Data.GraphViz.Attributes.Complete\n",
    "import           Data.Massiv.Array as Massiv\n",
    "import           Data.Text.IO\n",
    "import           Data.Tree\n",
    "import qualified RIO.HashMap as HashMap\n",
    "import qualified RIO.List as List\n",
    "import qualified RIO.Map as Map\n",
    "import qualified RIO.Set as Set\n",
    "import qualified RIO.Set.Partial as Set\n",
    "import qualified RIO.Set.Unchecked as Set\n",
    "import           RIO.State\n",
    "import           RIO.Text.Lazy (fromStrict)\n",
    "import qualified RIO.Text as Text\n",
    "import           Spicy.Attoparsec\n",
    "import           Spicy.FragmentMethods.SMF\n",
    "import           Spicy.FragmentMethods.GMBE\n",
    "import           Spicy.Math.Combinatorics\n",
    "import           Spicy.Molecule.Internal.Parser (xyz)\n",
    "import           Spicy.Molecule.Internal.Physical\n",
    "import           Spicy.Molecule.Internal.Util (guessBondMatrix, mol2Graph, changeBond)\n",
    "import           Spicy.Molecule.Molecule\n",
    "import           Spicy.Prelude as P\n",
    "import           Optics\n",
    "\n",
    "-- Helper functions\n",
    "prepMolDotGraph :: MolGraph -> Gr Text Text\n",
    "prepMolDotGraph molGr = gmap cntxtF molGr\n",
    "  where\n",
    "    bondF bondOrder = case bondOrder of\n",
    "      Single -> \"s\"\n",
    "      Multiple -> \"m\"\n",
    "      Coordinative -> \"c\"      \n",
    "    adjF (bondOrder, ngbhrIx) = (bondF bondOrder, ngbhrIx)\n",
    "    cntxtF (inLinks, aIx, a, outLinks) = (fmap adjF inLinks, aIx, tshow (a ^. #element, aIx), fmap adjF outLinks)\n",
    "    \n",
    "prepGroupDotGraph :: Gr Group () -> Gr Text Text\n",
    "prepGroupDotGraph groupGr = gmap cntxtF groupGr\n",
    "  where\n",
    "    cntxtF (inLinks, grIx, gr, outLinks) = (fmap adjF inLinks, grIx, tshow . IntSet.toAscList $ gr ^. #atoms, fmap adjF outLinks)\n",
    "    adjF (_, ngbhrIx) = (mempty, ngbhrIx)    \n",
    "\n",
    "-- Load a test system and assign physical properties.\n",
    "molRaw <- readFileUtf8 \"data/gmbe/molecule.xyz\" >>= parse' xyz\n",
    "bondMatrix <- guessBondMatrix Nothing (molRaw ^. #atoms) mempty\n",
    "let molBondsGuess = molRaw\n",
    "      -- Assign correct electronic structure\n",
    "      & #atoms % ix 18 % #formalCharge .~ (-1)\n",
    "      & #atoms % ix 40 % #formalCharge .~ (-1)\n",
    "      & #atoms % ix 41 % #formalCharge .~ 2\n",
    "      & #atoms % ix 41 % #formalExcEl .~ 2\n",
    "      -- Assign the guessed bond matrix\n",
    "      & #bonds .~ bondMatrix\n",
    "mol <- do\n",
    "  let bondF bondOrder accMol atomPair = accMol >>= \\m -> changeBond bondOrder m atomPair\n",
    "  molMultiAdj <- foldl' (bondF Multiple) (return molBondsGuess) [(13,16), (13,17), (13,18)]\n",
    "  molSingleAdj <- foldl' (bondF Single) (return molMultiAdj) [(22,25)]\n",
    "  return molSingleAdj"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c54a3e8-cd41-44ac-b05e-681b3b2974ff",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Generalised Many-Body-Expansion (GMBE)\n",
    "\n",
    "Literature:\n",
    "  - [Original GMBE paper](https://aip.scitation.org/doi/full/10.1063/1.4742816)\n",
    "\n",
    "For the purposes of GMBE, we can view a molecule as a set of atoms\n",
    "\\begin{equation}\n",
    "  A = \\{ a_1, a_2, a_3, \\dots \\}\n",
    "\\end{equation}\n",
    "that are connected by bonds of different bond orders. \n",
    "In the context of GMBE/SMF we will only distinguish three bond orders: single bonds ($s$), coordinative bonds ($c$), and multibonds ($m$).\n",
    "The bonds between the atoms can be stored in a sparse bond matrix $\\boldsymbol{B}$.\n",
    "Naturally, a molecule can be represented as a undirected graph, with labeled vertices and labeled edges, where the labels are the atoms and their properties, respective the bond orders.\n",
    "\n",
    "A graph representation of the example molecule looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8e1d4f0a-901c-464f-8c24-3c7f5bcadb75",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"data/gmbe/MolGraph.png\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "-- Make a undirected graph from the molecule and visualise it\n",
    "let molGraph = mol2Graph mol\n",
    "    molDotGraph = graphToDot (quickParams { isDirected = False }) . prepMolDotGraph $ molGraph\n",
    "runGraphviz molDotGraph Png \"data/gmbe/MolGraph.png\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e5379ce-b168-488a-8c5d-7a9eb7b1c672",
   "metadata": {},
   "source": [
    "![MolGraph](data/gmbe/MolGraph.png)\n",
    "\n",
    "The molecular graph can now be used to find \"groups\", where a group is defined by a non-separable set of atoms, which are a subset of all atoms. Furthermore, groups never overlap.\n",
    "\\begin{align}\n",
    "  G_i &\\subseteq A \\\\\n",
    "  G_i &\\cap G_j = \\emptyset \\quad \\forall i \\neq j\n",
    "\\end{align}\n",
    "The groups are usually found by the \"Systematic Molecule Fragmentation\" (SMF) algorithm:\n",
    "  1. Delete all bonds $s$, that do not involve a hydrogen atom. This disconnects large part of the molecule graph.\n",
    "  2. Look for charged or spin-polarised groups. Those groups re-introduce an edge to their direct neighbours.\n",
    "  3. Disjoint subgraphs form the individual groups $G$\n",
    "\n",
    "We can now draw a new graph (a hypergraph), that is a graph of groups with unlabeled edges, only indicating that the groups are connected at all."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "5b7cabbd-b478-4abf-af46-39aa25692269",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"data/gmbe/GroupGraph.png\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "G_0 = [1,2,3,4,5,6,7,8,9,11]\n",
       "G_1 = [10,20,21]\n",
       "G_2 = [12,13,14,15,16,17,18]\n",
       "G_3 = [19,22,23,25,39,40,41,42,43,44,45,46,47,48,49]\n",
       "G_4 = [24,28]\n",
       "G_5 = [26,37,38]\n",
       "G_6 = [27,30,31]\n",
       "G_7 = [29,33,34]\n",
       "G_8 = [32,35,36]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "-- Apply SMF to the molecular graph and reduce the molecular graph to a hyper-graph of groups.\n",
    "let groups = makeSMFgroups molGraph\n",
    "    groupGraph = toGroupGraph molGraph groups\n",
    "    groupDotGraph = graphToDot (defaultParams { isDirected = False }) . prepGroupDotGraph $ groupGraph\n",
    "runGraphviz groupDotGraph Png \"data/gmbe/GroupGraph.png\"\n",
    "\n",
    "-- Show all groups\n",
    "void $ IntMap.traverseWithKey (\\k g -> putStrLn $ \"G_\" <> tshow k <> \" = \" <> tshow (IntSet.toAscList $ g ^. #atoms)) groups"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5246f7ba-0f2c-48ff-a8cf-03224910ec3e",
   "metadata": {},
   "source": [
    "![GroupGraph](data/gmbe/GroupGraph.png)\n",
    "\n",
    "And we have obtained a set of groups, each of which is basically represented by a set of atoms. We can now form potentially overlapping fragments from those groups, which will form the basis of the GMBE. Fragments are therefore sets of groups.\n",
    "\\begin{equation}\n",
    "  F_i = \\{ G_a, G_b, \\dots \\}\n",
    "\\end{equation}\n",
    "There are multiple alternatives to define fragments:\n",
    "  1. Form all $n$-combinations of groups, potentially within a distance cutoff, e.g. $n=2$ all dimers.\n",
    "  2. Iterate over all groups and form ego-graphs of order $n$.\n",
    "  3. Same as 2, but remove all groups from the graph, that have already been assigned to another ego-graph.\n",
    "  4. Systematic Molecule Fragmentation By Annihilation (SMFA); sophisticated recursive algorithm, that eliminates groups from the graph, until a given fragment size has been reached.\n",
    "Options 2, 3, and 4 usually employ ring-avoidance rules.\n",
    "As a post processing step for the options 2, 3, and 4, $n$-combinations of the so obtained fragments may be formed again, leading to even more overlap.\n",
    "\n",
    "We are finally interested in calculating the energy $E$ and its first and second derivatives (gradient $\\boldsymbol{g} = \\nabla E$ and Hessian $\\boldsymbol{H} = \\nabla^2 E$) with respect to nuclear coordinates for the full system. GMBE gives us an expression to assemble calculations on small fragments into a term for the full system. For the energy, it does so, by summing up \"intersection\" corrected energies, where the \"intersection\" correction is necessary, as fragments may arbitrarily overlap and atoms are therefore counted multiple times. GMBE follow the principle of inclusion-exclusion to give us a valid expression:\n",
    "\\begin{equation}\n",
    "  E = \\sum\\limits_{\\alpha = 1}^N \\varepsilon_\\alpha\n",
    "\\end{equation}\n",
    "Here, $\\varepsilon_\\alpha$ is the intersection corrected energy of $F_\\alpha$, and $N$ is a linearised index over all our fragments. The intersection corrected energy for a fragment is now defined as:\n",
    "\\begin{equation}\n",
    "  \\varepsilon_\\alpha \n",
    "    = E_\\alpha \n",
    "    - \\sum\\limits_{\\substack{\\beta \\\\ (\\beta > \\alpha)}} E_{F_\\alpha \\cap F_\\beta}\n",
    "    + \\sum\\limits_{\\substack{\\beta, \\gamma \\\\ (\\gamma > \\beta > \\alpha)}} E_{F_\\alpha \\cap F_\\beta \\cap F_\\gamma}\n",
    "    - \\dots\n",
    "    + (-1)^{N - \\alpha} E_{F_\\alpha \\cap F_{\\alpha + 1} \\cap \\dots \\cap F_N}\n",
    "\\end{equation}\n",
    "\n",
    "The gradients and Hessians can be collected by the same equation and just need to be transformed from the basis of the fragments into the basis of the full system:\n",
    "\\begin{align}\n",
    "  \\boldsymbol{g}_\\alpha^{(P)} &= \\boldsymbol{g}_\\alpha \\boldsymbol{J}_\\alpha \\\\\n",
    "  \\boldsymbol{H}_\\alpha^{(P)} &= \\boldsymbol{J}_\\alpha^T \\boldsymbol{H}_\\alpha \\boldsymbol{J}_\\alpha\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "72a70b94-adf3-4a99-98fa-0718c54f6fcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "-- Simplify the groups for the example and strip away the physics\n",
    "let simpleGroups :: IntMap IntSet\n",
    "    simpleGroups = fmap (^. #atoms) groups\n",
    "\n",
    "-- Define fragments\n",
    "let frags = Set.fromList . fmap IntSet.fromList $ \n",
    "      [ [0,1,2,3],\n",
    "        [1,3,4],\n",
    "        [2,0,1],\n",
    "        [4,3,5,6],\n",
    "        [5,4,8],\n",
    "        [6,4,7],\n",
    "        [7,6,8],\n",
    "        [8,5,7],\n",
    "        [4,5,6,7,8]\n",
    "      ]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5f81f65-519e-4ffb-9ce0-33afbc966d5a",
   "metadata": {},
   "source": [
    "## Naive Implementation\n",
    "My naive implementation is based on the observation, that every intersection corrected energy can be expressed as a tree of intersections. Starting with the root of the tree and a fragment $F_\\alpha$, the first level of the tree would contain all intersections of $F_\\alpha$ with all fragments at a larger index, at the third level all intersections with an index larger than that of the parent node and so on. Unfortunately, this leads to enormous tree sizes, especially when $n$-combinations are used. Each level will introduce $(n - \\alpha - l)!$ (where $l$ is the level) and $n - \\alpha$ levels are required. The depth in the tree determines the sign in the sum and all intersections at a given level form one of the terms in the sum. This has to be repeated for every fragment of the original set, therefore we obtain a forest (a list of trees), that forms the final sum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "ddd0d787-18e4-4957-aeed-e75a88462943",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0,1,2,3] |-> 1\n",
       "[1,3] |-> -1\n",
       "[1,3,4] |-> 1\n",
       "[3,4] |-> -1\n",
       "[3,4,5,6] |-> 1\n",
       "[4,5,6] |-> -1\n",
       "[4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "-- Form fragments and then unfold the forest\n",
    "let fragmentsIndexed = \n",
    "      let fragVec = Massiv.fromList @B Par . Set.toList $ frags\n",
    "          groupKeys = IntMap.keys simpleGroups\n",
    "          Sz n = Massiv.size fragVec\n",
    "      in  Massiv.compute @B $ Massiv.szip (0 ... n - 1) fragVec\n",
    "    forest = \n",
    "      let unfoldF :: (Int, Fragment) -> (Fragment, [(Int, Fragment)])\n",
    "          unfoldF (alpha, alphaFrag) = \n",
    "            let (_, betaFrags) = sliceAt (Sz $ alpha + 1) fragmentsIndexed\n",
    "                abIntersections = Massiv.map (second (IntSet.intersection alphaFrag)) betaFrags\n",
    "                nonEmptyABintersections = Massiv.sfilter (\\(_, abFrag) -> not . IntSet.null $ abFrag) abIntersections\n",
    "            in  (alphaFrag, Massiv.stoList nonEmptyABintersections)\n",
    "      in  unfoldForest unfoldF . Massiv.stoList $ fragmentsIndexed\n",
    "\n",
    "-- Make each tree of the forest into a map of terms\n",
    "collapsTree :: Tree Fragment -> GMBEsum\n",
    "collapsTree tr = \n",
    "  let factorFrags = \n",
    "        simap (\\i lvlFrags -> \n",
    "          let factor = if even i then 1 else (-1)\n",
    "          in  List.zip lvlFrags (List.repeat factor)\n",
    "        ) . Massiv.sfromList \n",
    "          . levels \n",
    "          $ tr\n",
    "      levelMaps = fmap (Map.fromListWith (+)) factorFrags\n",
    "  in  Map.filter (/= 0) . Map.unionsWith (+) $ levelMaps\n",
    "\n",
    "-- Collaps all trees and collect all intersection corrected energies\n",
    "collapsForest :: (Foldable f, Functor f) => f (Tree Fragment) -> GMBEsum\n",
    "collapsForest frst = Map.filter (/=0) . Map.unionsWith (+) . fmap collapsTree $ frst\n",
    "\n",
    "-- Calculate the final GMBE sum\n",
    "let gmbeSum = collapsForest forest\n",
    "void . flip Map.traverseWithKey gmbeSum $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82656e7b-3b7f-4b6a-9189-c7afd8f0df11",
   "metadata": {},
   "source": [
    "## Improved Implementation\n",
    "Based on the ideas of [Fragment Combination Ranges](https://aip.scitation.org/doi/pdf/10.1063/5.0059598), a more efficient implementation of intersection corrected GMBE terms can be given.\n",
    "The FCR approach expresses a many body expansion as:\n",
    "\\begin{equation}\n",
    "  E(G_1, G_2, \\dots G_n) = \n",
    "    \\sum\\limits_\\alpha^n \\bar{E}^{(1)}_\\alpha (G_\\alpha) + \n",
    "    \\sum\\limits_{\\beta > \\alpha} \\bar{E}^{(2)}_{\\alpha \\beta} (G_\\alpha, G_\\beta) + \n",
    "    \\sum\\limits_{\\gamma > \\beta > \\alpha} \\bar{E}^{(3)}_{\\alpha \\beta \\gamma} (G_\\alpha, G_\\beta, G_\\gamma) +\n",
    "    \\dots\n",
    "\\end{equation}\n",
    "and all contributions $E^{(n)}$ of order $n$ would be given by an MBE-like:\n",
    "\\begin{align}\n",
    "  \\bar{E}_\\alpha^{(1)} (G_\\alpha) =& E_\\alpha^{(1)} (G_\\alpha) \\\\\n",
    "  \\bar{E}_{\\alpha \\beta}^{(2)} (G_\\alpha, G_\\beta) \n",
    "    =& E_{\\alpha \\beta}^{(2)} (G_\\alpha, G_\\beta) \\\\\n",
    "    -& \\bar{E}_\\alpha^{(1)} (G_\\alpha) - \\bar{E}_\\beta^{(1)} (G_\\beta) \\\\\n",
    "  \\bar{E}_{\\alpha \\beta \\gamma}^{(3)} (G_\\alpha, G_\\beta, G_\\gamma) \n",
    "    =& E_{\\alpha \\beta \\gamma}^{(3)} (G_\\alpha, G_\\beta) \\\\ \n",
    "    -& \\bar{E}_{\\alpha \\beta}^{(2)} (G_\\alpha, G_\\beta) - \\bar{E}_{\\alpha \\gamma}^{(2)} (G_\\alpha, G\\gamma) - \\bar{E}_{\\beta \\gamma}^{(2)} (G_\\beta, G_\\gamma) \\\\\n",
    "    +& \\bar{E}_\\alpha^{(1)} (G_\\alpha) + \\bar{E}_\\beta^{(1)} (G_\\beta) + \\bar{E}_\\gamma^{(1)} (G_\\gamma) \\\\\n",
    "  \\dots&\n",
    "\\end{align}\n",
    "Now the so called fragment combination ranges are defined, that include all combinations of groups, that appear in the given expansion:\n",
    "\\begin{equation}\n",
    "  E(G_\\alpha, G_\\beta, \\dots G_\\gamma) = \\sum\\limits_{\\mathbf{c}_l \\in \\{\\mathrm{FCR}\\}} \\bar{E}^{\\mathbf{c}_l} ( \\{G\\}^{\\mathbf{c}_l})\n",
    "\\end{equation}\n",
    "and $\\mathbf{c}_l$ is a fragment combination range of order $l$.\n",
    "Now the fragment combination range of any fragment $F$ is simply the number its constituting groups.\n",
    "$\\{G\\}^{\\mathbf{c}_l}$ Therefore denotes all its possible subsets, e.g. for a fragment of size $i$ all $[1 \\dots i]$-combinations of its constituting groups, where the $i$-combination of course gives the fragment itself.\n",
    "This property is called \"closed on forming subsets\" in the paper.\n",
    "The properties of a particular fragment can then be written as:\n",
    "\\begin{align}\n",
    "  \\bar{E}^{\\mathbf{c}_l} (\\{G\\}^{\\mathbf{c}_l}) &= \n",
    "    E^{\\mathbf{c}_l} (\\{G\\}^{\\mathbf{c}_l}) - \\sum\\limits_{\\mathbf{c}_s \\subset \\mathbf{c}_l} \\bar{E}^{\\mathbf{c}_s} (\\{G\\}^{\\mathbf{c}_s}) \\\\  \n",
    "  &= \\sum\\limits_{\\substack{\\mathbf{c}_s \\subseteq \\mathbf{c}_l \\\\ \\mathbf{c}_s \\in \\{\\mathrm{FCR}\\}}} k^{\\mathbf{c}_s, \\mathbf{c}_l} E^{\\mathbf{c}_s} (\\{G\\}^{\\mathbf{c}_s}) \\\\\n",
    "  k^{\\mathbf{c}_s, \\mathbf{c}_l} &= (-1)^{l - s}  \n",
    "\\end{align}\n",
    "The paper now claims that this term is equivalent to the intersection corrected energy/property of GMBE.\n",
    "This is not completely true; it only holds if the fragment combination range was actually built MBE-like, but it is not true, if arbitrary fragments are allowed.\n",
    "However, this does not matter; summing up the contribution from the initial set of fragments, cancels the terms, that do not appear in GMBE's intersection corrected energy exactly and the FCR-sum is then the GMBE-sum."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f1cdef26-ec42-4ccd-8ff5-d157f4ed1132",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0,1,2,3] |-> 1\n",
       "[1,3] |-> -1\n",
       "[1,3,4] |-> 1\n",
       "[3,4] |-> -1\n",
       "[3,4,5,6] |-> 1\n",
       "[4,5,6] |-> -1\n",
       "[4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "-- Build the GMBE terms by the FCR formalism. See \"Spicy.FragmentMethods.GMBE\" for details.\n",
    "let fcrGMBEres = fcrSum . frags2FCR $ frags\n",
    "\n",
    "void . flip Map.traverseWithKey fcrGMBEres $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 74,
   "id": "66621d40-64bc-414e-bc2b-9bd57ec05675",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "FCR(U)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[0,1,2,3,4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "--------------"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "FCR(HL)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[4,5,6] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "--------------"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "FCR(ML)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "--------------"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "FCR(LL)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[0,1,2,3,4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "=============="
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Coeffs(HL)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[4,5,6] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "--------------"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Coeffs(ML)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[4,5,6] |-> -1\n",
       "[4,5,6,7,8] |-> 1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "--------------"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Coeffs(LL)"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[0,1,2,3,4,5,6,7,8] |-> 1\n",
       "[4,5,6,7,8] |-> -1"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "=============="
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "Check"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[0,1,2,3,4,5,6,7,8] |-> 1\n",
       "[4,5,6] |-> 0\n",
       "[4,5,6,7,8] |-> 0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "-- Test the multilayer FCR formalism\n",
    "let fragsLL = Set.fromList . fmap IntSet.fromList $ \n",
    "      [ [0,1,2,3,4,5,6,7,8] ]\n",
    "    fragsML = Set.fromList . fmap IntSet.fromList $ \n",
    "      [ [4,5,6,7,8] ]\n",
    "    fragsHL = Set.fromList . fmap IntSet.fromList $ \n",
    "      [ [4,5,6]\n",
    "      ]\n",
    "\n",
    "-- Plainly generating the fragment combination ranges for a set of fragments without \n",
    "-- the clever constructions.\n",
    "makeFCR :: Set Fragment -> Set Fragment\n",
    "makeFCR frags = Set.unions . fmap makeFragFCR . Set.toList $ frags\n",
    "  where\n",
    "    makeFragFCR :: Fragment -> Set Fragment\n",
    "    makeFragFCR frg = Set.fromList . concat . intNcombinations (fromIntegral . IntSet.size $ frg) $ frg\n",
    "\n",
    "-- Fragment Combination ranges and GMBE for all three layers\n",
    "fcrLL = frags2FCR fragsLL\n",
    "fcrML = frags2FCR fragsML\n",
    "fcrHL = frags2FCR fragsHL\n",
    "fcrSumLL = fcrSum fcrLL\n",
    "fcrSumML = fcrSum fcrML\n",
    "fcrSumHL = fcrSum fcrHL\n",
    "\n",
    "-- Fragment combination range for the union of all layers and coefficients for those.\n",
    "fcrU = Set.unions [fcrLL, fcrML, fcrHL]\n",
    "fcrSumU = fcrSum fcrU\n",
    "fcrHLuML = Set.union fcrHL fcrML\n",
    "fcrSumHLuML = fcrSum fcrHLuML\n",
    "fcrMLuLL = Set.union fcrHL fcrLL\n",
    "fcrSumMLuLL = fcrSum fcrMLuLL\n",
    "\n",
    "-- Coefficients for a 3-layer version\n",
    "coeffsHL = fcrSumHL\n",
    "coeffsML = Map.unionWith (+) fcrSumHLuML . fmap negate $ fcrSumHL\n",
    "coeffsLL = Map.unionWith (+) fcrSumMLuLL . fmap negate $ fcrSumML\n",
    "\n",
    "-- Summ of all the coefficients.\n",
    "fcrMLcheck = Map.unionsWith (+) [coeffsHL, coeffsML, coeffsLL]\n",
    "\n",
    "-- Printing\n",
    "putStrLn \"FCR(U)\"\n",
    "void . flip Map.traverseWithKey fcrSumU $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"--------------\"\n",
    "putStrLn \"FCR(HL)\"\n",
    "void . flip Map.traverseWithKey fcrSumHL $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"--------------\"\n",
    "putStrLn \"FCR(ML)\"\n",
    "void . flip Map.traverseWithKey fcrSumML $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"--------------\"\n",
    "putStrLn \"FCR(LL)\"\n",
    "void . flip Map.traverseWithKey fcrSumLL $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"==============\"\n",
    "putStrLn \"Coeffs(HL)\"\n",
    "void . flip Map.traverseWithKey coeffsHL $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"--------------\"\n",
    "putStrLn \"Coeffs(ML)\"\n",
    "void . flip Map.traverseWithKey coeffsML $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"--------------\"\n",
    "putStrLn \"Coeffs(LL)\"\n",
    "void . flip Map.traverseWithKey coeffsLL $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac\n",
    "putStrLn \"==============\"\n",
    "putStrLn \"Check\"\n",
    "void . flip Map.traverseWithKey fcrMLcheck $ \\frag fac -> putStrLn $ tshow (IntSet.toAscList frag) <> \" |-> \" <> tshow fac"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "haskell",
   "file_extension": ".hs",
   "mimetype": "text/x-haskell",
   "name": "haskell",
   "pygments_lexer": "Haskell",
   "version": "8.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
