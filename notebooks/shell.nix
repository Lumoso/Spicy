let
  pkgs = (import ../nix/pkgs.nix).nixpkgs;
  jupyterWith = import (builtins.fetchGit {
    url = "https://github.com/tweag/jupyterWith";
  });
  haskellEnv = pkgs.haskellPackages.ghcWithPackages (p: with p; [
    ihaskell
    ihaskell-charts
    ihaskell-blaze
    spicy
    rio
    attoparsec
    massiv_1_0_1_1
    containers
    fgl
    optics_0_4
    graphviz
  ]);
  pythonEnv = pkgs.python3.withPackages (p: with p; [
    jupyterlab
    numpy
    scipy
    pandas
  ]);
  latexEnv = pkgs.texlive.combine {
    inherit (pkgs.texlive)
      scheme-small
      xetex
      tcolorbox
      environ
      upquote
      ucs
      adjustbox
      collectbox
      titling
      enumitem
      rsfs
    ;
  };
in with pkgs; mkShell {
  buildInputs = [
    haskellEnv
    pythonEnv
    latexEnv
    graphviz
    pandoc
  ];
  shellHook = ''
    # Haskell preparation.
    # The iHaskell kernel needs GHC_PACKAGE_PATH to actually
    # find the packages, that were given.
    # 'ihaskell install' is required, so that jupyter finds
    # the haskell kernel.
    export GHC_PACKAGE_PATH="$(echo ${haskellEnv}/lib/*/package.conf.d| tr ' ' ':'):$GHC_PACKAGE_PATH"
    ihaskell install
  '';
}
